﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xam_Delivery.Core.Enum;
using Xam_Delivery.Core.Events;
using Xam_Delivery.Core.Models;
using Xam_Delivery.Core.Models.Event;
using Xam_Delivery.Core.Models.Google;
using Xam_Delivery.Core.Services.Abstract;
using Xam_Delivery.Core.Services.Implementation;
using Xam_Delivery.Core.Util;
using Xam_Delivery.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Xam_Delivery.Views
{
    public partial class DeliveryPage : ContentPage
    {
        #region Properties
        /// <summary>
        /// Detect if the timer is active
        /// </summary>
        private bool TimerIsRunning { get; set; }

        /// <summary>
        /// The system was busy
        /// </summary>
        private bool IsLoading { get; set; }
        private RouteEventStatus DrawRouteEventStatus { get; set; }
        private MapCoordinates Destination { get; set; }
        #endregion

        #region Services
        protected IGoogleService GoogleService { get; set; }
        protected IEventAggregator EventAggregator { get; }
        #endregion

        public DeliveryPage()
        {
            InitializeComponent();

            DeliveryPageViewModel context = (BindingContext as DeliveryPageViewModel);
            this.EventAggregator = context.EventAggregator;
            this.GoogleService = context.GoogleService;

            EventAggregator.GetEvent<DealerRouteEvent>().Subscribe(this.DealerRouteEventChanged);
        }

        #region Implementation
        private void DealerRouteEventChanged(DealerRoute route)
        {
            if (route.Status == RouteEventStatus.Active && !this.TimerIsRunning)
            {
                this.Destination = route.Destination;
                this.DrawRouteEventStatus = RouteEventStatus.Active;

                if (route.Tracking)
                {
                    this.StartDrawingRoute();
                }
                else
                {
                    this.DrawRouteOneTime();
                }
            } else if (route.Status == RouteEventStatus.Clear)
            {
                this.DrawRouteEventStatus = RouteEventStatus.Stoped;
                mMap.MapElements.Clear();
                mMap.Pins.Clear();
            }
            else if (route.Status != RouteEventStatus.Active && this.TimerIsRunning)
            {
                this.DrawRouteEventStatus = route.Status;
            }
        }

        private void StartDrawingRoute()
        {
            Device.StartTimer(TimeSpan.FromSeconds(30), () =>
            {
                this.TimerIsRunning = true;

                // Stop the process if the status is stop
                if (this.DrawRouteEventStatus == RouteEventStatus.Stoped)
                {
                    this.TimerIsRunning = false;
                    return false;
                }

                Device.BeginInvokeOnMainThread(async () =>
                {
                    // Stop the process and wait for the previous one to complete
                    if (this.IsLoading)
                        return;

                    this.IsLoading = true;

                    try
                    {
                        var location = await Geolocation.GetLocationAsync();

                        MapCoordinates origin = new MapCoordinates() { Latitude = location.Latitude, Longitude = location.Longitude };

                        GoogleDirection direction = await this.GoogleService.GetDirections(origin, this.Destination);

                        if (direction.Routes != null && direction.Routes.Count > 0)
                        {
                            var polyline = new Xamarin.Forms.Maps.Polyline
                            {
                                StrokeColor = Color.Blue,
                                StrokeWidth = 3,
                                Geopath = { }
                            };

                            var positions = (Enumerable.ToList(PolylineHelper.Decode(direction.Routes.First().OverviewPolyline.Points)));
                            foreach (var route in positions)
                            {
                                polyline.Geopath.Add(route);
                            }

                            mMap.MapElements.Clear();
                            mMap.MapElements.Add(polyline);
                        }

                        Position myPosition = new Position(location.Latitude, location.Longitude);
                        MapSpan mapSpan = new MapSpan(myPosition, 0.002, 0.002);
                        mMap.MoveToRegion(mapSpan);
                        mMap.Pins.Clear();
                        mMap.Pins.Add(new Pin() { Label = "You", Type = PinType.Place, Position = myPosition });
                    }
                    catch (FeatureNotSupportedException fnsEx)
                    {
                        // Handle not supported on device exception
                    }
                    catch (FeatureNotEnabledException fneEx)
                    {
                        // Handle not enabled on device exception
                    }
                    catch (PermissionException pEx)
                    {
                        // Handle permission exception
                    }
                    catch (Exception ex)
                    {
                        // Unable to get location
                    }

                    this.IsLoading = false;
                });

                return true;
            });
        }

        private void StopDrawingRoute()
        {

        }

        private void DrawRouteOneTime()
        {

            // Stop the process and wait for the previous one to complete
            if (this.IsLoading)
                return;

            this.IsLoading = true;
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    GeolocationRequest request = new GeolocationRequest(GeolocationAccuracy.Best, timeout: TimeSpan.FromSeconds(5));
                    var location = await Geolocation.GetLocationAsync(request);
                    if (location != null)
                    {
                        // 20.632590, -87.092420
                        MapCoordinates origin = new MapCoordinates() { Latitude = location.Latitude, Longitude = location.Longitude };

                        GoogleDirection direction = await this.GoogleService.GetDirections(origin, this.Destination);

                        if (direction.Routes != null && direction.Routes.Count > 0)
                        {
                            var polyline = new Xamarin.Forms.Maps.Polyline
                            {
                                StrokeColor = Color.Blue,
                                StrokeWidth = 3,
                                Geopath = { }
                            };

                            var positions = (Enumerable.ToList(PolylineHelper.Decode(direction.Routes.First().OverviewPolyline.Points)));
                            foreach (var route in positions)
                            {
                                polyline.Geopath.Add(route);
                            }

                            mMap.MapElements.Clear();
                            mMap.MapElements.Add(polyline);
                        }

                        Position myPosition = new Position(location.Latitude, location.Longitude);
                        MapSpan mapSpan = new MapSpan(myPosition, 0.02, 0.02);
                        mMap.MoveToRegion(mapSpan);
                        mMap.Pins.Clear();
                        mMap.Pins.Add(new Pin() { Label = "You", Type = PinType.Place, Position = myPosition });
                    }
                }
                catch (FeatureNotSupportedException fnsEx)
                {
                    // Handle not supported on device exception
                }
                catch (FeatureNotEnabledException fneEx)
                {
                    // Handle not enabled on device exception
                }
                catch (PermissionException pEx)
                {
                    // Handle permission exception
                }
                catch (Exception ex)
                {
                    // Unable to get location
                }
            });
            this.IsLoading = false;
        }
        #endregion
    }
}
