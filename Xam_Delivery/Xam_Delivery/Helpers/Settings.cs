﻿
using Newtonsoft.Json;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using Xam_Delivery.Core.Models.LocalStorage;

namespace Xam_Delivery.Helpers
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants
        private static readonly string SettingsDefault = string.Empty;

        private const string UserKey = "user_key";
        #endregion


        public static UserSession User
        {
            get
            {
                string json = AppSettings.GetValueOrDefault(UserKey, SettingsDefault);
                return string.IsNullOrEmpty(json) ? null : JsonConvert.DeserializeObject<UserSession>(json);
            }
            set
            {
                string json = value == null ? "" : JsonConvert.SerializeObject(value);
                AppSettings.AddOrUpdateValue(UserKey, json);
            }
        }

        public static void Clear()
        {
            AppSettings.AddOrUpdateValue(UserKey, "");
        }
    }
}
