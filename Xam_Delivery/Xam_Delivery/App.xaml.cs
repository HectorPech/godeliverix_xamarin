using Prism;
using Prism.Ioc;
using Xam_Delivery.ViewModels;
using Xam_Delivery.Views;
using Xamarin.Essentials.Interfaces;
using Xamarin.Essentials.Implementation;
using Xamarin.Forms;
using Xam_Delivery.Views.Layout;
using Xam_Delivery.ViewModels.Layout;
using Xam_Delivery.Core.Services.Abstract;
using Xam_Delivery.Core.Services.Implementation;
using Xam_Delivery.Views.Dialog;
using Xam_Delivery.ViewModels.Dialog;

namespace Xam_Delivery
{
    public partial class App
    {
        public App(IPlatformInitializer initializer)
            : base(initializer)
        {
        }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            await NavigationService.NavigateAsync(nameof(SplashPage));
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IAppInfo, AppInfoImplementation>();
            containerRegistry.RegisterSingleton<ISingletonService, SingletonService>();

            // SERVICE
            containerRegistry.Register<IHttpRequestService, HttpRequestService>();
            containerRegistry.Register<IGoogleService, GoogleService>();

            // PAGE
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<SplashPage, SplashPageViewModel>();
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<NavigationTabbedPage, NavigationTabbedPageViewModel>();
            containerRegistry.RegisterForNavigation<HomePage, HomePageViewModel>();
            containerRegistry.RegisterForNavigation<AccountPage, AccountPageViewModel>();
            containerRegistry.RegisterForNavigation<AccountAddressesPage, AccountAddressesPageViewModel>();
            containerRegistry.RegisterForNavigation<AccountPhonesPage, AccountPhonesPageViewModel>();
            containerRegistry.RegisterForNavigation<CashBoxPage, CashBoxPageViewModel>();
            containerRegistry.RegisterForNavigation<WorkShiftPage, WorkShiftPageViewModel>();
            containerRegistry.RegisterForNavigation<PickUpOrderConfirmationPage, PickUpOrderConfirmationPageViewModel>();

            // DIALOG
            containerRegistry.RegisterDialog<DynamicInputFormDialog, DynamicInputFormDialogViewModel>();
            containerRegistry.RegisterForNavigation<DeliveryPage, DeliveryPageViewModel>();
        }
    }
}
