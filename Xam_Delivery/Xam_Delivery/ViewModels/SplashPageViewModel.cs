﻿using Prism.AppModel;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xam_Delivery.Core.Models.LocalStorage;
using Xam_Delivery.Helpers;
using Xam_Delivery.ViewModels.Common;
using Xam_Delivery.Views;
using Xam_Delivery.Views.Layout;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Xam_Delivery.ViewModels
{
    public class SplashPageViewModel : BaseNavigationViewModel, IPageLifecycleAware
    {
        public SplashPageViewModel(
            INavigationService navigationService
            ) : base(navigationService)
        {

        }

        private async Task InitializeApplication()
        {
             await Task.Delay(3000);

            // var status = await Permissions.CheckStatusAsync<Permissions.LocationAlways>();

            UserSession session = Settings.User;

            if (session == null)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await this.NavigationService.NavigateAsync($"/{nameof(LoginPage)}");
                });
            }
            else
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await this.NavigationService.NavigateAsync($"/{nameof(NavigationTabbedPage)}?selectedTab={nameof(CashBoxPage)}");
                });
            }
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
        }

        public void OnAppearing()
        {
            Task.Run(async () => await this.InitializeApplication())
                .ConfigureAwait(false);
        }

        public void OnDisappearing()
        {
        }
    }
}
