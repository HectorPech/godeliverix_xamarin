﻿using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xam_Delivery.Core.Enum;
using Xam_Delivery.Core.Models.Auth;
using Xam_Delivery.Core.Models.LocalStorage;
using Xam_Delivery.Core.Services.Abstract;
using Xam_Delivery.Core.Util;
using Xam_Delivery.Helpers;
using Xam_Delivery.Resources.Languages;
using Xam_Delivery.Resources.Languages.Auth;
using Xam_Delivery.ViewModels.Common;
using Xam_Delivery.Views;
using Xam_Delivery.Views.Layout;
using Xamarin.Forms;

namespace Xam_Delivery.ViewModels
{
    public class LoginPageViewModel : BaseNavigationViewModel
    {
        #region Properties
        private string username;
        public string Username
        {
            get { return username; }
            set { SetProperty(ref username, value); }
        }

        private string password;
        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }

        private bool isBusy;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        #endregion

        #region Services
        private IPageDialogService PageDialogService { get; set; }
        private IHttpRequestService HttpService { get; }
        private ISingletonService Singleton { get; }
        #endregion

        #region Commands
        public DelegateCommand LoginCommand { get; set; }
        public DelegateCommand RestorePasswordCommand { get; set; }
        #endregion

        public LoginPageViewModel(
            INavigationService navigationService,
            IPageDialogService pageDialogService,
            IHttpRequestService httpRequestService,
            ISingletonService singleton
            ) : base(navigationService)
        {
            this.PageDialogService = pageDialogService;
            this.HttpService = httpRequestService;
            this.Singleton = singleton;

            this.LoginCommand = new DelegateCommand(async () => { await this.Login(); });
            this.RestorePasswordCommand = new DelegateCommand(this.RestorePassword);
        }

        #region Implementation
        private async Task Login()
        {
            if (this.IsBusy)
            {
                return;
            }

            this.IsBusy = true;
            if (string.IsNullOrEmpty(this.Username))
            {
                await this.PageDialogService.DisplayAlertAsync(
                    GlobalLang.InvalidData,
                    $"{AuthLang.Username} {GlobalLang.IsRequiredData}",
                    GlobalLang.Accept);
                this.IsBusy = false;
                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await this.PageDialogService.DisplayAlertAsync(
                    GlobalLang.InvalidData,
                    $"{AuthLang.Password} {GlobalLang.IsRequiredData}",
                    GlobalLang.Accept);
                this.IsBusy = false;
                return;
            }

            LoginRequest request = new LoginRequest()
            {
                Password = this.password,
                Username = this.Username,
                ProfileUid = RoleGuid.Repartidor
            };
            string json = JsonConvert.SerializeObject(request);

            var loginRequest = await this.HttpService.PostAsync<LoginResponse>("Auth/Login_DeliveryMan", json);

            if (loginRequest.Code == HttpResponseCode.Success)
            {
                UserSession session = new UserSession()
                {
                    Uid = loginRequest.Result.Uid,
                    UidPerfil = loginRequest.Result.UidPerfil,
                    UidRepartidor = loginRequest.Result.UidRepartidor,
                    Nombre = loginRequest.Result.Nombre,
                    Usuario = loginRequest.Result.Usuario,
                    CorreoElectronico = loginRequest.Result.CorreoElectronico,
                    ApellidoMaterno = loginRequest.Result.ApellidoMaterno,
                    ApellidoPaterno = loginRequest.Result.ApellidoPaterno,
                    FechaNacimiento = loginRequest.Result.FechaNacimiento,
                };

                Settings.User = session;

                if (loginRequest.Result.Orden != null)
                {
                    this.Singleton.Order = loginRequest.Result.Orden;
                }

                if (loginRequest.Result.Turno != null)
                {
                    this.Singleton.DeliveryWorkShift = loginRequest.Result.Turno;
                }

                await Device.InvokeOnMainThreadAsync(async () =>
                {
                    await this.NavigationService.NavigateAsync($"/{nameof(NavigationTabbedPage)}?selectedTab={nameof(CashBoxPage)}");
                    this.IsBusy = false;
                });
            }
            else
            {
                await this.PageDialogService.DisplayAlertAsync(
                      GlobalLang.InvalidData,
                      AuthLang.InvalidPassword,
                      GlobalLang.Accept);
                this.IsBusy = false;
            }
        }

        private void RestorePassword()
        {
            if (this.IsBusy)
            {
                return;
            }
        }
        #endregion

        public override void Initialize(INavigationParameters parameters)
        {
            Settings.Clear();
        }
    }
}
