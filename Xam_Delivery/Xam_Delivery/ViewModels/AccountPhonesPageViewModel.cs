﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Delivery.Core.Enum;
using Xam_Delivery.Core.Models;
using Xam_Delivery.Core.Models.Profile;
using Xam_Delivery.Core.Services.Abstract;
using Xam_Delivery.Helpers;
using Xam_Delivery.ViewModels.Common;

namespace Xam_Delivery.ViewModels
{
    public class AccountPhonesPageViewModel : BaseNavigationViewModel
    {
        #region Properties
        private ObservableCollection<PhoneListViewModel> phones;
        public ObservableCollection<PhoneListViewModel> Phones
        {
            get { return phones; }
            set { SetProperty(ref phones, value); }
        }

        private bool isBusy;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }
        #endregion

        #region Private
        private IEnumerable<Phone> UserPhones { get; set; }
        public Guid UidUser { get; set; }
        #endregion

        #region Services
        private IHttpRequestService HttpService { get; }
        #endregion

        #region Command
        public DelegateCommand GoBackCommand { get; }
        public DelegateCommand<string> PhoneClickedCommand { get; }
        public DelegateCommand RefreshCommand { get; }
        #endregion

        public AccountPhonesPageViewModel(
            INavigationService navigationService,
            IHttpRequestService httpRequestService
            ) : base(navigationService)
        {
            this.HttpService = httpRequestService;

            this.GoBackCommand = new DelegateCommand(async () => { await this.NavigationService.GoBackAsync(); });
            this.RefreshCommand = new DelegateCommand(async () => { await this.ReadAll(); });
            this.PhoneClickedCommand = new DelegateCommand<string>(this.PhoneClicked);

            this.Phones = new ObservableCollection<PhoneListViewModel>();

            this.UidUser = Settings.User.Uid;
        }

        #region Implementation
        private void PhoneClicked(string uid)
        {
        }

        private async Task ReadAll()
        {
            this.IsBusy = true;
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("uidUser", this.UidUser.ToString());

            if (this.Phones.Any())
            {
                this.Phones = new ObservableCollection<PhoneListViewModel>();
            }

            var phonesRequest = await this.HttpService.GetAsync<IEnumerable<Phone>>("Dealers/ReadAllPhones", parameters);
            if (phonesRequest.Code == HttpResponseCode.Success)
            {
                this.UserPhones = phonesRequest.Result;
                foreach (var item in phonesRequest.Result)
                {
                    this.phones.Add(new PhoneListViewModel()
                    {
                        Uid = item.Uid,
                        Identifier = item.Tipo,
                        Phone = item.Numero
                    });
                }
            }
            this.IsBusy = false;
        }
        #endregion

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            Task.Run(async () => { await this.ReadAll(); });
        }
    }
}
