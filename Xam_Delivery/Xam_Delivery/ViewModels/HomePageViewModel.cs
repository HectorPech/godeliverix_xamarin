﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Delivery.Core.Enum;
using Xam_Delivery.Core.Events;
using Xam_Delivery.Core.Models;
using Xam_Delivery.Core.Models.Event;
using Xam_Delivery.Core.Models.LocalStorage;
using Xam_Delivery.Core.Services.Abstract;
using Xam_Delivery.Helpers;
using Xam_Delivery.Resources.Languages.Home;
using Xam_Delivery.Resources.Languages.Navigation;
using Xam_Delivery.ViewModels.Common;
using Xam_Delivery.Views;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Xam_Delivery.ViewModels
{
    public class HomePageViewModel : BaseNavigationViewModel
    {
        #region Properties
        private string welcomeText;
        public string WelcomeText
        {
            get { return welcomeText; }
            set { SetProperty(ref welcomeText, value); }
        }

        private bool showWarningCard;
        public bool ShowWarningCard
        {
            get { return showWarningCard; }
            set { SetProperty(ref showWarningCard, value); }
        }

        private string warningCardText;
        public string WarningCardText
        {
            get { return warningCardText; }
            set { SetProperty(ref warningCardText, value); }
        }

        private bool orderAvailableToConfirm;
        public bool OrderAvailableToConfirm
        {
            get { return orderAvailableToConfirm; }
            set { SetProperty(ref orderAvailableToConfirm, value); }
        }

        private bool isAssignedOrder;
        public bool IsAssignedOrder
        {
            get { return isAssignedOrder; }
            set { SetProperty(ref isAssignedOrder, value); }
        }

        private OrderDetail orderDetail;
        public OrderDetail OrderDetail
        {
            get { return orderDetail; }
            set { SetProperty(ref orderDetail, value); }
        }

        public ObservableCollection<OrderDetail> Orders { get; set; }
        #endregion

        #region Private
        public UserSession Session { get; set; }

        private MapCoordinates Origin { get; set; }

        private MapCoordinates Destination { get; set; }

        private WorkShift DeliveryWorkShift { get; set; }
        #endregion

        #region Commands
        public DelegateCommand AcceptOrderCommand { get; }
        public DelegateCommand RefuseOrderCommand { get; }
        public DelegateCommand ViewRouteCommand { get; }
        public DelegateCommand PickUpOrderConfirmationCommand { get; }
        #endregion

        #region Services
        private ISingletonService SingletonService { get; }
        private IEventAggregator EventAggregator { get; }
        #endregion

        public HomePageViewModel(
            INavigationService navigationService,
            ISingletonService singletonService,
            IEventAggregator eventAggregator
            ) : base(navigationService)
        {
            this.SingletonService = singletonService;
            this.EventAggregator = eventAggregator;

            this.AcceptOrderCommand = new DelegateCommand(this.AcceptOrder);
            this.RefuseOrderCommand = new DelegateCommand(this.RefuseOrder);
            this.ViewRouteCommand = new DelegateCommand(async () => { await this.ViewRoute(); });
            this.PickUpOrderConfirmationCommand = new DelegateCommand(async () => { await this.PickUpOrderConfirmation(); });

            // this.EventAggregator.GetEvent<DeliveryWorkShiftUpdatedEvent>().Subscribe(this.VerifyDeliveryWorkShiftStatus);

            this.Session = Settings.User;
            this.Title = NavigationLang.Home;
            this.WelcomeText = $"{HomeLang.Welcome}, {Session.Nombre} {Session.ApellidoPaterno}";

            this.Origin = new MapCoordinates() { Latitude = 20.625398, Longitude = -87.083978 };
            this.Destination = new MapCoordinates() { Latitude = 20.634664, Longitude = -87.072924 };
            this.Orders = new ObservableCollection<OrderDetail>();

            this.VerifyDeliveryWorkShiftStatus(null);
        }

        #region Implementation
        private void AcceptOrder()
        {
        }

        private void RefuseOrder()
        {
        }

        private async Task ViewRoute()
        {
            this.EventAggregator.GetEvent<DealerRouteEvent>().Publish(new DealerRoute() { Status = RouteEventStatus.Active, Destination = this.Destination }); ;

            //if (Device.RuntimePlatform == Device.iOS)
            //{
            //    // https://developer.apple.com/library/ios/featuredarticles/iPhoneURLScheme_Reference/MapLinks/MapLinks.html
            //    await Launcher.OpenAsync("http://maps.apple.com/?daddr=San+Francisco,+CA&saddr=cupertino");
            //}
            //else if (Device.RuntimePlatform == Device.Android)
            //{
            //    // opens the 'task chooser' so the user can pick Maps, Chrome or other mapping app
            //    await Launcher.OpenAsync("http://maps.google.com/?daddr=" + this.Origin.Latitude + "," + this.Origin.Longitude + "&saddr=" + this.Destination.Latitude + "," + this.Destination.Longitude);
            //    //await Launcher.OpenAsync("https://www.google.com/maps/dir/?api=1&origin=&destination=");
            //}
        }

        /// <summary>
        /// Valida el estatu actual del turno del repartidor
        /// </summary>
        /// <param name="employeeWorkShift"></param>
        private void VerifyDeliveryWorkShiftStatus(WorkShift employeeWorkShift)
        {
            this.DeliveryWorkShift = this.SingletonService.DeliveryWorkShift;
            if (this.DeliveryWorkShift == null)
            {
                this.ShowWarningCard = true;
                this.WarningCardText = HomeLang.OpenWorkShiftToViewAvailableOrders;
            }
            else
            {
                this.ShowWarningCard = false;
                this.WarningCardText = HomeLang.NoOrdersAvailable;

                this.OrderDetail = new OrderDetail()
                {
                    UrlLogo = "http://godeliverix.net/Vista/Img/Empresa/FotoPerfil/777419647vips.png",
                    NombreEmpresa = "Fogon",
                    NombreSucursal = "Fogon Av. 30",
                    Uid = Guid.NewGuid(),
                    Products = new List<ProductDetail>()
                    {
                        new ProductDetail() { Cantidad = 4, Nombre = "Tacos al pastor" },
                        new ProductDetail() { Cantidad = 2, Nombre = "Tortas de pastor" },
                        new ProductDetail() { Cantidad = 1, Nombre = "Agua de naranja" }
                    }
                };

                this.Orders.Clear();
                this.Orders.Add(this.OrderDetail);
                this.OrderAvailableToConfirm = true;
            }
        }

        private async Task PickUpOrderConfirmation()
        {
            await this.NavigationService.NavigateAsync($"{nameof(PickUpOrderConfirmationPage)}");
        }
        #endregion

        public override void Initialize(INavigationParameters parameters)
        {

        }
    }
}
