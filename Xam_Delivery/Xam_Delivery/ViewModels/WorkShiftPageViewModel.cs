﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using Xam_Delivery.Resources.Languages.Navigation;
using Xam_Delivery.ViewModels.Common;

namespace Xam_Delivery.ViewModels
{
    public class WorkShiftPageViewModel : BaseNavigationViewModel
    {
        public WorkShiftPageViewModel(
            INavigationService navigationService
            ) : base(navigationService)
        {
            this.Title = NavigationLang.WorkShift;
        }
    }
}
