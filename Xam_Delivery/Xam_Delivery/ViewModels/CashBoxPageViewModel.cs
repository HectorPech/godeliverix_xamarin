﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Delivery.Core.Enum;
using Xam_Delivery.Core.Events;
using Xam_Delivery.Core.Models;
using Xam_Delivery.Core.Models.CashBox;
using Xam_Delivery.Core.Models.LocalStorage;
using Xam_Delivery.Core.Services.Abstract;
using Xam_Delivery.Helpers;
using Xam_Delivery.Resources.Languages.CashBox;
using Xam_Delivery.Resources.Languages.Navigation;
using Xam_Delivery.ViewModels.Common;

namespace Xam_Delivery.ViewModels
{
    public class CashBoxPageViewModel : BaseNavigationViewModel
    {
        #region Properties
        private List<CardItem> leftItems;
        public List<CardItem> LeftItems
        {
            get { return leftItems; }
            set { SetProperty(ref leftItems, value); }
        }

        private List<CardItem> rightItems;
        public List<CardItem> RightItems
        {
            get { return rightItems; }
            set { SetProperty(ref rightItems, value); }
        }

        private bool workShiftOpened;
        public bool WorkShiftOpened
        {
            get { return workShiftOpened; }
            set { SetProperty(ref workShiftOpened, value); }
        }

        private DateTime workShiftStartDate;
        public DateTime WorkShiftStartDate
        {
            get { return workShiftStartDate; }
            set { SetProperty(ref workShiftStartDate, value); }
        }

        private decimal fondo;
        public decimal Fondo
        {
            get { return fondo; }
            set { SetProperty(ref fondo, value); }
        }

        private Int64 folio;
        public Int64 Folio
        {
            get { return folio; }
            set { SetProperty(ref folio, value); }
        }

        private string strFechaInicio;
        public string StrFechaInicio
        {
            get { return strFechaInicio; }
            set { SetProperty(ref strFechaInicio, value); }
        }

        private double ganancias;
        public double Ganancias
        {
            get { return ganancias; }
            set { SetProperty(ref ganancias, value); }
        }

        private bool isLoading;
        public bool IsLoading
        {
            get { return isLoading; }
            set { SetProperty(ref isLoading, value); }
        }
        #endregion

        #region Private
        private WorkShift DeliveryWorkShift { get; set; }
        private UserSession Session { get; }
        #endregion

        #region Commands
        public DelegateCommand OpenWorkShiftCommand { get; }
        public DelegateCommand CloseWorkShiftCommand { get; }
        public DelegateCommand ReloadCommand { get; }
        #endregion

        #region Services
        private ISingletonService SingletonService { get; }
        private IEventAggregator EventAggregator { get; }
        private IHttpRequestService HttpService { get; }
        #endregion

        public CashBoxPageViewModel(
            INavigationService navigationService,
            ISingletonService singletonService,
            IEventAggregator eventAggregator,
            IHttpRequestService httpRequestService
            ) : base(navigationService)
        {
            this.SingletonService = singletonService;
            this.EventAggregator = eventAggregator;
            this.HttpService = httpRequestService;

            this.OpenWorkShiftCommand = new DelegateCommand(this.OpenWorkShift);
            this.CloseWorkShiftCommand = new DelegateCommand(this.CloseWorkShift);
            this.ReloadCommand = new DelegateCommand(async () => await this.LoadWorkShift());

            this.Title = NavigationLang.WorkShift;
            this.WorkShiftStartDate = DateTime.Now;
            this.WorkShiftOpened = false;

            this.Session = Settings.User;
            this.DeliveryWorkShift = this.SingletonService.DeliveryWorkShift;

            // Izquierda
            this.LeftItems = new List<CardItem>();
            this.LeftItems.Add(new CardItem()
            {
                Title = CashBoxLang.Orders,
                Identifier = "Orders",
                Quantity = 0,
                IsMoneyType = false
            });
            this.LeftItems.Add(new CardItem()
            {
                Title = CashBoxLang.Tips,
                Identifier = "Tips",
                Quantity = 0
            });
            this.LeftItems.Add(new CardItem()
            {
                Title = CashBoxLang.Liquidations,
                Identifier = "Liquidations",
                Quantity = 0
            });
            //this.LeftItems.Add(new CardItem()
            //{
            //    Title = CashBoxLang.Profits,
            //    Quantity = 0
            //});
            this.LeftItems.Add(new CardItem()
            {
                Title = CashBoxLang.Payments,
                Identifier = "Payments",
                Quantity = 0
            });

            // Derecha
            this.RightItems = new List<CardItem>();
            this.RightItems.Add(new CardItem()
            {
                Title = CashBoxLang.ShippingAmount,
                Identifier = "ShippingAmount",
                Quantity = 0
            });
            this.RightItems.Add(new CardItem()
            {
                Title = CashBoxLang.AmountOrders,
                Identifier = "AmountOrders",
                Quantity = 0
            });
            this.RightItems.Add(new CardItem()
            {
                Title = CashBoxLang.PaidOrders,
                Identifier = "PaidOrders",
                Quantity = 0
            });
            this.RightItems.Add(new CardItem()
            {
                Title = CashBoxLang.Refills,
                Identifier = "Refills",
                Quantity = 0
            });
        }

        #region Implementation
        private void OpenWorkShift()
        {
            this.WorkShiftOpened = true;
            this.DeliveryWorkShift = new WorkShift()
            {
                Uid = Guid.NewGuid(),
                FechaInicio = DateTime.Now
            };
            this.WorkShiftStartDate = this.DeliveryWorkShift.FechaInicio;

            this.SingletonService.DeliveryWorkShift = DeliveryWorkShift;
            this.EventAggregator.GetEvent<DeliveryWorkShiftUpdatedEvent>().Publish(this.DeliveryWorkShift);

            Random random = new Random();
            for (int i = 0; i < this.LeftItems.Count; i++)
            {
                this.LeftItems[i].Quantity = random.Next(2, 999);
            }

            for (int i = 0; i < this.RightItems.Count; i++)
            {
                this.RightItems[i].Quantity = random.Next(2, 999);
            }
        }

        private void CloseWorkShift()
        {
            this.WorkShiftOpened = false;
            this.DeliveryWorkShift = null;

            this.SingletonService.DeliveryWorkShift = DeliveryWorkShift;
            this.EventAggregator.GetEvent<DeliveryWorkShiftUpdatedEvent>().Publish(this.DeliveryWorkShift);

            for (int i = 0; i < this.LeftItems.Count; i++)
            {
                this.LeftItems[i].Quantity = 0;
            }

            for (int i = 0; i < this.RightItems.Count; i++)
            {
                this.RightItems[i].Quantity = 0;
            }
        }

        private async Task LoadWorkShift()
        {
            this.IsLoading = true;
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userUid", this.Session.Uid.ToString());

            var request = await this.HttpService.GetAsync<WorkShift>("Dealers/GetLastWorkShiftSummary", parameters);
            if (request.Code == HttpResponseCode.Success)
            {
                this.DeliveryWorkShift = request.Result;
                this.SingletonService.DeliveryWorkShift = this.DeliveryWorkShift;
                this.StrFechaInicio = this.DeliveryWorkShift.FechaInicio.ToString("g");
                this.Folio = this.DeliveryWorkShift.Folio;
                this.Fondo = this.DeliveryWorkShift.Fondo;
                this.Ganancias = this.DeliveryWorkShift.Ganancias;

                foreach (CardItem item in this.LeftItems)
                {
                    switch (item.Identifier)
                    {
                        case "Orders":
                            item.Quantity = this.DeliveryWorkShift.TotalOrdenes;
                            break;
                        case "Tips":
                            item.Quantity = this.DeliveryWorkShift.Propina;
                            break;
                        case "Liquidations":
                            item.Quantity = this.DeliveryWorkShift.Liquidacion;
                            break;
                        case "Payments":
                            item.Quantity = this.DeliveryWorkShift.CantidadPagos;
                            break;
                    }
                }

                foreach (CardItem item in this.RightItems)
                {
                    switch (item.Identifier)
                    {
                        case "ShippingAmount":
                            item.Quantity = this.DeliveryWorkShift.PagosSucursales;
                            break;
                        case "AmountOrders":
                            item.Quantity = this.DeliveryWorkShift.Efectivo;
                            break;
                        case "PaidOrders":
                            item.Quantity = this.DeliveryWorkShift.PagosSucursales;
                            break;
                        case "Refills":
                            item.Quantity = this.DeliveryWorkShift.Recarga;
                            break;
                    }
                }


                this.EventAggregator.GetEvent<DeliveryWorkShiftUpdatedEvent>().Publish(this.DeliveryWorkShift);
            }
            this.IsLoading = false;
        }

        public override void Initialize(INavigationParameters parameters)
        {
            if (this.DeliveryWorkShift == null)
            {
                Task.Run(() => this.LoadWorkShift());
            }
        }
        #endregion
    }
}
