﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xam_Delivery.Core.Models;
using Xam_Delivery.ViewModels.Common;

namespace Xam_Delivery.ViewModels
{
    public class AccountAddressesPageViewModel : BaseNavigationViewModel
    {
        #region Properties
        private ObservableCollection<AddressListViewModel> addresses;
        public ObservableCollection<AddressListViewModel> Addresses
        {
            get { return addresses; }
            set { SetProperty(ref addresses, value); }
        }
        #endregion

        #region Services

        #endregion

        #region Command
        public DelegateCommand GoBackCommand { get; }
        public DelegateCommand<string> AddressClickedCommand { get; set; }
        #endregion

        public AccountAddressesPageViewModel(
            INavigationService navigationService
            ) : base(navigationService)
        {
            this.GoBackCommand = new DelegateCommand(async () => { await this.NavigationService.GoBackAsync(); });
            this.AddressClickedCommand = new DelegateCommand<string>(this.AddressClicked);

            this.Addresses = new ObservableCollection<AddressListViewModel>();
            this.Addresses.Add(new AddressListViewModel()
            {
                Identifier = "Casa",
                ShortAddress = "Calle 23 sur" 
            });
        }

        #region Implementation
        private void AddressClicked(string uid)
        {
        }
        #endregion
    }
}
