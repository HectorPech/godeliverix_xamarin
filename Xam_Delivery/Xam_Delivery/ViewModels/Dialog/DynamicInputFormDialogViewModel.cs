﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xam_Delivery.Core.Models.Dynamic;
using Xam_Delivery.Resources.Languages;
using Xam_Delivery.Resources.Languages.Profile;

namespace Xam_Delivery.ViewModels.Dialog
{
    public class DynamicInputFormDialogViewModel : BindableBase, IDialogAware
    {
        #region Properties
        public ObservableCollection<InputDynamic> FormFields { get; set; }

        private string cancelText;
        public string CancelText
        {
            get { return cancelText; }
            set { SetProperty(ref cancelText, value); }
        }

        private string saveText;
        public string SaveText
        {
            get { return saveText; }
            set { SetProperty(ref saveText, value); }
        }

        private string dialogTitle;
        public string DialogTitle
        {
            get { return dialogTitle; }
            set { SetProperty(ref dialogTitle, value); }
        }

        private string errorText;
        public string ErrorText
        {
            get { return errorText; }
            set { SetProperty(ref errorText, value); }
        }

        #endregion

        #region Command
        public DelegateCommand CloseDialogCommand { get; }
        public DelegateCommand SaveButtonCommand { get; }
        #endregion

        public DynamicInputFormDialogViewModel()
        {
            this.CloseDialogCommand = new DelegateCommand(() => RequestClose(null));
            this.SaveButtonCommand = new DelegateCommand(this.Save);

            this.FormFields = new ObservableCollection<InputDynamic>();
            this.CancelText = ProfileLang.Cancel;
            this.SaveText = ProfileLang.Update;
            this.DialogTitle = ProfileLang.Edit;
            this.ErrorText = string.Empty;
        }

        #region Implementation
        private void Save()
        {
            this.ErrorText = string.Empty;
            bool isValid = true;
            foreach (InputDynamic field in this.FormFields)
            {
                if (field.IsRequired)
                {
                    if (field.IsTextInput)
                    {
                        if (string.IsNullOrEmpty(field.TextValue))
                        {
                            this.ErrorText = $"{field.Name} {GlobalLang.IsRequiredData}";
                            isValid = false;
                            break;
                        }
                    }
                    else if (field.IsDateInput)
                    {

                    }
                }
            }


            if (!isValid)
            {
                return;
            }

            DialogParameters parameters = new DialogParameters();
            parameters.Add("fields", this.FormFields.ToList());
            RequestClose(parameters);
        }
        #endregion

        #region Dialog Aware
        public event Action<IDialogParameters> RequestClose;

        public bool CanCloseDialog() => true;

        public void OnDialogClosed()
        {
            // throw new NotImplementedException();
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            // throw new NotImplementedException();

            if (parameters.ContainsKey("fields"))
            {
                List<InputDynamic> inputs = parameters.GetValue<List<InputDynamic>>("fields");
                foreach (InputDynamic input in inputs)
                {
                    this.FormFields.Add(input);
                }
            }

            if (parameters.ContainsKey("cancelText"))
            {
                this.CancelText = parameters.GetValue<string>("cancelText");
            }

            if (parameters.ContainsKey("saveText"))
            {
                this.SaveText = parameters.GetValue<string>("saveText ");
            }

            if (parameters.ContainsKey("dialogTitle"))
            {
                this.DialogTitle = parameters.GetValue<string>("dialogTitle");
            }
        }
        #endregion
    }
}
