﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xam_Delivery.Core.Models;
using Xam_Delivery.ViewModels.Common;

namespace Xam_Delivery.ViewModels
{
    public class PickUpOrderConfirmationPageViewModel : BaseNavigationViewModel
    {
        #region Properties
        private string urlLogo;
        public string UrlLogo
        {
            get { return urlLogo; }
            set { SetProperty(ref urlLogo, value); }
        }

        private string nombreEmpresa;
        public string NombreEmpresa
        {
            get { return nombreEmpresa; }
            set { SetProperty(ref nombreEmpresa, value); }
        }

        private string nombreSucursal;
        public string NombreSucursal
        {
            get { return nombreSucursal; }
            set { SetProperty(ref nombreSucursal, value); }
        }

        public ObservableCollection<ProductDetail> Products { get; set; }

        private AssignedOrder order;
        public AssignedOrder Order
        {
            get { return order; }
            set { SetProperty(ref order, value); }
        }
        #endregion

        #region Command
        public DelegateCommand GoBackCommand { get; }
        #endregion

        public PickUpOrderConfirmationPageViewModel(
            INavigationService navigationService
            ) : base(navigationService)
        {
            this.GoBackCommand = new DelegateCommand(async () => { await this.NavigationService.GoBackAsync(); });
            this.Products = new ObservableCollection<ProductDetail>();

            //UrlLogo = "http://godeliverix.net/Vista/Img/Empresa/FotoPerfil/777419647vips.png";
            //NombreEmpresa = "Fogon";
            //NombreSucursal = "Fogon Av. 30";

            //Products = new ObservableCollection<ProductDetail>()
            //        {
            //            new ProductDetail() { Cantidad = 4, Nombre = "Tacos al pastor" },
            //            new ProductDetail() { Cantidad = 2, Nombre = "Tortas de pastor" },
            //            new ProductDetail() { Cantidad = 1, Nombre = "Agua de naranja" }
            //        };
        }

        public override void Initialize(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("order"))
            {
                this.Order = parameters.GetValue<AssignedOrder>("order");
                this.UrlLogo = this.Order.UrlLogoEmpresa;

                foreach (var p in this.Order.Productos)
                {
                    this.Products.Add(new ProductDetail() { Cantidad = p.Cantidad, Nombre = p.Nombre });
                }
            }
        }
    }
}
