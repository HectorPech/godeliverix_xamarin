﻿using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Delivery.Core.Enum;
using Xam_Delivery.Core.Models.Dynamic;
using Xam_Delivery.Core.Models.LocalStorage;
using Xam_Delivery.Core.Models.Profile;
using Xam_Delivery.Core.Services.Abstract;
using Xam_Delivery.Core.Util;
using Xam_Delivery.Helpers;
using Xam_Delivery.Resources.Languages;
using Xam_Delivery.Resources.Languages.Navigation;
using Xam_Delivery.Resources.Languages.Profile;
using Xam_Delivery.ViewModels.Common;
using Xam_Delivery.Views;
using Xam_Delivery.Views.Dialog;
using Xamarin.Forms;

namespace Xam_Delivery.ViewModels
{
    public class AccountPageViewModel : BaseViewModel
    {
        #region Properties
        private ObservableCollection<ProfileMenuItem> items;
        public ObservableCollection<ProfileMenuItem> Items
        {
            get { return items; }
            set { SetProperty(ref items, value); }
        }

        private bool isBusy;
        public bool IsBusy
        {
            get { return isBusy; }
            set { isBusy = value; }
        }

        #endregion

        #region Private
        private enum MenuAction
        {
            Fullname,
            Username,
            Birthday,
            Email,
            Addresses,
            Phones,
            Logout
        }

        protected UserSession session;
        #endregion

        #region Services
        private IHttpRequestService HttpService { get; }
        #endregion

        #region Commands
        public DelegateCommand<string> MenuItemClickedCommand { get; }
        #endregion

        public AccountPageViewModel(
            INavigationService navigationService,
            IDialogService dialogService,
            IPageDialogService pageDialogService,
            IHttpRequestService httpRequestService
            ) : base(navigationService, dialogService, pageDialogService)
        {
            this.HttpService = httpRequestService;

            this.Title = NavigationLang.Account;

            this.MenuItemClickedCommand = new DelegateCommand<string>(async (string value) => { await this.MenuItemClicked(value); });

            this.Items = new ObservableCollection<ProfileMenuItem>();

            this.session = Settings.User;

            this.Items.Add(new ProfileMenuItem()
            {
                Name = MenuAction.Fullname.ToString(),
                FontIcon = MaterialIcon.AccountOutline,
                Title = ProfileLang.FullName,
                Value = $"{session.Nombre} {session.ApellidoPaterno} {session.ApellidoMaterno}"
            });

            this.Items.Add(new ProfileMenuItem()
            {
                Name = MenuAction.Username.ToString(),
                FontIcon = MaterialIcon.BadgeAccountAlertOutline,
                Title = ProfileLang.Username,
                Value = session.Usuario
            });

            this.Items.Add(new ProfileMenuItem()
            {
                Name = MenuAction.Birthday.ToString(),
                FontIcon = MaterialIcon.Cake,
                Title = ProfileLang.Birthday,
                Value = session.FechaNacimiento.ToString("MM/dd/yyyy")
            }); ;

            this.Items.Add(new ProfileMenuItem()
            {
                Name = MenuAction.Email.ToString(),
                FontIcon = MaterialIcon.EmailOutline,
                Title = ProfileLang.Email,
                Value = session.CorreoElectronico
            });

            //this.Items.Add(new ProfileMenuItem()
            //{
            //    Name = MenuAction.Addresses.ToString(),
            //    FontIcon = MaterialIcon.MapMarkerOutline,
            //    Title = ProfileLang.Addresses
            //});

            this.Items.Add(new ProfileMenuItem()
            {
                Name = MenuAction.Phones.ToString(),
                FontIcon = MaterialIcon.PhoneOutline,
                Title = ProfileLang.Phones
            });

            this.Items.Add(new ProfileMenuItem()
            {
                Name = MenuAction.Logout.ToString(),
                FontIcon = MaterialIcon.Logout,
                Title = ProfileLang.Logout
            });
        }

        #region Implementation
        private async Task MenuItemClicked(string action)
        {
            if (this.IsBusy)
            {
                return;
            }

            await Device.InvokeOnMainThreadAsync(async () =>
            {
                ProfileMenuItem item = this.Items.FirstOrDefault(i => i.Name == action);

                if (action.Equals(MenuAction.Fullname.ToString()))
                {
                    List<InputDynamic> formFields = new List<InputDynamic>();
                    formFields.Add(new InputDynamic()
                    {
                        Identifier = "Name",
                        Name = ProfileLang.Name,
                        TextValue = this.session.Nombre
                    });
                    formFields.Add(new InputDynamic()
                    {
                        Identifier = "FirstLastname",
                        Name = ProfileLang.FirstLastname,
                        TextValue = this.session.ApellidoPaterno
                    });
                    formFields.Add(new InputDynamic()
                    {
                        Identifier = "SecondLastname",
                        Name = ProfileLang.SecondLastname,
                        TextValue = this.session.ApellidoMaterno
                    });

                    this.DialogService.ShowDialog(
                        nameof(DynamicInputFormDialog),
                        new DialogParameters()
                            {
                                {"fields" , formFields },
                                {"dialogTitle", ProfileLang.EditFullName}
                            },
                        async (IDialogResult dialogResult) => { await this.FullnameDialogClosed(dialogResult); });
                }
                else if (action.Equals(MenuAction.Username.ToString()))
                {

                }
                else if (action.Equals(MenuAction.Birthday.ToString()))
                {
                    List<InputDynamic> formFields = new List<InputDynamic>();
                    formFields.Add(new InputDynamic()
                    {
                        Identifier = "Birthday",
                        Name = ProfileLang.Birthday,
                        IsTextInput = false,
                        IsDateInput = true,
                        DateValue = this.session.FechaNacimiento
                    });

                    this.DialogService.ShowDialog(
                        nameof(DynamicInputFormDialog),
                        new DialogParameters()
                            {
                                {"fields" , formFields },
                                {"dialogTitle", ProfileLang.EditBirthday}
                            },
                        async (IDialogResult dialogResult) => { await this.BirthdayDialogClosed(dialogResult); });
                }
                else if (action.Equals(MenuAction.Email.ToString()))
                {

                }
                else if (action.Equals(MenuAction.Addresses.ToString()))
                {
                    await this.NavigationService.NavigateAsync($"{nameof(AccountAddressesPage)}");
                }
                else if (action.Equals(MenuAction.Phones.ToString()))
                {
                    await this.NavigationService.NavigateAsync($"{nameof(AccountPhonesPage)}");
                }
                else if (action.Equals(MenuAction.Logout.ToString()))
                {
                    await NavigationService.NavigateAsync($"/{nameof(LoginPage)}");
                }
            });
        }

        private async Task FullnameDialogClosed(IDialogResult dialogResult)
        {
            if (dialogResult.Parameters.ContainsKey("fields"))
            {
                this.IsBusy = true;
                List<InputDynamic> formFields = dialogResult.Parameters.GetValue<List<InputDynamic>>("fields");

                DealerAccountUpdate update = new DealerAccountUpdate()
                {
                    Uid = this.session.Uid,
                    Nombre = formFields.SingleOrDefault(f => f.Identifier == "Name").TextValue,
                    ApellidoPaterno = formFields.SingleOrDefault(f => f.Identifier == "FirstLastname").TextValue,
                    ApellidoMaterno = formFields.SingleOrDefault(f => f.Identifier == "SecondLastname").TextValue
                };

                await this.UpdatePersonalInformation(update);
            }
        }
        private async Task BirthdayDialogClosed(IDialogResult dialogResult)
        {
            if (dialogResult.Parameters.ContainsKey("fields"))
            {
                this.IsBusy = true;
                List<InputDynamic> formFields = dialogResult.Parameters.GetValue<List<InputDynamic>>("fields");

                DealerAccountUpdate update = new DealerAccountUpdate()
                {
                    Uid = this.session.Uid,
                    FechaNacimiento = formFields.SingleOrDefault(f => f.Identifier == "Birthday").DateValue
                };

                await this.UpdatePersonalInformation(update);
            }
        }

        private async Task UpdatePersonalInformation(DealerAccountUpdate update)
        {
            string json = JsonConvert.SerializeObject(update);
            var updateRequest = await this.HttpService.PostAsync("Dealers/Update", json);
            if (updateRequest == HttpResponseCode.Success)
            {
                if (!string.IsNullOrEmpty(update.Nombre)
                    && !string.IsNullOrEmpty(update.ApellidoMaterno)
                    && !string.IsNullOrEmpty(update.ApellidoPaterno)
                        )
                {
                    this.session.Nombre = update.Nombre;
                    this.session.ApellidoPaterno = update.ApellidoPaterno;
                    this.session.ApellidoMaterno = update.ApellidoMaterno;
                }

                if (update.FechaNacimiento.HasValue)
                {
                    this.session.FechaNacimiento = update.FechaNacimiento.Value;
                }

                Settings.User = this.session;
                this.UpdateOptionValues();
                this.IsBusy = false;
            }
            else
            {
                await this.PageDialogService.DisplayAlertAsync(
                    GlobalLang.Failed,
                    GlobalLang.UnexpectedErrorOccurred,
                    GlobalLang.Accept);
                this.IsBusy = false;
            }

        }

        private void UpdateOptionValues()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                foreach (ProfileMenuItem item in this.Items)
                {
                    if (item.Name.Equals(MenuAction.Fullname.ToString()))
                    {
                        item.Value = $"{this.session.Nombre} {this.session.ApellidoPaterno} {this.session.ApellidoMaterno}";
                    }

                    if (item.Name.Equals(MenuAction.Birthday.ToString()))
                    {
                        item.Value = session.FechaNacimiento.ToString("MM/dd/yyyy");
                    }
                }
            });
        }
        #endregion
    }
}
