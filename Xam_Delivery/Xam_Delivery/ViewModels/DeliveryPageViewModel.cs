﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xam_Delivery.Core.Enum;
using Xam_Delivery.Core.Events;
using Xam_Delivery.Core.Models;
using Xam_Delivery.Core.Models.Event;
using Xam_Delivery.Core.Models.LocalStorage;
using Xam_Delivery.Core.Services.Abstract;
using Xam_Delivery.Core.Util;
using Xam_Delivery.Helpers;
using Xam_Delivery.Resources.Languages;
using Xam_Delivery.Resources.Languages.Home;
using Xam_Delivery.Resources.Languages.Navigation;
using Xam_Delivery.ViewModels.Common;
using Xam_Delivery.Views;

namespace Xam_Delivery.ViewModels
{
    public class DeliveryPageViewModel : BaseViewModel
    {
        #region Properties
        private bool orderAvailable;
        public bool OrderAvailable
        {
            get { return orderAvailable; }
            set { SetProperty(ref orderAvailable, value); }
        }

        private bool showWarningCard;
        public bool ShowWarningCard
        {
            get { return showWarningCard; }
            set { SetProperty(ref showWarningCard, value); }
        }

        private string warningCardText;
        public string WarningCardText
        {
            get { return warningCardText; }
            set { SetProperty(ref warningCardText, value); }
        }

        private AssignedOrder order;
        public AssignedOrder Order
        {
            get { return order; }
            set { SetProperty(ref order, value); }
        }


        private string orderStatus;
        public string OrderStatus
        {
            get { return orderStatus; }
            set { SetProperty(ref orderStatus, value); }
        }

        private bool btnDetailsIsVisible;
        public bool BtnDetailsIsVisible
        {
            get { return btnDetailsIsVisible; }
            set { SetProperty(ref btnDetailsIsVisible, value); }
        }

        private bool btnPickUpIsVisible;
        public bool BtnPickUpIsVisible
        {
            get { return btnPickUpIsVisible; }
            set { SetProperty(ref btnPickUpIsVisible, value); }
        }

        private bool btnDeliveryRouteIsVisible;
        public bool BtnDeliveryRouteIsVisible
        {
            get { return btnDeliveryRouteIsVisible; }
            set { SetProperty(ref btnDeliveryRouteIsVisible, value); }
        }

        private bool btnPickUpRouteIsVisible;
        public bool BtnPickUpRouteIsVisible
        {
            get { return btnPickUpRouteIsVisible; }
            set { SetProperty(ref btnPickUpRouteIsVisible, value); }
        }

        private bool btnDeclineIsVisible;
        public bool BtnDeclineIsVisible
        {
            get { return btnDeclineIsVisible; }
            set { SetProperty(ref btnDeclineIsVisible, value); }
        }

        private bool btnAcceptIsVisible;
        public bool BtnAcceptIsVisible
        {
            get { return btnAcceptIsVisible; }
            set { SetProperty(ref btnAcceptIsVisible, value); }
        }


        private bool isLoading;
        public bool IsLoading
        {
            get { return isLoading; }
            set { SetProperty(ref isLoading, value); }
        }

        #endregion

        #region Private
        private UserSession Sesion { get; }
        #endregion

        #region Services
        public IHttpRequestService HttpService { get; }
        public IGoogleService GoogleService { get; }
        public IEventAggregator EventAggregator { get; }

        public ISingletonService Singleton { get; }
        #endregion

        #region Command
        public DelegateCommand ReloadCommand { get; }
        public DelegateCommand GoSettingsCommand { get; }

        public DelegateCommand ViewPickUpRouteCommad { get; }
        public DelegateCommand PickUpOrderCommand { get; }
        public DelegateCommand ConfirmOrderCommand { get; }
        public DelegateCommand RefuseOrderCommand { get; }
        #endregion

        public DeliveryPageViewModel(
            INavigationService navigationService,
            IDialogService dialogService,
            IPageDialogService pageDialogService,
            IEventAggregator eventAggregator,
            IGoogleService googleService,
            ISingletonService singleton,
            IHttpRequestService httpRequestService
            ) : base(navigationService, dialogService, pageDialogService)
        {
            this.HttpService = httpRequestService;
            this.EventAggregator = eventAggregator;
            this.GoogleService = googleService;
            this.Singleton = singleton;

            this.Title = NavigationLang.Map;
            this.Sesion = Settings.User;

            this.ReloadCommand = new DelegateCommand(async () => await this.Reload());

            this.GoSettingsCommand = new DelegateCommand(async () =>
            {
                await this.NavigationService.NavigateAsync(nameof(AccountPage));
            });

            this.ViewPickUpRouteCommad = new DelegateCommand(this.ViewPickUpRoute);
            this.RefuseOrderCommand = new DelegateCommand(this.RefuseOrder);
            this.ConfirmOrderCommand = new DelegateCommand(this.ConfirmOrder);
            this.PickUpOrderCommand = new DelegateCommand(this.PickUpOrder);

            this.EventAggregator.GetEvent<DeliveryWorkShiftUpdatedEvent>().Subscribe(this.VerifyDeliveryWorkShiftStatus);
        }

        #region Implementation
        private async Task Reload()
        {
            if (this.Singleton.DeliveryWorkShift != null)
            {
                this.ShowWarningCard = false;
                this.OrderAvailable = false;
                this.IsLoading = true;

                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("dealerWorkshiftUid", this.Singleton.DeliveryWorkShift.StrUid);

                var request = await this.HttpService.GetAsync<AssignedOrder>("Dealers/GetAssignedPurchaseShipmentSummary", parameters);
                if (request.Code == HttpResponseCode.Success)
                {
                    if (request.Result != null)
                    {
                        this.IsLoading = false;
                        this.OrderAvailable = true;
                        this.Order = request.Result;
                        this.VerifyOrder();
                    }
                    else
                    {
                        this.IsLoading = false;
                        this.ShowWarningCard = true;
                        this.WarningCardText = HomeLang.NoOrdersAvailable;
                    }
                }
                else
                {
                    this.IsLoading = false;
                    this.ShowWarningCard = true;
                    this.WarningCardText = HomeLang.NoOrdersAvailable;
                }
            }
        }

        private void VerifyOrder()
        {
            this.BtnAcceptIsVisible = false;
            this.BtnPickUpIsVisible = false;
            this.BtnDeliveryRouteIsVisible = false;
            this.BtnDetailsIsVisible = false;
            this.BtnPickUpRouteIsVisible = false;
            this.BtnDeclineIsVisible = false;

            if (this.Order.UidEstatusOrdenRepartidor == DeliveryOrderStatus.Pendiente)
            {
                BtnAcceptIsVisible = true;
                BtnPickUpRouteIsVisible = true;
                BtnDetailsIsVisible = true;
                BtnDeclineIsVisible = true;

                this.OrderStatus = GlobalLang.WaitingForConfirmation;
            }
            else if (this.Order.UidEstatusOrdenRepartidor == DeliveryOrderStatus.Recolectada)
            {
                BtnDetailsIsVisible = true;
                BtnDeliveryRouteIsVisible = true;

                this.OrderStatus = GlobalLang.OnDeliveryRoute;
            }
            else if (this.Order.UidEstatusOrdenRepartidor == DeliveryOrderStatus.Rechazada)
            {
                this.OrderStatus = GlobalLang.OrderRejected;
            }
            else if (this.Order.UidEstatusOrdenRepartidor == DeliveryOrderStatus.Confirmada)
            {
                BtnDetailsIsVisible = true;
                BtnPickUpRouteIsVisible = true;
                BtnPickUpIsVisible = true;

                this.OrderStatus = GlobalLang.OnTheCollectionRoute;
            }
            else if (this.Order.UidEstatusOrdenRepartidor == DeliveryOrderStatus.Entregada)
            {
                this.OrderStatus = GlobalLang.OrderDelivered;
            }
        }

        private void ViewPickUpRoute()
        {
            if (this.Order != null)
            {
                if (
                    string.IsNullOrEmpty(this.Order.LatSucursal.Trim())
                    || string.IsNullOrEmpty(this.Order.LongSucursal.Trim())
                    || this.Order.LatSucursal.Trim() == "0"
                    || this.Order.LongSucursal.Trim() == "0"
                    )
                {
                    return;
                }

                double latitude = 0;
                double longitude = 0;

                if (
                    !double.TryParse(this.Order.LatSucursal, out latitude)
                    || !double.TryParse(this.Order.LongSucursal, out longitude)
                    )
                {
                    return;
                }

                MapCoordinates destination = new MapCoordinates()
                {
                    Latitude = latitude,
                    Longitude = longitude
                };

                this.EventAggregator.GetEvent<DealerRouteEvent>()
                    .Publish(new DealerRoute()
                    {
                        Status = RouteEventStatus.Active,
                        Destination = destination,
                        Tracking = false
                    });
            }
        }

        private async void ConfirmOrder()
        {
            this.IsLoading = true;
            this.ShowWarningCard = false;
            this.OrderAvailable = false;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("StrParametro", "O");
            parameters.Add("UidUsuario", this.Sesion.Uid.ToString());
            parameters.Add("UidEstatus", DeliveryOrderStatus.Confirmada.ToString());
            parameters.Add("UidOrdenRepartidor", this.Order.UidOrdenRepartidor.ToString());

            var request = await this.HttpService.GetEmptyAsync("Profile/GetBitacoraRegistroRepartidores", parameters);
            if (request == HttpResponseCode.Success)
            {

            }

            await this.Reload();
        }

        private async void RefuseOrder()
        {
            this.IsLoading = true;
            this.ShowWarningCard = false;
            this.OrderAvailable = false;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("StrParametro", "O");
            parameters.Add("UidUsuario", this.Sesion.Uid.ToString());
            parameters.Add("UidEstatus", DeliveryOrderStatus.Rechazada.ToString());
            parameters.Add("UidOrdenRepartidor", this.Order.UidOrdenRepartidor.ToString());

            var request = await this.HttpService.GetEmptyAsync("Profile/GetBitacoraRegistroRepartidores", parameters);
            if (request == HttpResponseCode.Success)
            {

            }

            await this.Reload();
        }

        private async void PickUpOrder()
        {
            await this.NavigationService.NavigateAsync(nameof(PickUpOrderConfirmationPage), new NavigationParameters()
            {
                { "order", this.Order}
            });
        }

        private async void VerifyDeliveryWorkShiftStatus(WorkShift employeeWorkShift)
        {
            if (this.Singleton.DeliveryWorkShift == null)
            {
                this.ShowWarningCard = true;
                this.WarningCardText = HomeLang.OpenWorkShiftToViewAvailableOrders;
            }
            else
            {
                await this.Reload();
            }
        }

        public override void Initialize(INavigationParameters parameters)
        {
            if (this.Singleton.DeliveryWorkShift == null)
            {
                this.ShowWarningCard = true;
                this.WarningCardText = HomeLang.OpenWorkShiftToViewAvailableOrders;
            }
            else
            {
                if (this.Singleton.Order == null)
                {
                    this.ShowWarningCard = true;
                    this.WarningCardText = HomeLang.NoOrdersAvailable;
                }
                else
                {
                    this.Order = this.Singleton.Order;

                    this.VerifyOrder();

                    this.ShowWarningCard = false;
                    this.OrderAvailable = true;
                }
            }
        }
        #endregion
    }
}
