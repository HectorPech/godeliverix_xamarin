﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Delivery.Core.Enum
{
    public enum RouteEventStatus
    {
        Active,
        Stoped,
        NotAvailable,
        Clear
    }
}
