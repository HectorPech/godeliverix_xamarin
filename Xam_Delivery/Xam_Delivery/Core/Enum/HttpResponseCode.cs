﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Delivery.Core.Enum
{
    public enum HttpResponseCode
    {
        Success,
        Failed,
        NotFound,
        ServerError
    }
}
