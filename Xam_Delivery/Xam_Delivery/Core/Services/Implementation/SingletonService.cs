﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Delivery.Core.Models;
using Xam_Delivery.Core.Services.Abstract;

namespace Xam_Delivery.Core.Services.Implementation
{
    public class SingletonService : ISingletonService
    {
        public SingletonService()
        {
            this.DeliveryWorkShift = null;
            this.Order = null;
        }

        public WorkShift DeliveryWorkShift { get; set; }

        public AssignedOrder Order { get; set; }
    }
}
