﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xam_Delivery.Core.Enum;
using Xam_Delivery.Core.Models.Common;
using Xam_Delivery.Core.Services.Abstract;
using Xam_Delivery.Core.Util;

namespace Xam_Delivery.Core.Services.Implementation
{
    public class HttpRequestService : IHttpRequestService
    {
        protected HttpClient _HttpClient;

        public HttpRequestService()
        {
            this._HttpClient = new HttpClient();
            this._HttpClient.BaseAddress = new Uri(ApplicationConstants.BaseUrl);
            this._HttpClient.Timeout = TimeSpan.FromSeconds(10);
        }

        public async Task<HttpResponse<TResult>> GetAsync<TResult>(string url, IDictionary<string, string> parameters = null)
        {
            try
            {
                string urlParams = parameters == null ? "" : ConvertStringArguments(parameters);

                var response = await this._HttpClient.GetAsync(url + urlParams);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream stream = await response.Content.ReadAsStreamAsync();
                    StreamReader streamReader = new StreamReader(stream);
                    string contentResponse = streamReader.ReadToEnd();

                    var result = JsonConvert.DeserializeObject<TResult>(contentResponse);

                    return new HttpResponse<TResult>()
                    {
                        Code = HttpResponseCode.Success,
                        Result = result
                    };
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    return new HttpResponse<TResult>()
                    {
                        Code = HttpResponseCode.Failed
                    };
                }
                else
                {
                    return new HttpResponse<TResult>()
                    {
                        Code = HttpResponseCode.ServerError
                    };
                }
            }
            catch (Exception)
            {
                return new HttpResponse<TResult>()
                {
                    Code = HttpResponseCode.ServerError
                };
            }
        }

        public async Task<HttpResponseCode> GetEmptyAsync(string url, IDictionary<string, string> parameters = null)
        {
            try
            {
                string urlParams = parameters == null ? "" : ConvertStringArguments(parameters);

                var response = await this._HttpClient.GetAsync(url + urlParams);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return HttpResponseCode.Success;
                }
                else
                {
                    return HttpResponseCode.Failed;
                }
            }
            catch (Exception)
            {
                return HttpResponseCode.Failed;
            }
        }

        public async Task<HttpResponseCode> PostAsync(string url, string content, string contentType = "application/json")
        {
            try
            {
                HttpContent stringContent = new StringContent(content,
                            UnicodeEncoding.UTF8,
                            contentType);

                var response = await this._HttpClient.PostAsync(url, stringContent);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return HttpResponseCode.Success;
                }
                else
                {
                    return HttpResponseCode.Failed;
                }
            }
            catch (Exception)
            {
                return HttpResponseCode.ServerError;
            }
        }

        public async Task<HttpResponse<TResult>> PostAsync<TResult>(string url, string content, string contentType = "application/json")
        {
            try
            {
                HttpContent stringContent = new StringContent(content,
                            UnicodeEncoding.UTF8,
                            contentType);

                var response = await this._HttpClient.PostAsync(url, stringContent);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream stream = await response.Content.ReadAsStreamAsync();
                    StreamReader streamReader = new StreamReader(stream);
                    string contentResponse = streamReader.ReadToEnd();

                    var result = JsonConvert.DeserializeObject<TResult>(contentResponse);

                    return new HttpResponse<TResult>()
                    {
                        Code = HttpResponseCode.Success,
                        Result = result
                    };
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    return new HttpResponse<TResult>()
                    {
                        Code = HttpResponseCode.Failed
                    };
                }
                else
                {
                    return new HttpResponse<TResult>()
                    {
                        Code = HttpResponseCode.ServerError
                    };
                }
            }
            catch (Exception)
            {

                return new HttpResponse<TResult>()
                {
                    Code = HttpResponseCode.ServerError
                };
            }
        }

        #region UTIL
        private string ConvertStringArguments(IDictionary<string, string> arguments)
        {
            string urlArguments = string.Empty;
            foreach (var arg in arguments)
            {
                if (urlArguments == string.Empty)
                    urlArguments = arg.Value == string.Empty ? string.Empty : $"?{arg.Key}={arg.Value}";
                else
                    urlArguments += arg.Value == string.Empty ? string.Empty : $"&{arg.Key}={arg.Value}";
            }
            return urlArguments;
        }
        #endregion
    }
}
