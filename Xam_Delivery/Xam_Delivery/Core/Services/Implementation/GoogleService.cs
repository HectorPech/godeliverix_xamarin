﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xam_Delivery.Core.Models;
using Xam_Delivery.Core.Models.Google;
using Xam_Delivery.Core.Services.Abstract;
using Xam_Delivery.Core.Util;

namespace Xam_Delivery.Core.Services.Implementation
{
    public class GoogleService : IGoogleService
    {
        #region  Properties
        private HttpClient _HttpClient;
        #endregion

        public GoogleService()
        {
            this._HttpClient = new HttpClient();
            this._HttpClient.BaseAddress = new Uri("https://maps.googleapis.com/maps/");
        }

        public async Task<GoogleDirection> GetDirections(MapCoordinates origin, MapCoordinates destination)
        {
            GoogleDirection googleDirection = new GoogleDirection();

            var response = await _HttpClient.GetAsync($"api/directions/json?mode=driving&transit_routing_preference=less_driving&origin={origin.Latitude},{origin.Longitude}&destination={destination.Latitude},{destination.Longitude}&key={ApplicationConstants.GoogleDirectionsApiKey}").ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (!string.IsNullOrWhiteSpace(json))
                {
                    googleDirection = await Task.Run(() => JsonConvert.DeserializeObject<GoogleDirection>(json))
                        .ConfigureAwait(false);
                }
            }

            return googleDirection;
        }
    }
}
