﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Delivery.Core.Models;

namespace Xam_Delivery.Core.Services.Abstract
{
    public interface ISingletonService
    {
        WorkShift DeliveryWorkShift { get; set; }
        AssignedOrder Order { get; set; }
    }
}
