﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xam_Delivery.Core.Enum;
using Xam_Delivery.Core.Models.Common;
using Xam_Delivery.Core.Util;

namespace Xam_Delivery.Core.Services.Abstract
{
    public interface IHttpRequestService
    {
        /// <summary>
        /// Make a get http request
        /// </summary>
        Task<HttpResponse<TResult>> GetAsync<TResult>(string url, IDictionary<string, string> parameters = null);

        /// <summary>
        /// Make a get http request without response
        /// </summary>
        Task<HttpResponseCode> GetEmptyAsync(string url, IDictionary<string, string> parameters = null);

        /// <summary>
        /// Make an http request type post with no response
        /// </summary>
        Task<HttpResponseCode> PostAsync(string url, string content, string contentType = HttpContentType.Json);

        /// <summary>
        /// Make an http request type post with response
        /// </summary>
        Task<HttpResponse<TResult>> PostAsync<TResult>(string url, string content, string contentType = HttpContentType.Json);
    }
}
