﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xam_Delivery.Core.Models;
using Xam_Delivery.Core.Models.Google;

namespace Xam_Delivery.Core.Services.Abstract
{
    public interface IGoogleService
    {
        Task<GoogleDirection> GetDirections(MapCoordinates origin, MapCoordinates destination);
    }
}
