﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Text;
using Xam_Delivery.Core.Models.Event;

namespace Xam_Delivery.Core.Events
{
    class DealerRouteEvent : PubSubEvent<DealerRoute> { }
}
