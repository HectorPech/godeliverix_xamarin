﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Delivery.Core.Models.Common;

namespace Xam_Delivery.Core.Models.Profile
{
    public class ProfileMenuItem : BindableModel
    {
        public string Name { get; set; }

        public string FontIcon { get; set; }

        public string Title { get; set; }

        private string _value;
        public string Value
        {
            get { return _value; }
            set
            {
                _value = value;
                this.OnPropertyChanged("Value");
                this.OnPropertyChanged("HasValue");
            }
        }


        public bool HasValue => string.IsNullOrEmpty(this.Value) ? false : true;
    }
}
