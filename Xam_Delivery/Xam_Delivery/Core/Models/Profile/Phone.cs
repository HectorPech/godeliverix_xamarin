﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Delivery.Core.Models.Profile
{
    public class Phone
    {
        public Guid Uid { get; set; }

        public Guid UidTipo { get; set; }

        public string Numero { get; set; }

        public string Tipo { get; set; }
    }
}
