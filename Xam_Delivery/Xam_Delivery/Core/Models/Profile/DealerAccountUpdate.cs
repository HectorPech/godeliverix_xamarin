﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Delivery.Core.Models.Profile
{
    public class DealerAccountUpdate
    {
        public Guid Uid { get; set; }

        public string Nombre { get; set; } = "";

        public string ApellidoPaterno { get; set; } = "";

        public string ApellidoMaterno { get; set; } = "";

        public DateTime? FechaNacimiento { get; set; }
    }
}
