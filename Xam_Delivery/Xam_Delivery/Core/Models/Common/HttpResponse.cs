﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Delivery.Core.Enum;

namespace Xam_Delivery.Core.Models.Common
{
    public class HttpResponse<TResult>
    {
        public TResult Result { get; set; }

        public HttpResponseCode Code { get; set; }
    }
}
