﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Delivery.Core.Models.CashBox
{
    public class CardItemContainer
    {
        public CardItem Item1 { get; set; }
        public CardItem Item2 { get; set; }
    }
}
