﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Delivery.Core.Models.Common;

namespace Xam_Delivery.Core.Models.CashBox
{
    public class CardItem : BindableModel
    {
        public string Identifier { get; set; }

        public string Title { get; set; }

        private double quantity;
        public double Quantity
        {
            get { return quantity; }
            set
            {
                quantity = value;
                this.OnPropertyChanged("Quantity");
            }
        }

        private bool isMoneyType;
        public bool IsMoneyType
        {
            get { return isMoneyType; }
            set
            {
                isMoneyType = value;
                this.IsNumberType = !value;
                this.OnPropertyChanged("IsMoneyType");
                this.OnPropertyChanged("IsNumberType");
            }
        }

        private bool isNumberType;
        public bool IsNumberType
        {
            get { return isNumberType; }
            set { isNumberType = value; }
        }


        public string Description { get; set; }

        public bool HasDescription => !string.IsNullOrEmpty(this.Description);

        public CardItem()
        {
            this.IsMoneyType = true;
        }
    }
}
