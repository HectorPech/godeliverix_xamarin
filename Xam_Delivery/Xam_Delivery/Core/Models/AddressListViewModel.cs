﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Delivery.Core.Models
{
    public class AddressListViewModel
    {
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();

        public string Identifier { get; set; }

        public string ShortAddress { get; set; }
    }
}
