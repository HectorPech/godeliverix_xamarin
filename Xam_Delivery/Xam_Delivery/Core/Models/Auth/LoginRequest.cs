﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Delivery.Core.Models.Auth
{
    public class LoginRequest
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public Guid ProfileUid { get; set; }
    }
}
