﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Delivery.Core.Models.LocalStorage
{
    public class UserSession
    {
        public Guid Uid { get; set; }

        public string StrUid => this.Uid.ToString();

        public string Usuario { get; set; }

        public string Nombre { get; set; }

        public string ApellidoPaterno { get; set; }

        public string ApellidoMaterno { get; set; }

        public DateTime FechaNacimiento { get; set; }

        public Guid UidPerfil { get; set; }

        public Guid UidRepartidor { get; set; }

        public string CorreoElectronico { get; set; }
    }
}
