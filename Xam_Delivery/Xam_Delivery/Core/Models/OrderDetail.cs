﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Delivery.Core.Models
{
    public class OrderDetail
    {
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();

        public string UrlLogo { get; set; }

        public string NombreEmpresa { get; set; }

        public string NombreSucursal { get; set; }

        /// <summary>
        /// Sucursal
        /// </summary>
        public double LatitudePickup { get; set; }
        public double LongitudePickup { get; set; }

        /// <summary>
        /// Cliente
        /// </summary>
        public double LatitudeDelivery { get; set; }
        public double LongitudeDelivery { get; set; }

        public List<ProductDetail> Products { get; set; }

        public OrderDetail()
        {
            this.Products = new List<ProductDetail>();
        }
    }
}
