﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Delivery.Core.Models
{
    public class DeliveryOrderProductDetail
    {
        public string Nombre { get; set; }

        public int Cantidad { get; set; }
    }
}
