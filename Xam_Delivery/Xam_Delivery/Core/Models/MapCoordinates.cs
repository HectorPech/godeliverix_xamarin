﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Delivery.Core.Models
{
    public class MapCoordinates
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
