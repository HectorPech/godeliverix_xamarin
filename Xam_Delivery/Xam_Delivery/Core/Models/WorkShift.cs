﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Delivery.Core.Models
{
    public class WorkShift
    {
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();

        public Guid UidUsuario { get; set; }

        public Guid UidEstatusActual { get; set; }

        public DateTime FechaInicio { get; set; }

        public DateTime? FechaFin { get; set; }

        public Int64 Folio { get; set; }

        public decimal Fondo { get; set; }

        public int TotalOrdenes { get; set; }

        public double TotalSucursal { get; set; }

        public double TotalEnvio { get; set; }

        public double Efectivo { get; set; }

        public double Liquidacion { get; set; }

        public double Ganancias { get; set; }

        public double PagosSucursales { get; set; }

        public double CantidadPagos { get; set; }

        public double Recarga { get; set; }

        public double Propina { get; set; }
    }
}
