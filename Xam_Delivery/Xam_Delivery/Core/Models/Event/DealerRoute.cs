﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Delivery.Core.Enum;

namespace Xam_Delivery.Core.Models.Event
{
    public class DealerRoute
    {
        public MapCoordinates Origin { get; set; }

        public MapCoordinates Destination { get; set; }

        public RouteEventStatus Status { get; set; }

        public bool Tracking { get; set; } = true;
    }
}
