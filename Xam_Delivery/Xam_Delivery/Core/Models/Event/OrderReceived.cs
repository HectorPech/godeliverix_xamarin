﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Delivery.Core.Models.Event
{
    public class OrderReceived
    {
        public Guid OrderUid { get; set; }
    }
}
