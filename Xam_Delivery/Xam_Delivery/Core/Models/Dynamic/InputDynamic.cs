﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Delivery.Core.Models.Common;

namespace Xam_Delivery.Core.Models.Dynamic
{
    public class InputDynamic : BindableModel
    {
        public string Identifier { get; set; }

        public string Name { get; set; }

        public bool IsTextInput { get; set; } = true;

        private string textValue;
        public string TextValue
        {
            get { return textValue; }
            set
            {
                textValue = value;
                this.OnPropertyChanged("TextValue");
            }
        }

        public bool IsDateInput { get; set; } = false;

        private DateTime dateValue;
        public DateTime DateValue
        {
            get { return dateValue; }
            set
            {
                dateValue = value;
                OnPropertyChanged("DateValue");
            }
        }

        public bool IsEnabled { get; set; } = true;

        public bool IsRequired { get; set; } = true;

        public InputDynamic()
        {
            this.TextValue = "";
            this.DateValue = DateTime.Now;
        }
    }
}
