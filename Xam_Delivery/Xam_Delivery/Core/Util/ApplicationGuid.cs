﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Delivery.Core.Util
{
    /// <summary>
    /// Identificador del Rol del usuario
    /// </summary>
    public static class RoleGuid
    {
        public static Guid Repartidor = Guid.Parse("DFC29662-0259-4F6F-90EA-B24E39BE4346");
    }

    /// <summary>
    /// Identificador del estatus del turno
    /// <para>[EstatusTurno]</para>
    /// </summary>
    public static class WorkShiftStatus
    {
        public static string Abierto = "81494F49-F416-4431-99F4-E0AA4CF7E9F6";
        public static string Cerrado = "3BE9EF83-4A39-4A60-9FA9-7F50AD60CA3A";
        public static string Liquidando = "AE28F243-AA0D-43BD-BF10-124256B75B00";
        public static string Liquidado = "38FA16DF-4727-41FD-A03E-E2E43FA78F3F";
        public static string Recargando = "B03E3407-F76D-4DFA-8BF9-7F059DC76141";
        public static string Recargado = "CCAFB7D6-A27C-4F5B-A4A6-13D35138471F";
    }

    /// <summary>
    /// Identificador del estatus de la orden del repartidor 
    /// <para>[EstatusOrdenRepartidor]</para>
    /// </summary>
    public static class DeliveryOrderStatus
    {
        public static Guid Confirmada = Guid.Parse("A42B2588-D650-4DD9-829D-5978C927E2ED");
        public static Guid Recolectada = Guid.Parse("B6791F2C-FA16-40C6-B5F5-123232773612");
        public static Guid Entregada = Guid.Parse("7DA3A42F-2271-47B4-B9B8-EDD311F56864");
        public static Guid Rechazada = Guid.Parse("12748F8A-E746-427D-8836-B54432A38C07");
        public static Guid Pendiente = Guid.Parse("6294DACE-C9D1-4F9F-A942-FF12B6E7E957");
    }
}
