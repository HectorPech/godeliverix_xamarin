﻿using System;
using System.Globalization;
using Xam_Delivery.Core.Models.Profile;
using Xamarin.Forms;

namespace Xam_Delivery.Core.Converters
{
    public class ProfileMenuItemValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var itemTappedEventArgs = value as ItemTappedEventArgs;
            if (itemTappedEventArgs == null)
            {
                throw new ArgumentException("Expected value to be of type ItemTappedEventArgs", nameof(value));
            }
            return ((ProfileMenuItem)itemTappedEventArgs.Item).Name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
