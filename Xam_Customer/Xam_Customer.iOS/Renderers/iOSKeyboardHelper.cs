﻿using UIKit;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.iOS.Renderers;
using Xamarin.Forms;

[assembly: Dependency(typeof(iOSKeyboardHelper))]
namespace Xam_Customer.iOS.Renderers
{
    public class iOSKeyboardHelper : IKeyboardHelper
    {
        public iOSKeyboardHelper()
        {

        }

        public void HideKeyboard()
        {
            UIApplication.SharedApplication.KeyWindow.EndEditing(true);
        }
    }
}