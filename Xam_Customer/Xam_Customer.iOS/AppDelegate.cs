﻿using Com.OneSignal;
using Com.OneSignal.Abstractions;
using Foundation;
using Prism;
using Prism.Ioc;
using System.Collections.Generic;
using UIKit;


namespace Xam_Customer.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            Xamarin.Forms.Forms.SetFlags(new string[] { "Expander_Experimental", "Shapes_Experimental"});
            Rg.Plugins.Popup.Popup.Init();
            global::Xamarin.Forms.Forms.Init();
            global::Xamarin.Forms.FormsMaterial.Init();
            Xamarin.FormsMaps.Init();
            LoadApplication(new App(new iOSInitializer()));
            ZXing.Net.Mobile.Forms.iOS.Platform.Init();

            //Remove this method to stop OneSignal Debugging  
            OneSignal.Current.SetLogLevel(LOG_LEVEL.VERBOSE, LOG_LEVEL.NONE);
            //SandBox
            //OneSignal.Current.StartInit("2964843e-3741-468d-a8be-8b6d78df6759")
            //.Settings(new Dictionary<string, bool>() {
            //    { IOSSettings.kOSSettingsKeyAutoPrompt, false },
            //    { IOSSettings.kOSSettingsKeyInAppLaunchURL, false } })
            //.InFocusDisplaying(OSInFocusDisplayOption.Notification)
            //.EndInit();

            //Godeliverix
            OneSignal.Current.StartInit("b505c19f-b636-40a8-a6f6-35a3120eada8")
            .Settings(new Dictionary<string, bool>() {
                { IOSSettings.kOSSettingsKeyAutoPrompt, false },
                { IOSSettings.kOSSettingsKeyInAppLaunchURL, false } })
            .InFocusDisplaying(OSInFocusDisplayOption.Notification)
            .EndInit();

            // The promptForPushNotificationsWithUserResponse function will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 7)
            OneSignal.Current.RegisterForPushNotifications();

            return base.FinishedLaunching(app, options);
        }
    }

    public class iOSInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            // Register any platform specific implementations
        }
    }
}
