﻿using Android.App;
using Android.Content;
using Android.Views.InputMethods;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Droid.Renderers;
using Xamarin.Forms;

[assembly: Dependency(typeof(AndroidKeyboardHelper))]
namespace Xam_Customer.Droid.Renderers
{
    public class AndroidKeyboardHelper : IKeyboardHelper
    {
        public AndroidKeyboardHelper() { }

        public void HideKeyboard()
        {
            var context = Forms.Context;
            var inputMethodManager = context.GetSystemService(Context.InputMethodService) as InputMethodManager;
            if (inputMethodManager != null && context is Activity)
            {
                var activity = context as Activity;
                var token = activity.CurrentFocus?.WindowToken;
                inputMethodManager.HideSoftInputFromWindow(token, HideSoftInputFlags.None);

                activity.Window.DecorView.ClearFocus();
            }
        }
    }
}