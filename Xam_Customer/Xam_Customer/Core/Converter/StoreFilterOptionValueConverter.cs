﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xam_Customer.Core.Model;
using Xamarin.Forms;

namespace Xam_Customer.Core.Converter
{
    public class StoreFilterOptionValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var itemTappedEventArgs = value as SelectionChangedEventArgs;
            if (itemTappedEventArgs == null)
            {
                throw new ArgumentException("Expected value to be of type ItemTappedEventArgs", nameof(value));
            }

            return itemTappedEventArgs.CurrentSelection.Count > 0 ? (StoreFilterOption)itemTappedEventArgs.CurrentSelection[0] : null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
