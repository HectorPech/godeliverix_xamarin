﻿using System;
using System.Globalization;
using Xam_Customer.Core.Model;
using Xamarin.Forms;

namespace Xam_Customer.Core.Converter
{
    public class PurchaseConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var itemTappedEventArgs = value as ItemTappedEventArgs;
            if (itemTappedEventArgs == null)
            {
                throw new ArgumentException("Expected value to be of type ItemTappedEventArgs", nameof(value));
            }
            return ((Order)itemTappedEventArgs.Item).StrUid;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
