﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xamarin.Forms;

namespace Xam_Customer.Core.Converter
{
    public class GiroListValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var itemTappedEventArgs = value as SelectedItemChangedEventArgs;
            if (itemTappedEventArgs == null)
            {
                throw new ArgumentException("Expected value to be of type ItemTappedEventArgs", nameof(value));
            }
            return ((Giro)itemTappedEventArgs.SelectedItem).Uid;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
