﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xam_Customer.Core.Model;
using Xamarin.Forms;

namespace Xam_Customer.Core.Converter
{
    public class OfertaListValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var itemTappedEventArgs = value as ItemTappedEventArgs;
            if (itemTappedEventArgs == null)
            {
                throw new ArgumentException("Expected value to be of type ItemTappedEventArgs", nameof(value));
            }
            return ((Oferta)itemTappedEventArgs.Item).Identifier;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
