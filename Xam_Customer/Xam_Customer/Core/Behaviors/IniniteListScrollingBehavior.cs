﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xam_Customer.Core.Model;
using Xamarin.Forms;

namespace Xam_Customer.Core.Behaviors
{
    public class IniniteListScrollingBehavior : Behavior<ListView>
    {
        public ListView AssociatedObject { get; private set; }

        public static readonly BindableProperty CommandProperty = BindableProperty.Create("Command", typeof(ICommand), typeof(IniniteListScrollingBehavior), null);

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        protected override void OnAttachedTo(ListView bindable)
        {
            base.OnAttachedTo(bindable);

            AssociatedObject = bindable;
            bindable.BindingContextChanged += OnBindingContextChanged;
            bindable.ItemAppearing += OnItemAppearing;
        }

        protected override void OnDetachingFrom(ListView bindable)
        {
            base.OnDetachingFrom(bindable);

            bindable.BindingContextChanged -= OnBindingContextChanged;
            bindable.ItemAppearing -= OnItemAppearing;
            AssociatedObject = null;
        }

        private void OnBindingContextChanged(object sender, System.EventArgs e)
        {
            base.OnBindingContextChanged();
            BindingContext = AssociatedObject.BindingContext;
        }

        private void OnItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            var listview = ((ListView)sender);

            if (listview.IsRefreshing)
                return;

            if (Command == null)
            {
                return;
            }

            var source = ((ObservableCollection<ProductStore>)listview.ItemsSource);

            if (e.ItemIndex < (source.Count - 1))
                return;

            if (Command.CanExecute(e.Item))
            {
                Command.Execute(e.Item);
            }
        }
    }
}
