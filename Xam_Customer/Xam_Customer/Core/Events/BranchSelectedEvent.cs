﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Text;
using Xam_Customer.Core.Model.JsonPlaceHolder;

namespace Xam_Customer.Core.Events
{
    public class BranchSelectedEvent : PubSubEvent<Branch>
    {
    }
}
