﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Events
{
    public class CartCountEvent : PubSubEvent<int>
    {
    }
}
