﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Text;
using Xam_Customer.Core.Model;

namespace Xam_Customer.Core.Events
{
    public class StoreOptionChangedEvent : PubSubEvent<StoreOptionChanged> { }

    public class StoreOptionChanged
    {
        public Guid Uid { get; set; }

        public string Name { get; set; }

        public StoreFilterOptionType Type { get; set; }
    }
}
