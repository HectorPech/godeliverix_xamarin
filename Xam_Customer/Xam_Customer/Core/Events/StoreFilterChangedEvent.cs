﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Text;
using Xam_Customer.Core.Model;

namespace Xam_Customer.Core.Events
{
    public class StoreFilterChangedEvent : PubSubEvent<StoreFilter> { }
}
