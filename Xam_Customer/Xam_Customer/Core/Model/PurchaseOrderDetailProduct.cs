﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class ProductHistory
    {
        [JsonProperty("UidProductoEnOrden")]
        public string Uid { get; set; }
        [JsonProperty("UidSucursal")]
        public string UidBranch { get; set; }
        [JsonProperty("StrNombreProducto")]
        public string Name { get; set; }
        [JsonProperty("UidProducto")]
        public string UidProducto { get; set; }
        [JsonProperty("MTotal")]
        public decimal Price { get; set; }
        [JsonProperty("Imagen")]
        public string ImageProduct { get; set; }
        [JsonProperty("VisibilidadNota")]
        public string VisibilityNoteName { get; set; }
        [JsonProperty("intCantidad")]
        public int Quantity { get; set; }
        [JsonProperty("StrNota")]
        public string Notes { get; set; }
    }
}
