﻿using System;
using Xam_Customer.Core.Enum;

namespace Xam_Customer.Core.Model
{
    public class UserSignInRewardCode
    {
        public Guid Uid { get; set; }

        public Guid UserUid { get; set; }

        public Guid? ParentCodeUid { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Code { get; set; }

        public int Redeems { get; set; }

        public CodeRewardType RewardType { get; set; }

        public decimal RewardValue { get; set; }

        public int ActivationCount { get; set; }
    }
}
