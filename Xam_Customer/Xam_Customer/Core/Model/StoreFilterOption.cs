﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Customer.Core.Model.Core;

namespace Xam_Customer.Core.Model
{
    public class StoreFilterOption : NotifyPropertyChangedModel
    {
        private Guid uid;
        public Guid Uid
        {
            get { return uid; }
            set { uid = value; this.OnPropertyChanged("Uid"); }
        }

        public string StrUid => this.Uid.ToString();

        private string title;
        public string Title
        {
            get { return title; }
            set { title = value; this.OnPropertyChanged("Title"); }
        }

        public StoreFilterOptionType Type { get; set; }
    }

    public enum StoreFilterOptionType
    {
        // Sucursal
        Location,
        // Oferta
        Menu,
        // Seccion
        Section,
        OrderBy
    }
}
