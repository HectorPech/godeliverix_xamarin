﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Customer.Core.Model.Core;

namespace Xam_Customer.Core.Model
{
    public class ProfileItemsList : NotifyPropertyChangedModel
    {
        public string Name { get; set; }

        public string Icon { get; set; }

        public bool ShowCurrentValue => !string.IsNullOrEmpty(this.CurrentValue);

        public string CommandName { get; set; }

        private string currentValue;
        public string CurrentValue
        {
            get { return currentValue; }
            set
            {
                currentValue = value;
                this.OnPropertyChanged("CurrentValue");
                this.OnPropertyChanged("ShowCurrentValue");
            }
        }

    }
}
