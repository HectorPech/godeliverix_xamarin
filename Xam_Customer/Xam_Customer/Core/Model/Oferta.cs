﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class Oferta
    {
        public string Identifier { get; set; }
        public string Time { get; set; }
        public int Productos { get; set; }
    }
}
