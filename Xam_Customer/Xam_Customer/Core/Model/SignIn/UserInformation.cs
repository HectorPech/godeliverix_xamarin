﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Xam_Customer.Core.Model.JsonPlaceHolder;

namespace Xam_Customer.Core.Model.SignIn
{
    public class UserInformation
    {
        [JsonProperty("InformacionDeUsuario")]
        public User UserData { get; set; }
        [JsonProperty("ListaDeUsuarios")]
        public List<User> UserList { get; set; }
        [JsonProperty("InformacionDelTelefono")]
        public Telephone PhoneInformation { get; set; }
        [JsonProperty("CorreoElectronico")]
        public string Email { get; set; }
    }
}
