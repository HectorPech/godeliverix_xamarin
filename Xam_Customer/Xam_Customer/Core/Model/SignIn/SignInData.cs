﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.SignIn
{
    public class SignInData
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }


        public string Name { get; set; }
        public string FirstLastName { get; set; }
        public string SecondLastName { get; set; }
        public DateTime Birthday { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Lada { get; set; }
        public string IdTermsAndConditions { get; set; }
        public bool Accepted { get; set; }
    }
}
