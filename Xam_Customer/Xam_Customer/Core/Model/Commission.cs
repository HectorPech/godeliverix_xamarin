﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class Commission
    {
        [JsonProperty("ValorDeComision")]
        public double Percent { get; set; }
    }
}
