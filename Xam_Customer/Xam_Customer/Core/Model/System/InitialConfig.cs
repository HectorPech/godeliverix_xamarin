﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.System
{
    public class InitialConfig
    {
        [JsonProperty("Nombre")]
        public string Name { get; set; }
        [JsonProperty("Valor")]
        public string Value { get; set; }
    }
}
