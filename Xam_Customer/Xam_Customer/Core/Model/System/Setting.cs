﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.System
{
    /// <summary>
    /// This model is used in the settings module
    /// </summary>
    public class Setting
    {
        /// <summary>
        /// Configuration title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Name used to identify the setting in the backend
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Current setting value
        /// </summary>
        public string Value { get; set; }
    }
}
