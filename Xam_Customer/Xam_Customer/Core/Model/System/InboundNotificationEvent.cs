﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Customer.Core.Enum;

namespace Xam_Customer.Core.Model.System
{
    public class InboundNotificationEvent
    {
        public NotificationEventType Type {get;set;}

        public string Uid { get; set; }

        public string Message { get; set; }
    }
}
