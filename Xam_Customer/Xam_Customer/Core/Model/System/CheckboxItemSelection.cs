﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Xam_Customer.Core.Model.System
{
    public class CheckboxItemSelection : INotifyPropertyChanged
    {
        public Guid Uid { get; set; }
        public string StrUid => Uid.ToString();

        public int Id { get; set; }

        private bool _selected;
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; this.OnPropertyChanged("Selected"); }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; this.OnPropertyChanged("Name"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
