﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.System
{
    public class MapPin
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Identifier { get; set; }
    }
}
