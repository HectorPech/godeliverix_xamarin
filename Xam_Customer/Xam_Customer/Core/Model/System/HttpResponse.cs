﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Customer.Core.Enum;

namespace Xam_Customer.Core.Model.System
{
    public class HttpResponse<TResult>
    {
        public TResult Result { get; set; }

        public HttpResponseCode Code { get; set; }
    }
}
