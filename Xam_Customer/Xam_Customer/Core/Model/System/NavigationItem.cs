﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.System
{
    public class NavigationItem
    {
        public string Title { get; set; }

        public string Icon { get; set; }

        public string NamePage { get; set; }
    }
}
