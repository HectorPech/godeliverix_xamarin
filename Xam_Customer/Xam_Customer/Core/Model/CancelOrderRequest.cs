﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class CancelOrderRequest
    {
        public Guid UidOrdenSucursal { get; set; }
        public Guid UidUsuario { get; set; }
        public Guid UidDireccion { get; set; }
    }
}
