﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model.Core;

namespace Xam_Customer.Core.Model
{
    public class PaymentTypeItem : NotifyPropertyChangedModel
    {
        public string Icon { get; set; }

        public string Name { get; set; }

        private string description;
        public string Description
        {
            get { return description; }
            set
            {
                description = value;
                this.OnPropertyChanged("Description");
                this.OnPropertyChanged("HasDescription");
            }
        }

        public AvailablePaymentType Type { get; set; }

        #region View        
        private bool selected;
        public bool Selected
        {
            get { return selected; }
            set
            {
                selected = value;
                unselected = !value;
                this.OnPropertyChanged("Unselected");
                this.OnPropertyChanged("Selected");
            }
        }

        private bool unselected;
        public bool Unselected
        {
            get { return unselected; }
            set { unselected = value; }
        }

        public bool HasDescription => !string.IsNullOrEmpty(this.Description);
        #endregion
    }
}
