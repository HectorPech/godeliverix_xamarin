﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Internals;

namespace Xam_Customer.Core.Model
{
    public class UserSession : BindableBase
    {
        private Guid Uid { get; set; }

        public string StrUid { get; set; }

        private string _FisrtName;

        public string FisrtName
        {
            get { return _FisrtName; }
            set { SetProperty(ref _FisrtName, value); }
        }
        private string _FisrtLastName;

        public string FisrtLastName
        {
            get { return _FisrtLastName; }
            set { SetProperty(ref _FisrtLastName, value); }
        }
        private string _SecondLastName;

        public string SecondLastName
        {
            get { return _SecondLastName; }
            set { SetProperty(ref _SecondLastName, value); }
        }

        public string UserName { get; set; }
        public string BirthDay { get; set; }
        public string Status { get; set; }

    }
}
