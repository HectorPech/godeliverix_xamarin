﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.GoogleApi
{
    public class GoogleMapsPredictions
    {
        [JsonProperty("description")]
        public string PlaceName { get; set; }
        [JsonProperty("place_id")]
        public string IdPlace { get; set; }
    }
}
