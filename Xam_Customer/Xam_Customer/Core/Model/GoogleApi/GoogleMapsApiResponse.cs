﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.GoogleApi
{
    public class GoogleMapsApiResponse
    {
        //Resultado de predicciones
        [JsonProperty("predictions")]
        public IEnumerable<GoogleMapsPredictions> Predictions { get; set; }
        //Detalle del item
        [JsonProperty("result")]
        public GoogleMapsItemDetailLocation PlaceDetailsResult { get; set; }
        

    }
}
