﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.GoogleApi
{
    public class GoogleMapsItemDetailLocation
    {
        [JsonProperty("geometry")]
        public ItemLocationDetails PlaceLocationDetails { get; set; }
    }
}
