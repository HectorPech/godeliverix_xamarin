﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Customer.Core.Enum;
using Xamarin.Forms;

namespace Xam_Customer.Core.Model
{
    public class WalletTransaction
    {
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();

        public Guid UidConcept { get; set; }

        public Guid UidType { get; set; }

        public DateTime Date { get; set; }

        public long Folio { get; set; }

        public decimal Amount { get; set; }

        public string Type { get; set; }

        public string Concept { get; set; }

        public long? FolioOrdenSucursal { get; set; }

        public bool HasOrderNumber => this.FolioOrdenSucursal.HasValue;

        public bool IsIncome { get; set; }

        public bool IsExpense { get; set; }

        public Color AmountColor { get; set; }

        public EntityType Entity { get; set; }

        public string EntityValue { get; set; }

        public string Description { get; set; }
        public bool HasDescription => !string.IsNullOrEmpty(this.Description);
    }
}
