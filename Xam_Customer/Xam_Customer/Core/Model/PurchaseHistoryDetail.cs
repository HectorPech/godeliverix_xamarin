﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    class PurchaseHistoryDetail
    {
        public Guid PurchaseUid { get; set; }
        public Guid PaymentMethodUid { get; set; }
        public Guid PaymentStatusUid { get; set; }
        public string Folio { get; set; }
        public DateTime Date { get; set; }
        public decimal Total { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentStatus { get; set; }
        public string AddressIdentifier { get; set; }
        public string AddressStreet0 { get; set; }
        public string AddressLatitude { get; set; }
        public string AddressLongitude { get; set; }
        public string AddressCountry { get; set; }
        public string AddressState { get; set; }
        public string AddressCity { get; set; }
        public string AddressNeighborhood { get; set; }
        public decimal? WalletDiscount { get; set; }
        public decimal Tips { get; set; }
        public decimal DeliveryRate { get; set; }
        public IEnumerable<PurchaseHistoryDetailOrder> Orders { get; set; }

        public PurchaseHistoryDetail()
        {
            this.Orders = new HashSet<PurchaseHistoryDetailOrder>();
        }
    }
}
