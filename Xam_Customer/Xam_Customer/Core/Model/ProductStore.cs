﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class ProductStore
    {
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();
        public Guid UidCompany { get; set; }
        public string ImgUrl { get; set; }
        public string CompanyImgUrl { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CompanyName { get; set; }
        public decimal Price { get; set; }
        public bool Available { get; set; }
    }
}
