﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class OrderHistoryRoot
    {
        [JsonProperty("OrderList")]
        public IEnumerable<PurchaseOrderHistory> Purchases { get; set; }
    }

    public class PurchaseOrderHistory
    {
        [JsonProperty("Uidorden")]
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();

        [JsonProperty("LNGFolio")]
        public string Folio { get; set; }

        [JsonProperty("EstatusCobro")]
        public string PaymentStatus { get; set; }

        [JsonProperty("StrFormaDeCobro")]
        public string PaymentType { get; set; }

        [JsonProperty("FechaDeOrden")]
        public string StrDate { get; set; }

        [JsonProperty("MTotal")]
        public decimal Total { get; set; }

        [JsonProperty("StrDireccionDeEntrega")]
        public string DeliveryIdentifier { get; set; }

        [JsonProperty("ListaDeOrdenes")]
        public List<OrderSummaryHistory> Ordenes { get; set; }

        public Color StatusColor { get; set; }

        public string PaymentIcon { get; set; }

        public bool HasDiscount => this.WalletDiscount.HasValue;

        public decimal? WalletDiscount { get; set; }

        public decimal TotalNoDiscount { get; set; }
    }

    public class OrderSummaryHistory
    {
        [JsonProperty("UidRelacionOrdenSucursal")]
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();

        [JsonProperty("LNGFolio")]
        public double Folio { get; set; }

        [JsonProperty("MTotal")]
        public decimal Total { get; set; }

        [JsonProperty("StrNombreEmpresa")]
        public string CompanyName { get; set; }


        [JsonProperty("Identificador")]
        public string BrancheIdentifier { get; set; }

        [JsonProperty("StrEstatusOrdenSucursal")]
        public string Status { get; set; }

        public Color StatusColor { get; set; }

        [JsonProperty("Imagen")]
        public string UrlLogo { get; set; }

        [JsonProperty("intCantidad")]
        public int Products { get; set; }

        [JsonProperty("LngCodigoDeEntrega")]
        public long DeliveryCode { get; set; }
    }
}
