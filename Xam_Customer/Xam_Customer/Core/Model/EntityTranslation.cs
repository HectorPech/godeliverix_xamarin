﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Customer.Core.Enum;

namespace Xam_Customer.Core.Model
{
    public class EntityTranslation
    {
        public int Id { get; set; }
        public EntityType EntityType { get; set; }
        public Guid EntityUid { get; set; }
        public string Key { get; set; }
        public string En { get; set; }
        public string Es { get; set; }
    }
}
