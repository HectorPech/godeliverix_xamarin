﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class StoreListView
    {
        [JsonProperty("UIDEMPRESA")]
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();

        [JsonProperty("NOMBRECOMERCIAL")]
        public string Name { get; set; }

        [JsonProperty("SucursalesDisponibles")]
        public string NumberOfBranches { get; set; }

        [JsonProperty("StrRuta")]
        public string LogoUrl { get; set; }
    }
}
