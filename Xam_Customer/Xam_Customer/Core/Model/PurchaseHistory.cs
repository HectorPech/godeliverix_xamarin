﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Xam_Customer.Core.Model
{
    public class PurchaseHistory
    {
        public Guid PurchaseUid { get; set; }
        public string StrPurchaseUid => this.PurchaseUid.ToString();
        public Guid PaymentMethodUid { get; set; }
        public Guid PaymentStatusUid { get; set; }
        public string Folio { get; set; }
        public DateTime Date { get; set; }
        public decimal Total { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentStatus { get; set; }
        public string AddressIdentifier { get; set; }
        public IEnumerable<OrderHistory> Orders { get; set; }
        public decimal TotalNoDiscount { get; set; }
        public decimal? WalletDiscount { get; set; }

        public PurchaseHistory()
        {
            this.Orders = new HashSet<OrderHistory>();
        }

        public bool HasDiscount => this.WalletDiscount.HasValue;
        public Color StatusColor { get; set; }
        public string PaymentIcon { get; set; }
    }

    public class OrderHistory
    {
        /// <summary>
        /// Primary Key [OrdenSucursal]
        /// </summary>
        public Guid OrderUid { get; set; }
        /// <summary>
        /// Primary Key de la sucursal
        /// </summary>
        public Guid BrancheUid { get; set; }
        public Guid StatusUid { get; set; }
        /// <summary>
        /// Primary key de la empresa
        /// </summary>
        public Guid CompanyUid { get; set; }
        public string Company { get; set; }
        public string CompanyImg { get; set; }
        public string Branch { get; set; }
        public string BranchFolio { get; set; }
        public string DeliveryCode { get; set; }
        public string Status { get; set; }
        public decimal Total { get; set; }
        public decimal Tips { get; set; }
        public decimal Delivery { get; set; }
        public decimal? WalletDiscount { get; set; }
        public decimal CardPaymentComission { get; set; }
        public decimal DeliveryCardPaymentComission { get; set; }
        public bool IncludeCPTD { get; set; }
        public bool IncludeCPTS { get; set; }
        public int ProductCount { get; set; }
        public IEnumerable<OrdenProductoHistory> Products { get; set; }

        public OrderHistory()
        {
            this.Products = new HashSet<OrdenProductoHistory>();
        }

        public Color StatusColor { get; set; }
    }

    public class OrdenProductoHistory
    {
        public Guid ProductUid { get; set; }
        public Guid OrderUid { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}
