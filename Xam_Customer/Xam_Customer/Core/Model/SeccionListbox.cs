﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class SeccionListbox
    {
        public Guid Uid { get; set; }

        public string Name { get; set; }

        public bool Available { get; set; }
    }
}
