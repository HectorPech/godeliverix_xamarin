﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Xam_Customer.Core.Model
{
    public class PurchaseHistoryDetailOrder
    {
        /// <summary>
        /// Primary Key [OrdenSucursal]
        /// </summary>
        public Guid OrderUid { get; set; }
        /// <summary>
        /// Primary Key de la sucursal
        /// </summary>
        public Guid BrancheUid { get; set; }
        public Guid StatusUid { get; set; }
        /// <summary>
        /// Primary key de la empresa
        /// </summary>
        public Guid CompanyUid { get; set; }
        public string Company { get; set; }
        public string CompanyImg { get; set; }
        public string Branch { get; set; }
        public string BranchFolio { get; set; }
        public string DeliveryCode { get; set; }
        public string Status { get; set; }
        public decimal Total { get; set; }
        public decimal Tips { get; set; }
        public decimal Delivery { get; set; }
        public decimal? WalletDiscount { get; set; }
        public decimal CardPaymentComission { get; set; }
        public decimal DeliveryCardPaymentComission { get; set; }
        public bool IncludeCPTD { get; set; }
        public bool IncludeCPTS { get; set; }
        public IEnumerable<PurchaseHistoryDetailProduct> Products { get; set; }
        public int ProductCount { get; set; }

        public Color StatusColor { get; set; }
        public bool HasDiscount => this.WalletDiscount.HasValue;
        public decimal TotalNoDiscount { get; set; }

        public PurchaseHistoryDetailOrder()
        {
            this.Products = new HashSet<PurchaseHistoryDetailProduct>();
        }
    }
}
