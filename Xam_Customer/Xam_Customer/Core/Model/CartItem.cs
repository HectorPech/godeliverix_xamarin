﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    /// <summary>
    /// Elemento del carrito
    /// </summary>
    public class CartItem
    {
        public Guid CartId { get; set; }
        /// <summary>
        /// Identificador del elemento
        /// </summary>
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();

        /// <summary>
        /// Identificador del producto
        /// </summary>
        public Guid UidProduct { get; set; }
        public string StrUidProduct => this.UidProduct.ToString();

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Descripcion del product
        /// </summary>
        public string ProductDescription { get; set; }

        /// <summary>
        /// Url de la imagen del producto
        /// </summary>
        public string ImgPathProduct { get; set; }

        /// <summary>
        /// Precio unitario del producto
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Cantidad del producto
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Notas adicionales
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// Validar si tiene notas, se usa para mostrar en la vista 
        /// </summary>
        public bool HasNotes => string.IsNullOrEmpty(this.Notes) ? false : true;

        /// <summary>
        /// Costo total del producto
        /// </summary>
        public decimal Total
        {
            get
            {
                return (this.UnitPrice * this.Quantity);
            }
        }

        public List<CartItem> CartItems { get; internal set; }
        public decimal SubTotal { get; set; }
    }
}
