﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class Email
    {
        [JsonProperty("CorreoElectronico")]
        public string name { get; set; }
    }
}
