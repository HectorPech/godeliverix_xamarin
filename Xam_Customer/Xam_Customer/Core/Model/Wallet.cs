﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class Wallet
    {
        public Guid Uid { get; set; }

        public Guid UidUser { get; set; }

        public decimal Amount { get; set; }

        public DateTime? CreatedDate { get; set; }
    }
}
