﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class DeliveryInformation
    {
        [JsonProperty("DPrecio")]
        public decimal Price { get; set; }

        [JsonProperty("StrNombreEmpresa")]
        public string CompanyName { get; set; }

        [JsonProperty("StrCodigoDeEntrega")]
        public string DeliveryCode { get; set; }

        [JsonProperty("MPropina")]
        public decimal Tips { get; set; }
    }
}
