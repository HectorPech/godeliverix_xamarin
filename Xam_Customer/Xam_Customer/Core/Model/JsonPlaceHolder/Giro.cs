﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.JsonPlaceHolder
{
    public class Giro
    {
        [JsonProperty("RUTAIMAGEN")]
        public string ImageUrl { get; set; }

        [JsonProperty("UIDVM")]
        public string Uid { get; set; }

        [JsonProperty("STRNOMBRE")]
        public string Name { get; set; }

        [JsonProperty("STRDESCRIPCION")]
        public string Description { get; set; }
    }
}
