﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.JsonPlaceHolder
{
    public class Monedero
    {
        [JsonProperty("MMonto")]
        public decimal MontoMonedero { get; set; }
    }
}
