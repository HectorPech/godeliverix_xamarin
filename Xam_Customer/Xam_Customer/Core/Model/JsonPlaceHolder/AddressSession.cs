﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class AddressSession
    {
        #region Properties
        [JsonProperty("CIUDAD")]
        public string Uid { get; set; }
        [JsonProperty("NOMBRECIUDAD")]
        public string Name { get; set; }
        [JsonProperty("ESTADO")]
        public string UidState { get; set; }
        #endregion

        public string FullAddress { get; set; }


    }
}
