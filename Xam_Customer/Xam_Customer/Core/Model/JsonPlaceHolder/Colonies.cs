﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.JsonPlaceHolder
{
    public class Colonies
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string UidEstado { get; set; }
        [JsonProperty("IdColonia")]
        public string Uid { get; set; }
        [JsonProperty("Nombre")]
        public string Name { get; set; }
    }
}
