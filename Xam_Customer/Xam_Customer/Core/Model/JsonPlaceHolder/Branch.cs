﻿using Newtonsoft.Json;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Xam_Customer.Core.Model.JsonPlaceHolder
{
    public class Branch : BindableBase
    {
        [JsonProperty("UidSucursal")]
        public string Uid { get; set; }
        [JsonProperty("UID")]
        public string UidSeccion { get; set; }
        [JsonProperty("UIDEMPRESA")]
        public string UidEmpresa { get; set; }
        [JsonProperty("StrCosto")]
        public decimal Costoproducto { get; set; }
        [JsonProperty("StrIdentificador")]
        public string Identificador { get; set; }
        [JsonProperty("StrDireccion")]
        public string Direccion { get; set; }
        [JsonProperty("DtmVariableParaTiempo")]
        public DateTime Disponibilidad { get; set; }
        public string StrDisponibilidad { get; set; }

        public bool IncluyeComisionTarjetaProducto { get; set; }

        public bool IncluyeComisionTarjetaEnvio { get; set; }

        public Branch()
        {
            SelectedColorItem = Color.Transparent;
        }
        #region Propiedades de acciones
        private Color _selectedColorItem;

        public Color SelectedColorItem
        {
            get => _selectedColorItem; 
            set => SetProperty(ref _selectedColorItem, value); 
        }

        #endregion
    }
}
