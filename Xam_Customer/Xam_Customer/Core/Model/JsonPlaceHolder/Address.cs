﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Commands;
using System.Threading.Tasks;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Enum;
using Xamarin.Forms;
using Xam_Customer.Views.Profile;
using Xam_Customer.Core.Model.System;

namespace Xam_Customer.Core.Model.JsonPlaceHolder
{
    public class Address : BindableBase
    {

        public Address()
        {
        }
        [JsonProperty("ID")]
        public string Uid { get; set; }

        [JsonProperty("IDENTIFICADOR")]
        private string _Identifier;

        public string Identifier
        {
            get { return _Identifier; }
            set { SetProperty(ref _Identifier, value); }
        }

        [JsonProperty("COLONIA")]
        private string _UidSuburb;

        public string UidSuburb
        {
            get { return _UidSuburb; }
            set { SetProperty(ref _UidSuburb, value); }
        }

        [JsonProperty("NOMBRECOLONIA")]
        private string _SuburbName;

        public string SuburbName
        {
            get { return _SuburbName; }
            set { SetProperty(ref _SuburbName, value); }
        }

        [JsonProperty("CIUDAD")]
        private string _UidCity;

        public string UidCity
        {
            get { return _UidCity; }
            set { SetProperty(ref _UidCity, value); }
        }

        [JsonProperty("MUNICIPIO")]
        private string _Municipality;

        public string Municipality
        {
            get { return _Municipality; }
            set { SetProperty(ref _Municipality, value); }
        }

        [JsonProperty("PAIS")]
        private string _Country;

        public string Country
        {
            get { return _Country; }
            set { SetProperty(ref _Country, value); }
        }

        [JsonProperty("NombrePais")]
        private string _CountryName;

        public string CountryName
        {
            get { return _CountryName; }
            set { SetProperty(ref _CountryName, value); }
        }

        [JsonProperty("NOMBRECIUDAD")]
        private string _CityName;

        public string CityName
        {
            get { return _CityName; }
            set { SetProperty(ref _CityName, value); }
        }

        [JsonProperty("ESTADO")]
        private string _UidState;

        public string UidState
        {
            get { return _UidState; }
            set { SetProperty(ref _UidState, value); }
        }

        [JsonProperty("NombreEstado")]
        private string _StateName;

        public string StateName
        {
            get { return _StateName; }
            set { SetProperty(ref _StateName, value); }
        }

        [JsonProperty("CALLE0")]
        public string Street0 { get; set; }

        [JsonProperty("CALLE1")]
        public string Street1 { get; set; }

        [JsonProperty("CALLE2")]
        public string Street2 { get; set; }

        [JsonProperty("MANZANA")]
        public string Block { get; set; }

        [JsonProperty("LOTE")]
        public string Lot { get; set; }

        [JsonProperty("CodigoPostal")]
        public string ZipCode { get; set; }

        [JsonProperty("REFERENCIA")]
        private string _Reference;

        public string Reference
        {
            get { return _Reference; }
            set { SetProperty(ref _Reference, value); }
        }

        [JsonProperty("Longitud")]
        private string _Longitude;

        public string Longitude
        {
            get { return _Longitude; }
            set { SetProperty(ref _Longitude, value); }
        }

        [JsonProperty("Latitud")]
        private string _Latitude;

        public string Latitude
        {
            get { return _Latitude; }
            set { SetProperty(ref _Latitude, value); }
        }

        [JsonProperty("Status")]
        public bool Status { get; set; }

        private bool _DefaultAddress;

        [JsonProperty("DefaultAddress")]
        public bool DefaultAddress
        {
            get { return _DefaultAddress; }
            set { SetProperty(ref _DefaultAddress, value); }
        }

    }
}
