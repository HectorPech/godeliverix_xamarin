﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.JsonPlaceHolder
{
    public class Categoria
    {
        [JsonProperty("UIDCATEGORIA")]
        public string Uid { get; set; }
        [JsonProperty("STRNOMBRE")]
        public string Name { get; set; }
    }
}
