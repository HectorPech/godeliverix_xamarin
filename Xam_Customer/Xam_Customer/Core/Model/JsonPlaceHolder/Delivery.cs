﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.JsonPlaceHolder
{
    public class Delivery
    {
        #region Properties
        [JsonProperty("UidTarifario")]
        public string Uid { get; set; }
        [JsonProperty("StrNombreSucursal")]
        public string BranchName { get; set; }
        [JsonProperty("DPrecio")]
        public decimal Price { get; set; }
        [JsonProperty("StrRuta")]
        public string UrlImageCompany { get; set; }
        [JsonProperty("StrNombreEmpresa")]
        public string DeliveryCompanyName { get; set; }
        #endregion
    }
}
