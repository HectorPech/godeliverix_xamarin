﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.JsonPlaceHolder
{
    public class PostGrid : Post
    {
        public string Username { get; set; }

        public string Email { get; set; }
    }
}
