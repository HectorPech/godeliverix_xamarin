﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.JsonPlaceHolder
{
    public class Seccion
    {
        [JsonProperty("UID")]
        public string UID { get; set; }
        [JsonProperty("StrNombre")]
        public string Name { get; set; }
        [JsonProperty("StrHoraInicio")]
        public string StartTime { get; set; }
        [JsonProperty("StrHoraFin")]
        public string FinishTime { get; set; }
        [JsonProperty("IntEstatus")]
        public string Status { get; set; }
    }
}
