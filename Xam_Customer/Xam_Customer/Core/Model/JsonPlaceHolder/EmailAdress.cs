﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.JsonPlaceHolder
{
    public class EmailAdress
    {
        [JsonProperty("ID")]
        public string UID { get; set; }
        [JsonProperty("CORREO")]
        public string Mail { get; set; }
        [JsonProperty("UidPropietario")]
        public string UidUser { get; set; }
    }
}
