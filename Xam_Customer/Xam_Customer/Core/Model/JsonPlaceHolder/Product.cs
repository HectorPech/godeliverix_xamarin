﻿using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class Product : BindableBase
    {
        public int Quantity { get; set; }

        [JsonProperty("UID")]
        public string StrUid { get; set; }

        [JsonProperty("STRNOMBRE")]
        public string Name { get; set; }

        [JsonProperty("STRDESCRIPCION")]
        public string Description { get; set; }

        [JsonProperty("STRRUTA")]
        public string ImgPathProduct { get; set; }
        [JsonProperty("STRRUTAImagenEmpresa")]
        public string ImgPathCompany { get; set; }

        [JsonProperty("Empresa")]
        public string CompanyName { get; set; }

        [JsonProperty("StrCosto")]
        public decimal Price { get; set; }
        [JsonProperty("UidSucursal")]
        public string UidSucursal { get; set; }
        [JsonProperty("UidSeccion")]
        public string UidSeccion { get; set; }
        [JsonProperty("UidSeccionPoducto")]
        public string UidSeccionPoducto { get; set; }
        [JsonProperty("UidTarifario")]
        public string UidTarifario { get; set; }
        [JsonProperty("StrIdentificador")]
        public string StrIdentificador { get; set; }
        [JsonProperty("CostoEnvio")]
        public decimal CostoEnvio { get; set; }
        [JsonProperty("StrDeliveryBranch")]
        public string DeliveryBranch { get; set; }
        [JsonProperty("strDeliveryCompany")]
        public string DeliveryCompany { get; set; }

        public string Notes { get; set; }
    }
}
