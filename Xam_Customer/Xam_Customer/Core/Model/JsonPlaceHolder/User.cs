﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.JsonPlaceHolder
{
    public class User
    {
        [JsonProperty("Uid")]
        public string Uid { get; set; }

        [JsonProperty("StrNombre")]
        public string Name { get; set; }

        [JsonProperty("StrApellidoPaterno")]
        public string LastName { get; set; }

        [JsonProperty("StrUsuario")]
        public string Username { get; set; }

        [JsonProperty("StrPerfil")]
        public string Profile { get; set; }
        [JsonProperty("StrApellidoMaterno")]
        public string SecondLastName { get; set; }
        [JsonProperty("DtmFechaDeNacimiento")]
        public string BirthDay { get; set; }
        [JsonProperty("StrEstatus")]
        public string Status { get; set; }
        [JsonProperty("StrCotrasena")]
        public string Password { get; set; }

    }
}
