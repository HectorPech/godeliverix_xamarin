﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.JsonPlaceHolder
{
    public class AddressUpload
    {
        public Guid Uid { get; set; }
        public Guid UidPais { get; set; }
        public Guid UidEstado { get; set; }
        public Guid UidMunicipio { get; set; }
        public Guid UidCiudad { get; set; }
        public Guid UidColonia { get; set; }
        public string Identificador { get; set; }
        public string Calle { get; set; }
        public string EntreCalle { get; set; }
        public string yCalle { get; set; }
        public string Manzana { get; set; }
        public string Lote { get; set; }
        public string CodigoPostal { get; set; }
        public string Referencias { get; set; }

        public Guid UidUsuario { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public bool DefaultAddress { get; set; }
        public bool Status { get; set; }
    }
}
