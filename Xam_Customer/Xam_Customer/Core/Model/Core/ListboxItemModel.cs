﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.Core
{
    public class ListboxItemModel : NotifyPropertyChangedModel
    {
        public Guid Uid { get; set; }

        public string StrUid => this.Uid.ToString();

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; this.OnPropertyChanged("Name"); }
        }

        private bool selected;
        public bool Selected
        {
            get { return selected; }
            set { selected = value; this.OnPropertyChanged("Selected"); }
        }

        private bool available;
        public bool Available
        {
            get { return available; }
            set { available = value; this.OnPropertyChanged("Available"); }
        }

        private string caption;
        public string Caption
        {
            get { return caption; }
            set { caption = value; this.OnPropertyChanged("Caption"); this.OnPropertyChanged("ShowCaption"); }
        }

        public bool ShowCaption => !string.IsNullOrEmpty(this.Caption);

        public bool ShowAvailability { get; set; } = false;

        public ListboxItemModel()
        {
            this.Available = true;
        }
    }
}
