﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.Core
{
    public class CommonDataSource<T>
    {
        public IEnumerable<T> Payload { get; set; }

        public int Count { get; set; }

        public CommonDataSource()
        {
            this.Payload = new HashSet<T>();
            this.Count = 0;
        }
    }
}
