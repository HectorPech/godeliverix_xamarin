﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.Address
{
    public class Country
    {
        [JsonProperty("Uid")]
        public string Uid { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
    }
}
