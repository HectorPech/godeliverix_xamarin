﻿using Newtonsoft.Json;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class Pedidos : BindableBase
    {
        [JsonProperty("UidOrden")]
        public string UidOrder { get; set; }
        [JsonProperty("UidRelacionOrdenSucursal")]
        public string Uid { get; set; }
        [JsonProperty("BintCodigoEntrega")]
        public string DeliveryCode { get; set; }
        [JsonProperty("NVchRuta")]
        public string ImgBranch { get; set; }
        [JsonProperty("Identificador")]
        public string Identifier { get; set; }
        [JsonProperty("MPropina")]
        public decimal DeliveryTip { get; set; }
        [JsonProperty("MTotal")]
        public decimal OrderTotal { get; set; }
        [JsonProperty("LNGFolio")]
        public long Folio { get; set; }
        [JsonProperty("MTotalSucursal")]
        public decimal SubTotal { get; set; }
        [JsonProperty("uidSucursal")]
        public string UidSucursal { get; set; }
        [JsonProperty("CostoEnvio")]
        public decimal DeliveryRate { get; set; }
        [JsonProperty("StrEstatusOrdenSucursal")]
        private string _LastStatus;

        public string LastStatus
        {
            get { return _LastStatus; }
            set { SetProperty(ref _LastStatus, value); }
        }

        public Color StatusColor { get; set; }

        [JsonProperty("IntCantidad")]
        public string TotalProducts { get; set; }

        public decimal? WalletDiscount { get; set; }

        public decimal TotalNoDiscount { get; set; }

        public bool HasDiscount => this.WalletDiscount.HasValue;

        #region Commissions
        public bool SupplierIncludeCardPaymentCommission { get; set; }
        public bool DeliveryIncludeCardPaymentCommission { get; set; }
        public decimal OrderSubtotalComission { get; set; }
        public decimal DeliveryRateSubtotalComission { get; set; }
        public decimal DeliveryTipsSubtotalComission { get; set; }
        #endregion
    }
}
