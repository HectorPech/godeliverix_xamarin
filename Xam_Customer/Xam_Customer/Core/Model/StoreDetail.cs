﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class StoreDetail
    {
        [JsonProperty("UidComercio")]
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();

        [JsonProperty("NombreComercio")]
        public string Name { get; set; }

        [JsonProperty("LogoComercio")]
        public string LogoUrl { get; set; }

        [JsonProperty("SucursalesDisponibles")]
        public IEnumerable<StoreDetailBranch> Branches { get; }

        public IEnumerable<CompanyBranch> CompanyBranches { get; set; }

        public StoreDetail()
        {
            this.Branches = new HashSet<StoreDetailBranch>();
        }
    }

    public class StoreDetailBranch
    {
        [JsonProperty("ID")]
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();

        [JsonProperty("IDENTIFICADOR")]
        public string Identifier { get; set; }

        [JsonProperty("HORAAPARTURA")]
        public string OpenAt { get; set; }

        [JsonProperty("HORACIERRE")]
        public string CloseAt { get; set; }

        [JsonProperty("Estatus")]
        public string Status { get; set; }

        [JsonProperty("UidEmpresa")]
        public Guid UidStore { get; set; }
    }
}
