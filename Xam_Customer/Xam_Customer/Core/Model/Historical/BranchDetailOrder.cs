﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.Historical
{
    public class BranchDetailOrder
    {
        [JsonProperty("OrderInformation")]
        public OrderInformationBranch OrderInformation { get; set; }
        [JsonProperty("DeliveryInformation")]
        public DeliveryInformation DeliveryInformation { get; set; }
        [JsonProperty("StatusInformation")]
        public List<StatusOrder> StatusList { get; set; }
        [JsonProperty("ProductsInformation")]
        public List<ProductHistory> ProductsList { get; set; }

    }
}
