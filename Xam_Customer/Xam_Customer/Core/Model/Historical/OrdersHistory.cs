﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class OrdersHistory
    {
        [JsonProperty("OrderList")]
        public List<Order> MainOrdersList { get; set; }
    }
}
