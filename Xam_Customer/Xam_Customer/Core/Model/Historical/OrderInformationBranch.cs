﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.Historical
{
    public class OrderInformationBranch
    {
        [JsonProperty("Uidorden")]
        public string Uid { get; set; }
        [JsonProperty("LNGFolio")]
        public long Folio { get; set; }
        [JsonProperty("Identificador")]
        public string Identificador { get; set; }
        [JsonProperty("MTotal")]
        public decimal Total { get; set; }
        [JsonProperty("FechaDeOrden")]
        public string Date { get; set; }
        [JsonProperty("StrNombreSucursal")]
        public string CompanyName { get; set; }
        [JsonProperty("Imagen")]
        public string ImageCompany { get; set; }

        public decimal? WalletDiscount { get; set; }


        #region Commissions
        [JsonProperty("IncludeCPTS")]
        public bool SupplierIncludeCardPaymentCommission { get; set; }

        [JsonProperty("IncludeCPTD")]
        public bool DeliveryIncludeCardPaymentCommission { get; set; }

        [JsonProperty("CardPaymentCommission")]
        public decimal OrderComission { get; set; }

        [JsonProperty("DeliveryCardPaymentCommission")]
        public decimal DeliveryRateComission { get; set; }

        [JsonProperty("ComisionPagoTarjetaPropina")]
        public decimal DeliveryTipsComission { get; set; }
        #endregion
    }
}
