﻿using Newtonsoft.Json;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Xam_Customer.Core.Model
{
    /// <summary>
    /// Orden
    /// </summary>
    public class Order : BindableBase
    {
        [JsonProperty("Uidorden")]
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();

        [JsonProperty("FechaDeOrden")]
        public string Date { get; set; }

        [JsonProperty("MTotal")]
        public decimal Total { get; set; }

        [JsonProperty("EstatusCobro")]
        public string UidStatus { get; set; }

        [JsonProperty("EstatusCobroName")]
        public string StatusName { get; set; }

        public string StatusNameTranslated { get; set; }

        [JsonProperty("LNGFolio")]
        public string Folio { get; set; }

        [JsonProperty("StrFormaDeCobro")]
        private string _PaymentType;

        public string PaymentType
        {
            get { return _PaymentType; }
            set { SetProperty(ref _PaymentType, value); }
        }

        [JsonProperty("StrDireccionDeEntrega")]
        public string DeliveredAddressName { get; set; }
        [JsonProperty("IntCantidadDeOrdenes")]
        public int TotalOrders { get; set; }
        [JsonProperty("IntCantidadProductos")]
        public int TotalProducts { get; set; }
        public string FormaPago { get; set; }

        public Color StatusColor { get; set; }

        public string PaymentIcon { get; set; }
    }
}
