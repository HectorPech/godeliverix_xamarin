﻿using Newtonsoft.Json;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model.Historical
{
    public class StatusOrder : BindableBase
    {
        [JsonProperty("DtmFechaDeEstatus")]
        public string DateCreated { get; set; }

        [JsonProperty("NOMBRE")]
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { SetProperty(ref _Name, value); }
        }
        private string _Fecha;

        public string Fecha
        {
            get { return _Fecha; }
            set { SetProperty(ref _Fecha, value); }
        }
        private string _Hora;

        public string Hora
        {
            get { return _Hora; }
            set { SetProperty(ref _Hora, value); }
        }

        [JsonProperty("DtFecha")]
        public DateTime Date { get; set; }

        public bool NotLastOne { get; set; } = true;

        public bool First { get; set; }

        public bool NotFirst { get; set; } = true;
    }
}
