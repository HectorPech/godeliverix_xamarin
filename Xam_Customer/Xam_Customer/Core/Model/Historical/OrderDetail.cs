﻿using Newtonsoft.Json;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;
using Xam_Customer.Core.Model.JsonPlaceHolder;

namespace Xam_Customer.Core.Model
{
    public class OrderDetail:BindableBase
    {
        [JsonProperty("UidOrden")]
        public string UidOrden { get; set; }

        [JsonProperty("FolioOrden")]
        public long Folio { get; set; }

        [JsonProperty("FormaDeCobro")]
        private string _PaymentType;

        public string PaymentType
        {
            get { return _PaymentType; }
            set { SetProperty(ref _PaymentType , value); }
        }

        [JsonProperty("EstatusCobro")]
        public string PaymentStatus { get; set; }

        [JsonProperty("oDeliveryAddress")]
        public Xam_Customer.Core.Model.JsonPlaceHolder.Address DeliveryAddress { get; set; }

        [JsonProperty("PedidosList")]
        public List<Pedidos> OrderList { get; set; }
    }
}
