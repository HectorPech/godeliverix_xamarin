﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Util;

namespace Xam_Customer.Core.Model
{
    public class ProductFilter
    {
        public FilterParameterType Parameter { get; set; } = FilterParameterType.None;

        public Guid UidSelected { get; set; } = Guid.Empty;

        public Guid UidType { get; set; } = Guid.Empty;

        public Guid UidCategory { get; set; } = Guid.Empty;

        public Guid UidSubcategory { get; set; } = Guid.Empty;

        public ApplicationFilter OrderBy { get; set; }

        public bool OnlyAvailable { get; set; } = false;
    }
}
