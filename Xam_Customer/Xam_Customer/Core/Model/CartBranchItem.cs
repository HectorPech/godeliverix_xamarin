﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xam_Customer.ViewModels.Core;

namespace Xam_Customer.Core.Model
{
    /// <summary>
    /// Para la vista principal del carrito
    /// </summary>
    public class CartBranchItem
    {
        /// <summary>
        /// Identificador del elemento
        /// </summary>
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();

        /// <summary>
        /// Identificador de la sucursal
        /// </summary>
        public Guid UidBranch { get; set; }
        public string StrUidBranch => this.UidBranch.ToString();

        /// <summary>
        /// Nombre/Identificador de la sucursal
        /// </summary>
        public string BranchName { get; set; }

        /// <summary>
        /// Nombre de la empresa
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Url del logo de la empresa
        /// </summary>
        public string CompanyImgUrl { get; set; }

        /// <summary>
        /// Cantidad de items dentro de la orden
        /// </summary>
        public int QuantityItems
        {
            get
            {
                return this.CartItems.Count();
            }
        }

        /// <summary>
        /// Subtotal de los items agreados a la orden
        /// </summary>
        public decimal ItemsSubtotal
        {
            get => this.CartItems.Sum(i => i.Total);
        }

        #region Propiedades del envio
        /// <summary>
        /// Identificador del tarifario
        /// </summary>
        public string UidDelivery { get; set; }

        /// <summary>
        /// Tarifa de envio
        /// </summary>
        public decimal DeliveryRate { get; set; }
        /// <summary>
        /// Imagen del distribuidor
        /// </summary>
        public string ImageDeliveryCompany { get; set; }
        /// <summary>
        /// Identificador de la sucursal
        /// </summary>
        public string DeliveryBranch { get; set; }
        /// <summary>
        /// Nombre de la empresa distribuidora
        /// </summary>
        public string DeliveryCompany { get; set; }
        /// <summary>
        /// Propina del repartidor
        /// </summary>
        public decimal DeliveryTips { get; set; }
        #endregion

        #region Comisiones
        //public bool IncludeCardPaymentCommission { get; set; }

        //public bool IncludeDeliveryCardPaymentCommission { get; set; }

        //public decimal CardPaymentCommission { get; set; }

        //public decimal DeliveryCardPaymentCommission { get; set; }

        public bool ShowCardPaymentCommission { get; set; }

        //public decimal TotalComission
        //{
        //    get { return this.CardPaymentCommission + this.DeliveryCardPaymentCommission; }
        //}

        //public decimal IncludedCardPaymentCommission
        //{
        //    get { return (this.IncludeCardPaymentCommission ? this.CardPaymentCommission : 0); }
        //}

        //public decimal IncludedDeliveryCardPaymentCommission
        //{
        //    get { return (this.IncludeDeliveryCardPaymentCommission ? this.DeliveryCardPaymentCommission : 0); }
        //}

        //public decimal CardPaymentCommissionSummary
        //{
        //    get { return (this.IncludeCardPaymentCommission ? 0 : this.CardPaymentCommission) + (this.IncludeDeliveryCardPaymentCommission ? 0 : this.DeliveryCardPaymentCommission); }
        //}

        //public decimal OrderComission
        //{
        //    get => this.Subtotal - this.IncludedCardPaymentCommission;
        //}

        //public decimal SubtotalComission
        //{
        //    get { return (this.Subtotal - this.IncludedCardPaymentCommission) + this.DeliveryRate + this.DeliveryTips; }
        //}

        #endregion

        /// <summary>
        /// Productos
        /// </summary>

        private List<CartItem> _CartItems;

        public List<CartItem> CartItems
        {
            get { return _CartItems; }
            set { _CartItems = value; }
        }


        #region Descuento
        public decimal? WalletDiscount { get; set; }

        public bool HasDiscount { get { return this.WalletDiscount.HasValue; } }
        #endregion

        public CartBranchItem()
        {
            this.CartItems = new List<CartItem>();
        }

        //public void CalculatePaymentCommission(double commissionPercent)
        //{
        //    decimal comissionT = ((decimal)commissionPercent / 100);
        //    decimal comissionR = ((decimal)(1 - commissionPercent) / 100);

        //    this.CardPaymentCommission = (this.Subtotal * (decimal)commissionPercent) / 100;

        //    this.DeliveryCardPaymentCommission = ((this.DeliveryRate + this.DeliveryTips) * (decimal)commissionPercent) / (100 - (decimal)commissionPercent);


        //    this.ShowCardPaymentCommission = !this.IncludeCardPaymentCommission || !this.IncludeDeliveryCardPaymentCommission;
        //}


        #region Commissions
        /// <summary>
        /// La Empresa suministradora absorbe la comisión del pago con tarjeta
        /// </summary>
        public bool SupplierIncludeCardPaymentCommission { get; set; }

        /// <summary>
        /// El Distribuidor absorbe la comisión del pago con tarjeta
        /// </summary>
        public bool DeliveryIncludeCardPaymentCommission { get; set; }

        /// <summary>
        /// Costo del Subtotal de la orden
        /// </summary>
        public decimal OrderSubtotal
        {
            get
            {
                return this.ItemsSubtotal - (this.SupplierIncludeCardPaymentCommission ? OrderSubtotalComission : 0);
            }
        }
        /// <summary>
        /// Comisiones del subtotal de la orden
        /// </summary>
        public decimal OrderSubtotalComission { get; set; }

        /// <summary>
        /// Subtotal del costo de envio
        /// </summary>
        public decimal DeliveryRateSubtotal
        {
            get
            {
                return this.DeliveryRate - (this.DeliveryIncludeCardPaymentCommission ? DeliveryRateSubtotalComission : 0);
            }
        }
        /// <summary>
        /// Comisiones del subtotal del costo de envio
        /// </summary>
        public decimal DeliveryRateSubtotalComission { get; set; }

        /// <summary>
        /// Subtotal de las propinas del envio
        /// </summary>
        public decimal DeliveryTipsSubtotal
        {
            get
            {
                return this.DeliveryTips - (this.DeliveryIncludeCardPaymentCommission ? DeliveryTipsSubtotalComission : 0);
            }
        }
        /// <summary>
        /// Comisiones del subtotal de las propinas
        /// </summary>
        public decimal DeliveryTipsSubtotalComission { get; set; }

        /// <summary>
        /// Subtotal de la orden de la compra
        /// </summary>
        public decimal Subtotal
        {
            get
            {
                return (this.OrderSubtotal + this.DeliveryRateSubtotal + this.DeliveryTipsSubtotal) - (this.WalletDiscount.HasValue ? this.WalletDiscount.Value : 0);
            }
        }

        /// <summary>
        /// Total de las comisiones que el usuario debe PAGAR
        /// </summary>
        public decimal SubtotalCommissions
        {
            get => (this.SupplierIncludeCardPaymentCommission ? 0 : this.OrderSubtotalComission) + (this.DeliveryIncludeCardPaymentCommission ? 0 : this.DeliveryRateSubtotalComission + this.DeliveryTipsSubtotalComission);
        }

        /// <summary>
        /// Total de las comisiones que se generaron
        /// </summary>
        public decimal TotalCommissions
        {
            get => this.OrderSubtotalComission + this.DeliveryRateSubtotalComission + this.DeliveryTipsSubtotalComission;
        }

        /// <summary>
        /// Total de la 
        /// </summary>
        public decimal Total
        {
            get { return (this.ItemsSubtotal + this.DeliveryRate + this.DeliveryTips + this.SubtotalCommissions) - (this.WalletDiscount.HasValue ? this.WalletDiscount.Value : 0); }
        }

        /// <summary>
        /// El total de la orden sin comisiones ni descuentos
        /// </summary>
        public decimal TotalWithoutComissions
        {
            get => this.ItemsSubtotal + this.DeliveryRate + this.DeliveryTips;
        }

        /// <summary>
        /// Calcular las comisiones
        /// </summary>
        /// <param name="commissionPercent"></param>
        public void CalculateComission()
        {
            this.OrderSubtotalComission = Math.Round((this.ItemsSubtotal * (decimal)CommissionPercent) / (this.SupplierIncludeCardPaymentCommission ? 100 : 100 - (decimal)CommissionPercent), 2);
            this.DeliveryRateSubtotalComission = Math.Round((this.DeliveryRate * (decimal)CommissionPercent) / (this.DeliveryIncludeCardPaymentCommission ? 100 : 100 - (decimal)CommissionPercent), 2);
            this.DeliveryTipsSubtotalComission = Math.Round((this.DeliveryTips * (decimal)CommissionPercent) / (this.DeliveryIncludeCardPaymentCommission ? 100 : 100 - (decimal)CommissionPercent), 2);

            this.ShowCardPaymentCommission = true;
        }

        /// <summary>
        /// Limpiar los valores de las comisiones
        /// </summary>
        public void RemoveComission()
        {
            this.OrderSubtotalComission = 0;
            this.DeliveryRateSubtotalComission = 0;
            this.DeliveryTipsSubtotalComission = 0;

            this.ShowCardPaymentCommission = false;
        }

        public double CommissionPercent { get; set; }
        #endregion
    }
}
