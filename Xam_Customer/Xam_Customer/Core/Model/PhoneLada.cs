﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class PhoneLada
    {
        [JsonProperty("UidLada")]
        public string Uid { get; set; }
        [JsonProperty("StrLada")]
        public string StrLada { get; set; }
    }
}
