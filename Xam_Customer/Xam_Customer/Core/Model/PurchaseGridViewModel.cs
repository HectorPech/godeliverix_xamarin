﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class PurchaseGridViewModel
    {
        [JsonProperty("Uidorden")]
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();

        [JsonProperty("FechaDeOrden")]
        public string StrFecha { get; set; }

        [JsonProperty("MTotal")]
        public double Total { get; set; }

        [JsonProperty("LNGFolio")]
        public int Folio { get; set; }

        [JsonProperty("EstatusCobro")]
        public string Estatus { get; set; }

        [JsonProperty("StrFormaDeCobro")]
        public string FormaPago { get; set; }

        public Color StatusColor { get; set; }

        public string PaymentIcon { get; set; }
    }
}
