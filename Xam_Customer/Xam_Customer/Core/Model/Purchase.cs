﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    /// <summary>
    /// Pedido/Compra
    /// </summary>
    public class Purchase
    {
        [JsonProperty("Uidorden")]
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();
    }
}
