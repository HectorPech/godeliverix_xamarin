﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Customer.Core.Model.Core;

namespace Xam_Customer.Core.Model
{
    public class AddressListView : NotifyPropertyChangedModel
    {
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();

        public string Identifier { get; set; }

        public string FullAddress { get; set; }

        private bool _default;
        public bool Default
        {
            get { return _default; }
            set
            {
                _default = value;
                this.NoDefault = !value;
                OnPropertyChanged("Default");
                OnPropertyChanged("NoDefault");
            }
        }


        public bool NoDefault { get; set; }
    }
}
