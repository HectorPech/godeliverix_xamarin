﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class SearchCompanyDetail
    {
        public Guid Uid { get; set; }

        public string Name { get; set; }

        public string ImgUrl { get; set; }

        public IEnumerable<CompanyBranch> Branches { get; set; }

        public SearchCompanyDetail()
        {
            this.Branches = new HashSet<CompanyBranch>();
        }
    }
}
