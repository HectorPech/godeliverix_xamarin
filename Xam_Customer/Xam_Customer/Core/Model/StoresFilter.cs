﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Customer.Core.Util;

namespace Xam_Customer.Core.Model
{
    public class StoresFilter
    {
        public Guid UidType { get; set; } = Guid.Empty;

        public Guid UidCategory { get; set; } = Guid.Empty;

        public Guid UidSubcategory { get; set; } = Guid.Empty;

        public ApplicationFilter OrderBY { get; set; } = new ApplicationFilter();

    }

    public static class StoresFilterName
    {
        public static string Giro = "Giro";
        public static string Categoria = "Categoria";
        public static string Subcategoria = "Subcategoria";
    }
}
