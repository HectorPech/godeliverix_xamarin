﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class Telephone
    {
        [JsonProperty("ID")]
        public string Uid { get; set; }
        [JsonProperty("NUMERO")]
        public string number { get; set; }
        [JsonProperty("UidTipo")]
        public string UidType { get; set; }
        [JsonProperty("StrNombreTipoDeTelefono")]
        public string NameTipe { get; set; }
        [JsonProperty("resultado")]
        public bool RequestStatus { get; set; }
        [JsonProperty("UidLada")]
        public string UidLada { get; set; }

    }
}
