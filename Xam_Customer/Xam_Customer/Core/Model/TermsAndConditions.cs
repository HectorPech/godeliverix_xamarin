﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class TermsAndConditions
    {
        [JsonProperty("Uid")]
        public string Uid { get; set; }
        [JsonProperty("url")]
        public string URL { get; set; }
    }
}
