﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class ConfiguracionClienteMovil
    {
        public Guid Uid { get; set; }
        public string VchDescripcion { get; set; }
        public string VchValor { get; set; }
        public string VchNombre { get; set; }
    }
}
