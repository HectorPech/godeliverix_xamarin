﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class CompanyStoreGrid
    {
        public Guid Uid { get; set; }

        public string StrUid => this.Uid.ToString();

        public string Name { get; set; }

        public string ImgUrl { get; set; }

        public int AvailableBranches { get; set; }

        public string AvailableBranchesSummary { get; set; }

        public bool Available { get; set; }
        public string AvailableText { get; set; }

        public string OpenAt { get; set; }

        public string ClosedAt { get; set; }

        public bool BeforeOpen { get; set; }

        public bool AfterClose { get; set; }
    }
}
