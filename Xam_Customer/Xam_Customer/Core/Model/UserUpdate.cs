﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class UserUpdate
    {
        [JsonProperty("Uid")]
        public Guid Uid { get; set; }

        [JsonProperty("StrUsuario")]
        public string Usuario { get; set; }

        [JsonProperty("StrCotrasena")]
        public string Contrasenia { get; set; }

        [JsonProperty("StrNombre")]
        public string Nombre { get; set; }

        [JsonProperty("StrApellidoPaterno")]
        public string ApellidoPaterno { get; set; }

        [JsonProperty("StrApellidoMaterno")]
        public string ApellidoMaterno { get; set; }

        [JsonProperty("DtmFechaDeNacimiento")]
        public string StrFechaNacimiento { get; set; }

        [JsonProperty("StrEstatus")]
        public string StrEstatus { get; set; }
    }
}
