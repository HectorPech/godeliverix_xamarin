﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class CartBranchItemListView
    {
        /// <summary>
        /// Identificador del elemento
        /// </summary>
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();

        /// <summary>
        /// Identificador de la sucursal
        /// </summary>
        public Guid UidBranch { get; set; }
        public string StrUidBranch => this.UidBranch.ToString();

        /// <summary>
        /// Nombre/Identificador de la sucursal
        /// </summary>
        public string BranchName { get; set; }

        /// <summary>
        /// Nombre de la empresa
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Url del logo de la empresa
        /// </summary>
        public string CompanyImgUrl { get; set; }

        /// <summary>
        /// Cantidad de items dentro de la orden
        /// </summary>
        public int QuantityItems { get; set; }

        /// <summary>
        /// Texto de la Cantidad de items dentro de la orden traducido
        /// </summary>
        public string StrQuantityItems { get; set; }

        /// <summary>
        /// Total de la orden
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// Tarifa de envio
        /// </summary>
        public decimal DeliveryRate { get; set; }
    }
}
