﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Model
{
    public class OrderHistoryDetail
    {
        /// <summary>
        /// Primary Key [OrdenSucursal]
        /// </summary>
        public Guid OrderUid { get; set; }
        public Guid PurchaseUid { get; set; }
        /// <summary>
        /// Primary Key de la sucursal
        /// </summary>
        public Guid BrancheUid { get; set; }
        public Guid StatusUid { get; set; }
        /// <summary>
        /// Primary key de la empresa
        /// </summary>
        public Guid CompanyUid { get; set; }
        public string Company { get; set; }
        public string CompanyImg { get; set; }
        public string Branch { get; set; }
        public string BranchFolio { get; set; }
        public string DeliveryCode { get; set; }
        public string Status { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentStatus { get; set; }
        public decimal Total { get; set; }
        public decimal Tips { get; set; }
        public decimal Delivery { get; set; }
        public decimal? WalletDiscount { get; set; }
        public decimal CardPaymentComission { get; set; }
        public decimal DeliveryCardPaymentComission { get; set; }
        public decimal DeliveryTipsCardPaymentComission { get; set; }
        public bool IncludeCPTD { get; set; }
        public bool IncludeCPTS { get; set; }
        public IEnumerable<OrderHistoryDetailProduct> Products { get; set; }
        public int ProductCount { get; set; }
        public IEnumerable<OrderHistoryDetailTimeLine> Timeline { get; set; }

        public OrderHistoryDetail()
        {
            this.Products = new HashSet<OrderHistoryDetailProduct>();
            this.Timeline = new HashSet<OrderHistoryDetailTimeLine>();
        }
    }

    public class OrderHistoryDetailProduct
    {
        public Guid ProductUid { get; set; }
        public Guid OrderUid { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public string Notes { get; set; }
        public decimal Total { get; set; }
    }

    public class OrderHistoryDetailTimeLine
    {
        public Guid StatusUid { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }

        /* UI */
        public bool First { get; set; }
        public bool NotLastOne { get; set; }
        public bool NotFirst { get; set; }
    }
}
