﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Enum
{
    public enum CodeRewardType
    {
        /// <summary>
        /// Ninguna
        /// </summary>
        None,
        /// <summary>
        /// Abono al monedero
        /// </summary>
        WalletAmount,
        /// <summary>
        /// Envio gratis
        /// </summary>
        FreeDelivery,
        /// <summary>
        /// Porcentage de descuento
        /// </summary>
        PercentageDiscount
    }
}
