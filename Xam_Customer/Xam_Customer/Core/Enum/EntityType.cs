﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Enum
{
    public enum EntityType
    {
        None = -1,
        Empresa,
        Sucursal,
        Productos,
        Monedero,
        Movimientos,
        Orden,
        OndenSucursal,
        UserSignInRewardCode,
        AllUserSignInRewardCode,
        AppContactView
    }
}
