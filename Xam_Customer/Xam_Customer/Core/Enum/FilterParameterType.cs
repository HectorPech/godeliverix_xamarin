﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Enum
{
    public enum FilterParameterType
    {
        None,
        Giro,
        Categoria,
        Subcategoria
    }
}
