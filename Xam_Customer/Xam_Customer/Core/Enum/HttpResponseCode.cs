﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Enum
{
    public enum HttpResponseCode
    {
        Success,
        Failed,
        NotFound,
        ServerError
    }
}
