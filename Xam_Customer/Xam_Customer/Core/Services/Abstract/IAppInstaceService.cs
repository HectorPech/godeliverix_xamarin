﻿using System;
using System.Collections.Generic;
using System.Text;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Util;

namespace Xam_Customer.Core.Services.Abstract
{
    public interface IAppInstaceService
    {
        void SetUset(UserSession session);

        UserSession GetUset();

        bool HasAddresses { get; set; }

        #region Filtros de busqueda
        void AddGiros(Giro Item);
        List<Giro> GetGiroList();
        void SelectGiro(string value);
        string GetGiroSelected();
        void SelectCategoria(string value);
        string GetCategoriaSelected();
        void SelectSubcategoria(string value);
        string GetSubcategoriaSelected();
        void ChangeSearchType(string value);
        string GetSearchType();
        void SetUidFilter(string value);
        string GetUidFilter();
        void SetDayOfTheWeek(string value);
        string GetDayOfTheWeek();
        #endregion


        #region Delivery demo address
        void SelectDeliveryAddress(string estado, string colonia, string nombreColonia, double longitude, double latitude);
        string GetDeliverySelected();
        string GetDeliveryState();
        string GetDeliveryColonie();
        double GetDeliveryLong();
        double GetDeliveryLat();
        #endregion


        #region User delivery address
        void UserSelectedDeliveryAddress(string UidDireccion, string UidEstado, string UidColonia, string identificador, double latitude, double longitude);
        string GetUserDeliverySelected();
        string GetUserSelectedDeliveryAddress();
        void SetAddressTotal(int quantity);
        int GetAddressTotal();
        #endregion

        #region Carrito
        /// <summary>
        /// Agregar un nuevo producto al carrito de la orden de la sucursal
        /// </summary>
        /// <param name="item"></param>
        CartItem GetCartItemByIdFromBranch(Guid CartBranchItemUid, Guid uid);
        void AddCartItemToBranchItem(Guid CartBranchItemUid, CartItem item);
        void UpdateCartItemFromBranchItem(Guid CartBranchItemUid, CartItem item);
        void RemoveCartItemFromBranch(Guid CartBranchItemUid, Guid uid);
        void RemoveAllitemsFromCart();
        void RemoveBranchItemsFromCart(string CartBranchUid);
        void UpdateDeliveryOrder(Guid UidBranch, string UidDelivery, decimal price);
        void UpdateDeliveryTip(Guid UidBranch, decimal tip);
        List<CartBranchItem> ReadAllCartBranchItems();
        CartBranchItem GetCartBranchItemById(Guid Uid);
        CartBranchItem GetCartBranchItemByBranch(Guid Uid);
        void AddCartBranchItem(CartBranchItem item);
        void ApplyCommissionCartBranchItem(Guid CartBranchItemUid, double commissionPercent);
        void ApplyCommissionCartBranchItem(Guid CartBranchItemUid);
        void ClearCommissionCartBranchItem(Guid CartBranchItemUid);
        void ClearCart();

        AvailablePaymentType PaymentType { get; set; }
        #endregion

        int ProductsCount();

        void ApplyWalletDiscount(decimal discount);
        void RemoveWalletDiscount();

        /// <summary>
        /// Valores de los filtros de la busqueda de productos
        /// </summary>
        ProductFilter ProductFilter { get; set; }

        /// <summary>
        /// Valores de los filtros de busqueda de la busqueda de empresas
        /// </summary>
        StoreFilter StoreFilter { get; set; }
    }
}
