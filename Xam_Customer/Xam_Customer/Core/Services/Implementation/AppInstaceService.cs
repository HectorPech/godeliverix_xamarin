﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Util;

namespace Xam_Customer.Core.Services.Implementation
{
    public class AppInstaceService : IAppInstaceService
    {
        #region Filtros de busqueda
        public List<Giro> ListaDeGiros { get; set; }
        public string SearchType { get; set; }
        public string GiroSelected { get; set; }
        public string CategoriaSelected { get; set; }
        public string SubcategoriaSelected { get; set; }
        public string UidBusquedaCategorias { get; set; }
        public string DayOfTheWeek { get; set; }
        public void AddGiros(Giro Item)
        {
            if (ListaDeGiros == null)
            {
                ListaDeGiros = new List<Giro>();
            }
            ListaDeGiros.Add(Item);
        }
        public List<Giro> GetGiroList()
        {
            return ListaDeGiros;
        }
        public void SelectGiro(string value)
        {
            GiroSelected = value;
        }
        public string GetGiroSelected()
        {
            if (string.IsNullOrEmpty(GiroSelected))
            {
                GiroSelected = string.Empty;
            }
            return GiroSelected;
        }
        public void SelectCategoria(string value)
        {
            CategoriaSelected = value;
        }
        public string GetCategoriaSelected()
        {
            if (string.IsNullOrEmpty(CategoriaSelected))
            {
                CategoriaSelected = string.Empty;
            }
            return CategoriaSelected;
        }
        public void SelectSubcategoria(string value)
        {
            SubcategoriaSelected = value;
        }
        public string GetSubcategoriaSelected()
        {
            if (string.IsNullOrEmpty(SubcategoriaSelected))
            {
                SubcategoriaSelected = string.Empty;
            }
            return SubcategoriaSelected;
        }
        public void ChangeSearchType(string value)
        {
            SearchType = value;
        }
        public string GetSearchType()
        {
            return SearchType;
        }
        public void SetUidFilter(string value)
        {
            UidBusquedaCategorias = value;
        }
        public string GetUidFilter()
        {
            return UidBusquedaCategorias;
        }
        public void SetDayOfTheWeek(string value)
        {
            DayOfTheWeek = value;
        }
        public string GetDayOfTheWeek()
        {
            return DayOfTheWeek;
        }
        #endregion
        #region Propiedades de la direccion de entrega
        public string Estado { get; set; }
        public string Colonia { get; set; }
        public string NombreDeColonia { get; set; }
        public string UidDelivery { get; set; }
        public string Identifier { get; set; }
        public double Long { get; set; }
        public double Lat { get; set; }
        private int _AddressCount;

        public int AddressCount
        {
            get { return _AddressCount; }
            set { _AddressCount = value; }
        }

        public void SelectDeliveryAddress(string estado, string colonia, string nombreColonia, double longitude, double latitude)
        {
            Estado = estado;
            Colonia = colonia;
            NombreDeColonia = nombreColonia;
            Long = longitude;
            Lat = latitude;
        }
        public void UserSelectedDeliveryAddress(string UidDireccion, string UidEstado, string UidColonia, string identificador, double latitude, double longitude)
        {
            UidDelivery = UidDireccion;
            Estado = UidEstado;
            Colonia = UidColonia;
            Identifier = identificador;
            Lat = latitude;
            Long = longitude;
        }
        public string GetUserSelectedDeliveryAddress()
        {
            return UidDelivery;
        }
        public string GetUserDeliverySelected()
        {
            return Identifier;
        }
        public string GetDeliverySelected()
        {
            return NombreDeColonia;
        }
        public string GetDeliveryState()
        {
            return Estado;
        }
        public string GetDeliveryColonie()
        {
            return Colonia;
        }
        public double GetDeliveryLong()
        {
            return Long;
        }
        public double GetDeliveryLat()
        {
            return Lat;
        }

        public void SetAddressTotal(int quantity)
        {
            AddressCount = quantity;
        }
        public int GetAddressTotal()
        {
            return AddressCount;
        }
        #endregion
        UserSession userSession { get; set; }

        public AppInstaceService()
        {
            this.CartBranchItems = new List<CartBranchItem>();
            this.PaymentType = AvailablePaymentType.None;
                        
            this.ProductFilter = new ProductFilter() { OrderBy = ProductFilterOrder.None };
            this.StoreFilter = new StoreFilter() { OrderBy = ProductFilterOrder.None };
        }

        public UserSession GetUset()
        {
            return this.userSession;
        }
        public bool HasAddresses { get; set; }
        public void SetUset(UserSession session)
        {
            userSession = session;
        }

        #region Carrito
        public List<CartItem> CartItems { get; set; }
        public List<CartItem> BranchInformation { get; set; }
        public List<CartBranchItem> CartBranchItems { get; set; }
        public AvailablePaymentType PaymentType { get; set; }

        public CartItem GetCartItemById(Guid uid)
        {
            int branchIndex = this.CartBranchItems.FindIndex(i => i.Uid == uid);

            if (branchIndex >= 0)
            {
                return this.CartBranchItems[branchIndex].CartItems.FirstOrDefault(p => p.Uid == uid);
            }
            {
                return null;
            }
        }
        public void AddCartItemToBranchItem(Guid CartBranchItemUid, CartItem item)
        {
            // Buscar la orden de la sucursal
            int branchIndex = this.CartBranchItems.FindIndex(i => i.Uid == CartBranchItemUid);

            if (branchIndex >= 0)
            {
                // Verificar si no tiene notas
                if (string.IsNullOrEmpty(item.Notes))
                {
                    // Verificar si no existe el mismo producto dentro de la orden
                    int productIndex = this.CartBranchItems[branchIndex].CartItems.FindIndex(p => p.UidProduct == item.UidProduct && string.IsNullOrEmpty(p.Notes.Trim()));
                    if (productIndex >= 0)
                    {
                        int currentQuantity = this.CartBranchItems[branchIndex].CartItems[productIndex].Quantity;
                        this.CartBranchItems[branchIndex].CartItems[productIndex].Quantity = currentQuantity + item.Quantity;
                    }
                    else
                    {
                        this.CartBranchItems[branchIndex].CartItems.Add(item);
                    }
                }
                else
                {
                    this.CartBranchItems[branchIndex].CartItems.Add(item);
                }
            }
        }
        public void UpdateCartItemFromBranchItem(Guid CartBranchItemUid, CartItem item)
        {
            var Branch = CartBranchItems.SingleOrDefault(s => s.Uid == CartBranchItemUid);
            var Producto = Branch.CartItems.FindIndex(p => p.Uid == item.Uid);
            
            Branch.CartItems[Producto].Quantity = item.Quantity;
            Branch.CartItems[Producto].UnitPrice = item.UnitPrice;
            Branch.CartItems[Producto].Notes = item.Notes.Trim();
        }
        public void UpdateDeliveryOrder(Guid UidBranch, string UidDelivery, decimal price)
        {
            var index = CartBranchItems.FindIndex(s => s.Uid == UidBranch);
            var Branch = CartBranchItems.SingleOrDefault(s => s.Uid == UidBranch);
            Branch.UidDelivery = UidDelivery.ToString();
            Branch.DeliveryRate = price;
            CartBranchItems[index] = Branch;
        }
        public void UpdateDeliveryTip(Guid UidBranch, decimal tip)
        {
            var index = CartBranchItems.FindIndex(s => s.Uid == UidBranch);
            var Branch = CartBranchItems.SingleOrDefault(s => s.Uid == UidBranch);
            Branch.DeliveryTips = tip;
            CartBranchItems[index] = Branch;
        }
        public void RemoveCartItemFromBranch(Guid CartBranchItemUid, Guid uid)
        {
            int branchIndex = this.CartBranchItems.FindIndex(i => i.Uid == CartBranchItemUid);

            if (branchIndex >= 0)
            {
                int productIndex = this.CartBranchItems[branchIndex].CartItems.FindIndex(p => p.Uid == uid);
                if (productIndex >= 0)
                {
                    this.CartBranchItems[branchIndex].CartItems.RemoveAt(productIndex);

                    if (this.CartBranchItems[branchIndex].CartItems.Count() == 0)
                    {
                        this.CartBranchItems.RemoveAt(branchIndex);
                    }
                }
            }
        }
        public void RemoveAllitemsFromCart() { CartBranchItems.Clear(); }
        public void RemoveBranchItemsFromCart(string CartBranchUid)
        {
            var orden = CartBranchItems.Find(x => x.StrUid == CartBranchUid);
            CartBranchItems.Remove(orden);
        }
        public void ApplyCommissionCartBranchItem(Guid CartBranchItemUid, double commissionPercent)
        {
            int branchIndex = this.CartBranchItems.FindIndex(i => i.Uid == CartBranchItemUid);

            if (branchIndex >= 0)
            {
                this.CartBranchItems[branchIndex].CommissionPercent = commissionPercent;
                this.CartBranchItems[branchIndex].CalculateComission();
            }
        }
        public void ApplyCommissionCartBranchItem(Guid CartBranchItemUid)
        {
            int branchIndex = this.CartBranchItems.FindIndex(i => i.Uid == CartBranchItemUid);

            if (branchIndex >= 0)
            {
                this.CartBranchItems[branchIndex].CalculateComission();
            }
        }
        public void ClearCommissionCartBranchItem(Guid CartBranchItemUid)
        {
            int branchIndex = this.CartBranchItems.FindIndex(i => i.Uid == CartBranchItemUid);

            if (branchIndex >= 0)
            {
                this.CartBranchItems[branchIndex].RemoveComission();
            }
        }

        public void ClearCart()
        {
            CartBranchItems.Clear();
        }

        public List<CartBranchItem> ReadAllCartBranchItems()
        {
            return this.CartBranchItems;
        }
        public CartBranchItem GetCartBranchItemById(Guid Uid)
        {
            return this.CartBranchItems.SingleOrDefault(b => b.Uid == Uid);
        }
        /// <summary>
        /// Obtener el elemento del carrito por el Id de la sucursal
        /// </summary>
        /// <param name="Uid">Id de la sucursal</param>
        /// <returns></returns>
        public CartBranchItem GetCartBranchItemByBranch(Guid Uid)
        {
            return this.CartBranchItems.SingleOrDefault(b => b.UidBranch == Uid);
        }
        public void AddCartBranchItem(CartBranchItem item)
        {
            // Validar si el nuevo item tiene productos 
            if (item.CartItems.Count() == 0)
            {
                return;
            }

            // Buscar si existe un item del carrito con la misma sucursal
            int branchIndex = this.CartBranchItems.FindIndex(i => i.UidBranch == item.UidBranch);

            if (branchIndex >= 0)
            {
                CartBranchItem existingItem = this.CartBranchItems[branchIndex];

            }
            else
            {
                this.CartBranchItems.Add(item);
            }
        }

        public CartItem GetCartItemByIdFromBranch(Guid CartBranchItemUid, Guid uid)
        {
            throw new NotImplementedException();
        }

        #endregion

        public int ProductsCount()

        {

            return this.CartBranchItems.Sum(b => b.CartItems.Sum(i => i.Quantity));

        }

        public void ApplyWalletDiscount(decimal discount)
        {
            decimal origin = discount;
            foreach (CartBranchItem item in this.CartBranchItems)
            {
                if (origin == 0)
                {
                    item.WalletDiscount = null;
                }
                else if (origin >= item.TotalWithoutComissions)
                {
                    item.WalletDiscount = item.TotalWithoutComissions;
                    origin = (origin - item.TotalWithoutComissions);
                }
                else if (origin < item.TotalWithoutComissions && origin > 0)
                {
                    item.WalletDiscount = origin;
                    origin = origin - origin;
                }
            }
        }

        public void RemoveWalletDiscount()
        {
            foreach (CartBranchItem item in this.CartBranchItems)
            {
                item.WalletDiscount = null;
            }
        }

        public ProductFilter ProductFilter { get; set; }

        public StoreFilter StoreFilter { get; set; }
    }
}
