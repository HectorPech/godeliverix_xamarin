﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace Xam_Customer.Core.Util
{
    public static class ApplicationConstants
    {
        //Conexion nube
        public static string BaseUrl = "https://www.godeliverix.net/api/";
        //Conexion local Manuel
        //public static string BaseUrl = "http://192.168.1.78/api/";
        public static string Version = AppInfo.VersionString;
        public static string RowsToShow = string.Empty;
        public static string GoogleMapsApiKey = "AIzaSyCv_RWS83ffESAt6TfET-63l72EJe9efyU";
    }
}
