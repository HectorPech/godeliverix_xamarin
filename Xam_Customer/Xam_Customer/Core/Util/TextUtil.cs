﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Util
{
    public static class TextUtil
    {
        public static string Cut(this string text, int lenght)
        {
            if (text.Length <= lenght)
            {
                return text;
            }

            return text.Substring(0, lenght);
        }
    }
}
