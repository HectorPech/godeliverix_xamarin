﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Util
{
    public static class ProductFilterOrder
    {
        public static ApplicationFilter None = new ApplicationFilter { Index = 0, Value = string.Empty, Direction = "ASC" };
        public static ApplicationFilter NameAsc = new ApplicationFilter { Index = 1, Value = "Name", Direction = "ASC"};
        public static ApplicationFilter NameDesc = new ApplicationFilter { Index = 2, Value = "Name", Direction = "DESC" };
        public static ApplicationFilter PriceAsc = new ApplicationFilter { Index = 3, Value = "Price", Direction = "ASC" };
        public static ApplicationFilter PriceDesc = new ApplicationFilter { Index = 4, Value = "Price", Direction = "DESC" };

    }

    public class ApplicationFilter
    {
        public int Index { get; set; }

        public string Value { get; set; }

        public string Direction { get; set; }
    }
}
