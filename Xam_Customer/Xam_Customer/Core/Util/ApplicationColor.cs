﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Xam_Customer.Core.Util
{
    public static class OrderStatusColor
    {
        /// <summary>
        /// Creado
        /// </summary>
        public static GuidColor Created = new GuidColor() { Uid = Guid.Parse("2D2F38B8-7757-45FB-9CA6-6ECFE20356ED"), Color = Color.FromHex("#9A999E"), Name = "Creado" };

        /// <summary>
        /// Elaborado
        /// </summary>
        public static GuidColor Elaborated = new GuidColor() { Uid = Guid.Parse("C412D367-7D05-45D8-AECA-B8FABBF129D9"), Color = Color.FromHex("#640963"), Name = "Elaborado" };

        /// <summary>
        ///Entregado
        /// </summary>
        public static GuidColor Delivered = new GuidColor() { Uid = Guid.Parse("2FDEE8E7-0D54-4616-B4C1-037F5A37409D"), Color = Color.FromHex("#20396E"), Name = "Entregado" };

        /// <summary>
        ///Finalizado
        /// </summary>
        public static GuidColor Finalized = new GuidColor() { Uid = Guid.Parse("1E57F588-52FB-49E7-B53E-0FC23824E1E8"), Color = Color.FromHex("#DD5D16"), Name = "Finalizado" };

        /// <summary>
        ///Cancelado
        /// </summary>
        public static GuidColor Canceled = new GuidColor() { Uid = Guid.Parse("A2D33D7C-2E2E-4DC6-97E3-73F382F30D93"), Color = Color.FromHex("#E42E24"), Name = "Cancelado" };

        /// <summary>
        ///Espera de confirmacion
        /// </summary>
        public static GuidColor WaitingForConfirmation = new GuidColor() { Uid = Guid.Parse("DE294EFC-C549-4DDD-A0D1-B0E1E2039ECC"), Color = Color.FromHex("#7cc46f"), Name = "Espera de confirmacion" };

        /// <summary>
        ///Confirmado
        /// </summary>
        public static GuidColor Confirmed = new GuidColor() { Uid = Guid.Parse("27E80591-3D79-4F77-B736-56C21A9113F6"), Color = Color.FromHex("#054d05"), Name = "Confirmado" };

        /// <summary>
        ///Enviando
        /// </summary>
        public static GuidColor Sended = new GuidColor() { Uid = Guid.Parse("B6BFC834-7CC4-4E67-817D-5ECB0EB2FFA7"), Color = Color.FromHex("#00A6D6"), Name = "Enviando" };
    }

    public class GuidColor
    {
        public Guid Uid { get; set; }

        public string Name { get; set; }

        public Color Color { get; set; }
    }
}
