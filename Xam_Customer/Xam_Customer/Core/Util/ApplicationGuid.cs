﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.Core.Util
{
    public static class TipoMovimiento
    {
        public static Guid Agregar = Guid.Parse("E85F0486-1FBE-494C-86A2-BFDDC733CA5D");
        public static Guid Retirar = Guid.Parse("6C7F4C2E-0D27-4200-9485-7BE331066D33");
    }

    public static class Concepto
    {
        public static Guid TarjetaPrepago = Guid.Parse("5CCE4BD9-3AC4-40A5-A0A9-FA0234A3F848");
        public static Guid DepositoBancario = Guid.Parse("C7233E52-74CC-434E-9D37-AA170848FC7A");
        public static Guid PagoGoDeliverix = Guid.Parse("DCA75F23-5DDC-4EA5-B088-6D5B187F76F4");
        public static Guid Reembolso = Guid.Parse("2AABDF7F-EDCE-455F-B775-6283654D7DA0");
        public static Guid Promocion = Guid.Parse("8565A18B-FD65-4B18-9B9F-2154147D3179");
    }
}
