﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Model.System;

namespace Xam_Customer.Helpers
{
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Constants
        private static readonly string StorageDefaultValue = string.Empty;

        private static readonly string UserSessionKey = "userSession";
        private static readonly string AddressSessionKey = "addressSession";
        private static readonly string RememberUserkey = "false";
        private static readonly string RememberUserName = string.Empty;
        private static readonly string InboundNotificationKey = "inboundNotification";
        #endregion

        #region READ
        public static UserSession UserSession
        {
            get
            {
                string value = AppSettings.GetValueOrDefault(UserSessionKey, StorageDefaultValue);
                return string.IsNullOrEmpty(value) ? null : JsonConvert.DeserializeObject<UserSession>(value);
            }
            set
            {
                AppSettings.AddOrUpdateValue(UserSessionKey, JsonConvert.SerializeObject(value));
            }
        }
        public static string RememberLastUser
        {
            get
            {
                return AppSettings.GetValueOrDefault(RememberUserName, StorageDefaultValue);
            }
            set
            {
                AppSettings.AddOrUpdateValue(RememberUserName, value);
            }
        }
        public static string RememberUser
        {
            get
            {
                return AppSettings.GetValueOrDefault(RememberUserkey, StorageDefaultValue);
            }
            set
            {
                AppSettings.AddOrUpdateValue(RememberUserkey, value);
            }
        }

        public static Colonies AddressSession
        {
            get
            {
                string value = AppSettings.GetValueOrDefault(AddressSessionKey, StorageDefaultValue);
                return string.IsNullOrEmpty(value) ? null : JsonConvert.DeserializeObject<Colonies>(value);
            }
            set
            {
                AppSettings.AddOrUpdateValue(AddressSessionKey, JsonConvert.SerializeObject(value));
            }
        }

        public static InboundNotificationEvent InboundNotificationSession
        {
            get
            {
                string value = AppSettings.GetValueOrDefault(InboundNotificationKey, StorageDefaultValue);
                return string.IsNullOrEmpty(value) ? null : JsonConvert.DeserializeObject<InboundNotificationEvent>(value);
            }
            set
            {
                AppSettings.AddOrUpdateValue(InboundNotificationKey, value == null ? "" : JsonConvert.SerializeObject(value));
            }
        }

        public static void Clear()
        {
            AppSettings.AddOrUpdateValue(AddressSessionKey, "");
            AppSettings.AddOrUpdateValue(UserSessionKey, "");
            AppSettings.AddOrUpdateValue(InboundNotificationKey, "");
        }
        #endregion
    }
}
