﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Xam_Customer.Resources.LangResx.Profile {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ProfileLang {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ProfileLang() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Xam_Customer.Resources.LangResx.Profile.ProfileLang", typeof(ProfileLang).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Birthday.
        /// </summary>
        public static string AccountForm_Birthday {
            get {
                return ResourceManager.GetString("AccountForm_Birthday", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm.
        /// </summary>
        public static string AccountForm_Confirm {
            get {
                return ResourceManager.GetString("AccountForm_Confirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contact Information.
        /// </summary>
        public static string AccountForm_ContactInformation {
            get {
                return ResourceManager.GetString("AccountForm_ContactInformation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email.
        /// </summary>
        public static string AccountForm_Email {
            get {
                return ResourceManager.GetString("AccountForm_Email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First last name.
        /// </summary>
        public static string AccountForm_FirstLastname {
            get {
                return ResourceManager.GetString("AccountForm_FirstLastname", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string AccountForm_Name {
            get {
                return ResourceManager.GetString("AccountForm_Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Personal Information.
        /// </summary>
        public static string AccountForm_PersonalInformation {
            get {
                return ResourceManager.GetString("AccountForm_PersonalInformation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Phone.
        /// </summary>
        public static string AccountForm_Phone {
            get {
                return ResourceManager.GetString("AccountForm_Phone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Second last name.
        /// </summary>
        public static string AccountForm_SecondLastname {
            get {
                return ResourceManager.GetString("AccountForm_SecondLastname", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Terms and conditions.
        /// </summary>
        public static string AccountForm_TermsAndConditions {
            get {
                return ResourceManager.GetString("AccountForm_TermsAndConditions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No registered addresses.
        /// </summary>
        public static string Addresses_Empty {
            get {
                return ResourceManager.GetString("Addresses_Empty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click on an address to see more details.
        /// </summary>
        public static string Addresses_Subtitle {
            get {
                return ResourceManager.GetString("Addresses_Subtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Addresses.
        /// </summary>
        public static string Addresses_Title {
            get {
                return ResourceManager.GetString("Addresses_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        public static string AddressForm_Add_Confirm {
            get {
                return ResourceManager.GetString("AddressForm_Add_Confirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New address.
        /// </summary>
        public static string AddressForm_Add_Text {
            get {
                return ResourceManager.GetString("AddressForm_Add_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Register a new address to search for products in your location.
        /// </summary>
        public static string AddressForm_AddDefault {
            get {
                return ResourceManager.GetString("AddressForm_AddDefault", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Added Successfully.
        /// </summary>
        public static string AddressForm_AddedSuccessfully {
            get {
                return ResourceManager.GetString("AddressForm_AddedSuccessfully", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to and street.
        /// </summary>
        public static string AddressForm_AndStreet {
            get {
                return ResourceManager.GetString("AddressForm_AndStreet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Between street.
        /// </summary>
        public static string AddressForm_BetweenStreet {
            get {
                return ResourceManager.GetString("AddressForm_BetweenStreet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Block.
        /// </summary>
        public static string AddressForm_Block {
            get {
                return ResourceManager.GetString("AddressForm_Block", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to City*.
        /// </summary>
        public static string AddressForm_City {
            get {
                return ResourceManager.GetString("AddressForm_City", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Country*.
        /// </summary>
        public static string AddressForm_Country {
            get {
                return ResourceManager.GetString("AddressForm_Country", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set as default.
        /// </summary>
        public static string AddressForm_DefaultAddress {
            get {
                return ResourceManager.GetString("AddressForm_DefaultAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you want to delete the follow location.
        /// </summary>
        public static string AddressForm_DeleteLocationMessage {
            get {
                return ResourceManager.GetString("AddressForm_DeleteLocationMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete location.
        /// </summary>
        public static string AddressForm_DeleteLocationTitle {
            get {
                return ResourceManager.GetString("AddressForm_DeleteLocationTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update.
        /// </summary>
        public static string AddressForm_Edit_Confirm {
            get {
                return ResourceManager.GetString("AddressForm_Edit_Confirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit Address.
        /// </summary>
        public static string AddressForm_Edit_Text {
            get {
                return ResourceManager.GetString("AddressForm_Edit_Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Location data.
        /// </summary>
        public static string AddressForm_FieldsTitle {
            get {
                return ResourceManager.GetString("AddressForm_FieldsTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Find location.
        /// </summary>
        public static string AddressForm_FindLocation {
            get {
                return ResourceManager.GetString("AddressForm_FindLocation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Location alias*.
        /// </summary>
        public static string AddressForm_Identifier {
            get {
                return ResourceManager.GetString("AddressForm_Identifier", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tip not valid, try with decimal numbers.
        /// </summary>
        public static string AddressForm_InvalidTip {
            get {
                return ResourceManager.GetString("AddressForm_InvalidTip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delivery location.
        /// </summary>
        public static string AddressForm_LocationTitle {
            get {
                return ResourceManager.GetString("AddressForm_LocationTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lot.
        /// </summary>
        public static string AddressForm_Lot {
            get {
                return ResourceManager.GetString("AddressForm_Lot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set the location.
        /// </summary>
        public static string AddressForm_MarkLocation {
            get {
                return ResourceManager.GetString("AddressForm_MarkLocation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you want to set as the default the follow location.
        /// </summary>
        public static string AddressForm_MessageDefaultAddress {
            get {
                return ResourceManager.GetString("AddressForm_MessageDefaultAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This will clear the  shopping cart.
        /// </summary>
        public static string AddressForm_MessageDefaultAddress2 {
            get {
                return ResourceManager.GetString("AddressForm_MessageDefaultAddress2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Municipality*.
        /// </summary>
        public static string AddressForm_Municipality {
            get {
                return ResourceManager.GetString("AddressForm_Municipality", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Neighborhood*.
        /// </summary>
        public static string AddressForm_Neighborhood {
            get {
                return ResourceManager.GetString("AddressForm_Neighborhood", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postal Code*.
        /// </summary>
        public static string AddressForm_PostalCode {
            get {
                return ResourceManager.GetString("AddressForm_PostalCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to References*.
        /// </summary>
        public static string AddressForm_References {
            get {
                return ResourceManager.GetString("AddressForm_References", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to is a required data.
        /// </summary>
        public static string AddressForm_RequiredField {
            get {
                return ResourceManager.GetString("AddressForm_RequiredField", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select a location on the map.
        /// </summary>
        public static string AddressForm_RequiredLongLat {
            get {
                return ResourceManager.GetString("AddressForm_RequiredLongLat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to State*.
        /// </summary>
        public static string AddressForm_State {
            get {
                return ResourceManager.GetString("AddressForm_State", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Street*.
        /// </summary>
        public static string AddressForm_Street {
            get {
                return ResourceManager.GetString("AddressForm_Street", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set new deafult address.
        /// </summary>
        public static string AddressForm_TittleDefaultAddress {
            get {
                return ResourceManager.GetString("AddressForm_TittleDefaultAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Updated Successfully.
        /// </summary>
        public static string AddressForm_UpdatedSuccessfully {
            get {
                return ResourceManager.GetString("AddressForm_UpdatedSuccessfully", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Latitude.
        /// </summary>
        public static string AddressLatitud {
            get {
                return ResourceManager.GetString("AddressLatitud", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Latitude invalid!.
        /// </summary>
        public static string AddressLatitudMessage {
            get {
                return ResourceManager.GetString("AddressLatitudMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Longitude.
        /// </summary>
        public static string AddressLongitud {
            get {
                return ResourceManager.GetString("AddressLongitud", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Longitude invalid!.
        /// </summary>
        public static string AddressLongitudMessage {
            get {
                return ResourceManager.GetString("AddressLongitudMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Next.
        /// </summary>
        public static string Continue_Button {
            get {
                return ResourceManager.GetString("Continue_Button", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Copied To Clipboard.
        /// </summary>
        public static string CopiedToClipboard {
            get {
                return ResourceManager.GetString("CopiedToClipboard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter code.
        /// </summary>
        public static string EnterCode {
            get {
                return ResourceManager.GetString("EnterCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Advanced options.
        /// </summary>
        public static string MoreOptions {
            get {
                return ResourceManager.GetString("MoreOptions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Place.
        /// </summary>
        public static string Option_Address {
            get {
                return ResourceManager.GetString("Option_Address", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Places.
        /// </summary>
        public static string Option_Addresses {
            get {
                return ResourceManager.GetString("Option_Addresses", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Personal information.
        /// </summary>
        public static string Option_EditAccount {
            get {
                return ResourceManager.GetString("Option_EditAccount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Access data.
        /// </summary>
        public static string Option_EditUser {
            get {
                return ResourceManager.GetString("Option_EditUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Share this code with your friends to get Godeliverix rewards, such as discounts or free shipping..
        /// </summary>
        public static string ShareThisCode {
            get {
                return ResourceManager.GetString("ShareThisCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Share your code.
        /// </summary>
        public static string ShareYourCode {
            get {
                return ResourceManager.GetString("ShareYourCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        public static string String1 {
            get {
                return ResourceManager.GetString("String1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My PrAccount data
        ///Account data.
        /// </summary>
        public static string Title {
            get {
                return ResourceManager.GetString("Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string UserForm_Cancel {
            get {
                return ResourceManager.GetString("UserForm_Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm.
        /// </summary>
        public static string UserForm_Confirm {
            get {
                return ResourceManager.GetString("UserForm_Confirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm Password.
        /// </summary>
        public static string UserForm_ConfirmPassword {
            get {
                return ResourceManager.GetString("UserForm_ConfirmPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The current password entered is invalid.
        /// </summary>
        public static string UserForm_CurrentPasswordInvalid {
            get {
                return ResourceManager.GetString("UserForm_CurrentPasswordInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Current password.
        /// </summary>
        public static string UserForm_CurrentPasword {
            get {
                return ResourceManager.GetString("UserForm_CurrentPasword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email.
        /// </summary>
        public static string UserForm_Email {
            get {
                return ResourceManager.GetString("UserForm_Email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Password.
        /// </summary>
        public static string UserForm_NewPassword {
            get {
                return ResourceManager.GetString("UserForm_NewPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Passwords do not match.
        /// </summary>
        public static string UserForm_PasswordNotMatch {
            get {
                return ResourceManager.GetString("UserForm_PasswordNotMatch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The password was updated successfully.
        /// </summary>
        public static string UserForm_PasswordRestoredSuccessfully {
            get {
                return ResourceManager.GetString("UserForm_PasswordRestoredSuccessfully", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Passwords are required.
        /// </summary>
        public static string UserForm_PasswordsAreRequired {
            get {
                return ResourceManager.GetString("UserForm_PasswordsAreRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Restore password.
        /// </summary>
        public static string UserForm_RestorePassword {
            get {
                return ResourceManager.GetString("UserForm_RestorePassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit access data.
        /// </summary>
        public static string UserForm_Title {
            get {
                return ResourceManager.GetString("UserForm_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Username.
        /// </summary>
        public static string UserForm_Username {
            get {
                return ResourceManager.GetString("UserForm_Username", resourceCulture);
            }
        }
    }
}
