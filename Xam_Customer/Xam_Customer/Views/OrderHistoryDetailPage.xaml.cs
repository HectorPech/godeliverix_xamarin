﻿using Xamarin.Forms;
using Plugin.Clipboard;
using ZXing.Net.Mobile.Forms;
using ZXing.QrCode;
using ZXing;
using Prism.Events;
using Xam_Customer.Core.Events;
using Xam_Customer.Core.Model.System;
using Prism.Services.Dialogs;
using Prism.Navigation;
using Prism.Services;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.PurchaseHistory;

namespace Xam_Customer.Views
{
    public partial class OrderHistoryDetailPage : ContentPage
    {
        public OrderHistoryDetailPage(IEventAggregator ea, IPageDialogService dialogService)
        {
            InitializeComponent();

            Device.InvokeOnMainThreadAsync(() =>
            {
                var forgetPassword_tap = new TapGestureRecognizer();
                forgetPassword_tap.Tapped += (s, e) =>
                {
                    //CrossClipboard.Current.SetText(lblCodigo.Text);
                    dialogService.DisplayAlertAsync(PurchaseHistoryLang.CopiedCode_Title, PurchaseHistoryLang.CopiedCode_Message, AppResources.AlertAcceptText);
                };
                //QRCodeImage.BarcodeValue = lblCodigo.Text;
                ea.GetEvent<GetCodeQREvent>().Subscribe(CreateQR);
                //lblCodigoEntrega.GestureRecognizers.Add(forgetPassword_tap);
            });

        }

        public void CreateQR(CODE value)
        {
            Device.InvokeOnMainThreadAsync(() =>
            {
                QRCodeImage.BarcodeValue = value.Code;
            });
        }


    }
}
