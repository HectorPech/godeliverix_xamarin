﻿using Prism.Events;
using Xam_Customer.Core.Events;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Xam_Customer.Views
{
    public partial class ProfileAddressFormPage : ContentPage
    {
        public ProfileAddressFormPage(IEventAggregator ea)
        {
            InitializeComponent();

            ea.GetEvent<ProfilePickAddressEvent>().Subscribe(CreateMapPin);
        }

        private void CreateMapPin(Core.Model.System.MapPin pin)
        {
            double longitude;
            double latitude;

            if (double.TryParse(pin.Latitude, out latitude) || double.TryParse(pin.Longitude, out longitude))
            {
                longitude = double.Parse(pin.Longitude);
                latitude = double.Parse(pin.Latitude);
                Device.InvokeOnMainThreadAsync(() =>
                {
                    Position myPosition = new Position(latitude, longitude);
                    MapSpan mapSpan = new MapSpan(myPosition, 0.002, 0.002);
                    mLocation.MoveToRegion(mapSpan);
                    if(mLocation.Pins.Count > 0)
                    {
                        this.mLocation.Pins.Clear();
                    }
                    mLocation.Pins.Add(new Pin() { Label = pin.Identifier, Type = PinType.Place, Position = myPosition });
                });
            }
        }
    }
}
