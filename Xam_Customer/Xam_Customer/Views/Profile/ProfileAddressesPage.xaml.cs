﻿using Xam_Customer.ViewModels;
using Xamarin.Forms;

namespace Xam_Customer.Views
{
    public partial class ProfileAddressesPage : ContentPage
    {
        public ProfileAddressesPage()
        {
            InitializeComponent();
        }

        private void btnDelete_Clicked(object sender, System.EventArgs e)
        {
            Device.InvokeOnMainThreadAsync(() =>
            {
                var viewmodel = BindingContext as ProfileAddressesPageViewModel;
                viewmodel.eliminar(((ToolbarItem)sender).CommandParameter.ToString());
            });
        }

        private void btnEdit_Clicked(object sender, System.EventArgs e)
        {
            Device.InvokeOnMainThreadAsync(() =>
            {
                var viewmodel = BindingContext as ProfileAddressesPageViewModel;
                viewmodel.AddressEdit(((ToolbarItem)sender).CommandParameter.ToString());
            });
        }

        private void btnDefaultAddress_Clicked(object sender, System.EventArgs e)
        {
            Device.InvokeOnMainThreadAsync(() =>
            {
                var viewmodel = BindingContext as ProfileAddressesPageViewModel;
                viewmodel.ChangeDetaulftAddress(((ToolbarItem)sender).CommandParameter.ToString());
            });
        }
    }
}
