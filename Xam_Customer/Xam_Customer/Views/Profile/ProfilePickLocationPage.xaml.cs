﻿using Prism.Events;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Events;
using Xam_Customer.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Xam_Customer.Views
{
    public partial class ProfilePickLocationPage : ContentPage
    {
        public ProfilePickLocationPage(
            IEventAggregator ea
            )
        {
            InitializeComponent();
            DeviceLocation();
            ea.GetEvent<ProfilePickLocationEvent>().Subscribe(CreateMapPinFromEvent);
        }

        private async void mLocation_MapClicked(object sender, Xamarin.Forms.Maps.MapClickedEventArgs e)
        {
            try
            {
                this.CreateMapPin(e.Position.Latitude, e.Position.Longitude, "Mi ubicacion");

                var geo = new Geocoder();
                var placemarks = await Geocoding.GetPlacemarksAsync(e.Position.Latitude, e.Position.Longitude);

                Placemark placemark = placemarks.FirstOrDefault(p =>
                     !string.IsNullOrEmpty(p.AdminArea)
                     && !string.IsNullOrEmpty(p.CountryName)
                     && !string.IsNullOrEmpty(p.Locality)
                     && !string.IsNullOrEmpty(p.Thoroughfare)
                 );


                if (placemark != null)
                {
                    var viewModel = BindingContext as ProfilePickLocationPageViewModel;
                    viewModel.Country = placemark.CountryName;
                    viewModel.State = placemark.AdminArea;
                    viewModel.City = placemark.Locality;
                    viewModel.Longitude = e.Position.Longitude;
                    viewModel.Latitude = e.Position.Latitude;
                    viewModel.FullAddress = placemark.Thoroughfare;
                    await viewModel.LoadAddress();
                };
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
              await  Task.Run(async () => { await this.DisplayMessageDialog(fnsEx.Message); });
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
               await Task.Run(async () => { await this.DisplayMessageDialog(fneEx.Message); });
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
               await Task.Run(async () => { await this.DisplayMessageDialog(pEx.Message); });
            }
            catch (Exception ex)
            {
                // Unable to get location
                await Task.Run(async () => { await this.DisplayMessageDialog(ex.Message); });
            }
        }

        #region Util
        private async void DeviceLocation()
        {
            try
            {
                var location = await Geolocation.GetLocationAsync();
                if (location != null)
                {
                    this.CreateMapPin(location.Latitude, location.Longitude, "Mi ubicacion");

                    var geo = new Geocoder();
                    var placemarks = await Geocoding.GetPlacemarksAsync(location.Latitude, location.Longitude);

                    Placemark placemark = placemarks.FirstOrDefault(p =>
                        !string.IsNullOrEmpty(p.AdminArea)
                        && !string.IsNullOrEmpty(p.CountryName)
                        && !string.IsNullOrEmpty(p.Locality)
                        && !string.IsNullOrEmpty(p.Thoroughfare)
                    );

                    if (placemark != null)
                    {

                        var viewModel = BindingContext as ProfilePickLocationPageViewModel;
                        viewModel.Country = placemark.CountryName;
                        viewModel.State = placemark.AdminArea;
                        viewModel.City = placemark.Locality;
                        viewModel.Longitude = location.Longitude;
                        viewModel.Latitude = location.Latitude;
                        viewModel.FullAddress = placemark.Thoroughfare;
                        await viewModel.LoadAddress();
                    }
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                await Task.Run(async () => { await this.DisplayMessageDialog(fnsEx.Message); });
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
                await Task.Run(async () => { await this.DisplayMessageDialog(fneEx.Message); });
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                await Task.Run(async () => { await this.DisplayMessageDialog(pEx.Message); });
            }
            catch (Exception ex)
            {
                // Unable to get location
                await Task.Run(async () => { await this.DisplayMessageDialog(ex.Message); });
            }
        }
        private void CreateMapPin(double latitude, double longitude, string tag = "Ubicacion de entrega")
        {
            Position myPosition = new Position(latitude, longitude);
            MapSpan mapSpan = new MapSpan(myPosition, 0.002, 0.002);
            mLocation.MoveToRegion(mapSpan);
            mLocation.Pins.Clear();
            mLocation.Pins.Add(new Pin() { Label = tag, Type = PinType.Place, Position = myPosition });
        }
        private async Task DisplayMessageDialog(string message, string title = "")
        {
            await DisplayAlert(title, message, "Aceptar");
        }

        private void CreateMapPinFromEvent(Core.Model.System.MapPin pin)
        {
            double longitude;
            double latitude;

            if (double.TryParse(pin.Latitude, out latitude) || double.TryParse(pin.Longitude, out longitude))
            {
                longitude = double.Parse(pin.Longitude);
                latitude = double.Parse(pin.Latitude);
                Device.InvokeOnMainThreadAsync(() =>
                {
                    Position myPosition = new Position(latitude, longitude);
                    MapSpan mapSpan = new MapSpan(myPosition, 0.002, 0.002);
                    mLocation.MoveToRegion(mapSpan);
                    mLocation.Pins.Add(new Pin() { Label = pin.Identifier, Type = PinType.Place, Position = myPosition });
                });
            }
        }
        #endregion
    }
}
