﻿using Prism.Navigation;
using Prism.Services;
using System.Collections.Generic;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.ViewModels;
using Xamarin.Forms;

namespace Xam_Customer.Views
{
    public partial class ProfileAddAddressStep2
    {
        public ProfileAddAddressStep2()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
