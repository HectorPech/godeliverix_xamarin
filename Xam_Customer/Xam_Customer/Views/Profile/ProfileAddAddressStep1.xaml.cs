﻿using Acr.UserDialogs;
using Prism.Events;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Events;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Model.System;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Xam_Customer.Views.Profile
{
    public partial class ProfileAddAddressStep1 : ContentPage
    {
        public ProfileAddAddressStep1(IEventAggregator ea)
        {
            InitializeComponent();
            ListaDePredicciones.IsVisible = false;
            ea.GetEvent<UpdateMapPinEvent>().Subscribe(CreateMapPinEdit);
        }

        private void CreateMapPinEdit(MapPin location)
        {
            double longitude;
            double latitude;

            if (double.TryParse(location.Latitude, out latitude) || double.TryParse(location.Longitude, out longitude))
            {
                longitude = double.Parse(location.Longitude);
                latitude = double.Parse(location.Latitude);

                Position myPosition = new Position(latitude, longitude);
                MapSpan mapSpan = new MapSpan(myPosition, 0.002, 0.002);
                mLocation.MoveToRegion(mapSpan);
                mLocation.Pins.Clear();
                mLocation.Pins.Add(new Pin() { Label = location.Identifier, Type = PinType.Place, Position = myPosition });
                UserDialogs.Instance.HideLoading();
            }
        }

        private void Button_Clicked(object sender, System.EventArgs e)
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                var viewmodelAddres1 = BindingContext as ProfileAddAddressStep1ViewModel;
                await viewmodelAddres1.UbicacionActual();
            });
        }
        protected override bool OnBackButtonPressed()
        {
            var bindingContext = BindingContext as ProfileAddAddressStep1ViewModel;
            var result = bindingContext?.OnBackButtonPressed() ?? base.OnBackButtonPressed();
            return result;
        }

        private void mLocation_MapClicked(object sender, MapClickedEventArgs e)
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                var viewmodelAddres1 = BindingContext as ProfileAddAddressStep1ViewModel;
                await viewmodelAddres1.MuestraUbicacion(e.Position.Latitude, e.Position.Longitude);
            });
        }
        private void BtnBuscarDirecciones_SearchButtonPressed(object sender, EventArgs e)
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                var bindingContext = BindingContext as ProfileAddAddressStep1ViewModel;
                if (string.IsNullOrEmpty(bindingContext.SearchPlace))
                {
                    ListaDePredicciones.IsVisible = false;
                }
                else
                {
                    ListaDePredicciones.IsVisible = true;
                    await bindingContext.BuscarDireccionesEnGoogle();
                    ListaDePredicciones.ItemsSource = bindingContext.ListPredictions;
                }
            });
        }

        private void BtnBuscarDirecciones_TextChanged(object sender, TextChangedEventArgs e)
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                var bindingContext = BindingContext as ProfileAddAddressStep1ViewModel;
                if (string.IsNullOrEmpty(bindingContext.SearchPlace))
                {
                    bindingContext.SearchVisible = false;
                }
                else
                {
                    await bindingContext.BuscarDireccionesEnGoogle();
                    ListaDePredicciones.ItemsSource = bindingContext.ListPredictions;
                }
            });
        }
    }
}
