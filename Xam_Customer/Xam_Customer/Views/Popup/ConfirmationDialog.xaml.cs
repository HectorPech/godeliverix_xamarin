﻿using Xamarin.Forms.Xaml;

namespace Xam_Customer.Views.Popup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConfirmationDialog 
    {
        public ConfirmationDialog()
        {
            InitializeComponent();
        }
    }
}