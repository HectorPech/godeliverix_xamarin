﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Xam_Customer.Views
{
    public partial class LocationPage : ContentPage
    {
        #region Properties
        private string City { get; set; }
        private string State { get; set; }
        private string Country { get; set; }
        private string Neighborhood { get; set; }
        public List<Colonies> Colonies { get; set; }
        #endregion

        #region Services
        public IAppInstaceService AppInstaceService { get; set; }
        private readonly IHttpRequestService HttpService;
        #endregion

        public LocationPage(
            IAppInstaceService appInstaceService,
            IHttpRequestService httpRequestService
            )
        {
            AppInstaceService = appInstaceService;
            this.HttpService = httpRequestService;
            InitializeComponent();
            LoadSavedLocation();
        }

        private void MyLocation_Clicked(object sender, System.EventArgs e)
        {
            NuevaCarga();
        }

        private async void MLocation_MapClicked(object sender, Xamarin.Forms.Maps.MapClickedEventArgs e)
        {
            try
            {
                this.CreateMapPin(e.Position.Latitude, e.Position.Longitude, "Mi ubicacion");

                var geo = new Geocoder();
                var placemarks = await Geocoding.GetPlacemarksAsync(e.Position.Latitude, e.Position.Longitude);

                Placemark placemark = placemarks.FirstOrDefault(p =>
                    !string.IsNullOrEmpty(p.AdminArea)
                    && !string.IsNullOrEmpty(p.CountryName)
                    && !string.IsNullOrEmpty(p.Locality)
                    && !string.IsNullOrEmpty(p.Thoroughfare)
                );

                if (placemark != null)
                {
                    this.Country = placemark.CountryName;
                    this.State = placemark.AdminArea;
                    this.City = placemark.Locality;
                    this.Neighborhood = placemark.SubLocality;

                    var viewModel = BindingContext as LocationPageViewModel;
                    viewModel.Country = placemark.CountryName;
                    viewModel.State = placemark.AdminArea;
                    viewModel.City = placemark.Locality;
                    viewModel.Longitude = e.Position.Longitude;
                    viewModel.Latitude = e.Position.Latitude;
                    viewModel.FullAddress = placemark.Thoroughfare;

                    this.GetColonies();
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                this.DisplayMessageDialog(fnsEx.Message);
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
                this.DisplayMessageDialog(fneEx.Message);
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                this.DisplayMessageDialog(pEx.Message);
            }
            catch (Exception ex)
            {
                // Unable to get location
                this.DisplayMessageDialog(ex.Message);
            }
        }

        private void pColonies_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (pColonies.SelectedIndex > 0)
            {
                Colonies selectedColony = this.Colonies[pColonies.SelectedIndex];
                var viewModel = BindingContext as LocationPageViewModel;
                viewModel.SelectedColonie = selectedColony;
            }
        }

        #region Requests
        private async void GetColonies()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("StrNombreCiudad", City);
            parameters.Add("CodigoEstado", State);
            parameters.Add("CodigoPais", Country);

            var ColoniesRequest = await this.HttpService.GetAsync<IEnumerable<AddressSession>>("Direccion/GetObtenerDireccionConDatosDeGoogle_Movil", parameters);
            if (ColoniesRequest.Code == HttpResponseCode.Success)
            {
                var ciudades = ColoniesRequest.Result.ToList();

                // No hay ciudades
                if (ciudades.Count == 0)
                {
                    this.DisplayMessageDialog(AppResources.Location_NocitiesAvaibles);
                    return;
                }

                AddressSession ciudad = ciudades[0];
                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add("UidCiudad", ciudad.Uid);
                var ColonieRequest = await this.HttpService.GetAsync<IEnumerable<Colonies>>("Direccion/GetObtenerColonias_Movil", param);

                if (ColonieRequest.Code == HttpResponseCode.Success)
                {
                    var Ubicacion = BindingContext as LocationPageViewModel;
                    Ubicacion.UidState = ciudad.UidState;

                    this.Colonies = ColonieRequest.Result.ToList();
                    // No hay ciudades
                    if (Colonies.Count == 0)
                    {
                        this.DisplayMessageDialog(AppResources.Location_NoSubursAvaibles);
                        return;
                    }
                    pColonies.ItemsSource = this.Colonies;
                    var location = AppInstaceService.GetDeliverySelected();
                    if (location == null)
                    {
                        Colonies colony = this.Colonies.SingleOrDefault(c => c.Name.ToLower().Equals(this.Neighborhood.ToLower()));
                        if (colony != null)
                        {
                            int index = this.Colonies.IndexOf(colony);
                            pColonies.SelectedIndex = index;
                        }
                    }
                    else
                    {
                        Colonies colony = this.Colonies.SingleOrDefault(c => c.Name.ToLower().Equals(location.ToLower()));
                        if (colony != null)
                        {
                            int index = this.Colonies.IndexOf(colony);
                            pColonies.SelectedIndex = index;
                        }
                    }

                }
                else
                {
                    this.DisplayMessageDialog(AppResources.Error_General_Message);
                }

            }
        }
        #endregion

        #region Util

        private async void NuevaCarga()
        {
            try
            {
                var location = await Geolocation.GetLocationAsync();
                if (location != null)
                {
                    var geo = new Geocoder();
                    var placemarks = await Geocoding.GetPlacemarksAsync(location.Latitude, location.Longitude);
                    this.CreateMapPin(location.Latitude, location.Longitude, "Mi ubicacion");

                    Placemark placemark = placemarks.FirstOrDefault(p =>
                        !string.IsNullOrEmpty(p.AdminArea)
                        && !string.IsNullOrEmpty(p.CountryName)
                        && !string.IsNullOrEmpty(p.Locality)
                        && !string.IsNullOrEmpty(p.Thoroughfare)
                    );

                    if (placemark != null)
                    {

                        this.Country = placemark.CountryName;
                        this.State = placemark.AdminArea;
                        this.City = placemark.Locality;
                        this.Neighborhood = placemark.SubLocality;

                        var viewModel = BindingContext as LocationPageViewModel;
                        viewModel.Country = placemark.CountryName;
                        viewModel.State = placemark.AdminArea;
                        viewModel.City = placemark.Locality;
                        viewModel.Longitude = location.Longitude;
                        viewModel.Latitude = location.Latitude;
                        viewModel.FullAddress = placemark.Thoroughfare;

                        this.GetColonies();
                    }
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                this.DisplayMessageDialog(fnsEx.Message);
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
                this.DisplayMessageDialog(fneEx.Message);
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                this.DisplayMessageDialog(pEx.Message);
            }
            catch (Exception ex)
            {
                // Unable to get location
                this.DisplayMessageDialog(ex.Message);
            }
        }
        private void CreateMapPin(double latitude, double longitude, string tag = "Ubicacion de entrega")
        {
            Position myPosition = new Position(latitude, longitude);
            MapSpan mapSpan = new MapSpan(myPosition, 0.002, 0.002);
            mLocation.MoveToRegion(mapSpan);
            mLocation.Pins.Clear();
            mLocation.Pins.Add(new Pin() { Label = tag, Type = PinType.Place, Position = myPosition });
        }

        private async void DisplayMessageDialog(string message, string title = "")
        {
            await DisplayAlert(title, message, "Aceptar");
        }

        private async void LoadSavedLocation()
        {
            var location = AppInstaceService.GetDeliveryColonie();
            if (location == null)
            {
                NuevaCarga();
            }
            else
            {
                // Marcar en el mapa
                this.CreateMapPin(AppInstaceService.GetDeliveryLat(), AppInstaceService.GetDeliveryLong(), AppInstaceService.GetDeliverySelected());

                // Obtener datos de la ubicacion
                var geo = new Geocoder();
                var placemarks = await Geocoding.GetPlacemarksAsync(AppInstaceService.GetDeliveryLat(), AppInstaceService.GetDeliveryLong());

                // Leer datos de la ubicacion
                Placemark placemark = placemarks.FirstOrDefault(p =>
                    !string.IsNullOrEmpty(p.AdminArea)
                    && !string.IsNullOrEmpty(p.CountryName)
                    && !string.IsNullOrEmpty(p.Locality)
                    && !string.IsNullOrEmpty(p.Thoroughfare)
                );

                if (placemark != null)
                {
                    this.Country = placemark.CountryName;
                    this.State = placemark.AdminArea;
                    this.City = placemark.Locality;
                    this.Neighborhood = AppInstaceService.GetDeliveryColonie();

                    var viewModel = BindingContext as LocationPageViewModel;
                    viewModel.Country = placemark.CountryName;
                    viewModel.State = placemark.AdminArea;
                    viewModel.City = placemark.Locality;
                    viewModel.Longitude = AppInstaceService.GetDeliveryLong();
                    viewModel.Latitude = AppInstaceService.GetDeliveryLat();
                    viewModel.FullAddress = placemark.Thoroughfare;
                    //viewModel.GetSaveColonie();
                    this.GetColonies();
                }
            }
        }
        #endregion
    }
}
