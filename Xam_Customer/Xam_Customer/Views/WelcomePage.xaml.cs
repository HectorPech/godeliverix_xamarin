﻿using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Xam_Customer.Views
{
    public partial class WelcomePage : ContentPage
    {
        public WelcomePage()
        {
            InitializeComponent();
            //Solicita permisos de ubicacion.(No quitar, es importante)
            Device.InvokeOnMainThreadAsync(async () => { var location = await Geolocation.GetLocationAsync(); });
        }
    }
}
