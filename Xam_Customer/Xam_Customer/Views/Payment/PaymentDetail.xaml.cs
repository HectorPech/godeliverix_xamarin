﻿using Prism.Events;
using System.Threading.Tasks;
using Xam_Customer.Core.Events;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Xam_Customer.Views.Payment
{
    public partial class PaymentDetail : ContentPage
    {
        public PaymentDetail(
            IEventAggregator ea
            )
        {
            InitializeComponent();

            ea.GetEvent<UpdateMapPinEvent>().Subscribe(CreateMapPin);
        }

        private void CreateMapPin(Core.Model.System.MapPin pin)
        {
            double longitude;
            double latitude;

            if (double.TryParse(pin.Latitude, out latitude) || double.TryParse(pin.Longitude, out longitude))
            {
                longitude = double.Parse(pin.Longitude);
                latitude = double.Parse(pin.Latitude);
                Device.InvokeOnMainThreadAsync(() =>
                {
                    Position myPosition = new Position(latitude, longitude);
                    MapSpan mapSpan = new MapSpan(myPosition, 0.002, 0.002);
                    mLocation.MoveToRegion(mapSpan);
                    mLocation.Pins.Clear();
                    mLocation.Pins.Add(new Pin() { Label = AppResources.DefaultDelivery, Type = PinType.Place, Position = myPosition });
                });
            }
        }

        private void CreateMapPin(double latitude, double longitude, string tag = "Ubicacion de entrega")
        {
            Position myPosition = new Position(latitude, longitude);
            MapSpan mapSpan = new MapSpan(myPosition, 0.002, 0.002);
            mLocation.MoveToRegion(mapSpan);
            mLocation.Pins.Clear();
            mLocation.Pins.Add(new Pin() { Label = tag, Type = PinType.Place, Position = myPosition });
            mLocation.HasZoomEnabled = false;
        }
    }
}
