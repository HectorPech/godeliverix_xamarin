﻿using Acr.UserDialogs;
using Prism.Services;
using System.Collections.Generic;
using System.Timers;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.ViewModels;
using Xamarin.Forms;

namespace Xam_Customer.Views.Payment
{
    public partial class CardPaymentPage : ContentPage
    {
        Timer tiempo = new Timer();
        #region Services
        public IHttpRequestService _httpRequestService { get; set; }

        #endregion
        public CardPaymentPage(IHttpRequestService httpRequestService)
        {
            InitializeComponent();
            _httpRequestService = httpRequestService;
            //var viewmodel = BindingContext as CardPaymentPageViewModel;
            //Device.InvokeOnMainThreadAsync(async () => { await viewmodel.ObtenerLigaDePago(); });
            //WVWebPay.Source = viewmodel.URLPAY;

            tiempo.Interval = 2000;
            tiempo.Elapsed += new ElapsedEventHandler(VerificaPago);
            tiempo.Start();
            Device.InvokeOnMainThreadAsync(() => { UserDialogs.Instance.ShowLoading(AppResources.Text_Loading); }); WVWebPay.IsVisible = false;
        }

        private void LoadWebPay() { }
        private void WVWebPay_Navigating(object sender, WebNavigatingEventArgs e)
        {
        }

        private void WVWebPay_Navigated(object sender, WebNavigatedEventArgs e)
        {
            WVWebPay.IsVisible = true;
            Device.InvokeOnMainThreadAsync(() => { UserDialogs.Instance.HideLoading(); });
        }
        protected void VerificaPago(object sender, ElapsedEventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (this.BindingContext == null)
                {
                    tiempo.Stop();
                    return;
                }

                var viewmodel = BindingContext as CardPaymentPageViewModel;
                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add("UidOrdenFormaDeCobro", viewmodel.UidOrden);
                var OrderPaidRequest = await _httpRequestService.GetAsync<bool>("Pagos/GetValidarPagoOrdenTarjeta_Movil", param);


                if (OrderPaidRequest.Result)
                {
                    param = new Dictionary<string, string>();
                    param.Add("UidOrdenFormaDeCobro", viewmodel.UidOrden);
                    var PaidStatusRequest = await _httpRequestService.GetAsync<string>("Pagos/GetObtenerPagoTarjeta_movil", param);

                    if (PaidStatusRequest.Result == "denied")
                    {
                        // UidOrdenPago = Guid.NewGuid();
                        //GenerateMessage("Alerta del sistema", "El pago ha sido denegado, intente de nuevo o consulte con su banco", "Aceptar");
                        //await Navigation.PopAsync();
                    }
                    if (PaidStatusRequest.Result == "approved")
                    {
                        viewmodel.SendOrder();
                        tiempo.Stop();
                    }
                    if (PaidStatusRequest.Result == "error")
                    {
                        //UidOrdenPago = Guid.NewGuid();
                        //await DisplayAlert("Alerta del sistema", "Ha ocurrio un error al efectuar el pago,intente de nuevo o consulte con su banco", "Aceptar");
                        //await Navigation.PopAsync();
                        //await Navigation.PopToRootAsync();
                    }
                }
                //}
            });

        }

    }
}
