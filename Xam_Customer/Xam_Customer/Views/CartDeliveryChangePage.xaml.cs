﻿using Prism.Services;
using System.Threading.Tasks;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.Profile;
using Xam_Customer.ViewModels;
using Xamarin.Forms;

namespace Xam_Customer.Views
{
    public partial class CartDeliveryChangePage : ContentPage
    {
        #region Internal
        public IPageDialogService _pageDialogService { get; set; }
        #endregion
        public CartDeliveryChangePage(IPageDialogService pageDialogService)
        {
            InitializeComponent();
            _pageDialogService = pageDialogService;
        }

        private void Entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            var txt = sender as Entry;
            var tipstring = "";
            tipstring = txt.Text.Replace("$", " ");
            decimal valor = 0.0m;
            if (!decimal.TryParse(tipstring, out valor))
            {
                Device.InvokeOnMainThreadAsync(async () => { await _pageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_InvalidTip}", AppResources.AlertAcceptText); });
                return;
            }
            decimal deliveryTip = decimal.Parse(tipstring.Trim());
            var modelview = BindingContext as CartDeliveryChangePageViewModel;
            modelview.changeDeliveryAction(deliveryTip);
        }

        private void Entry_TextChanged(object sender, FocusEventArgs e)
        {

        }
    }
}
