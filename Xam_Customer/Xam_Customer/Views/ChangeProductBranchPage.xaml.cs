﻿using Prism.Events;
using System.Linq;
using Xam_Customer.Core.Events;
using Xam_Customer.Core.Services.Abstract;
using Xamarin.Forms;

namespace Xam_Customer.Views
{
    public partial class ChangeProductBranchPage : ContentPage
    {
        public ChangeProductBranchPage(IEventAggregator eventAggregator)
        {
            InitializeComponent();

            eventAggregator.GetEvent<CartCountEvent>().Subscribe((int value) =>
            {
                DependencyService.Get<IToolbarItemBadgeService>().SetBadge(this, ToolbarItems.First(), $"{value}", Color.Red, Color.White);
            });
        }
    }
}
