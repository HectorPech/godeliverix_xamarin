﻿using Acr.UserDialogs;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.ViewModels;
using Xamarin.Forms;

namespace Xam_Customer.Views.About
{
    public partial class TermsAndConditionsPage : ContentPage
    {
        public TermsAndConditionsPage()
        {
            InitializeComponent();
        }
        private void WVWebPay_Navigating(object sender, WebNavigatingEventArgs e)
        {
            Device.InvokeOnMainThreadAsync(() =>
            {
                UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);
            });
        }

        private void WVWebPay_Navigated(object sender, WebNavigatedEventArgs e)
        {
            Device.InvokeOnMainThreadAsync(() =>
            {
                UserDialogs.Instance.HideLoading();
            });
        }
    }
}
