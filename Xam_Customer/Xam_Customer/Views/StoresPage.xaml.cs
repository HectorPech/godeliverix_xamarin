﻿using Prism.Events;
using Rg.Plugins.Popup.Services;
using System.Linq;
using Xam_Customer.Core.Events;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.ViewModels;
using Xam_Customer.Views.RgPopup;
using Xamarin.Forms;

namespace Xam_Customer.Views
{
    public partial class StoresPage : ContentPage
    {
        public StoresPage(IEventAggregator eventAggregator)
        {
            InitializeComponent();

            eventAggregator.GetEvent<CartCountEvent>().Subscribe((int value) =>
            {
                DependencyService.Get<IToolbarItemBadgeService>().SetBadge(this, ToolbarItems.First(), $"{value}", Color.Red, Color.White);
            });
        }

    }
}
