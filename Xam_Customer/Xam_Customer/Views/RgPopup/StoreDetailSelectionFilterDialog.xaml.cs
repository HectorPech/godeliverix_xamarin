﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xam_Customer.Core.Events;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.Core;
using Xam_Customer.Resources.LangResx;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Xam_Customer.Views.RgPopup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StoreDetailSelectionFilterDialog : Rg.Plugins.Popup.Pages.PopupPage
    {
        private IEventAggregator EventAggregator { get; }
        private IEnumerable<ListboxItemModel> Options { get; set; }
        private StoreFilterOptionType Type { get; set; }

        public StoreDetailSelectionFilterDialog(
            IEventAggregator ea,
            IEnumerable<ListboxItemModel> options,
            StoreFilterOptionType type
            )
        {
            InitializeComponent();
            this.EventAggregator = ea;

            this.Options = options;
            this.Type = type;
            this.lvOptions.ItemsSource = this.Options;

            if (type == StoreFilterOptionType.OrderBy)
            {
                lblTitle.Text = AppResources.OrderBy;
            }
            else
            {
                lblTitle.Text = AppResources.SelectBy;
            }
        }

        protected override void OnDisappearing()
        {
            this.EventAggregator.GetEvent<StoreOptionChangedEvent>().Publish(null);
            base.OnDisappearing();
        }

        private void btnApplySelection_Clicked(object sender, EventArgs e)
        {
            StoreOptionChanged selected = null;

            foreach (var option in Options)
            {
                if (option.Selected)
                {
                    selected = new StoreOptionChanged() { Uid = option.Uid, Name = option.Name, Type = this.Type };
                    break;
                }
            }

            this.EventAggregator.GetEvent<StoreOptionChangedEvent>().Publish(selected);
        }

        private void lvOptions_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) { return; }

            foreach (var option in Options)
            {
                option.Selected = (option.Uid == ((ListboxItemModel)e.SelectedItem).Uid);
            }
        }
    }
}