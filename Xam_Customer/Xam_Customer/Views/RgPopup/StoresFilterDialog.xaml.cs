﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xam_Customer.Core.Events;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Model.System;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Util;
using Xam_Customer.Resources.LangResx;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Xam_Customer.Views.RgPopup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StoresFilterDialog : Rg.Plugins.Popup.Pages.PopupPage
    {
        public StoresFilterDialog(
            IEventAggregator eventAggregator,
            IHttpRequestService httpRequestService,
            IAppInstaceService appInstaceService
            )
        {
            InitializeComponent();
            this.BindingContext = new StoresFilterDialogViewModel(eventAggregator, httpRequestService, appInstaceService);
        }

    }


    public class StoresFilterDialogViewModel : BindableBase
    {
        #region Properties
        private ObservableCollection<CheckboxItemSelection> _types;
        public ObservableCollection<CheckboxItemSelection> Types
        {
            get { return _types; }
            set { SetProperty(ref _types, value); }
        }

        private ObservableCollection<CheckboxItemSelection> _categories;
        public ObservableCollection<CheckboxItemSelection> Categories
        {
            get { return _categories; }
            set { SetProperty(ref _categories, value); }
        }

        private ObservableCollection<CheckboxItemSelection> _subcategories;
        public ObservableCollection<CheckboxItemSelection> Subcategories
        {
            get { return _subcategories; }
            set { SetProperty(ref _subcategories, value); }
        }

        private CheckboxItemSelection _selectedType;
        public CheckboxItemSelection SelectedType
        {
            get { return _selectedType; }
            set { SetProperty(ref _selectedType, value); }
        }

        private CheckboxItemSelection _selectedCategory;
        public CheckboxItemSelection SelectedCategory
        {
            get { return _selectedCategory; }
            set { SetProperty(ref _selectedCategory, value); }
        }

        private CheckboxItemSelection _selectedSubcategory;
        public CheckboxItemSelection SelectedSubcategory
        {
            get { return _selectedSubcategory; }
            set { SetProperty(ref _selectedSubcategory, value); }
        }

        private bool isTypesExpanded;
        public bool IsTypesExpanded
        {
            get { return isTypesExpanded; }
            set
            {
                SetProperty(ref isTypesExpanded, value);
                if (value)
                {
                    this.IsCategoryExpanded = false;
                    this.IsSubcategoriesExpanded = false;
                }
            }
        }

        private bool isCategoryExpanded;
        public bool IsCategoryExpanded
        {
            get { return isCategoryExpanded; }
            set
            {
                SetProperty(ref isCategoryExpanded, value);
                if (value)
                {
                    this.IsTypesExpanded = false;
                    this.IsSubcategoriesExpanded = false;
                }
            }
        }

        private bool isSubcategoriesExpanded;
        public bool IsSubcategoriesExpanded
        {
            get { return isSubcategoriesExpanded; }
            set
            {
                SetProperty(ref isSubcategoriesExpanded, value);
                if (value)
                {
                    this.IsTypesExpanded = false;
                    this.IsCategoryExpanded = false;
                }
            }
        }

        private bool _isLoadingCategories;
        public bool IsLoadingCategories
        {
            get { return _isLoadingCategories; }
            set { SetProperty(ref _isLoadingCategories, value); }
        }

        private bool _isLoadingSubcategories;
        public bool IsLoadingSubcategories
        {
            get { return _isLoadingSubcategories; }
            set { SetProperty(ref _isLoadingSubcategories, value); }
        }

        #endregion

        #region Private
        public bool IsCollapsing { get; set; }
        #endregion

        #region Services
        private IEventAggregator EventAggregator { get; }
        private IHttpRequestService HttpService { get; }
        private IAppInstaceService Instance { get; }
        #endregion

        #region Commands
        public DelegateCommand<string> TypeSelectedCommand { get; }
        public DelegateCommand<string> CategorySelectedCommand { get; }
        public DelegateCommand<string> SubcategorySelectedCommand { get; }

        public DelegateCommand ApplyFilterCommand { get; set; }
        #endregion

        public StoresFilterDialogViewModel(
            IEventAggregator eventAggregator,
            IHttpRequestService httpRequestService,
            IAppInstaceService appInstaceService
            )
        {
            this.EventAggregator = eventAggregator;
            this.HttpService = httpRequestService;
            this.Instance = appInstaceService;

            this.TypeSelectedCommand = new DelegateCommand<string>(this.TypeSelected, this.CanSelectType);
            this.CategorySelectedCommand = new DelegateCommand<string>(this.CategorySelected, this.CanSelectCategory);
            this.SubcategorySelectedCommand = new DelegateCommand<string>(this.SubcategorySelected);
            this.ApplyFilterCommand = new DelegateCommand(this.ApplyFilter);

            this.Types = new ObservableCollection<CheckboxItemSelection>();
            this.Categories = new ObservableCollection<CheckboxItemSelection>();
            this.Subcategories = new ObservableCollection<CheckboxItemSelection>();

            this.SelectedType = new CheckboxItemSelection() { Name = AppResources.All, Uid = Guid.Empty };
            this.SelectedCategory = new CheckboxItemSelection() { Name = AppResources.All, Uid = Guid.Empty };
            this.SelectedSubcategory = new CheckboxItemSelection() { Name = AppResources.All, Uid = Guid.Empty };

            this.LoadTypes();
        }

        #region Implementation
        /// <summary>
        /// Evento para capturar cuando un Type (Giro) fue seleccionado
        /// </summary>
        /// <param name="uid"></param>
        private void TypeSelected(string strUid)
        {
            Guid uid = Guid.Parse(strUid);
            this.SelectedType.Uid = uid;
            foreach (var giro in this.Types)
            {
                giro.Selected = giro.Uid == this.SelectedType.Uid;

                if (giro.Selected)
                {
                    this.SelectedType.Name = giro.Name;
                }
            }

            this.SelectedCategory = new CheckboxItemSelection() { Name = AppResources.All, Uid = Guid.Empty };
            this.SelectedSubcategory = new CheckboxItemSelection() { Name = AppResources.All, Uid = Guid.Empty };

            Task.Run(async () => await this.ReadAllCategories());
        }
        private bool CanSelectType(string value)
        {
            return !this.IsLoadingCategories;
        }

        /// <summary>
        /// Evento para capturar cuando una categoria fue seleccionada
        /// </summary>
        /// <param name="uid"></param>
        private void CategorySelected(string strUid)
        {
            Guid uid = Guid.Parse(strUid);
            this.SelectedCategory.Uid = uid;

            foreach (var category in this.Categories)
            {
                category.Selected = category.Uid == this.SelectedCategory.Uid;

                if (category.Selected)
                {
                    this.SelectedCategory.Name = category.Name;
                }
            }

            this.SelectedSubcategory = new CheckboxItemSelection() { Name = AppResources.All, Uid = Guid.Empty };

            Task.Run(async () => await this.ReadAllSubcategories());
        }
        private bool CanSelectCategory(string value)
        {
            return !this.IsLoadingSubcategories;
        }

        /// <summary>
        /// Evento para detectar cuando una subcategoria fue seleccionada
        /// </summary>
        /// <param name="uid"></param>
        private void SubcategorySelected(string strUid)
        {
            Guid uid = Guid.Parse(strUid);
            this.SelectedSubcategory.Uid = uid;

            foreach (var subcategory in this.Subcategories)
            {
                subcategory.Selected = subcategory.Uid == this.SelectedSubcategory.Uid;

                if (subcategory.Selected)
                {
                    this.SelectedSubcategory.Name = subcategory.Name;
                }
            }
        }

        /// <summary>
        /// Aplicar filtros seleccionados en la busqueda actual
        /// </summary>
        private void ApplyFilter()
        {
            StoresFilter filter = new StoresFilter()
            {
                UidType = this.SelectedType.Uid,
                UidCategory = this.SelectedCategory.Uid,
                UidSubcategory = this.SelectedSubcategory.Uid
            };

            this.EventAggregator.GetEvent<StoresFilterChangedEvent>().Publish(filter);
        }

        /// <summary>
        /// Cargar todos los types (giros) del app instance
        /// </summary>
        private void LoadTypes()
        {
            var giros = this.Instance.GetGiroList();

            this.Types.Add(new CheckboxItemSelection()
            {
                Uid = Guid.Empty,
                Selected = Guid.Empty == this.SelectedType.Uid,
                Name = AppResources.All
            });

            if (Guid.Empty == this.SelectedType.Uid)
            {
                this.SelectedType.Name = AppResources.All;
            }

            foreach (var giro in giros)
            {
                Guid uid = Guid.Parse(giro.Uid);
                this.Types.Add(new CheckboxItemSelection()
                {
                    Uid = uid,
                    Selected = uid == this.SelectedType.Uid,
                    Name = giro.Name
                });

                if (uid == this.SelectedType.Uid)
                {
                    this.SelectedType.Name = giro.Name;
                }
            }
        }

        private async Task ReadAllCategories()
        {
            this.IsLoadingCategories = true;
            this.Categories = new ObservableCollection<CheckboxItemSelection>();

            if (this.SelectedType.Uid != Guid.Empty)
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("UidGiro", SelectedType.Uid.ToString());
                parameters.Add("TipoDeRespuesta", "seleccionar");
                var CategoriaRequest = await this.HttpService.GetAsync<IEnumerable<Categoria>>("CatalogosDeFiltros/GetCategoriaMovil", parameters);
                if (CategoriaRequest.Code == Xam_Customer.Core.Enum.HttpResponseCode.Success)
                {
                    this.Categories.Add(new CheckboxItemSelection()
                    {
                        Uid = Guid.Empty,
                        Selected = Guid.Empty == this.SelectedType.Uid,
                        Name = AppResources.All
                    });

                    foreach (var category in CategoriaRequest.Result)
                    {
                        Guid uid = Guid.Parse(category.Uid);
                        if (uid != Guid.Empty)
                        {
                            this.Categories.Add(new CheckboxItemSelection()
                            {
                                Uid = uid,
                                Selected = uid == this.SelectedCategory.Uid,
                                Name = category.Name
                            });

                            if (uid == this.SelectedCategory.Uid)
                            {
                                this.SelectedCategory.Name = category.Name;
                            }
                        }
                    }
                }
                else
                {
                    this.Categories.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Name = AppResources.All, Selected = true });
                }

                this.IsLoadingCategories = false;
            }
            else
            {
                this.Categories.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Name = AppResources.All, Selected = true });
                this.IsLoadingCategories = false;
            }
        }

        private async Task ReadAllSubcategories()
        {
            this.IsLoadingSubcategories = true;
            this.Subcategories = new ObservableCollection<CheckboxItemSelection>();

            if (this.SelectedCategory.Uid != Guid.Empty)
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("uidCategoria", SelectedCategory.Uid.ToString());
                parameters.Add("TipoDeRespuesta", "seleccionar");

                var SubcategoriaRequest = await this.HttpService.GetAsync<IEnumerable<Subcategoria>>("CatalogosDeFiltros/GetSubCategoriaMovil", parameters);
                if (SubcategoriaRequest.Code == Xam_Customer.Core.Enum.HttpResponseCode.Success)
                {
                    this.Subcategories.Add(new CheckboxItemSelection()
                    {
                        Uid = Guid.Empty,
                        Name = AppResources.All,
                        Selected = this.SelectedSubcategory.Uid == Guid.Empty
                    });

                    foreach (var subcategoria in SubcategoriaRequest.Result)
                    {
                        Guid uid = Guid.Parse(subcategoria.Uid);
                        if (uid != Guid.Empty)
                        {
                            this.Subcategories.Add(new CheckboxItemSelection()
                            {
                                Uid = uid,
                                Selected = uid == this.SelectedSubcategory.Uid,
                                Name = subcategoria.Name
                            });

                            if (uid == this.SelectedSubcategory.Uid)
                            {
                                this.SelectedSubcategory.Name = subcategoria.Name;
                            }
                        }
                    }
                }
                else
                {
                    this.Subcategories.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Name = AppResources.All, Selected = true });
                }
                this.IsLoadingSubcategories = false;
            }
            else
            {
                this.Subcategories.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Name = AppResources.All, Selected = true });
                this.IsLoadingSubcategories = false;
            }
        }
        #endregion
    }
}