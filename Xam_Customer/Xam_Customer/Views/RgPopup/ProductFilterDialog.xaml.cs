﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Events;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Model.System;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Services.Implementation;
using Xam_Customer.Core.Util;
using Xam_Customer.Resources.LangResx;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Xam_Customer.Views.RgPopup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductFilterDialog : Rg.Plugins.Popup.Pages.PopupPage
    {
        public ProductFilterDialog(
            IAppInstaceService appInstaceService,
            IEventAggregator eventAggregator,
            DialogSource source
            )
        {
            InitializeComponent();
            this.BindingContext = new ProductFilterDialogViewModel(appInstaceService, eventAggregator, source);
        }

        private async void GoBack_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }
    }

    public class ProductFilterDialogViewModel : BindableBase
    {
        #region Properties
        private ObservableCollection<CheckboxItemSelection> orderByOptions;
        public ObservableCollection<CheckboxItemSelection> OrderByOptions
        {
            get { return orderByOptions; }
            set { SetProperty(ref orderByOptions, value); }
        }

        private IList<CheckboxItemSelection> types;
        public IList<CheckboxItemSelection> Types
        {
            get { return types; }
            set { SetProperty(ref types, value); }
        }

        private IList<CheckboxItemSelection> categories;
        public IList<CheckboxItemSelection> Categories
        {
            get { return categories; }
            set { SetProperty(ref categories, value); }
        }

        private IList<CheckboxItemSelection> subcategories;
        public IList<CheckboxItemSelection> Subcategories
        {
            get { return subcategories; }
            set { SetProperty(ref subcategories, value); }
        }

        private CheckboxItemSelection selectedType;
        public CheckboxItemSelection SelectedType
        {
            get { return selectedType; }
            set { SetProperty(ref selectedType, value); }
        }

        private CheckboxItemSelection selectedCategory;
        public CheckboxItemSelection SelectedCategory
        {
            get { return selectedCategory; }
            set { SetProperty(ref selectedCategory, value); }
        }

        private CheckboxItemSelection selectedSubcategory;
        public CheckboxItemSelection SelectedSubcategory
        {
            get { return selectedSubcategory; }
            set { SetProperty(ref selectedSubcategory, value); }
        }

        private bool isLoadingCategories;
        public bool IsLoadingCategories
        {
            get { return isLoadingCategories; }
            set { SetProperty(ref isLoadingCategories, value); }
        }

        private bool isLoadingSubcategories;
        public bool IsLoadingSubcategories
        {
            get { return isLoadingSubcategories; }
            set { SetProperty(ref isLoadingSubcategories, value); }
        }

        private bool isTypesExpanded;
        public bool IsTypesExpanded
        {
            get { return isTypesExpanded; }
            set
            {
                SetProperty(ref isTypesExpanded, value);
                if (value)
                {
                    this.IsCategoryExpanded = false;
                    this.IsSubcategoriesExpanded = false;
                }
            }
        }

        private bool isCategoryExpanded;
        public bool IsCategoryExpanded
        {
            get { return isCategoryExpanded; }
            set
            {
                SetProperty(ref isCategoryExpanded, value);
                if (value)
                {
                    this.IsTypesExpanded = false;
                    this.IsSubcategoriesExpanded = false;
                }
            }
        }

        private bool isSubcategoriesExpanded;
        public bool IsSubcategoriesExpanded
        {
            get { return isSubcategoriesExpanded; }
            set
            {
                SetProperty(ref isSubcategoriesExpanded, value);
                if (value)
                {
                    this.IsTypesExpanded = false;
                    this.IsCategoryExpanded = false;
                }
            }
        }

        private bool onlyAvailable;
        public bool OnlyAvailable
        {
            get { return onlyAvailable; }
            set { SetProperty(ref onlyAvailable, value); }
        }

        private bool showOnlyAvailable;
        public bool ShowOnlyAvailable
        {
            get { return showOnlyAvailable; }
            set { SetProperty(ref showOnlyAvailable, value); }
        }

        #endregion

        #region Private
        private ApplicationFilter SelectedOrderByFilter { get; set; }

        private DialogSource Source { get; }

        private bool IsFirstLoading { get; set; }
        #endregion

        #region Services
        private IAppInstaceService Instace { get; }
        private IHttpRequestService HttpService { get; }
        private IEventAggregator EventAggregator { get; }
        #endregion

        #region Command
        public DelegateCommand<CheckboxItemSelection> SelectOrderByCommand { get; }
        public DelegateCommand<CheckboxItemSelection> SelectTypeCommand { get; }
        public DelegateCommand<CheckboxItemSelection> SelectCategoryCommand { get; }
        public DelegateCommand<CheckboxItemSelection> SelectSubcategoryCommand { get; }

        public DelegateCommand ApplyFilterCommand { get; }
        public DelegateCommand ClearFilterCommand { get; }
        #endregion

        public ProductFilterDialogViewModel(
             IAppInstaceService appInstaceService,
             IEventAggregator eventAggregator,
             DialogSource source
            )
        {
            this.Instace = appInstaceService;
            this.EventAggregator = eventAggregator;
            this.HttpService = new HttpRequestService();
            this.Source = source;

            this.SelectOrderByCommand = new DelegateCommand<CheckboxItemSelection>(this.SelectOrderBy);
            this.SelectTypeCommand = new DelegateCommand<CheckboxItemSelection>(async (CheckboxItemSelection item) => await this.SelectType(item), this.CanSelectType);
            this.SelectCategoryCommand = new DelegateCommand<CheckboxItemSelection>(async (CheckboxItemSelection item) => await this.SelectCategory(item), this.CanSelectCategory);
            this.SelectSubcategoryCommand = new DelegateCommand<CheckboxItemSelection>(async (CheckboxItemSelection item) => await this.SelectSubcategory(item));

            this.Types = new ObservableCollection<CheckboxItemSelection>();

            this.Categories = new ObservableCollection<CheckboxItemSelection>();
            this.Categories.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Name = AppResources.All, Selected = true });

            this.Subcategories = new ObservableCollection<CheckboxItemSelection>();
            this.Subcategories.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Name = AppResources.All, Selected = true });

            this.OrderByOptions = new ObservableCollection<CheckboxItemSelection>();
            this.OrderByOptions.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Id = ProductFilterOrder.None.Index, Name = AppResources.DontOrder, Selected = true });
            this.OrderByOptions.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Id = ProductFilterOrder.NameAsc.Index, Name = AppResources.AscendingName, Selected = false });
            this.OrderByOptions.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Id = ProductFilterOrder.NameDesc.Index, Name = AppResources.DescendingName, Selected = false });

            this.ShowOnlyAvailable = true;

            if (this.Source == DialogSource.ProductsPage)
            {
                this.OnlyAvailable = this.Instace.ProductFilter.OnlyAvailable;

                this.OrderByOptions.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Id = ProductFilterOrder.PriceAsc.Index, Name = AppResources.LowestPrice, Selected = false });
                this.OrderByOptions.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Id = ProductFilterOrder.PriceDesc.Index, Name = AppResources.HighestPrice, Selected = false });
            }
            else if (this.Source == DialogSource.StoresPage)
            {
                this.OnlyAvailable = this.Instace.StoreFilter.OnlyAvailable;
            }

            this.SelectedOrderByFilter = ProductFilterOrder.None;

            this.SelectOrderBy(new CheckboxItemSelection() { Id = this.Instace.ProductFilter.OrderBy.Index });

            this.SelectedType = new CheckboxItemSelection() { Uid = Guid.Empty, Name = AppResources.All };
            this.SelectedCategory = new CheckboxItemSelection() { Uid = Guid.Empty, Name = AppResources.All };
            this.SelectedSubcategory = new CheckboxItemSelection() { Uid = Guid.Empty, Name = AppResources.All };

            this.IsFirstLoading = true;
            this.LoadGiros();

            this.ApplyFilterCommand = new DelegateCommand(async () =>
            {
                if (this.Source == DialogSource.ProductsPage)
                {
                    ProductFilter filter = new ProductFilter();
                    filter.UidType = this.SelectedType.Uid;
                    filter.UidCategory = this.SelectedCategory.Uid;
                    filter.UidSubcategory = this.SelectedSubcategory.Uid;
                    filter.OrderBy = this.SelectedOrderByFilter;
                    filter.OnlyAvailable = this.OnlyAvailable;

                    this.EventAggregator.GetEvent<ProductFilterChangedEvent>().Publish(filter);
                }
                else if (this.Source == DialogSource.StoresPage)
                {
                    StoreFilter filter = new StoreFilter();
                    filter.UidType = this.SelectedType.Uid;
                    filter.UidCategory = this.SelectedCategory.Uid;
                    filter.UidSubcategory = this.SelectedSubcategory.Uid;
                    filter.OrderBy = this.SelectedOrderByFilter;
                    filter.OnlyAvailable = this.OnlyAvailable;

                    this.EventAggregator.GetEvent<StoreFilterChangedEvent>().Publish(filter);
                }

                await PopupNavigation.Instance.PopAsync();
            });

            this.ClearFilterCommand = new DelegateCommand(async () =>
            {
                if (this.Source == DialogSource.ProductsPage)
                {
                    ProductFilter filter = new ProductFilter();
                    filter.UidType = Guid.Empty;
                    filter.UidCategory = Guid.Empty;
                    filter.UidSubcategory = Guid.Empty;
                    filter.OrderBy = ProductFilterOrder.None;
                    filter.OnlyAvailable = false;

                    this.EventAggregator.GetEvent<ProductFilterChangedEvent>().Publish(filter);
                }
                else if (this.Source == DialogSource.StoresPage)
                {
                    StoreFilter filter = new StoreFilter();
                    filter.UidType = Guid.Empty;
                    filter.UidCategory = Guid.Empty;
                    filter.UidSubcategory = Guid.Empty;
                    filter.OrderBy = ProductFilterOrder.None;

                    this.EventAggregator.GetEvent<StoreFilterChangedEvent>().Publish(filter);
                }
                await PopupNavigation.Instance.PopAsync();
            });
        }

        #region Implementation
        private async Task SelectType(CheckboxItemSelection item)
        {
            foreach (CheckboxItemSelection option in this.Types)
            {
                if (option.Uid == item.Uid)
                    option.Selected = true;
                else
                    option.Selected = false;
            }

            this.SelectedType.Uid = item.Uid;
            this.SelectedType.Name = item.Name;

            this.IsLoadingCategories = true;


            this.Categories = new ObservableCollection<CheckboxItemSelection>();
            this.Categories.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Name = AppResources.All, Selected = true });
            this.SelectedCategory.Uid = Guid.Empty;
            this.SelectedCategory.Name = AppResources.All;

            this.Subcategories = new ObservableCollection<CheckboxItemSelection>();
            this.Subcategories.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Name = AppResources.All, Selected = true });
            this.SelectedSubcategory.Uid = Guid.Empty;
            this.SelectedSubcategory.Name = AppResources.All;

            await this.LoadCategories();
        }

        private async Task SelectCategory(CheckboxItemSelection item)
        {
            foreach (CheckboxItemSelection option in this.Categories)
            {
                if (option.Uid == item.Uid)
                    option.Selected = true;
                else
                    option.Selected = false;
            }

            this.SelectedCategory.Uid = item.Uid;
            this.SelectedCategory.Name = item.Name;

            this.Subcategories = new ObservableCollection<CheckboxItemSelection>();
            this.Subcategories.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Name = AppResources.All, Selected = true });
            this.SelectedSubcategory.Uid = Guid.Empty;
            this.SelectedSubcategory.Name = AppResources.All;

            await this.LoadSubcategories();
        }

        private async Task SelectSubcategory(CheckboxItemSelection item)
        {
            foreach (CheckboxItemSelection option in this.Subcategories)
            {
                if (option.Uid == item.Uid)
                    option.Selected = true;
                else
                    option.Selected = false;
            }

            this.SelectedSubcategory.Uid = item.Uid;
            this.SelectedSubcategory.Name = item.Name;
        }

        private bool CanSelectType(CheckboxItemSelection item)
        {
            return !this.IsLoadingCategories || !this.IsLoadingSubcategories;
        }

        private bool CanSelectCategory(CheckboxItemSelection item)
        {
            return !this.IsLoadingSubcategories || !this.IsLoadingSubcategories;
        }

        private void SelectOrderBy(CheckboxItemSelection item)
        {
            foreach (CheckboxItemSelection option in this.OrderByOptions)
            {
                if (option.Id == item.Id)
                    option.Selected = true;
                else
                    option.Selected = false;
            }

            switch (item.Id)
            {
                case 0:
                    this.SelectedOrderByFilter = ProductFilterOrder.None;
                    break;
                case 1:
                    this.SelectedOrderByFilter = ProductFilterOrder.NameAsc;
                    break;
                case 2:
                    this.SelectedOrderByFilter = ProductFilterOrder.NameDesc;
                    break;
                case 3:
                    this.SelectedOrderByFilter = ProductFilterOrder.PriceAsc;
                    break;
                case 4:
                    this.SelectedOrderByFilter = ProductFilterOrder.PriceDesc;
                    break;
            }
        }

        public async void LoadGiros()
        {
            var giros = this.Instace.GetGiroList();

            if (this.IsFirstLoading && this.CurrentTypeSessionFilter() != Guid.Empty)
            {
                var exists = giros.FirstOrDefault(c => Guid.Parse(c.Uid) == this.CurrentTypeSessionFilter());
                if (exists != null)
                {
                    this.SelectedType = new CheckboxItemSelection() { Uid = Guid.Parse(exists.Uid), Name = exists.Name };
                }
            }

            foreach (var giro in giros)
            {
                Guid uid = Guid.Parse(giro.Uid);

                CheckboxItemSelection item = new CheckboxItemSelection()
                {

                    Uid = uid,
                    Selected = uid == this.SelectedType.Uid,
                    Name = giro.Name
                };

                this.Types.Add(item);
            }

            if (this.IsFirstLoading && this.SelectedType.Uid != Guid.Empty)
            {
                await LoadCategories();
            }
            else if (IsFirstLoading)
            {
                IsFirstLoading = false;
            }
        }

        public async Task LoadCategories()
        {
            if (SelectedType.Uid != Guid.Empty)
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("UidGiro", SelectedType.StrUid);
                parameters.Add("TipoDeRespuesta", "seleccionar");

                var CategoriaRequest = await this.HttpService.GetAsync<IEnumerable<Categoria>>("CatalogosDeFiltros/GetCategoriaMovil", parameters);
                if (CategoriaRequest.Code == Xam_Customer.Core.Enum.HttpResponseCode.Success)
                {
                    if (this.IsFirstLoading && this.CurrentCategorySessionFilter() != Guid.Empty)
                    {
                        var exists = CategoriaRequest.Result.FirstOrDefault(c => Guid.Parse(c.Uid) == this.CurrentCategorySessionFilter());
                        if (exists != null)
                        {
                            this.SelectedCategory = new CheckboxItemSelection() { Uid = Guid.Parse(exists.Uid), Name = exists.Name };
                            this.Categories.First().Selected = false;
                        }
                    }

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        foreach (var category in CategoriaRequest.Result)
                        {
                            Guid uid = Guid.Parse(category.Uid);
                            if (uid != Guid.Empty)
                            {
                                var item = new CheckboxItemSelection()
                                {
                                    Uid = uid,
                                    Selected = uid == this.SelectedCategory.Uid,
                                    Name = category.Name
                                };
                                this.Categories.Add(item);

                                if (uid == this.SelectedCategory.Uid)
                                {
                                    this.SelectedCategory.Name = category.Name;
                                }
                                else if (uid.ToString() == this.Instace.GetUidFilter() && this.Source == DialogSource.StoresPage)
                                {
                                    this.SelectedCategory.Name = category.Name;
                                    SelectedCategory = item;
                                    SelectedCategory.Selected = true;
                                    Device.InvokeOnMainThreadAsync(async () => { await LoadSubcategories(); });
                                }
                            }
                        }
                    });

                    if (this.IsFirstLoading && this.SelectedCategory.Uid != Guid.Empty)
                    {
                        await LoadSubcategories();
                    }
                    else if (IsFirstLoading)
                    {
                        IsFirstLoading = false;
                    }
                }
            }

            this.IsLoadingCategories = false;
        }

        public async Task LoadSubcategories()
        {
            if (this.SelectedCategory.Uid != Guid.Empty)
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("uidCategoria", SelectedCategory.StrUid);
                parameters.Add("TipoDeRespuesta", "seleccionar");

                var SubcategoriaRequest = await this.HttpService.GetAsync<IEnumerable<Subcategoria>>("CatalogosDeFiltros/GetSubCategoriaMovil", parameters);
                if (SubcategoriaRequest.Code == Xam_Customer.Core.Enum.HttpResponseCode.Success)
                {
                    if (this.IsFirstLoading && this.CurrentSubcategorySessionFilter() != Guid.Empty)
                    {
                        var exists = SubcategoriaRequest.Result.FirstOrDefault(c => Guid.Parse(c.Uid) == this.CurrentSubcategorySessionFilter());
                        if (exists != null)
                        {
                            this.SelectedSubcategory = new CheckboxItemSelection() { Uid = Guid.Parse(exists.Uid), Name = exists.Name };
                            this.Subcategories.First().Selected = false;
                        }
                    }

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        foreach (var subcategoria in SubcategoriaRequest.Result)
                        {
                            Guid uid = Guid.Parse(subcategoria.Uid);
                            if (uid != Guid.Empty)
                            {
                                var item = new CheckboxItemSelection()
                                {
                                    Uid = uid,
                                    Selected = uid == this.SelectedSubcategory.Uid,
                                    Name = subcategoria.Name
                                };
                                this.Subcategories.Add(item);

                                if (uid == this.SelectedSubcategory.Uid)
                                {
                                    this.SelectedSubcategory.Name = subcategoria.Name;
                                }
                                else if (uid.ToString() == this.Instace.GetUidFilter() && this.Source == DialogSource.StoresPage)
                                {
                                    this.SelectedSubcategory.Name = subcategoria.Name;
                                    SelectedSubcategory = item;
                                    SelectedSubcategory.Selected = true;
                                }
                            }
                        }
                    });

                    if (IsFirstLoading)
                    {
                        IsFirstLoading = false;
                    }
                }
            }

            this.IsLoadingSubcategories = false;
        }

        private Guid CurrentTypeSessionFilter()
        {
            Guid result = Guid.Empty;
            if (this.Source == DialogSource.ProductsPage)
            {
                result = this.Instace.ProductFilter.UidType;
            }
            else if (this.Source == DialogSource.StoresPage)
            {
                result = this.Instace.StoreFilter.UidType;
            }
            return result;
        }
        private Guid CurrentCategorySessionFilter()
        {
            Guid result = Guid.Empty;
            if (this.Source == DialogSource.ProductsPage)
            {
                result = this.Instace.ProductFilter.UidCategory;
            }
            else if (this.Source == DialogSource.StoresPage)
            {
                result = this.Instace.StoreFilter.UidCategory;
            }
            return result;
        }
        private Guid CurrentSubcategorySessionFilter()
        {
            Guid result = Guid.Empty;
            if (this.Source == DialogSource.ProductsPage)
            {
                result = this.Instace.ProductFilter.UidType;
            }
            else if (this.Source == DialogSource.StoresPage)
            {
                result = this.Instace.StoreFilter.UidType;
            }
            return result;
        }
        #endregion
    }

    public enum DialogSource
    {
        ProductsPage,
        StoresPage
    }
}