﻿using Xamarin.Forms;

namespace Xam_Customer.Views.Error
{
    /// <summary>
    /// <param name="previousPage">previousPage: Pagina de origen</param>
    /// </summary>
    public partial class NetworkErrorPage : ContentPage
    {
        public NetworkErrorPage()
        {
            InitializeComponent();
        }
    }
}
