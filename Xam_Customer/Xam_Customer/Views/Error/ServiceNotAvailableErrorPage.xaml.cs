﻿using Xamarin.Forms;

namespace Xam_Customer.Views.Error
{
    /// <summary>
    /// <param name="previousPage">previousPage: Pagina de origen</param>
    /// </summary>
    public partial class ServiceNotAvailableErrorPage : ContentPage
    {
        public ServiceNotAvailableErrorPage()
        {
            InitializeComponent();
        }
    }
}
