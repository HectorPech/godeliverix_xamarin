﻿using Acr.UserDialogs;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.SignIn;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Views.Registry;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class TermsAndConditionsPageViewModel : BindableBase, INavigationAware
    {
        #region Properties

        private bool _AcionesPage;

        public bool AccionesPage
        {
            get { return _AcionesPage; }
            set { SetProperty(ref _AcionesPage, value); }
        }
        #region Atributes
        private SignInData _SignInData { get; set; }
        #endregion

        #region Private
        private string _Uid;

        public string Uid
        {
            get { return _Uid; }
            set { SetProperty(ref _Uid, value); }
        }

        private WebViewSource _URLPAY;
        public WebViewSource URLPAY
        {
            get { return this._URLPAY; }
            set { SetProperty(ref this._URLPAY, value); }
        }
        private bool _IsPdf;
        public bool IsPdf
        {
            get { return this._IsPdf; }
            set { SetProperty(ref this._IsPdf, value); }
        }
        public string baseUrl { get; set; }
        #endregion
        #endregion
        #region Services
        public IAppInstaceService _appInstaceService { get; set; }
        public IHttpRequestService _httpRequestService { get; set; }
        public IPageDialogService _pageDialogService { get; set; }
        public INavigationService _navigationService { get; set; }
        #endregion

        #region Command
        public DelegateCommand GoBackCommand { get; set; }
        public DelegateCommand AceptCommand { get; set; }
        public DelegateCommand CancelCommand { get; set; }
        #endregion
        public TermsAndConditionsPageViewModel(IAppInstaceService appInstaceService,
            IHttpRequestService httpRequestService,
            IPageDialogService pageDialogService,
            INavigationService navigationService)
        {
            _httpRequestService = httpRequestService;
            _appInstaceService = appInstaceService;
            _pageDialogService = pageDialogService;
            _navigationService = navigationService;
            AccionesPage = false;
            baseUrl = "http://www.godeliverix.net/";
            this.GoBackCommand = new DelegateCommand(async () => { await this._navigationService.GoBackAsync(); });
            this.AceptCommand = new DelegateCommand(Acept);
            this.CancelCommand = new DelegateCommand(cancel);
        }

        #region Implementacion

        private void Acept()
        {
            //llamado desde el registro
            if (_SignInData != null)
            {
                _SignInData.IdTermsAndConditions = Uid;
                _SignInData.Accepted = true;
                Device.InvokeOnMainThreadAsync(async () =>
                {
                    await this._navigationService.NavigateAsync($"{nameof(RegistryStep4Page)}", new NavigationParameters()
                {
                    { "data", this._SignInData}
                });
                });
            }
            else //Llamado desde el usuario
            {

            }

        }
        private void cancel()
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
               await _navigationService.GoBackAsync();
            });
        }
        private async Task CargaTerminosYCondicionesUsuario()
        {
            await Device.InvokeOnMainThreadAsync(async () =>
            {
                _SignInData = null;
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("UidUsuario", Settings.UserSession.StrUid);
                parameters.Add("Languaje", CultureInfo.CurrentCulture.TwoLetterISOLanguageName);
                var userRequest = await this._httpRequestService.GetAsync<TermsAndConditions>("TerminosYCondiciones/SearchTermsAndConditions", parameters);
                if (userRequest.Code == HttpResponseCode.Success)
                {
                    var tyc = userRequest.Result;
                    Uid = tyc.Uid;
                    URLPAY = new UrlWebViewSource() { Url = (baseUrl + "tyc.aspx?url=" + baseUrl + URLPAY + tyc.URL) };
                    IsPdf = true;
                }
            });

        }
        private async Task CargaTerminosYCondicionesRegistro(string UidLada)
        {
            await Device.InvokeOnMainThreadAsync(async () =>
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("UidLada", UidLada);
                parameters.Add("Languaje", CultureInfo.CurrentCulture.TwoLetterISOLanguageName);
                var userRequest = await this._httpRequestService.GetAsync<TermsAndConditions>("TerminosYCondiciones/SearchTermsAndConditions", parameters);
                if (userRequest.Code == HttpResponseCode.Success)
                {
                    var tyc = userRequest.Result;
                    Uid = tyc.Uid;
                    URLPAY = new UrlWebViewSource() { Url = (baseUrl + "tyc.aspx?url=" + baseUrl + URLPAY + tyc.URL) };
                    IsPdf = true;
                }
            });


        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("data"))
            {
                this._SignInData = parameters.GetValue<SignInData>("data");
                string Lada = parameters.GetValue<string>("lada");
                AccionesPage = true;
                Device.InvokeOnMainThreadAsync(async () => { await CargaTerminosYCondicionesRegistro(Lada); });
            }
            else
            {
                Device.InvokeOnMainThreadAsync(async () => { await CargaTerminosYCondicionesUsuario(); });
            }
        }
        #endregion
    }
}
