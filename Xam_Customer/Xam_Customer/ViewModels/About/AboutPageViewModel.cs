﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Util;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.Profile;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views.About;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class AboutPageViewModel : BaseNavigationViewModel
    {
        public ObservableCollection<ProfileItemsList> Options { get; set; }
        private enum OptionCommandName
        {
            TermsAndConditions
        }
        #region Propiedades
        private string _StrVersion;
        public string StrVersion
        {
            get { return _StrVersion; }
            set { SetProperty(ref _StrVersion, value); }
        }
        #endregion
        #region Command
        public DelegateCommand<ProfileItemsList> OptionSelectedCommand { get; }

        #endregion
        #region Services

        #endregion
        public AboutPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            StrVersion =  ApplicationConstants.Version;
            this.Options = new ObservableCollection<ProfileItemsList>();
            this.OptionSelectedCommand = new DelegateCommand<ProfileItemsList>(this.OptionSelected);
            this.Options.Clear();

            this.Options.Add(new ProfileItemsList()
            {
                Icon = MaterialFontIcons.FileDocumentOutline,
                Name = ProfileLang.AccountForm_TermsAndConditions,
                CommandName = OptionCommandName.TermsAndConditions.ToString(),
            });
        }
        private void OptionSelected(ProfileItemsList item)
        {
            string navigateTo = string.Empty;
            NavigationParameters parameters = new NavigationParameters();

            switch (item.CommandName)
            {
                case "TermsAndConditions":
                    navigateTo = nameof(TermsAndConditionsPage);
                    break;
                default:
                    break;
            }
            if (!string.IsNullOrEmpty(navigateTo))
            {
                Device.InvokeOnMainThreadAsync(async () =>
                {
                    await this.NavigationService.NavigateAsync(navigateTo, parameters);
                });
            }

        }
    }
}
