﻿using Acr.UserDialogs;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Resources.LangResx;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class ForgotAccountPageViewModel : BindableBase
    {
        #region Properties
        private bool _Validemail;

        public bool Validemail
        {
            get { return _Validemail; }
            set { SetProperty(ref _Validemail, value); }
        }
        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { SetProperty(ref _Email, value); }
        }
        public EmailAdress EmainInfo { get; set; }
        #endregion
        #region Command
        public DelegateCommand GoBackCommand { get; }
        public DelegateCommand RecoveryAccount { get; }

        #endregion
        #region Services
        public INavigationService _navigationService { get; set; }
        public IHttpRequestService _HttpService { get; set; }
        public IPageDialogService _pageDialogService { get; set; }
        #endregion
        public ForgotAccountPageViewModel(INavigationService navigationService,
            IHttpRequestService httpRequestService,
            IPageDialogService pageDialogService)
        {
            _navigationService = navigationService;
            _HttpService = httpRequestService;
            _pageDialogService = pageDialogService;
            this.GoBackCommand = new DelegateCommand(async () => { await this._navigationService.GoBackAsync(); });
            this.RecoveryAccount = new DelegateCommand(SendRecoveryMail);
        }

        private async void SendRecoveryMail()
        {
            await Device.InvokeOnMainThreadAsync(() =>
            {
                UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);
            });
            await ValidateEmail();
            //Si existe manda del correo, de lo contrario avisa que no se encuentra
            if (Validemail)
            {
                string password = Guid.NewGuid().ToString().Substring(0,8);
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("UidUsuario", EmainInfo.UidUser);
                parameters.Add("perfil", "4F1E1C4B-3253-4225-9E46-DD7D1940DA19");
                parameters.Add("password", password);
                var userRequest = await this._HttpService.GetEmptyAsync("Usuario/GetActualizarUsuario_Movil", parameters);
                if (userRequest == HttpResponseCode.Success)
                {
                    parameters = new Dictionary<string, string>();
                    parameters.Add("strCorreoElectronico", Email);
                    var emailRequest = await this._HttpService.GetEmptyAsync("CorreoElectronico/GetRecuperarContrasena", parameters);
                    if (emailRequest == HttpResponseCode.Success)
                    {
                        await this._pageDialogService.DisplayAlertAsync("", AppResources.ForgotAccount_SuccessMessage + Email, AppResources.AlertAcceptText);
                        await this._navigationService.GoBackAsync();
                    }
                    else
                    {
                        await this._pageDialogService.DisplayAlertAsync("", AppResources.Error_General_Message, AppResources.AlertAcceptText);
                    }
                }
                else
                {
                    await this._pageDialogService.DisplayAlertAsync("", AppResources.Error_General_Message, AppResources.AlertAcceptText);
                }
            }
            else
            {
                await this._pageDialogService.DisplayAlertAsync("", AppResources.ForgtoAccount_InvalidEmail, AppResources.AlertAcceptText);
            }
            await Device.InvokeOnMainThreadAsync(() =>
            {
                UserDialogs.Instance.HideLoading();
            });
        }

        #region Implementation
        public async Task ValidateEmail()
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("strParametroDebusqueda", "Usuario");
            param.Add("strCorreoElectronico", Email);
            var EmailRequest = await this._HttpService.GetAsync<EmailAdress>("CorreoElectronico/GetBuscarCorreo_movil", param);
            EmailAdress lista = EmailRequest.Result;
            if (string.IsNullOrEmpty(lista.Mail))
            {
                Validemail = false;
            }
            else
            {
                EmainInfo = lista;
                Validemail = true;
            }
        }
        #endregion
    }

}
