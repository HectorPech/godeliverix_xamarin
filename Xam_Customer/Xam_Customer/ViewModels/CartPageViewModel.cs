﻿using Prism.AppModel;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.Cart;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xam_Customer.Views.Payment;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class CartPageViewModel : BaseViewModel, IPageLifecycleAware
    {
        #region Properties

        private ObservableCollection<CartBranchItemListView> _cartBranchItems;
        public ObservableCollection<CartBranchItemListView> CartBranchItems
        {
            get { return _cartBranchItems; }
            set { SetProperty(ref _cartBranchItems, value); }
        }

        private decimal _subtotal;
        public decimal Subtotal
        {
            get { return this._subtotal; }
            set { SetProperty(ref this._subtotal, value); }
        }

        private decimal _deliveryRate;
        public decimal DeliveryRate
        {
            get { return this._deliveryRate; }
            set { SetProperty(ref this._deliveryRate, value); }
        }
        private decimal _deliveryTip;
        public decimal DeliveryTip
        {
            get { return this._deliveryTip; }
            set { SetProperty(ref this._deliveryTip, value); }
        }

        private decimal _total;
        public decimal Total
        {
            get { return this._total; }
            set { SetProperty(ref this._total, value); }
        }

        private bool _noCartItemsVisible;
        public bool NoCartItemsVisible
        {
            get { return this._noCartItemsVisible; }
            set { SetProperty(ref this._noCartItemsVisible, value); }
        }

        private decimal cardPaymentCommission;
        public decimal CardPaymentCommission
        {
            get { return cardPaymentCommission; }
            set { SetProperty(ref cardPaymentCommission, value); }
        }

        private bool showCardPaymentCommission;
        public bool ShowCardPaymentCommission
        {
            get { return showCardPaymentCommission; }
            set { SetProperty(ref showCardPaymentCommission, value); }
        }

        private string paymentTypeName;
        public string PaymentTypeName
        {
            get { return paymentTypeName; }
            set { SetProperty(ref paymentTypeName, value); }
        }

        private bool showPaymentTypeName;
        public bool ShowPaymentTypeName
        {
            get { return showPaymentTypeName; }
            set { SetProperty(ref showPaymentTypeName, value); }
        }

        private decimal walletDiscount;
        public decimal WalletDiscount
        {
            get { return walletDiscount; }
            set { SetProperty(ref walletDiscount, value); }
        }

        private bool useEWalletToPayment;
        public bool UseEWalletToPayment
        {
            get { return useEWalletToPayment; }
            set { SetProperty(ref useEWalletToPayment, value); }
        }

        #endregion

        #region Private
        private List<CartBranchItem> _CartItems { get; set; }
        #endregion

        #region Command
        public DelegateCommand GoToPaymentCommand { get; }
        public DelegateCommand RemoveCommand { get; set; }

        public DelegateCommand<string> GoToCartBranchDetail { get; }
        #endregion

        #region Services
        private readonly IAppInstaceService AppInstaceService;
        public IPageDialogService _PageDialogService { get; set; }
        #endregion

        public CartPageViewModel(
            INavigationService navigationService,
            IDialogService dialogService,
            IPageDialogService pageDialogService,
            IAppInstaceService appInstaceService
            ) : base(navigationService, dialogService, pageDialogService)
        {
            this.AppInstaceService = appInstaceService;
            this._PageDialogService = pageDialogService;

            this.GoToPaymentCommand = new DelegateCommand(async () => { await this.GoToPayment(); });
            this.GoToCartBranchDetail = new DelegateCommand<string>(this.GoToBranchDetail);
            this.RemoveCommand = new DelegateCommand(this.Remove);

            this.CartBranchItems = new ObservableCollection<CartBranchItemListView>();
            this._CartItems = this.AppInstaceService.ReadAllCartBranchItems();
            this.Subtotal = 0;
            this.DeliveryRate = 0;
            this.Total = 0;
            this.NoCartItemsVisible = false;
            this.ShowCardPaymentCommission = false;
            this.CardPaymentCommission = 0;
            this.ShowPaymentTypeName = false;
            this.PaymentTypeName = string.Empty;

            this.WalletDiscount = 0;
            this.UseEWalletToPayment = false;
        }

        #region Implementation
        private async void Remove()
        {
            bool answer = await this._PageDialogService.DisplayAlertAsync("", CartLang.Cart_ClearCartMessagge, CartLang.Cart_Yes, CartLang.Cart_No);
            if (answer)
            {
                this.AppInstaceService.RemoveAllitemsFromCart();
                await this.NavigationService.GoBackAsync();
            }
        }
        private async void GoToBranchDetail(string uid)
        {

            await this.NavigationService.NavigateAsync($"{nameof(CartOrderDetailPage)}", new NavigationParameters()
            {
                {"uid",  Guid.Parse(uid)}
            });
        }
        private async Task GoToPayment()
        {
            if (this._CartItems.Count() == 0)
            {
                await this._PageDialogService.DisplayAlertAsync("", CartLang.Cart_Empty, AppResources.AlertAcceptText);
                return;
            }

            if (Settings.UserSession != null)
            {
                await Device.InvokeOnMainThreadAsync(async () =>
                {
                    await this.NavigationService.NavigateAsync($"{nameof(PaymentType)}");
                });
            }
            else
            {
                bool answer = await this._PageDialogService.DisplayAlertAsync("", CartLang.Payment_RequiredLogin, CartLang.Paymetn_Ok, AppResources.Welcome_LoginBtn);
                if (!answer)
                {
                    await Device.InvokeOnMainThreadAsync(async () =>
                    {
                        await this.NavigationService.NavigateAsync($"/{nameof(LoginPage)}");
                    });
                }
            }
        }

        private void GenerateCartItems()
        {
            this.CartBranchItems = new ObservableCollection<CartBranchItemListView>();

            if (this._CartItems.Count() > 0)
            {
                List<CartBranchItemListView> branchItems = new List<CartBranchItemListView>();
                foreach (CartBranchItem item in this._CartItems)
                {
                    int ProductsTotal = 0;
                    var _CartBranchItem = this.AppInstaceService.GetCartBranchItemById(item.Uid);
                    foreach (var pro in _CartBranchItem.CartItems)
                    {
                        ProductsTotal += pro.Quantity;
                    }
                    CartBranchItemListView cartBranch = new CartBranchItemListView()
                    {
                        Uid = item.Uid,
                        BranchName = item.BranchName,
                        CompanyImgUrl = item.CompanyImgUrl,
                        CompanyName = item.CompanyName,
                        DeliveryRate = item.DeliveryRate,
                        QuantityItems = ProductsTotal,
                        StrQuantityItems = ProductsTotal > 1 ? $"{ProductsTotal} {CartLang.Cart_Items_Product_Label}" : $"{ProductsTotal} {CartLang.Cart_Item_Product_Label}",
                        Total = item.Total,
                        UidBranch = item.UidBranch
                    };
                    this.CartBranchItems.Add(cartBranch);
                }

                AvailablePaymentType paymentType = this.AppInstaceService.PaymentType;
                if (paymentType == AvailablePaymentType.None)
                {
                    this.CardPaymentCommission = 0;
                    this.ShowCardPaymentCommission = false;
                    this.ShowPaymentTypeName = false;
                }
                else
                {
                    this.ShowCardPaymentCommission = paymentType == AvailablePaymentType.CreditDebitCard;

                    switch (paymentType)
                    {
                        case AvailablePaymentType.Cash:
                            this.PaymentTypeName = CartLang.PayMent_Cash;
                            break;
                        case AvailablePaymentType.CreditDebitCard:
                            this.PaymentTypeName = CartLang.Payment_Card;
                            break;
                        case AvailablePaymentType.Wallet:
                            this.PaymentTypeName = CartLang.Payment_wallet;
                            break;
                        case AvailablePaymentType.None:
                            break;
                        default:
                            break;
                    }

                    if (paymentType == AvailablePaymentType.CreditDebitCard)
                    {
                        foreach (var cart in this._CartItems)
                        {
                            this.AppInstaceService.ApplyCommissionCartBranchItem(cart.Uid);
                        }
                    }
                    else
                    {
                        foreach (var cart in this._CartItems)
                        {
                            this.AppInstaceService.ClearCommissionCartBranchItem(cart.Uid);
                        }
                    }

                    this._CartItems = this.AppInstaceService.ReadAllCartBranchItems();

                    this.CardPaymentCommission = this._CartItems.Sum(c => c.TotalCommissions);
                    this.ShowPaymentTypeName = true;
                }

                this.Subtotal = this._CartItems.Sum(i => i.OrderSubtotal);
                this.DeliveryRate = this._CartItems.Sum(i => (i.DeliveryRateSubtotal));
                this.DeliveryTip = this._CartItems.Sum(i => (i.DeliveryTipsSubtotal));

                this.WalletDiscount = 0;
                this.UseEWalletToPayment = false;

                if (this._CartItems.Where(i => i.WalletDiscount.HasValue).Any())
                {
                    this.UseEWalletToPayment = true;
                    this.WalletDiscount = this._CartItems.Sum(i => i.WalletDiscount.HasValue ? i.WalletDiscount.Value : 0);
                }

                this.Total = (this.Subtotal + this.DeliveryRate + this.DeliveryTip + this.CardPaymentCommission) - this.WalletDiscount;
            }
            else
            {
                this.Subtotal = 0;
                this.DeliveryRate = 0;
                this.Total = 0;
            }

            this.NoCartItemsVisible = this._CartItems.Count() > 0 ? false : true;
        }
        #endregion

        #region Page Lifecycle
        public void OnAppearing()
        {
            this.GenerateCartItems();
        }

        public void OnDisappearing()
        {
        }
        #endregion
    }
}
