﻿using Acr.UserDialogs;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Events;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Services.Implementation;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Views;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class ProductSearchDescriptionPageViewModel : BindableBase, INavigatedAware, IInitialize
    {
        private List<Branch> _BranchList;
        public List<Branch> BranchList
        {
            get => _BranchList;
            set => SetProperty(ref _BranchList, value);
        }

        private Branch _SelectedBranch;
        public Branch SelectedBranch
        {
            get => _SelectedBranch;
            set => SetProperty(ref _SelectedBranch, value);
        }

        private Product _SelectedProduct;
        public Product Selectedproduct
        {
            get => _SelectedProduct;
            set => SetProperty(ref _SelectedProduct, value);
        }

        private string _CompanyName;
        public string CompanyName
        {
            get => _CompanyName;
            set => SetProperty(ref _CompanyName, value);
        }
        private string _Name;
        public string Name
        {
            get => _Name;
            set => SetProperty(ref _Name, value);
        }
        private string _ImagePath;

        public string ImagePath
        {
            get => _ImagePath;
            set => SetProperty(ref _ImagePath, value);
        }
        private string _ImageInc;

        public string ImageInc
        {
            get => _ImageInc;
            set => SetProperty(ref _ImageInc, value);
        }
        private string _Description;

        public string Description
        {
            get => _Description;
            set => SetProperty(ref _Description, value);
        }
        private string _TotalAAgregarEnCarrito;

        public string AmountShopCard
        {
            get => _TotalAAgregarEnCarrito;
            set => SetProperty(ref _TotalAAgregarEnCarrito, value);
        }

        private int _Quantity;
        public int Quantity
        {
            get => _Quantity;
            set => SetProperty(ref _Quantity, value);
        }

        private string _notes;
        public string Notes
        {
            get { return _notes; }
            set { SetProperty(ref _notes, value); }
        }

        /// <summary>
        /// Determinar el el producto proviene del carrito
        /// </summary>
        private bool FromCartItem { get; set; }
        private Guid CartItemUid { get; set; }
        private Guid CartBranchItemUid { get; set; }

        #region Branch properties

        private string _identifier;

        public string Identifier
        {
            get => _identifier;
            set => SetProperty(ref _identifier, value);
        }

        private string _availability;

        public string availability
        {
            get => _availability;
            set => SetProperty(ref _availability, value);
        }
        private decimal _price;

        public decimal price
        {
            get => _price;
            set => SetProperty(ref _price, value);
        }
        #endregion
        #region Internal
        private IHttpRequestService _HttpService { get; }
        private INavigationService _navigationServices { set; get; }
        private IPageDialogService _pageDialogService { set; get; }
        private IEventAggregator _ea { set; get; }
        private IAppInstaceService _appInstaceService { set; get; }
        #endregion

        #region Command
        public DelegateCommand cmdAddToShopCar { get; set; }
        public DelegateCommand cmdChanceCompanyBranch { get; set; }
        public DelegateCommand CmdAdd { get; set; }
        public DelegateCommand CmdQuit { get; set; }
        public DelegateCommand GoToCart { get; }

        #endregion

        public ProductSearchDescriptionPageViewModel(
            INavigationService navigationService,
            IHttpRequestService httpRequestService,
            IAppInstaceService appInstaceService,
            IPageDialogService pageDialogService,
            IEventAggregator ea)
        {
            _ea = ea;
            _pageDialogService = pageDialogService;
            _ea.GetEvent<BranchSelectedEvent>().Subscribe(ChangeBranch);
            _appInstaceService = appInstaceService;
            _HttpService = httpRequestService;
            _navigationServices = navigationService;
            cmdAddToShopCar = new DelegateCommand(async () => { await AddShopCart(); });
            cmdChanceCompanyBranch = new DelegateCommand(ViewBranches);
            Quantity = 1;
            CmdAdd = new DelegateCommand(AddOne);
            CmdQuit = new DelegateCommand(QuitOne);
            this.GoToCart = new DelegateCommand(async () => await this._navigationServices.NavigateAsync($"{nameof(CartPage)}"));
        }

        private void ChangeBranch(Branch obj)
        {
            SelectedBranch = obj;
        }

        #region Navigation aware
        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            // throw new NotImplementedException();
        }
        public void OnNavigatedTo(INavigationParameters parameters)
        {
            Device.InvokeOnMainThreadAsync(() =>
            {
                int count = this._appInstaceService.ProductsCount();
                this._ea.GetEvent<CartCountEvent>().Publish(count);
            });
            if (parameters.ContainsKey("Data"))
            {
                this.Selectedproduct = parameters.GetValue<Product>("Data");
                Task.Run(async () => { await this.Cargaproducto(); });
            }

            if (parameters.ContainsKey("CartItemId") && parameters.ContainsKey("CartBranchItemId"))
            {
                this.CartItemUid = parameters.GetValue<Guid>("CartItemId");
                this.CartBranchItemUid = parameters.GetValue<Guid>("CartBranchItemId");
                this.PopulateDataFromCartItem();
            }
        }


        #endregion

        #region Implementation
        private async Task Cargaproducto()
        {
            await Device.InvokeOnMainThreadAsync(() =>
            {
                UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);
            });

            Name = Selectedproduct.Name;
            ImagePath = Selectedproduct.ImgPathProduct;
            ImageInc = Selectedproduct.ImgPathCompany;
            CompanyName = Selectedproduct.CompanyName;
            Description = Selectedproduct.Description;
            Dictionary<string, string> parametro = new Dictionary<string, string>();
            parametro.Add("StrParametroBusqueda", _appInstaceService.GetSearchType());
            parametro.Add("StrDia", _appInstaceService.GetDayOfTheWeek());
            parametro.Add("UidEstado", _appInstaceService.GetDeliveryState());
            parametro.Add("UidColonia", _appInstaceService.GetDeliveryColonie());
            parametro.Add("UidBusquedaCategorias", _appInstaceService.GetUidFilter());
            parametro.Add("UidProducto", Selectedproduct.StrUid);
            var ProductRequest = await this._HttpService.GetAsync<IEnumerable<Branch>>("Producto/GetObtenerInformacionDeProductoDeLaSucursal_Movil", parametro);
            BranchList = ProductRequest.Result.ToList();
            SelectedBranch = BranchList[0];
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("UIDSECCION", SelectedBranch.UidSeccion);
            param.Add("UidEstado", "1FCE366D-C225-47FD-B4BB-5EE4549FE913");
            param.Add("UidColonia", "DD873F59-BF4F-4EEF-8C1B-5CDE89C8D62B");
            var SeccionRequest = await this._HttpService.GetAsync<Seccion>("Seccion/GetBuscarSeccion_movil", param);
            var obj = SeccionRequest.Result as Seccion;
            availability = AppResources.ProductAvailability + " " + obj.FinishTime;
            Identifier = SelectedBranch.Identificador;
            price = SelectedBranch.Costoproducto;

            AmountShopCard = AppResources.AddShoppingCart + "  $" + (price * Quantity);

            await Device.InvokeOnMainThreadAsync(() =>
            {
                UserDialogs.Instance.HideLoading();
            });
        }

        /// <summary>
        /// Rellenar la informacion con los datos del item del carrito
        /// </summary>
        /// <param name="uid"></param>
        private void PopulateDataFromCartItem()
        {
            CartItem item = this._appInstaceService.GetCartItemByIdFromBranch(this.CartBranchItemUid, this.CartItemUid);
            if (item != null)
            {
                this.FromCartItem = true;

                this.Quantity = item.Quantity;
                this.Notes = item.Notes;
            }
        }

        private void AddOne()
        {
            Quantity += 1;
            AmountShopCard = AppResources.AddShoppingCart + "  $" + (price * Quantity);

        }
        private void QuitOne()
        {
            if (Quantity != 1)
            {
                Quantity -= 1;
                AmountShopCard = AppResources.AddShoppingCart + "  $" + (price * Quantity);
            }
        }

        protected async Task AddShopCart()
        {
            await Device.InvokeOnMainThreadAsync(() =>
            {
                UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);
            });
            if (Quantity > 0)
            {

                int cantidad = 0;
                if (price == 0)
                {
                    await Device.InvokeOnMainThreadAsync(() =>
                    {
                        UserDialogs.Instance.HideLoading();
                    });
                    await this._pageDialogService.DisplayAlertAsync("", AppResources.AddShoppingCart_InvalidPrice, AppResources.AlertAcceptText);
                    return;
                }
                if (!int.TryParse(Quantity.ToString(), out cantidad))
                {
                    await Device.InvokeOnMainThreadAsync(() =>
                    {
                        UserDialogs.Instance.HideLoading();
                    });
                    await this._pageDialogService.DisplayAlertAsync("", AppResources.AddShoppingCart_InvalidQuantityValue, AppResources.AlertAcceptText);
                    return;
                }
                Dictionary<string, string> parametro = new Dictionary<string, string>();
                parametro.Add("UidSeccion", SelectedBranch.UidSeccion);
                parametro.Add("UidSucursal", SelectedBranch.Uid);
                parametro.Add("UidProducto", Selectedproduct.StrUid);
                parametro.Add("UidColoniaAEntregar", _appInstaceService.GetDeliveryColonie());
                var ProductRequest = await this._HttpService.GetAsync<Product>("Producto/GetInformacionProductoAlCarrito", parametro);
                var producto = ProductRequest.Result;

                if (string.IsNullOrEmpty(Notes))
                {
                    Notes = string.Empty;
                }

                CartBranchItem cartBranchItem = this._appInstaceService.GetCartBranchItemByBranch(Guid.Parse(this.SelectedBranch.Uid));
                CartItem cartItem = new CartItem()
                {
                    CartId = Guid.NewGuid(),
                    Uid = new Guid(producto.UidSeccionPoducto),
                    UidProduct = Guid.Parse(producto.StrUid),
                    ImgPathProduct = this.Selectedproduct.ImgPathProduct,
                    Notes = this.Notes.Trim(),
                    ProductDescription = this.Description,
                    ProductName = producto.Name,
                    Quantity = this.Quantity,
                    UnitPrice = (this.SelectedBranch.Costoproducto)
                };

                if (cartBranchItem == null)
                {
                    cartBranchItem = new CartBranchItem()
                    {
                        Uid = Guid.NewGuid(),
                        UidBranch = Guid.Parse(producto.UidSucursal),
                        BranchName = producto.StrIdentificador,
                        CompanyImgUrl = this.Selectedproduct.ImgPathCompany,
                        CompanyName = this.Selectedproduct.CompanyName,
                        UidDelivery = producto.UidTarifario,
                        DeliveryRate = producto.CostoEnvio,
                        DeliveryBranch = producto.DeliveryBranch,
                        DeliveryCompany = producto.DeliveryCompany,
                        DeliveryTips = 15,
                        ImageDeliveryCompany = "https://www.godeliverix.net/Vista/" + producto.ImgPathCompany.Remove(0, 3),
                        CartItems = new List<CartItem>() { cartItem }
                    };

                    cartBranchItem.SupplierIncludeCardPaymentCommission = this.SelectedBranch.IncluyeComisionTarjetaProducto;
                    cartBranchItem.DeliveryIncludeCardPaymentCommission = this.SelectedBranch.IncluyeComisionTarjetaEnvio;

                    this._appInstaceService.AddCartBranchItem(cartBranchItem);
                }
                else
                {
                    if (this.FromCartItem)
                    {
                        cartItem.Uid = this.CartItemUid;
                        this._appInstaceService.UpdateCartItemFromBranchItem(cartBranchItem.Uid, cartItem);
                    }
                    else
                    {
                        this._appInstaceService.AddCartItemToBranchItem(cartBranchItem.Uid, cartItem);
                    }
                }
                await Device.InvokeOnMainThreadAsync(() =>
                {
                    UserDialogs.Instance.HideLoading();
                });
                await _navigationServices.GoBackAsync();

            }
            else
            {

                await Device.InvokeOnMainThreadAsync(() =>
                {
                    UserDialogs.Instance.HideLoading();
                });
                await this._pageDialogService.DisplayAlertAsync("", AppResources.AddShoppingCart_InvalidQuantity, AppResources.AlertAcceptText);

            }
        }
        protected async void ViewBranches()
        {
            //var param = new NavigationParameters();
            //param.Add("Data",BranchList);
            //param.Add("ImgInc",ImageInc);
            await _navigationServices.NavigateAsync($"{nameof(ChangeProductBranchPage)}", new NavigationParameters()
                {
                    { "Data", this.BranchList},
                    { "UidCompany", SelectedBranch.UidEmpresa},
                    { "CompanyName", CompanyName},
                    { "ImgInc", this.ImageInc},
                    {"SelectedBrach",SelectedBranch }
                });
        }

        public void Initialize(INavigationParameters parameters)
        {
            Device.InvokeOnMainThreadAsync(() =>
            {
                int count = this._appInstaceService.ProductsCount();
                this._ea.GetEvent<CartCountEvent>().Publish(count);
            });
        }
        #endregion
    }
}
