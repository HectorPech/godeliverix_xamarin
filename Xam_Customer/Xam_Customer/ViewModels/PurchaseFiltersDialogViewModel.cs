﻿using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.ViewModels
{
    public class PurchaseFiltersDialogViewModel : BindableBase, IDialogAware
    {
        public PurchaseFiltersDialogViewModel()
        {

        }

        #region Dialog
        public event Action<IDialogParameters> RequestClose;

        public bool CanCloseDialog() => true;

        public void OnDialogClosed()
        {
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
        }
        #endregion
    }
}
