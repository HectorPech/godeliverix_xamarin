﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.Navigation;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xam_Customer.Views.Layout;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class LocationPageViewModel : BaseViewModel
    {
        #region Properties
        private List<Colonies> _coloniesList;
        public List<Colonies> ColoniesList
        {
            get { return _coloniesList; }
            set { SetProperty(ref _coloniesList, value); }
        }
        private bool _Loading;
        public bool Loading
        {
            get { return _Loading; }
            set { SetProperty(ref _Loading, value); }
        }
        private Colonies _SelectedColonie;
        public Colonies SelectedColonie
        {
            get { return _SelectedColonie; }
            set { SetProperty(ref _SelectedColonie, value); }
        }


        private string _city;
        public string City
        {
            get { return _city; }
            set { SetProperty(ref _city, value); }
        }

        private string _Uidstate;
        public string UidState
        {
            get { return _Uidstate; }
            set { SetProperty(ref _Uidstate, value); }
        }
        private string _state;
        public string State
        {
            get { return _state; }
            set { SetProperty(ref _state, value); }
        }

        private string _country;
        public string Country
        {
            get { return _country; }
            set { SetProperty(ref _country, value); }
        }

        private string _fullAddress;
        public string FullAddress
        {
            get { return _fullAddress; }
            set { SetProperty(ref _fullAddress, value); }
        }
        private double _long;
        public double Longitude
        {
            get { return _long; }
            set { SetProperty(ref _long, value); }
        }
        private double _lat;
        public double Latitude
        {
            get { return _lat; }
            set { SetProperty(ref _lat, value); }
        }


        #region Propiedades para etiquetas estaticas
        private string _label1;
        public string Label1
        {
            get { return _label1; }
            set { SetProperty(ref _label1, value); }
        }
        private string _label2;
        public string Label2
        {
            get { return _label2; }
            set { SetProperty(ref _label2, value); }
        }
        private string _label3;
        public string Label3
        {
            get { return _label3; }
            set { SetProperty(ref _label3, value); }
        }
        #endregion
        #endregion

        #region Command
        public DelegateCommand SetLocationCommand { get; }
        public DelegateCommand GoBackCommand { get; }
        #endregion

        #region Internal
        public IHttpRequestService _httpService { get; set; }
        public IAppInstaceService _appInstaceService { get; set; }
        #endregion

        public LocationPageViewModel(
            INavigationService navigationService,
            IDialogService dialogService,
            IPageDialogService pageDialogService,
            IHttpRequestService httpService,
            IAppInstaceService appInstaceService
            ) : base(navigationService, dialogService, pageDialogService)
        {
            _httpService = httpService;
            _appInstaceService = appInstaceService;
            Label1 = AppResources.LocationSelectColonie;
            Label2 = AppResources.LocationColonieDescription;
            Label3 = AppResources.LocationAvaible;
            this.SetLocationCommand = new DelegateCommand(this.SetLocation);
            this.GoBackCommand = new DelegateCommand(async () => { await navigationService.GoBackAsync(); });
        }

        #region Implementation

        private async void LoadData()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("StrParametroBusqueda", "Giro");
            parameters.Add("StrDia", "miércoles");
            parameters.Add("UidEstado", "1FCE366D-C225-47FD-B4BB-5EE4549FE913");
            parameters.Add("UidColonia", "DD873F59-BF4F-4EEF-8C1B-5CDE89C8D62B");
            parameters.Add("UidBusquedaCategorias", "efaedc66-7834-4066-a634-41244a171175");
            var ColoniesRequest = await this._httpService.GetAsync<IEnumerable<Product>>("Producto/GetBuscarProductosCliente_Movil", parameters);
            if (ColoniesRequest.Code == HttpResponseCode.Success)
            {
            }
        }
        public async void GetColonies()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("StrNombreCiudad", City);
            parameters.Add("CodigoEstado", State);
            parameters.Add("CodigoPais", Country);
            var ColoniesRequest = await this._httpService.GetAsync<IEnumerable<AddressSession>>("Direccion/GetObtenerDireccionConDatosDeGoogle_Movil", parameters);
            if (ColoniesRequest.Code == HttpResponseCode.Success)
            {
                var ciudades = ColoniesRequest.Result.ToList();
                AddressSession ciudad = ciudades[0];
                UidState = ciudad.UidState;
                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add("UidCiudad", ciudad.Uid);
                var ColonieRequest = await this._httpService.GetAsync<IEnumerable<Colonies>>("Direccion/GetObtenerColonias_Movil", param);
                ColoniesList = ColonieRequest.Result.ToList();
                SelectedColonie = ColoniesList[0];
            }
        }
        public async void GetSaveColonie()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("StrNombreCiudad", City);
            parameters.Add("CodigoEstado", State);
            parameters.Add("CodigoPais", Country);
            var ColoniesRequest = await this._httpService.GetAsync<IEnumerable<AddressSession>>("Direccion/GetObtenerDireccionConDatosDeGoogle_Movil", parameters);
            if (ColoniesRequest.Code == HttpResponseCode.Success)
            {
                var ciudades = ColoniesRequest.Result.ToList();
                AddressSession ciudad = ciudades[0];
                UidState = ciudad.UidState;
                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add("UidCiudad", ciudad.Uid);
                var ColonieRequest = await this._httpService.GetAsync<IEnumerable<Colonies>>("Direccion/GetObtenerColonias_Movil", param);
                ColoniesList = ColonieRequest.Result.ToList();
                SelectedColonie = ColoniesList.Find(c => c.Name == _appInstaceService.GetDeliverySelected());
            }
        }

        private void SetLocation()
        {
            Task.Run(async () =>
            {
                if (SelectedColonie == null)
                {
                    await this.PageDialogService.DisplayAlertAsync("", AppResources.SelectALocation, AppResources.AlertAcceptText);
                    return;
                }
                Settings.AddressSession = new Colonies()
                {
                    UidEstado = UidState,
                    Uid = SelectedColonie.Uid,
                    Longitude = Longitude,
                    Latitude = Latitude,
                    Name = SelectedColonie.Name
                };
                _appInstaceService.SelectDeliveryAddress(UidState, SelectedColonie.Uid, SelectedColonie.Name, Longitude, Latitude);

                Device.BeginInvokeOnMainThread(async () =>
                {
                    NavigationParameters pairs = new NavigationParameters();
                    pairs.Add("navigationType", NavigationType.Guest);

                    await NavigationService.NavigateAsync($"/{nameof(NavigationMasterDetailPage)}/NavigationPage/{nameof(StoresPage)}", pairs);
                });
            });
        }
        #endregion
    }
}
