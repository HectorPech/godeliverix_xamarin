﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.System;
using Xam_Customer.Core.Util;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx.Navigation;
using Xam_Customer.Views;
using Xam_Customer.Views.Layout;

namespace Xam_Customer.ViewModels.Layout
{
    public class NavigationMasterDetailPageViewModel : BindableBase, INavigationAware
    {
        #region Properties
        private ObservableCollection<NavigationItem> _navigationItems;
        public ObservableCollection<NavigationItem> NavigationItems
        {
            get { return _navigationItems; }
            set { SetProperty(ref _navigationItems, value); }
        }

        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }
        #endregion

        #region private

        #endregion

        #region Services
        private INavigationService NavigationService { get; }
        #endregion

        public NavigationMasterDetailPageViewModel(
            INavigationService navigationService
            )
        {
            this.NavigationService = navigationService;

            this.NavigateCommand = new DelegateCommand<string>(this.NavigateAsync);

            this.NavigationItems = new ObservableCollection<NavigationItem>();

            LoadUser();

            //AddressSession address = Settings.AddressSession;
            //if (address != null)
            //{

            //}
        }

        #region Command
        public DelegateCommand<string> NavigateCommand { get; }
        #endregion

        #region Implementation
        private void LoadUser()
        {
            UserSession user = Settings.UserSession;
            if (user != null)
            {
                this.Username = $"{user.FisrtName} {user.FisrtLastName}";
            }
            else
            {
                this.Username = "Invitado";
            }
        }
        private void GenerateNavigationItems(NavigationType type)
        {
            List<NavigationItem> navigationItems = new List<NavigationItem>();
            navigationItems.Add(new NavigationItem()
            {
                Title = NavigationLang.Nav_Restaurants,
                Icon = MaterialFontIcons.SilverwareForkKnife,
                NamePage = $"{nameof(StoresPage)}"
            });
            navigationItems.Add(new NavigationItem()
            {
                Title = NavigationLang.Nav_Search,
                Icon = MaterialFontIcons.Magnify,
                NamePage = "Default"
            });
            switch (type)
            {
                case NavigationType.Guest:
                    navigationItems.Add(new NavigationItem()
                    {
                        Title = NavigationLang.Nav_Login,
                        Icon = "\U000f0306",
                        NamePage = "LoginPage"
                    });
                    //navigationItems.Add(new NavigationItem()
                    //{
                    //    Title = NavigationLang.Nav_Restaurants,
                    //    Icon = MaterialFontIcons.SilverwareForkKnife,
                    //    NamePage = ""
                    //});
                    navigationItems.Add(new NavigationItem()
                    {
                        Title = NavigationLang.Nav_Location,
                        Icon = MaterialFontIcons.MapMarker,
                        NamePage = "LocationPage"
                    });

                    navigationItems.Add(new NavigationItem()
                    {
                        Title = NavigationLang.Nav_Contact,
                        Icon = MaterialFontIcons.PhoneOutline,
                        NamePage = $"{nameof(ContactPage)}"
                    });
                    break;
                case NavigationType.Logged:

                    navigationItems.Add(new NavigationItem()
                    {
                        Title = NavigationLang.Nav_Orders,
                        Icon = MaterialFontIcons.History,
                        NamePage = $"{nameof(PurchasesHistoryPage)}"
                    });
                    navigationItems.Add(new NavigationItem()
                    {
                        Title = NavigationLang.Nav_Profile,
                        Icon = MaterialFontIcons.AccountOutline,
                        NamePage = nameof(ProfilePage)
                    });
                    navigationItems.Add(new NavigationItem()
                    {
                        Title = NavigationLang.Nav_About,
                        Icon = MaterialFontIcons.InformationOutline,
                        NamePage = nameof(AboutPage)
                    });

                    navigationItems.Add(new NavigationItem()
                    {
                        Title = NavigationLang.Nav_Contact,
                        Icon = MaterialFontIcons.PhoneOutline,
                        NamePage = $"{nameof(ContactPage)}"
                    });

                    navigationItems.Add(new NavigationItem()
                    {
                        Title = NavigationLang.Nav_Logout,
                        Icon = MaterialFontIcons.Logout,
                        NamePage = "Logout"
                    });
                    break;
                default:
                    break;
            }


            NavigationItems = new ObservableCollection<NavigationItem>();
            this.NavigationItems = new ObservableCollection<NavigationItem>(navigationItems);
        }

        private async void NavigateAsync(string view)
        {
            if (view.Equals("Logout"))
            {
                // Eliminar datos almacenados
                Settings.Clear();

                await this.NavigationService.NavigateAsync($"/{nameof(WelcomePage)} ");
            }
            else if (!string.IsNullOrEmpty(view))
            {
                await this.NavigationService.NavigateAsync($"NavigationPage/{view}");
            }
        }
        #endregion

        #region Navigation  
        /// <summary>
        /// Called when the implementer has been navigated away from.
        /// </summary>
        public void OnNavigatedFrom(INavigationParameters parameters)
        {

        }

        /// <summary>
        /// Called when the implementer has been navigated to.
        /// </summary>
        public void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("navigationType"))
            {
                NavigationType type = parameters.GetValue<NavigationType>("navigationType");
                this.GenerateNavigationItems(type);
                LoadUser();
            }
        }
        #endregion
    }
}
