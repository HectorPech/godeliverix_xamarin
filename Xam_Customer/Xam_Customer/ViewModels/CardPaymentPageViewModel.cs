﻿using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.SignIn;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx.Cart;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class CardPaymentPageViewModel : BindableBase, INavigationAware
    {
        #region Properties
        private string _UidOrden;
        public string UidOrden
        {
            get { return this._UidOrden; }
            set { SetProperty(ref this._UidOrden, value); }
        }
        private decimal _subtotal;
        public decimal Subtotal
        {
            get { return this._subtotal; }
            set { SetProperty(ref this._subtotal, value); }
        }

        private decimal _deliveryRate;
        public decimal DeliveryRate
        {
            get { return this._deliveryRate; }
            set { SetProperty(ref this._deliveryRate, value); }
        }
        private decimal _deliveryTip;
        public decimal DeliveryTip
        {
            get { return this._deliveryTip; }
            set { SetProperty(ref this._deliveryTip, value); }
        }

        private decimal _total;
        public decimal Total
        {
            get { return this._total; }
            set { SetProperty(ref this._total, value); }
        }

        private decimal cardPaymentCommission;
        public decimal CardPaymentCommission
        {
            get { return cardPaymentCommission; }
            set { SetProperty(ref cardPaymentCommission, value); }
        }

        private string Email;
        public string email
        {
            get { return Email; }
            set { SetProperty(ref Email, value); }
        }

        private decimal walletDiscount;
        public decimal WalletDiscount
        {
            get { return walletDiscount; }
            set { SetProperty(ref walletDiscount, value); }
        }

        private bool useEWalletToPayment;
        public bool UseEWalletToPayment
        {
            get { return useEWalletToPayment; }
            set { SetProperty(ref useEWalletToPayment, value); }
        }
        #endregion

        #region Private
        private List<CartBranchItem> _CartItems { get; set; }
        private string _URLPAY;
        public string URLPAY
        {
            get { return this._URLPAY; }
            set { SetProperty(ref this._URLPAY, value); }
        }

        /// <summary>
        /// El total real sin descuentos
        /// </summary>
        public decimal TotalWithTaxes { get; set; }
        #endregion

        #region Internal
        public IAppInstaceService _appInstaceService { get; set; }
        public IHttpRequestService _httpRequestService { get; set; }
        public IPageDialogService _pageDialogService { get; set; }
        public INavigationService _navigationService { get; set; }
        #endregion
        public CardPaymentPageViewModel(IAppInstaceService appInstaceService,
            IHttpRequestService httpRequestService,
            IPageDialogService pageDialogService,
            INavigationService navigationService)
        {
            _httpRequestService = httpRequestService;
            _appInstaceService = appInstaceService;
            _pageDialogService = pageDialogService;
            _navigationService = navigationService;
        }
        #region Implementation
        public async Task ObtenerLigaDePago()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidUsuario", Settings.UserSession.StrUid);
            parameters.Add("UIDPERFIL", "4F1E1C4B-3253-4225-9E46-DD7D1940DA19");
            var userRequest = await this._httpRequestService.GetAsync<UserInformation>("Usuario/GetBuscarUsuarios_Movil", parameters);
            if (userRequest.Code == HttpResponseCode.Success)
            {
                email = userRequest.Result.Email;
            }
            else
            {
                //await this.PageDialogService.DisplayAlertAsync(AppResources.Error, AppResources.UnexpectedErrorOccurred, AppResources.AlertAcceptText);
                await this._navigationService.GoBackAsync();
                return;
            }
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("UidUsuario", Settings.UserSession.StrUid);
            var FolioRequest = await _httpRequestService.GetAsync<string>("Usuario/GetObtenerFolioCliente_Movil", param);
            var Folio = FolioRequest.Result;
            string ArchivoXml = "" +
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" +
                "<P>\r\n  <business>\r\n" +
                "    <id_company>Z937</id_company>\r\n" +
                "    <id_branch>1050</id_branch>\r\n" +
                "    <user>Z937SDUS1</user>\r\n" +
                "    <pwd>09K1HT91B3</pwd>\r\n" +
                "  </business>\r\n" +
                "  <url>\r\n" +
                "    <reference>" + UidOrden + "</reference>\r\n" +
                "    <amount>" + Total + "</amount>\r\n" +
                "    <moneda>MXN</moneda>\r\n" +
                "    <canal>W</canal>\r\n" +
                "    <omitir_notif_default>1</omitir_notif_default>\r\n" +
                "    <st_correo>1</st_correo>\r\n" +
                "    <mail_cliente>" + email + "</mail_cliente>\r\n" +
                "    <datos_adicionales>\r\n" +
                "      <data id=\"1\" display=\"false\">\r\n" +
                "        <label>PRINCIPAL</label>\r\n" +
                "        <value>" + Folio + "</value>\r\n" +
                "      </data>\r\n" +
                "      <data id=\"2\" display=\"true\">\r\n" +
                "        <label>Concepto:</label>\r\n" +
                "        <value>Orden en GoDeliverix.</value>\r\n" +
                "      </data>\r\n" +
                "      <data id=\"3\" display=\"false\">\r\n" +
                "        <label>Color</label>\r\n" +
                "        <value>Azul</value>\r\n" +
                "      </data>\r\n" +
                "    </datos_adicionales>\r\n" +
                "  </url>\r\n" +
                "</P>\r\n";
            string originalString = ArchivoXml;
            string key = "7AACFE849FABD796F6DCB947FD4D5268";
            AESCrypto o = new AESCrypto();
            string encryptedString = o.encrypt(originalString, key);
            string finalString = encryptedString.Replace("%", "%25").Replace(" ", "%20").Replace("+", "%2B").Replace("=", "%3D").Replace("/", "%2F");

            string encodedString = HttpUtility.UrlEncode("<pgs><data0>9265655113</data0><data>" + encryptedString + "</data></pgs>");
            string postParam = "xml=" + encodedString;

            var client = new RestClient("https://bc.mitec.com.mx/p/gen");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("application/x-www-form-urlencoded", postParam, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            //lblRespuesta.Text = o.decrypt(key, content);
            string decryptedString = o.decrypt(key, content);
            string str1 = decryptedString.Replace("<P_RESPONSE><cd_response>success</cd_response><nb_response></nb_response><nb_url>", "");
            URLPAY = str1.Replace("</nb_url></P_RESPONSE>", "");

        }

        internal async void SendOrder()
        {
            UserSession session = Settings.UserSession;

            for (int i = 0; i < _CartItems.Count; i++)
            {
                Random Codigo = new Random();
                long DeliveryCode = Codigo.Next(1000, 9999);
                decimal TotalBranch = 0.00m;
                string UidOrderBranch = Guid.NewGuid().ToString();

                this.Subtotal = this._CartItems.Sum(c => c.ItemsSubtotal);
                this.DeliveryRate = this._CartItems.Sum(c => (c.DeliveryRate));
                this.DeliveryTip = this._CartItems.Sum(c => (c.DeliveryTips));
                this.CardPaymentCommission = this._CartItems.Sum(c => c.SubtotalCommissions);
                this.Total = this.Subtotal + this.DeliveryRate + DeliveryTip + this.CardPaymentCommission;
                TotalBranch = this._CartItems[i].CartItems.Sum(o => o.Total);

                foreach (var item in _CartItems[i].CartItems)
                {
                    string UidNote = Guid.Empty.ToString();
                    if (item.HasNotes)
                    {
                        UidNote = Guid.NewGuid().ToString();
                    }
                    Dictionary<string, string> parametros = new Dictionary<string, string>();
                    parametros.Add("UIDORDEN", UidOrderBranch);
                    parametros.Add("UIDSECCIONPRODUCTO", item.Uid.ToString());
                    parametros.Add("INTCANTIDAD", item.Quantity.ToString());
                    parametros.Add("STRCOSTO", item.Total.ToString());
                    parametros.Add("UidSucursal", _CartItems[i].UidBranch.ToString());
                    parametros.Add("UidRegistroEncarrito", item.CartId.ToString());
                    parametros.Add("UidNota", UidNote);
                    parametros.Add("StrMensaje", item.Notes);
                    parametros.Add("UidTarifario", _CartItems[i].UidDelivery);
                    //Envia los productos a la base de datos
                    await _httpRequestService.GetEmptyAsync("Orden/GetGuardarProductos_Movil", parametros);

                }

                //Envia la orden a la base de datos
                //Dictionary<string, string> param = new Dictionary<string, string>();
                //param.Add("UIDORDEN", UidOrden);
                //param.Add("Total", Total.ToString());
                //param.Add("Uidusuario", Settings.UserSession.StrUid);
                //param.Add("UidDireccion", _appInstaceService.GetUserSelectedDeliveryAddress());
                //param.Add("Uidsucursal", _CartItems[i].UidBranch.ToString());
                //param.Add("totalSucursal", TotalBranch.ToString());
                //param.Add("UidRelacionOrdenSucursal", UidOrderBranch);
                //param.Add("LngCodigoDeEntrega", DeliveryCode.ToString());
                //param.Add("UidTarifario", _CartItems[i].UidDelivery);
                //await _httpRequestService.GetEmptyAsync("Orden/GetGuardarOrden_Movil", param);

                // Guardar orden de la sucursal
                BranchePaymentRequest request = new BranchePaymentRequest()
                {
                    UidOrden = Guid.Parse(UidOrden),
                    UidDireccion = Guid.Parse(_appInstaceService.GetUserSelectedDeliveryAddress()),
                    UidRelacionOrdenSucursal = Guid.Parse(UidOrderBranch),
                    UidSucursal = this._CartItems[i].UidBranch,
                    UidTarifario = Guid.Parse(this._CartItems[i].UidDelivery),
                    UidUsuario = Guid.Parse(session.StrUid),
                    Monto = this.TotalWithTaxes,
                    MontoSucursal = TotalBranch,
                    CodigoEntrega = DeliveryCode,
                    DescuentoMonedero = this._CartItems[i].WalletDiscount,
                    ComisionTarjeta = this._CartItems[i].OrderSubtotalComission,
                    ComisionTarjetaRepartidor = (this._CartItems[i].DeliveryRateSubtotalComission + this._CartItems[i].DeliveryTipsSubtotalComission),
                    IncludeCPTS = this._CartItems[i].SupplierIncludeCardPaymentCommission,
                    IncludeCPTD = this._CartItems[i].DeliveryIncludeCardPaymentCommission,
                };

                await this._httpRequestService.PostAsync("Payments/RegistryBranchePayment", JsonConvert.SerializeObject(request));

                //Envia la relacion al tarifario
                Dictionary<string, string> parametro = new Dictionary<string, string>();
                parametro.Add("UidOrdenSucursal", UidOrderBranch);
                parametro.Add("DPropina", _CartItems[i].DeliveryTips.ToString());
                parametro.Add("UidTarifario", _CartItems[i].UidDelivery.ToString());
                await _httpRequestService.GetEmptyAsync("Tarifario/GetGuardarTarifario_Movil", parametro);
                //Agrega el estatus a la orden
                parametro = new Dictionary<string, string>();
                parametro.Add("UidEstatus", "DE294EFC-C549-4DDD-A0D1-B0E1E2039ECC");
                parametro.Add("StrParametro", "U");
                parametro.Add("UidUsuario", Settings.UserSession.StrUid);
                parametro.Add("UidOrden", UidOrderBranch);
                parametro.Add("UidSucursal", _CartItems[i].UidBranch.ToString());
                await _httpRequestService.GetEmptyAsync("Orden/GetAgregaEstatusALaOrden_Movil", parametro);
            }
            //Aqui se llama el tipo de pago
            Dictionary<string, string> pagoParam = new Dictionary<string, string>();
            pagoParam.Add("UIDORDEN", UidOrden);
            pagoParam.Add("UidPago", Guid.NewGuid().ToString());
            pagoParam.Add("UidFormaDeCobro", "30545834-7FFE-4D1A-AA94-D6E569371C60");
            pagoParam.Add("MMonto", Total.ToString());
            pagoParam.Add("UidEstatusDeCobro", "E728622B-97D7-431F-B01C-7E0B5F8F3D31");
            await _httpRequestService.GetEmptyAsync("Pagos/GetInsertarPago_Movil", pagoParam);
            _appInstaceService.ClearCart();
            await Device.InvokeOnMainThreadAsync(async () =>
            {
                await _pageDialogService.DisplayAlertAsync("", CartLang.Cart_Paid + " " + CartLang.Payment_Card, CartLang.Paymetn_Ok);
            });

            await Device.InvokeOnMainThreadAsync(async () => { await _navigationService.GoBackToRootAsync(); });
        }
        #endregion
        #region Navigation
        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            parameters.Add("Data", AvailablePaymentType.CreditDebitCard);
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                this._CartItems = this._appInstaceService.ReadAllCartBranchItems();
                this.Subtotal = this._CartItems.Sum(c => c.ItemsSubtotal);
                this.DeliveryRate = this._CartItems.Sum(c => (c.DeliveryRateSubtotal));
                this.DeliveryTip = this._CartItems.Sum(c => (c.DeliveryTipsSubtotal));
                this.CardPaymentCommission = this._CartItems.Sum(c => c.TotalCommissions);

                this.WalletDiscount = 0;
                this.UseEWalletToPayment = false;

                if (this._CartItems.Where(i => i.WalletDiscount.HasValue).Any())
                {
                    this.UseEWalletToPayment = true;
                    this.WalletDiscount = this._CartItems.Sum(i => i.WalletDiscount.HasValue ? i.WalletDiscount.Value : 0);
                }

                this.Total = (this.Subtotal + this.DeliveryRate + DeliveryTip + this.CardPaymentCommission) - this.WalletDiscount;

                this.TotalWithTaxes = this.Subtotal + this.DeliveryRate + DeliveryTip + this.CardPaymentCommission;

                UidOrden = Guid.NewGuid().ToString();
                await ObtenerLigaDePago();
            });
        }
        #endregion
    }
}
