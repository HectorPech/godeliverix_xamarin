﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Resources.LangResx;

namespace Xam_Customer.ViewModels
{
    public class CartProductDescriptionViewModel : BindableBase, INavigatedAware
    {

        private Product _SelectedProduct;
        public Product Selectedproduct
        {
            get => _SelectedProduct;
            set => SetProperty(ref _SelectedProduct, value);
        }

        private string _Name;
        public string Name
        {
            get => _Name;
            set => SetProperty(ref _Name, value);
        }

        private string _ImagePath;
        public string ImagePath
        {
            get => _ImagePath;
            set => SetProperty(ref _ImagePath, value);
        }

        private string _ImageInc;
        public string ImageInc
        {
            get => _ImageInc;
            set => SetProperty(ref _ImageInc, value);
        }

        private string _Description;
        public string Description
        {
            get => _Description;
            set => SetProperty(ref _Description, value);
        }

        private string _TotalAAgregarEnCarrito;
        public string AmountShopCard
        {
            get => _TotalAAgregarEnCarrito;
            set => SetProperty(ref _TotalAAgregarEnCarrito, value);
        }


        private int _Quantity;
        public int Quantity
        {
            get => _Quantity;
            set => SetProperty(ref _Quantity, value);
        }

        private string _notes;
        public string Notes
        {
            get { return _notes; }
            set { SetProperty(ref _notes, value); }
        }

        /// <summary>
        /// Determinar el el producto proviene del carrito
        /// </summary>
        private bool FromCartItem { get; set; }
        private Guid CartItemUid { get; set; }
        private Guid CartBranchItemUid { get; set; }

        #region Branch properties

        private string _identifier;

        public string Identifier
        {
            get => _identifier;
            set => SetProperty(ref _identifier, value);
        }

        private string _availability;

        public string availability
        {
            get => _availability;
            set => SetProperty(ref _availability, value);
        }
        private decimal _price;

        public decimal price
        {
            get => _price;
            set => SetProperty(ref _price, value);
        }
        #endregion

        #region Internal
        private IHttpRequestService _HttpService { get; }
        private INavigationService _navigationServices { set; get; }
        private IAppInstaceService _appInstaceService { set; get; }
        #endregion

        #region Command
        public DelegateCommand CmdAdd { get; set; }
        public DelegateCommand CmdQuit { get; set; }

        public DelegateCommand RemoveCommand { get; set; }

        public DelegateCommand ConfirmChangesCommand { get; set; }
        #endregion

        public CartProductDescriptionViewModel(
            INavigationService navigationService,
            IHttpRequestService httpRequestService,
            IAppInstaceService appInstaceService)
        {
            _appInstaceService = appInstaceService;
            _HttpService = httpRequestService;
            _navigationServices = navigationService;
            CmdAdd = new DelegateCommand(AddOne);
            CmdQuit = new DelegateCommand(QuitOne);

            this.RemoveCommand = new DelegateCommand(this.Remove);
            this.ConfirmChangesCommand = new DelegateCommand(this.Confirm);
        }

        #region Navigation aware
        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            // throw new NotImplementedException();
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {

            if (parameters.ContainsKey("Data"))
            {
                this.Selectedproduct = parameters.GetValue<Product>("Data");
                //Cargaproducto();
                ImagePath = Selectedproduct.ImgPathProduct;
                Quantity = Selectedproduct.Quantity;
                Name = Selectedproduct.Name;
                Description = Selectedproduct.Description;
                price = Selectedproduct.Price;
                this.Notes = Selectedproduct.Notes ?? "";
            }

            if (parameters.ContainsKey("CartItemId") && parameters.ContainsKey("CartBranchItemId"))
            {
                this.CartItemUid = parameters.GetValue<Guid>("CartItemId");
                this.CartBranchItemUid = parameters.GetValue<Guid>("CartBranchItemId");
                this.PopulateDataFromCartItem();
            }

            //var mode = parameters.GetNavigationMode();
            //switch (mode)
            //{
            //    case NavigationMode.New:
            //        if (parameters.ContainsKey("Data"))
            //        {
            //            this.Selectedproduct = parameters.GetValue<Product>("Data");
            //            Cargaproducto();
            //        }
            //        break;
            //    case NavigationMode.Back:

            //        break;
            //    case NavigationMode.Forward:
            //        break;
            //}
        }
        #endregion

        #region Implementation
        //private async void Cargaproducto()
        //{
        //    Name = Selectedproduct.Name;
        //    ImagePath = Selectedproduct.ImgPathProduct;
        //    ImageInc = Selectedproduct.ImgPathCompany;
        //    Description = Selectedproduct.Description;
        //    Dictionary<string, string> parametro = new Dictionary<string, string>();
        //    parametro.Add("StrParametroBusqueda", _appInstaceService.GetSearchType());
        //    parametro.Add("StrDia", _appInstaceService.GetDayOfTheWeek());
        //    parametro.Add("UidEstado", _appInstaceService.GetDeliveryState());
        //    parametro.Add("UidColonia", _appInstaceService.GetDeliveryColonie());
        //    parametro.Add("UidBusquedaCategorias", _appInstaceService.GetUidFilter());
        //    parametro.Add("UidProducto", Selectedproduct.StrUid);
        //    var ProductRequest = await this._HttpService.GetAsync<IEnumerable<Branch>>("Producto/GetObtenerInformacionDeProductoDeLaSucursal_Movil", parametro);
        //    BranchList = ProductRequest.Result.ToList();
        //    Dictionary<string, string> param = new Dictionary<string, string>();
        //    param.Add("UIDSECCION", SelectedBranch.UidSeccion);
        //    param.Add("UidEstado", "1FCE366D-C225-47FD-B4BB-5EE4549FE913");
        //    param.Add("UidColonia", "DD873F59-BF4F-4EEF-8C1B-5CDE89C8D62B");
        //    var SeccionRequest = await this._HttpService.GetAsync<Seccion>("Seccion/GetBuscarSeccion_movil", param);
        //    var obj = SeccionRequest.Result as Seccion;
        //    availability = AppResources.ProductAvailability + obj.FinishTime;
        //    Identifier = SelectedBranch.Identificador;
        //    price = SelectedBranch.Costoproducto;
        //    AmountShopCard = AppResources.AddShoppingCart + "  $" + SelectedBranch.Costoproducto.ToString();
        //}

        /// <summary>
        /// Rellenar la informacion con los datos del item del carrito
        /// </summary>
        /// <param name="uid"></param>
        private void PopulateDataFromCartItem()
        {
            var item = this._appInstaceService.GetCartItemByIdFromBranch(this.CartBranchItemUid, this.CartItemUid);
            if (item != null)
            {
                var product = item.CartItems.Find(p => p.CartId == this.CartItemUid);
                this.FromCartItem = true;
                this.Quantity = product.Quantity;
                this.Notes = product.Notes;
            }
        }

        private void AddOne()
        {
            var ProductPrice = (price / Quantity);
            Quantity += 1;
            price = (ProductPrice * Quantity);
        }

        private void QuitOne()
        {
            int newQuantity = this.Quantity - 1;
            if (newQuantity == 0)
            {
                return;
            }

            var ProductPrice = (price / Quantity);
            Quantity -= 1;
            price = (ProductPrice * Quantity);

        }

        private async void Remove()
        {
            this._appInstaceService.RemoveCartItemFromBranch(this.CartBranchItemUid, this.CartItemUid);

            await this._navigationServices.GoBackAsync();
        }

        private void Confirm()
        {
            CartItem cartItem = new CartItem()
            {
                Uid = new Guid(Selectedproduct.UidSeccionPoducto),
                UidProduct = Guid.Parse(Selectedproduct.StrUid),
                ImgPathProduct = this.Selectedproduct.ImgPathProduct,
                Notes = this.Notes.Trim(),
                ProductDescription = this.Description,
                ProductName = Selectedproduct.Name,
                Quantity = this.Quantity,
                UnitPrice = price / Quantity
            };

            this._appInstaceService.UpdateCartItemFromBranchItem(CartBranchItemUid, cartItem);

            Xamarin.Forms.Device.BeginInvokeOnMainThread(async () =>
            {
                await this._navigationServices.GoBackAsync();
            });
        }
        #endregion
    }
}
