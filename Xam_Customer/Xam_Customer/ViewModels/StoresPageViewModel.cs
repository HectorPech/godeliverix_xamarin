using Prism.AppModel;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Events;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.Core;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Util;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xam_Customer.Views.RgPopup;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class StoresPageViewModel : BaseNavigationViewModel, IPageLifecycleAware, IInitialize
    {
        #region Properties
        private ObservableCollection<CompanyStoreGrid> stores;
        public ObservableCollection<CompanyStoreGrid> Stores
        {
            get { return stores; }
            set { SetProperty(ref stores, value); }
        }

        private string search;
        public string Search
        {
            get { return search; }
            set { SetProperty(ref search, value); }
        }

        private bool isLoading;
        public bool IsLoading
        {
            get { return isLoading; }
            set { SetProperty(ref isLoading, value); }
        }

        private string resultSummary;
        public string ResultSummary
        {
            get { return resultSummary; }
            set { SetProperty(ref resultSummary, value); }
        }

        private string deliveryLocation;
        public string DeliveryLocation
        {
            get { return deliveryLocation; }
            set { SetProperty(ref deliveryLocation, value); }
        }

        private bool noStores;
        public bool NoStores
        {
            get { return noStores; }
            set { SetProperty(ref noStores, value); }
        }

        #endregion

        #region Services
        private IHttpRequestService HttpService { get; }
        private IAppInstaceService Instance { get; }
        private IEventAggregator EventAggregator { get; }
        private UserSession user { get; }
        #endregion

        #region Private
        private StoresFilter Filters { get; set; }
        private int PageIndex { get; set; }
        private int ListCount { get; set; }
        private bool IsLoadingPopup { get; set; }
        #endregion

        #region Commands
        public DelegateCommand ShowFilterCommand { get; }
        public DelegateCommand SearchCommand { get; }
        public DelegateCommand<string> ListViewSelectedItemCommmand { get; }
        public DelegateCommand GoToCart { get; }
        public DelegateCommand ChangeLocationCommand { get; }
        public DelegateCommand DisplayFilterDialogCommand { get; }
        #endregion

        public StoresPageViewModel(
            INavigationService navigationService,
            IHttpRequestService httpRequestService,
            IAppInstaceService appInstaceService,
            IEventAggregator eventAggregator
            ) : base(navigationService)
        {
            this.HttpService = httpRequestService;
            this.Instance = appInstaceService;
            this.EventAggregator = eventAggregator;

            this.ListViewSelectedItemCommmand = new DelegateCommand<string>(this.ListViewSelectedItem);
            this.ShowFilterCommand = new DelegateCommand(async () =>
            {
                if (this.IsLoadingPopup) return;

                this.IsLoadingPopup = true;
                await PopupNavigation.Instance.PushAsync(new StoresFilterDialog(this.EventAggregator, this.HttpService, this.Instance));
            });
            this.SearchCommand = new DelegateCommand(async () =>
            {
                DependencyService.Get<IKeyboardHelper>().HideKeyboard();

                this.PageIndex = 0;
                this.ListCount = 0;
                this.Stores.Clear();
                await this.ReadAllStores();
            });
            this.GoToCart = new DelegateCommand(async () => await this.NavigationService.NavigateAsync($"{nameof(CartPage)}"));
            this.ChangeLocationCommand = new DelegateCommand(this.ChangeLocation);

            this.Stores = new ObservableCollection<CompanyStoreGrid>();
            this.user = Settings.UserSession;

            LoadDeliveryAddres();

            this.Filters = new StoresFilter() { UidType = Guid.Empty, UidCategory = Guid.Empty, UidSubcategory = Guid.Empty };
            this.EventAggregator.GetEvent<StoresFilterChangedEvent>().Subscribe(this.Filter);

            this.DisplayFilterDialogCommand = new DelegateCommand(async () =>
            {
                if (this.IsLoadingPopup) return;

                this.IsLoadingPopup = true;
                await PopupNavigation.Instance.PushAsync(new ProductFilterDialog(this.Instance, this.EventAggregator, DialogSource.StoresPage));
                this.IsLoadingPopup = false;
            });
            this.EventAggregator.GetEvent<StoreFilterChangedEvent>().Subscribe(async (StoreFilter obj) =>
            {
                FilterParameterType parameterType = FilterParameterType.None;
                Guid uidParameter = Guid.Empty;

                if (obj.UidSubcategory != Guid.Empty)
                {
                    parameterType = FilterParameterType.Subcategoria;
                    uidParameter = obj.UidSubcategory;
                }
                else if (obj.UidCategory != Guid.Empty)
                {
                    parameterType = FilterParameterType.Categoria;
                    uidParameter = obj.UidCategory;
                }
                else if (obj.UidType != Guid.Empty)
                {
                    parameterType = FilterParameterType.Giro;
                    uidParameter = obj.UidType;
                }

                this.Instance.StoreFilter = obj;
                this.Instance.StoreFilter.Parameter = parameterType;
                this.Instance.StoreFilter.UidSelected = uidParameter;

                this.PageIndex = 0;
                this.ListCount = 0;
                this.Stores.Clear();
                await Device.InvokeOnMainThreadAsync(async () =>
{
    await this.ReadAllStores();
});
            });
        }

        #region Implementation
        private void LoadDeliveryAddres()
        {
            if (Settings.UserSession == null)
            {
                this.DeliveryLocation = AppResources.DefaultDelivery + " " + Instance.GetDeliverySelected();
            }
            else
            {
                if (!string.IsNullOrEmpty(Instance.GetUserDeliverySelected()))
                {
                    this.DeliveryLocation = AppResources.DefaultDelivery + " " + Instance.GetUserDeliverySelected();
                }
                else
                {
                    Device.InvokeOnMainThreadAsync(async () => { await this.NavigationService.NavigateAsync($"{nameof(ProfileAddressesPage)}"); });
                }
            }
        }
        public async Task ReadAllStores()
        {
            if (this.IsLoading)
                return;

            //if (this.Stores.Count() > 0 && this.Stores.Count() == this.ListCount)
            //{
            //    this.IsLoading = false;
            //    return;
            //}
            Stores.Clear();
            this.IsLoading = true;

            List<CompanyStoreGrid> lStores = new List<CompanyStoreGrid>();
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("TipoFiltro", Instance.StoreFilter.Parameter.ToString());
            parameters.Add("Dia", Instance.GetDayOfTheWeek());
            parameters.Add("UidEstado", Instance.GetDeliveryState());
            parameters.Add("UidColonia", Instance.GetDeliveryColonie());
            parameters.Add("UidFiltro", Instance.StoreFilter.UidSelected.ToString());
            parameters.Add("PageNumber", this.PageIndex.ToString());
            parameters.Add("PageSize", ApplicationConstants.RowsToShow);

            parameters.Add("SortField", this.Instance.StoreFilter.OrderBy.Value);
            parameters.Add("SortDirection", this.Instance.StoreFilter.OrderBy.Direction);

            if (Search != null)
            {
                if (!string.IsNullOrEmpty(this.Search.Trim()))
                {
                    parameters.Add("filtro", this.Search.Trim());
                }
            }

            parameters.Add("Available", this.Instance.StoreFilter.OnlyAvailable ? "true" : "false");

            var request = await this.HttpService.GetAsync<CommonDataSource<CompanyStoreGrid>>("Store/ReadAllCompanies", parameters);

            if (request.Code == HttpResponseCode.Success)
            {
                this.NoStores = !request.Result.Payload.Any() && this.Stores.Count == 0;

                foreach (CompanyStoreGrid store in request.Result.Payload)
                {
                    string branchText = store.AvailableBranches > 1 ? $"{store.AvailableBranches} {AppResources.Branches}" : $"{store.AvailableBranches} {AppResources.Branch}";
                    store.AvailableBranchesSummary = branchText;
                    store.ImgUrl = "https://www.godeliverix.net/Vista/" + store.ImgUrl;

                    if (!store.Available)
                    {
                        if (store.BeforeOpen)
                            store.AvailableText = $"{AppResources.AvailableFrom} {store.OpenAt}";
                        else if (store.AfterClose)
                            store.AvailableText = AppResources.Closed;
                        else
                            store.AvailableText = AppResources.NotAvailable;
                    }

                    lStores.Add(store);
                }

                this.ListCount = request.Result.Count;

                if (!NoStores)
                    this.PageIndex++;

                this.ResultSummary = $"{lStores.Count()} {AppResources.Of} {this.ListCount} {AppResources.Results}";
            }

            this.Stores = new ObservableCollection<CompanyStoreGrid>(lStores);
            this.IsLoading = false;
        }

        private async void ListViewSelectedItem(string uid)
        {
            await this.NavigationService
                .NavigateAsync(nameof(StoreDetailPage), new NavigationParameters()
                {
                    { "uid", Guid.Parse(uid) }
                });
        }

        private async void ChangeLocation()
        {
            if (user == null)
            {
                await this.NavigationService.NavigateAsync($"/{nameof(LocationPage)}");
            }
            else
            {
                NavigationParameters parameters = new NavigationParameters();
                parameters.Add("Seleccion", "Seleccion");
                await this.NavigationService.NavigateAsync($"{nameof(ProfileAddressesPage)}", parameters);
            }
        }

        private async void Filter(StoresFilter filter)
        {
            this.Filters = filter;
            await PopupNavigation.Instance.PopAsync();
            await this.ReadAllStores();
        }

        public override void Initialize(INavigationParameters parameters)
        {
            Task.Run(async () =>
            {
                this.PageIndex = 0;
                this.ListCount = 0;
                this.Stores.Clear();
                await this.ReadAllStores();
            });

            Device.InvokeOnMainThreadAsync(() =>
            {
                int count = this.Instance.ProductsCount();
                this.EventAggregator.GetEvent<CartCountEvent>().Publish(count);
            });
            LoadDeliveryAddres();
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            Device.InvokeOnMainThreadAsync(() =>
            {
                if (parameters.ContainsKey("Pago"))
                {
                    Device.InvokeOnMainThreadAsync(async () =>
                    {
                        await NavigationService.NavigateAsync($"{nameof(PurchasesHistoryPage)}");
                    });
                }
                int count = this.Instance.ProductsCount();
                this.EventAggregator.GetEvent<CartCountEvent>().Publish(count);
            });
            LoadDeliveryAddres();
        }
        #endregion

        #region Page Lifecycle
        public void OnDisappearing()
        {
        }

        public void OnAppearing()
        {
            this.PageIndex = 0;
            Device.InvokeOnMainThreadAsync(async () => { LoadDeliveryAddres(); await this.ReadAllStores(); });
        }
        #endregion
    }
}


public class Busqueda
{
    List<StoreListView> EmpresasDisponibles { get; set; }
}