﻿using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.PurchaseHistory;
using Xam_Customer.ViewModels.Core;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class OrderHistoryPageViewModel : BaseViewModel
    {
        #region Property
        private List<Order> _orders;
        public List<Order> Orders
        {
            get { return _orders; }
            set { SetProperty(ref _orders, value); }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { _isBusy = value; }
        }
        #endregion

        #region Command
        public DelegateCommand<string> ViewOrderDetailCommand { get; set; }
        #endregion

        #region Services
        private readonly IHttpRequestService _HttpService;
        private readonly IAppInstaceService _appInstaceService;
        #endregion

        public OrderHistoryPageViewModel(
                INavigationService navigationService,
                IDialogService dialogService,
                IPageDialogService pageDialogService,
                IHttpRequestService httpRequestService,
                IAppInstaceService appInstaceService
            ) : base(navigationService, dialogService, pageDialogService)
        {
            this._HttpService = httpRequestService;
            this._appInstaceService = appInstaceService;
            this.ViewOrderDetailCommand = new DelegateCommand<string>(this.ViewOrderDetail, this.CanSubmit);
        }

        #region Implementation
        private async void ViewOrderDetail(string StrUid)
        {
            Guid uid = Guid.Parse(StrUid);

            var status = await this.NavigationService.NavigateAsync($"", new NavigationParameters() {
                { "uid", uid }
            });

            if (status.Success)
            {

            }
        }

        private async Task ReadAllOrders()
        {
            this.IsBusy = true;

            // Validar acceso a internet
            if (Connectivity.NetworkAccess != NetworkAccess.Internet)
            {
                this.IsBusy = false;
                await this.NetworkError("OrderHistoryPage");
                return;
            }

            // Realizar solicitud HTTP
            Dictionary<string, string> pairs = new Dictionary<string, string>();
            pairs.Add("UidUsuario", Settings.UserSession.StrUid);
            pairs.Add("parametro", "Usuario");
            var orderRequest = await this._HttpService.GetAsync<IEnumerable<Order>>("Orden/GetObtenerOrdenesCliente_Movil", pairs);

            // Validar Respuesta
            if (orderRequest.Code == HttpResponseCode.ServerError)
            {
                this.IsBusy = false;
                this.ServiceNotAvailable("OrderHistoryPage");
            }
            else if (orderRequest.Code == HttpResponseCode.Failed)
            {
                this.IsBusy = false;
                await this.PageDialogService.DisplayAlertAsync(AppResources.Error_General_Title, AppResources.Error_General_Message, AppResources.Error_General_Confirm);
            }
            else if (orderRequest.Code == HttpResponseCode.Success)
            {
                this.IsBusy = false;

                if (orderRequest.Result.Any())
                {
                    Orders = orderRequest.Result.ToList();
                    foreach (Order order in Orders)
                    {
                        switch (order.UidStatus)
                        {
                            case "Cancelado":
                                order.StatusNameTranslated = PurchaseHistoryLang.PaymentStatus_Cancelado;
                                break;
                            case "Denegado":
                                order.StatusNameTranslated = PurchaseHistoryLang.PaymentStatus_Denegado;
                                break;
                            case "Error":
                                order.StatusNameTranslated = PurchaseHistoryLang.PaymentStatus_Error;
                                break;
                            case "Pagado":
                                order.StatusNameTranslated = PurchaseHistoryLang.PaymentStatus_Pagado;
                                order.StatusColor = Color.FromHex("#254e70");
                                break;
                            case "Pendiente":
                                order.StatusNameTranslated = PurchaseHistoryLang.PaymentStatus_Pendiente;
                                order.StatusColor = Color.FromHex("#c33c54"); 
                                break;
                            case "Reembolsado":
                                order.StatusNameTranslated = PurchaseHistoryLang.PaymentStatus_Reembolsado;
                                break;
                            default:
                                order.StatusNameTranslated = PurchaseHistoryLang.PaymentStatus_Undefined;
                                break;
                        }
                    }
                }
            }
        }

        bool CanSubmit(string value)
        {
            return !this.IsBusy;
        }

        #endregion

        public override void Initialize(INavigationParameters parameters)
        {
            Task.Run(async () => { await this.ReadAllOrders(); });
        }
    }
}
