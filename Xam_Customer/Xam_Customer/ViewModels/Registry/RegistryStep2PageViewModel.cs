﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Model.SignIn;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Views.Registry;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class RegistryStep2PageViewModel : BindableBase
    {
        #region Properties
        private bool _ValidUser;
        public bool ValidUser
        {
            get { return _ValidUser; }
            set { SetProperty(ref _ValidUser, value); }
        }
        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private string _confirmPassword;
        public string ConfirmPassword
        {
            get { return _confirmPassword; }
            set { SetProperty(ref _confirmPassword, value); }
        }
        #endregion

        #region Internal
        private readonly INavigationService _navigationService;
        private readonly IPageDialogService pageDialogService;
        private readonly IHttpRequestService _HttpService;
        #endregion

        #region Command
        public DelegateCommand BackCommand { get; set; }
        public DelegateCommand NextCommand { get; set; }
        public DelegateCommand GoBackCommand { get; }
        #endregion

        public RegistryStep2PageViewModel(
            INavigationService navigationService,
            IPageDialogService pageDialogService,
            IHttpRequestService httpRequestService
            )
        {
            _HttpService = httpRequestService;
            _navigationService = navigationService;
            this.pageDialogService = pageDialogService;

            this.BackCommand = new DelegateCommand(this.Back);
            this.NextCommand = new DelegateCommand(this.Next);
            this.GoBackCommand = new DelegateCommand(async () => { await this._navigationService.GoBackAsync(); });
        }

        #region Implementation
        public void UserAvaible()
        {
            Task.Run(() => { CheckUser(); });
        }
        public void CheckUser()
        {
            //Dictionary<string, string> param = new Dictionary<string, string>();
            //param.Add("USER", Username);
            //param.Add("UIDPERFIL", "4f1e1c4b-3253-4225-9e46-dd7d1940da19");
            //var UserRequest = await this._HttpService.GetAsync<UserInformation>("Usuario/GetBuscarUsuarios_movil", param);
            //var user = UserRequest.Result;
            //if (user.UserList.Count == 0)
            //{
            ValidUser = true;
            //}
            //else
            //{
            //    //await this.pageDialogService.DisplayAlertAsync("", AppResources.SignInStepTwo_ValidUser, AppResources.AlertAcceptText);
            //    ValidUser = false;
            //}
        }
        private async void Back()
        {
            await this._navigationService.GoBackAsync();
        }

        private async void Next()
        {


            this.CheckUser();

            //if (string.IsNullOrEmpty(this.Username))
            //{
            //    await this.pageDialogService.DisplayAlertAsync("", AppResources.SignInStepTwo_RequiredUsername, AppResources.AlertAcceptText);
            //    return;
            //}
            if (!ValidUser)
            {
                await this.pageDialogService.DisplayAlertAsync("", AppResources.SignInStepTwo_ValidUser, AppResources.AlertAcceptText);
                return;
            }
            if (string.IsNullOrEmpty(this.Password) || string.IsNullOrEmpty(this.ConfirmPassword))
            {
                await this.pageDialogService.DisplayAlertAsync("", AppResources.SignInStepTwo_RequiredPassword, AppResources.AlertAcceptText);
                return;
            }

            if (!this.Password.Equals(this.ConfirmPassword))
            {
                await this.pageDialogService.DisplayAlertAsync("", AppResources.SignInStepTwo_InvalidPassword, AppResources.AlertAcceptText);
                return;
            }

            SignInData data = new SignInData()
            {
                Username = this.Username,
                Password = this.Password,
                ConfirmPassword = this.ConfirmPassword
            };

            await this._navigationService.NavigateAsync($"{nameof(RegistryStep3Page)}", new NavigationParameters()
                {
                    { "data", data}
                });

        }
        #endregion
    }
}
