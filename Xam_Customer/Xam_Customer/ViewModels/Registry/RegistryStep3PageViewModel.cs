﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Model.SignIn;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Views.About;
using Xam_Customer.Views.Registry;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class RegistryStep3PageViewModel : BindableBase, INavigationAware
    {
        #region Properties
        private List<PhoneLada> _LadaList;
        public List<PhoneLada> LadaList
        {
            get { return _LadaList; }
            set { SetProperty(ref _LadaList, value); }
        }
        private PhoneLada _Lada;

        public PhoneLada Lada
        {
            get { return _Lada; }
            set { SetProperty(ref _Lada, value); }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        private string _firstLastName;
        public string FirstLastName
        {
            get { return _firstLastName; }
            set { SetProperty(ref _firstLastName, value); }
        }

        private string _secondLastName;
        public string SecondLastName
        {
            get { return _secondLastName; }
            set { SetProperty(ref _secondLastName, value); }
        }

        private bool _ValidEmail;
        public bool Validemail
        {
            get { return _ValidEmail; }
            set { SetProperty(ref _ValidEmail, value); }
        }
        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        private string _phoneNumber;
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { SetProperty(ref _phoneNumber, value); }
        }

        private DateTime _birthday;
        public DateTime Birthday
        {
            get { return _birthday; }
            set { SetProperty(ref _birthday, value); }
        }
        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }
        #endregion

        #region Atributes
        private SignInData _SignInData { get; set; }
        #endregion

        #region Internal
        private readonly INavigationService _navigationService;
        private readonly IPageDialogService _pageDialogService;
        private readonly IHttpRequestService _HttpService;
        #endregion

        #region Command
        public DelegateCommand BackCommand { get; set; }
        public DelegateCommand NextCommand { get; set; }
        public DelegateCommand GoBackCommand { get; }
        #endregion

        public RegistryStep3PageViewModel(
            INavigationService navigationService,
            IPageDialogService pageDialogService,
            IHttpRequestService httpRequestService
            )
        {
            this._HttpService = httpRequestService;
            this._navigationService = navigationService;
            this._pageDialogService = pageDialogService;

            this.BackCommand = new DelegateCommand(this.Back);
            this.NextCommand = new DelegateCommand(this.Next);
            this.GoBackCommand = new DelegateCommand(async () => { await this._navigationService.GoBackAsync(); });
            LadaList = new List<PhoneLada>();
            this.Birthday = DateTime.Today;
        }

        #region Implementation
        private async void Back()
        {
            await this._navigationService.GoBackAsync();
        }
        public async Task ValidateEmail()
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("strParametroDebusqueda", "Usuario");
            param.Add("strCorreoElectronico", Email);
            var EmailRequest = await this._HttpService.GetAsync<EmailAdress>("CorreoElectronico/GetBuscarCorreo_movil", param);
            EmailAdress lista = EmailRequest.Result;
            if (string.IsNullOrEmpty(lista.Mail))
            {
                Validemail = true;
            }
            else
            {
                await this._pageDialogService.DisplayAlertAsync("", AppResources.SignInStepThree_EmailNotValid, AppResources.AlertAcceptText);
                Validemail = false;
            }
        }
        private async void Next()
        {
            if (
                string.IsNullOrEmpty(this.Name)
                || string.IsNullOrEmpty(this.FirstLastName)
                || string.IsNullOrEmpty(this.Email)
                || string.IsNullOrEmpty(this.PhoneNumber)
                )
            {
                await this._pageDialogService.DisplayAlertAsync("", AppResources.SignInStepThree_InvalidFields, AppResources.AlertAcceptText);
                return;
            }

            DateTime today = DateTime.Today;
            if ((today.Year - this.Birthday.Year) < 18)
            {
                await this._pageDialogService.DisplayAlertAsync("", AppResources.SignInStepThree_InvalidBirthday, AppResources.AlertAcceptText);
                return;
            }
            if (string.IsNullOrEmpty(this.Password))
            {
                await this._pageDialogService.DisplayAlertAsync("", AppResources.SignInStepTwo_RequiredPassword, AppResources.AlertAcceptText);
                return;
            }

            await this.ValidateEmail();

            if (Validemail)
            {
                this._SignInData.Name = this.Name;
                this._SignInData.FirstLastName = this.FirstLastName;
                this._SignInData.SecondLastName = string.Empty;
                this._SignInData.Email = this.Email;
                this._SignInData.Phone = this.PhoneNumber;
                this._SignInData.Lada = this.Lada.Uid;
                this._SignInData.Birthday = this.Birthday;
                this._SignInData.Username = this.Email;
                this._SignInData.Password = this.Password;

                await this._navigationService.NavigateAsync($"{nameof(TermsAndConditionsPage)}", new NavigationParameters()
                {
                    { "data", this._SignInData},
                    { "lada", this.Lada.Uid}
                });
            }

        }
        #endregion

        #region Navigation Aware 
        public void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                var userRequest = await this._HttpService.GetAsync<IEnumerable<PhoneLada>>("Telefono/GetReadAllInternationalLadas");
                LadaList = userRequest.Result.ToList();
                Lada = LadaList[0];
                _SignInData = new SignInData();
            });
        }
        #endregion
    }
}
