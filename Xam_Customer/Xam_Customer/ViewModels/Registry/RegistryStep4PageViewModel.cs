﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Model.SignIn;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Resources.LangResx;
using System.Globalization;
using Xamarin.Forms;
using Xam_Customer.Views;
using Xam_Customer.Helpers;
using Xam_Customer.Core.Model;

namespace Xam_Customer.ViewModels
{
    public class RegistryStep4PageViewModel : BindableBase, INavigatedAware
    {
        #region Properties
        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }
        private int _Codigo;
        public int Codigo
        {
            get { return _Codigo; }
            set { SetProperty(ref _Codigo, value); }
        }
        private string _EntryCode;
        public string EntryCode
        {
            get { return _EntryCode; }
            set { SetProperty(ref _EntryCode, value); }
        }

        private bool _sendingEmailVisible;
        public bool SendingEmailVisible
        {
            get { return _sendingEmailVisible; }
            set { SetProperty(ref _sendingEmailVisible, value); }
        }

        private bool _confirmEmailVisible;
        public bool ConfirmEmailVisible
        {
            get { return _confirmEmailVisible; }
            set { SetProperty(ref _confirmEmailVisible, value); }
        }

        private bool _errorVisible;
        public bool ErrorVisible
        {
            get { return _errorVisible; }
            set { SetProperty(ref _errorVisible, value); }
        }

        private bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set { _isLoading = value; }
        }

        private bool _showFinishButton;
        public bool ShowFinishButton
        {
            get { return _showFinishButton; }
            set { SetProperty(ref _showFinishButton, value); }
        }

        private string promoCode;
        public string PromoCode
        {
            get { return promoCode; }
            set { SetProperty(ref promoCode, value.ToUpper()); }
        }

        #endregion

        #region Atributes
        private SignInData _SignInData { get; set; }
        #endregion

        #region Internal
        private readonly INavigationService _navigationService;
        private readonly IHttpRequestService _httpService;
        private readonly IPageDialogService _pageDialogService;
        #endregion

        #region Command
        public DelegateCommand FinishSignInCommand { get; set; }
        public DelegateCommand CancelCommand { get; }
        public DelegateCommand GoBackCommand { get; }
        #endregion

        public RegistryStep4PageViewModel(
            INavigationService navigationService,
            IHttpRequestService httpRequestService,
            IPageDialogService pageDialogService
            )
        {
            _pageDialogService = pageDialogService;
            _navigationService = navigationService;
            this._httpService = httpRequestService;

            FinishSignInCommand = new DelegateCommand(FinishRegistry);
            this.CancelCommand = new DelegateCommand(async () => { await this._navigationService.NavigateAsync($"/{nameof(WelcomePage)}"); });
            this.GoBackCommand = new DelegateCommand(async () => { await this._navigationService.GoBackAsync(); });
            PromoCode = string.Empty;
            this.IsLoading = false;
            this.ShowFinishButton = true;
        }

        #region Implementation
        protected async void FinishRegistry()
        {
            if (ErrorVisible)
                return;

            this.IsLoading = true;
            this.ShowFinishButton = false;

            bool isValidPromoCode = true;
            string promoCodeMessage = string.Empty;

            if (!string.IsNullOrEmpty(this.PromoCode.Trim()))
            {
                isValidPromoCode = false;
                Dictionary<string, string> pairs = new Dictionary<string, string>();
                pairs.Add("code", this.PromoCode.Trim());
                var codeRequest = await this._httpService.GetAsync<CodeResult>("v2/Codes/VerifySignInCode", pairs);
                if (codeRequest.Code == HttpResponseCode.Success)
                {
                    if (codeRequest.Result.Code == -5)
                    {
                        promoCodeMessage = AppResources.UnexpectedErrorOccurred;
                        this.ShowFinishButton = true;
                    }
                    else if (codeRequest.Result.Code == -2)
                    {
                        promoCodeMessage = AppResources.PromotionCodeDoesntExists;
                        this.ShowFinishButton = true;
                    }
                    else if (codeRequest.Result.Code == -1)
                    {
                        promoCodeMessage = AppResources.PromotionCodeHasAlreadyUsed;
                        this.ShowFinishButton = true;
                    }
                    else if (codeRequest.Result.Code == 0)
                    {
                        promoCodeMessage = AppResources.PromotionCodeHasExpired;
                        this.ShowFinishButton = true;
                    }
                    else if (codeRequest.Result.Code == 1)
                    {
                        isValidPromoCode = true;
                    }

                }
                else
                {
                    promoCodeMessage = AppResources.UnexpectedErrorOccurred;
                    this.ShowFinishButton = true;
                }
            }

            if (!isValidPromoCode)
            {
                await this._pageDialogService.DisplayAlertAsync("", promoCodeMessage, AppResources.AlertAcceptText);
                return;
            }

            if (EntryCode == Codigo.ToString())
            {
                var uidUsuario = Guid.NewGuid().ToString();
                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add("UidUsuario", uidUsuario);
                param.Add("nombre", _SignInData.Name);
                param.Add("apellidoP", _SignInData.FirstLastName);
                param.Add("usuario", _SignInData.Email);
                param.Add("contrasena", _SignInData.Password);
                param.Add("fechaNacimiento", _SignInData.Birthday.Month + "/" + _SignInData.Birthday.Day + "/" + _SignInData.Birthday.Year);
                param.Add("codigoPromocion", string.IsNullOrEmpty(this.PromoCode.Trim()) ? "None" : this.PromoCode.Trim());
                var UserRequest = await this._httpService.GetEmptyAsync("Usuario/GetGuardarusuarioCliente_Movil", param);
                if (UserRequest == HttpResponseCode.Success)
                {
                    param = new Dictionary<string, string>();
                    param.Add("UidPropietario", uidUsuario);
                    param.Add("strParametroDeInsercion", "Usuario");
                    param.Add("strCorreoElectronico", _SignInData.Email);
                    param.Add("UidCorreoElectronico", Guid.NewGuid().ToString());
                    var EmailRequest = await this._httpService.GetEmptyAsync("CorreoElectronico/GetAgregarCorreo", param);
                    if (EmailRequest == HttpResponseCode.Success)
                    {
                        param = new Dictionary<string, string>();
                        param.Add("uidUsuario", uidUsuario);
                        param.Add("Parametro", "Usuario");
                        param.Add("UidTelefono", Guid.NewGuid().ToString());
                        param.Add("Numero", _SignInData.Phone);
                        param.Add("uidlada", _SignInData.Lada);
                        param.Add("UidTipoDeTelefono", "f7bdd1d0-28e5-4f52-bc26-a17cd5c297de");
                        var PhoneRequest = await this._httpService.GetEmptyAsync("Telefono/GetGuardaTelefonoApi", param);
                        if (PhoneRequest == HttpResponseCode.Success)
                        {
                            param = new Dictionary<string, string>();
                            param.Add("uidUser", uidUsuario);
                            param.Add("uidTermsAndConditions", _SignInData.IdTermsAndConditions);
                            var TermsAndConditionsRequest = await this._httpService.GetEmptyAsync("TerminosYCondiciones/UserTermsAndConditionsAccepted", param);
                            if (TermsAndConditionsRequest == HttpResponseCode.Success)
                            {
                                await this._pageDialogService.DisplayAlertAsync("", AppResources.SingInStepFour_RegistrySuccess, AppResources.AlertAcceptText);
                                Settings.RememberLastUser = _SignInData.Email;
                                await _navigationService.NavigateAsync($"{nameof(Xam_Customer.Views.WelcomePage)}");
                            }
                        }
                        else
                        {
                            this.IsLoading = false;
                            this.ShowFinishButton = true;

                            await this._pageDialogService.DisplayAlertAsync("", AppResources.SingInStepFour_InvalidCode, AppResources.AlertAcceptText);
                        }
                    }
                    else
                    {

                        this.IsLoading = false;
                        this.ShowFinishButton = true;

                        await this._pageDialogService.DisplayAlertAsync("", AppResources.SingInStepFour_InvalidCode, AppResources.AlertAcceptText);
                    }
                }
                else
                {
                    this.IsLoading = false;
                    this.ShowFinishButton = true;

                    await this._pageDialogService.DisplayAlertAsync("", AppResources.SingInStepFour_InvalidCode, AppResources.AlertAcceptText);
                }
            }
            else
            {
                this.IsLoading = false;
                this.ShowFinishButton = true;

                await this._pageDialogService.DisplayAlertAsync("", AppResources.SingInStepFour_InvalidCode, AppResources.AlertAcceptText);
            }

        }

        private async void SendConfirmationCode()
        {
            Random random = new Random();
            Codigo = random.Next(100000, 999999);
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("nombre", _SignInData.Name);
            param.Add("apellidoP", _SignInData.FirstLastName);
            param.Add("codigo", Codigo.ToString());
            param.Add("correo", _SignInData.Email);
            param.Add("idioma", CultureInfo.CurrentCulture.TwoLetterISOLanguageName);
            var postRequest = await this._httpService.GetEmptyAsync("Usuario/GetEnviarCodigoDeConfirmacion", param);

            if (postRequest == HttpResponseCode.Success)
            {
                this.SendingEmailVisible = false;
                this.ConfirmEmailVisible = true;
            }
            else
            {
                this.SendingEmailVisible = false;
                this.ConfirmEmailVisible = false;
                this.ErrorVisible = true;
            }
        }

        private void Cancel()
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                await this._navigationService.GoBackToRootAsync();
            });
        }
        #endregion

        #region Navigation Aware 
        public void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("data"))
            {
                this._SignInData = parameters.GetValue<SignInData>("data");
                this.Email = this._SignInData.Email;

                this.ConfirmEmailVisible = false;
                this.SendingEmailVisible = true;
                this.SendConfirmationCode();
            }
        }
        #endregion
    }
}
