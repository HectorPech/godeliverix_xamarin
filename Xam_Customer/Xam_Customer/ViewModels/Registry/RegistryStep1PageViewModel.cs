﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using Xam_Customer.Views.Registry;

namespace Xam_Customer.ViewModels
{
    public class RegistryStep1PageViewModel : BindableBase
    {
        #region Internal
        private readonly INavigationService _navigationService;
        #endregion

        #region Command
        public DelegateCommand NextCommand { get; set; }
        public DelegateCommand GoBackCommand { get; }
        #endregion

        public RegistryStep1PageViewModel(INavigationService navigationService)
        {
            this._navigationService = navigationService;

            this.NextCommand = new DelegateCommand(this.Next);
            this.GoBackCommand = new DelegateCommand(async () => { await this._navigationService.GoBackAsync(); });
        }

        private async void Next()
        {
            await _navigationService.NavigateAsync($"{nameof(RegistryStep3Page)}");
        }
    }
}
