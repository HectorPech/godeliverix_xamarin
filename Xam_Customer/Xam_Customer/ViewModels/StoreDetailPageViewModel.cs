﻿using Acr.UserDialogs;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Events;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.Core;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Util;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xam_Customer.Views.RgPopup;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class StoreDetailPageViewModel : BaseNavigationViewModel
    {
        #region Properties
        private SearchCompanyDetail storeDetail;
        public SearchCompanyDetail StoreDetail
        {
            get { return storeDetail; }
            set { SetProperty(ref storeDetail, value); }
        }

        private ObservableCollection<ProductStore> products;
        public ObservableCollection<ProductStore> Products
        {
            get { return products; }
            set { SetProperty(ref products, value); }
        }

        private bool isRefreshing;
        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { SetProperty(ref isRefreshing, value); }
        }

        private string resultSummary;
        public string ResultSummary
        {
            get { return resultSummary; }
            set { SetProperty(ref resultSummary, value); }
        }

        private ListboxItemModel selectedFilterLocation;
        public ListboxItemModel SelectedFilterLocation
        {
            get { return selectedFilterLocation; }
            set { SetProperty(ref selectedFilterLocation, value); }
        }

        private ListboxItemModel selectedFilterMenu;
        public ListboxItemModel SelectedFilterMenu
        {
            get { return selectedFilterMenu; }
            set { SetProperty(ref selectedFilterMenu, value); }
        }

        private ListboxItemModel selectedFilterSection;
        public ListboxItemModel SelectedFilterSection
        {
            get { return selectedFilterSection; }
            set { SetProperty(ref selectedFilterSection, value); }
        }

        private string search;
        public string Search
        {
            get { return search; }
            set { SetProperty(ref search, value); }
        }

        private bool onlyAvailable;
        public bool OnlyAvailable
        {
            get { return onlyAvailable; }
            set
            {
                if (this.IsRefreshing)
                {
                    SetProperty(ref onlyAvailable, false);
                }
                else
                {
                    SetProperty(ref onlyAvailable, value);

                    this.Products.Clear();
                    this.PageIndex = 0;
                    Device.BeginInvokeOnMainThread(async () => await this.ReadAllProducts());
                }
            }
        }
        #endregion

        #region Private
        /// <summary>
        /// Primary key of 'Comercio'
        /// </summary>
        private Guid UidStore { get; set; }

        /// <summary>
        /// Current option clicked
        /// </summary>
        private StoreFilterOptionType SelectedOptionType { get; set; }

        private List<ListboxItemModel> FilterSelectionOptions { get; set; }

        private SearchCompanyDetail PrivateStoreDetail { get; set; }

        private List<ListboxItemModel> FilterMenuItems { get; set; }

        private List<ListboxItemModel> FilterSectionItems { get; set; }

        private List<ListboxItemModel> OrderByItems { get; set; }

        private ApplicationFilter SelectedOrderBy { get; set; }

        /// <summary>
        /// The application was trying to display a popup
        /// </summary>
        public bool IsLoadingPopup { get; set; }
        private int PageIndex { get; set; }
        private int ListCount { get; set; }
        #endregion

        #region Services
        private IHttpRequestService HttpService { get; }
        private IEventAggregator EventAggregator { get; }
        private IAppInstaceService Instance { get; }
        #endregion

        #region Commands
        public DelegateCommand RefreshProductsCommand { get; }
        public DelegateCommand<string> SelectProductCommand { get; }
        public DelegateCommand FilterCommand { get; }
        public DelegateCommand<string> FilterByItemCommand { get; }
        public DelegateCommand GoToCart { get; }

        public DelegateCommand ClearFiltersCommand { get; }
        public DelegateCommand SearchCommand { get; }
        public DelegateCommand SearchEmptyCommand { get; }
        public DelegateCommand OrderByCommand { get; }
        #endregion

        public StoreDetailPageViewModel(
            INavigationService navigationService,
            IHttpRequestService httpService,
            IEventAggregator eventAggregator,
            IAppInstaceService appInstaceService
            ) : base(navigationService)
        {
            this.HttpService = httpService;
            this.EventAggregator = eventAggregator;
            this.Instance = appInstaceService;

            this.SelectProductCommand = new DelegateCommand<string>(this.SelectProduct);
            this.RefreshProductsCommand = new DelegateCommand(async () =>
            {
                await this.ReadAllProducts();
            });
            this.FilterCommand = new DelegateCommand(async () =>
            {
                //await PopupNavigation.Instance.PushAsync(new StoresFilterDialog(this.EventAggregator, this.HttpService));
            });
            this.FilterByItemCommand = new DelegateCommand<string>(this.FilterByItem);
            this.GoToCart = new DelegateCommand(async () => await this.NavigationService.NavigateAsync($"{nameof(CartPage)}"));
            this.ClearFiltersCommand = new DelegateCommand(this.ClearFilters);

            this.Products = new ObservableCollection<ProductStore>();

            this.EventAggregator.GetEvent<StoreOptionChangedEvent>().Subscribe(this.OptionChanged);

            this.SelectedFilterLocation = new ListboxItemModel() { Uid = Guid.Empty, Name = AppResources.Location };
            this.SelectedFilterMenu = new ListboxItemModel() { Uid = Guid.Empty, Name = AppResources.Menu };
            this.SelectedFilterSection = new ListboxItemModel() { Uid = Guid.Empty, Name = AppResources.Section };

            this.FilterSectionItems = new List<ListboxItemModel>();
            this.FilterMenuItems = new List<ListboxItemModel>();

            this.SearchCommand = new DelegateCommand(async () =>
            {
                this.PageIndex = 0;
                this.Products.Clear();
                await this.ReadAllProducts();
            });

            this.SearchEmptyCommand = new DelegateCommand(async () =>
            {
                if (string.IsNullOrEmpty(this.Search))
                {
                    this.PageIndex = 0;
                    this.Products.Clear();
                    await this.ReadAllProducts();
                }
            });

            this.OrderByCommand = new DelegateCommand(this.OrderBy);
            this.SelectedOrderBy = ProductFilterOrder.None;
            this.OrderByItems = new List<ListboxItemModel>();
            this.OrderByItems.Add(new ListboxItemModel()
            {
                Uid = Guid.Parse($"{ProductFilterOrder.None.Index}0000000-0000-0000-0000-000000000000"),
                Name = AppResources.DontOrder,
                Selected = true
            });
            this.OrderByItems.Add(new ListboxItemModel()
            {
                Uid = Guid.Parse($"{ ProductFilterOrder.NameAsc.Index}0000000-0000-0000-0000-000000000000"),
                Name = AppResources.AscendingName,
                Selected = false
            });
            this.OrderByItems.Add(new ListboxItemModel()
            {
                Uid = Guid.Parse($"{ProductFilterOrder.NameDesc.Index}0000000-0000-0000-0000-000000000000"),
                Name = AppResources.DescendingName,
                Selected = false
            });
            this.OrderByItems.Add(new ListboxItemModel()
            {
                Uid = Guid.Parse($"{ProductFilterOrder.PriceAsc.Index}0000000-0000-0000-0000-000000000000"),
                Name = AppResources.LowestPrice,
                Selected = false
            });
            this.OrderByItems.Add(new ListboxItemModel()
            {
                Uid = Guid.Parse($"{ProductFilterOrder.PriceDesc.Index}0000000-0000-0000-0000-000000000000"),
                Name = AppResources.HighestPrice,
                Selected = false
            });
        }

        #region Implementation
        private async Task InitializeData()
        {
            await Device.InvokeOnMainThreadAsync(() =>
            {
                UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);
            });

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidEmpresa", this.UidStore.ToString());
            parameters.Add("UidEstado", this.Instance.GetDeliveryState());
            parameters.Add("UidColonia", this.Instance.GetDeliveryColonie());

            var request = await this.HttpService.GetAsync<SearchCompanyDetail>("Store/GetCompanyDetail", parameters);

            if (request.Code == HttpResponseCode.Success)
            {
                this.PrivateStoreDetail = request.Result;
                this.StoreDetail = new SearchCompanyDetail()
                {
                    Uid = this.UidStore,
                    Name = request.Result.Name,
                    ImgUrl = "http://godeliverix.net/Vista/" + request.Result.ImgUrl
                };
            }
            else
            {

            }

            await this.ReadAllProducts();

            await Device.InvokeOnMainThreadAsync(() =>
            {
                UserDialogs.Instance.HideLoading();
            });
        }

        private async Task ReadAllProducts()
        {
            if (this.IsRefreshing)
                return;

            if (this.Products.Count() > 0 && this.Products.Count() == this.ListCount)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    this.IsRefreshing = false;
                });
                return;
            }

            this.IsRefreshing = true;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("TipoFiltro", this.Instance.GetSearchType());
            parameters.Add("Dia", this.Instance.GetDayOfTheWeek());
            parameters.Add("UidEstado", this.Instance.GetDeliveryState());
            parameters.Add("UidColonia", this.Instance.GetDeliveryColonie());
            parameters.Add("UidFiltro", this.Instance.GetUidFilter());
            parameters.Add("UidEmpresa", this.UidStore.ToString());
            parameters.Add("PageNumber", this.PageIndex.ToString());
            parameters.Add("PageSize", ApplicationConstants.RowsToShow);

            parameters.Add("SortField", this.SelectedOrderBy.Value);
            parameters.Add("SortDirection", this.SelectedOrderBy.Direction);

            if (this.selectedFilterMenu.Uid != Guid.Empty)
            {
                parameters.Add("UidOferta", this.SelectedFilterMenu.StrUid);
            }

            if (this.SelectedFilterSection.Uid != Guid.Empty)
            {
                parameters.Add("UidSeccion", this.SelectedFilterSection.StrUid);
            }

            if (!string.IsNullOrEmpty(this.Search))
            {
                parameters.Add("filtro", this.Search);
            }

            parameters.Add("Available", this.OnlyAvailable ? "true" : "false");

            var request = await this.HttpService.GetAsync<CommonDataSource<ProductStore>>("Store/SearchProducts", parameters);
            if (request.Code == HttpResponseCode.Success)
            {
                foreach (ProductStore item in request.Result.Payload)
                {
                    item.ImgUrl = "https://www.godeliverix.net/Vista/" + item.ImgUrl;
                    item.CompanyImgUrl = "https://www.godeliverix.net/Vista/" + item.CompanyImgUrl;
                    Products.Add(item);
                }
                this.ListCount = request.Result.Count;
                this.ResultSummary = $"{this.Products.Count()} {AppResources.Of} {this.ListCount} {AppResources.DefaultSearch}";

                this.PageIndex++;
            }
            this.IsRefreshing = false;
        }

        private async Task ReadAllMenus()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("dia", this.Instance.GetDayOfTheWeek());
            parameters.Add("uidSucursal", this.selectedFilterLocation.Uid.ToString());

            var request = await this.HttpService.GetAsync<IEnumerable<OfertaListbox>>("Store/GetBranchDeals", parameters);
            List<ListboxItemModel> items = new List<ListboxItemModel>();

            if (request.Code == HttpResponseCode.Success)
            {
                foreach (OfertaListbox item in request.Result)
                {
                    items.Add(new ListboxItemModel() { Uid = item.Uid, Available = item.Available, Name = item.Name, ShowAvailability = true });
                }
            }

            this.FilterMenuItems = items;
        }

        private async Task ReadAllSections()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("uidEstado", this.Instance.GetDeliveryState());
            parameters.Add("uidOferta", this.SelectedFilterMenu.Uid.ToString());

            var request = await this.HttpService.GetAsync<IEnumerable<SeccionListbox>>("Store/GetDealSections", parameters);
            List<ListboxItemModel> items = new List<ListboxItemModel>();
            if (request.Code == HttpResponseCode.Success)
            {
                foreach (SeccionListbox item in request.Result)
                {
                    items.Add(new ListboxItemModel() { Uid = item.Uid, Available = item.Available, Name = item.Name, ShowAvailability = true });
                }
            }

            this.FilterSectionItems = items;
        }

        private async void FilterByItem(string type)
        {
            if (IsLoadingPopup) { return; }

            IsLoadingPopup = true;
            this.SelectedOptionType = (StoreFilterOptionType)int.Parse(type);
            Random random = new Random();

            this.FilterSelectionOptions = new List<ListboxItemModel>();
            this.FilterSelectionOptions.Add(new ListboxItemModel() { Uid = Guid.Empty, Name = AppResources.All });

            switch (this.SelectedOptionType)
            {
                case StoreFilterOptionType.Location:
                    this.FilterSelectionOptions[0].Selected = this.SelectedFilterLocation.Uid == Guid.Empty;

                    foreach (CompanyBranch branch in this.PrivateStoreDetail.Branches)
                    {
                        this.FilterSelectionOptions.Add(new ListboxItemModel()
                        {
                            Uid = branch.Uid,
                            Name = branch.Identifier,
                            Selected = this.SelectedFilterLocation.Uid == branch.Uid,
                            Available = branch.Available,
                            ShowAvailability = true
                        });
                    }
                    break;
                case StoreFilterOptionType.Menu:
                    this.FilterSelectionOptions[0].Selected = this.SelectedFilterMenu.Uid == Guid.Empty;

                    foreach (ListboxItemModel item in this.FilterMenuItems)
                    {
                        this.FilterSelectionOptions.Add(new ListboxItemModel()
                        {
                            Uid = item.Uid,
                            Name = item.Name,
                            Selected = this.SelectedFilterMenu.Uid == item.Uid,
                            Available = item.Available,
                            ShowAvailability = true
                        });
                    }
                    break;
                case StoreFilterOptionType.Section:
                    this.FilterSelectionOptions[0].Selected = this.SelectedFilterSection.Uid == Guid.Empty;

                    foreach (ListboxItemModel item in this.FilterSectionItems)
                    {
                        this.FilterSelectionOptions.Add(new ListboxItemModel()
                        {
                            Uid = item.Uid,
                            Name = item.Name,
                            Selected = this.SelectedFilterSection.Uid == item.Uid,
                            Available = item.Available,
                            ShowAvailability = true
                        });
                    }
                    break;
                default:
                    break;
            }

            StoreDetailSelectionFilterDialog dialog = new StoreDetailSelectionFilterDialog(this.EventAggregator, this.FilterSelectionOptions, this.SelectedOptionType);
            await PopupNavigation.Instance.PushAsync(dialog);
            IsLoadingPopup = false;
        }

        private async void SelectProduct(string uid)
        {
            var product = Products.Where(p => p.StrUid == uid).FirstOrDefault();
            if (product.Available)
            {
                var param = new NavigationParameters();
                param.Add("Data", new Product()
                {
                    CompanyName = product.CompanyName,
                    StrUid = product.StrUid,
                    CostoEnvio = 0,
                    Name = product.Name,
                    Description = product.Description,
                    Price = product.Price,
                    ImgPathProduct = product.ImgUrl,
                    ImgPathCompany = product.CompanyImgUrl
                });
                await this.NavigationService.NavigateAsync($"{nameof(ProductSearchDescriptionPage)}", param);
            }
            else
            {
                UserDialogs.Instance.Toast(AppResources.ProductNotAvailable);
            }
        }

        private async void OptionChanged(StoreOptionChanged option)
        {
            await PopupNavigation.Instance.PopAsync();

            if (option == null)
            {
                return;
            }

            this.Products.Clear();
            this.PageIndex = 0;

            switch (option.Type)
            {
                case StoreFilterOptionType.Location:

                    this.SelectedFilterLocation.Uid = option.Uid;
                    this.SelectedFilterLocation.Name = option.Uid == Guid.Empty ? AppResources.Location : $"{option.Name.Cut(8)}...";

                    this.FilterMenuItems = new List<ListboxItemModel>();
                    this.SelectedFilterMenu.Uid = Guid.Empty;
                    this.SelectedFilterMenu.Name = AppResources.Menu;

                    this.FilterSectionItems = new List<ListboxItemModel>();
                    this.SelectedFilterSection.Uid = Guid.Empty;
                    this.SelectedFilterSection.Name = AppResources.Section;

                    if (option.Uid != Guid.Empty)
                    {
                        await this.ReadAllMenus();
                    }

                    await this.ReadAllProducts();
                    break;
                case StoreFilterOptionType.Menu:

                    this.SelectedFilterMenu.Uid = option.Uid;
                    this.SelectedFilterMenu.Name = option.Uid == Guid.Empty ? AppResources.Menu : $"{option.Name.Cut(8)}...";

                    this.FilterSectionItems = new List<ListboxItemModel>();
                    this.SelectedFilterSection.Uid = Guid.Empty;
                    this.SelectedFilterSection.Name = AppResources.Section;

                    if (option.Uid != Guid.Empty)
                    {
                        await this.ReadAllSections();
                    }

                    await this.ReadAllProducts();

                    break;
                case StoreFilterOptionType.Section:

                    this.SelectedFilterSection.Uid = option.Uid;
                    this.SelectedFilterSection.Name = option.Uid == Guid.Empty ? AppResources.Section : $"{option.Name.Cut(8)}...";

                    await this.ReadAllProducts();
                    break;
                case StoreFilterOptionType.OrderBy:
                    string strGuid = option.Uid.ToString().Substring(0, 1);
                    int id = int.Parse(strGuid);
                    switch (id)
                    {
                        case 0:
                            this.SelectedOrderBy = ProductFilterOrder.None;
                            break;
                        case 1:
                            this.SelectedOrderBy = ProductFilterOrder.NameAsc;
                            break;
                        case 2:
                            this.SelectedOrderBy = ProductFilterOrder.NameDesc;
                            break;
                        case 3:
                            this.SelectedOrderBy = ProductFilterOrder.PriceAsc;
                            break;
                        case 4:
                            this.SelectedOrderBy = ProductFilterOrder.PriceDesc;
                            break;
                        default:
                            break;
                    }

                    await this.ReadAllProducts();
                    break;
                default:
                    break;
            }
        }

        private async void OrderBy()
        {
            if (IsLoadingPopup) { return; }

            IsLoadingPopup = true;

            StoreDetailSelectionFilterDialog dialog = new StoreDetailSelectionFilterDialog(this.EventAggregator, this.OrderByItems, StoreFilterOptionType.OrderBy);

            await PopupNavigation.Instance.PushAsync(dialog);
            IsLoadingPopup = false;
        }

        private async void ClearFilters()
        {
            this.SelectedFilterLocation.Uid = Guid.Empty;
            this.SelectedFilterLocation.Name = AppResources.Location;


            this.SelectedFilterMenu.Uid = Guid.Empty;
            this.SelectedFilterMenu.Name = AppResources.Menu;

            this.FilterMenuItems = new List<ListboxItemModel>();

            this.SelectedFilterSection.Uid = Guid.Empty;
            this.SelectedFilterSection.Name = AppResources.Section;

            this.FilterSectionItems = new List<ListboxItemModel>();

            this.PageIndex = 0;
            this.Products.Clear();
            await this.ReadAllProducts();
        }
        public override void Initialize(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("uid"))
            {
                this.UidStore = parameters.GetValue<Guid>("uid");
                this.PageIndex = 0;
                Task.Run(async () => await this.InitializeData());
            }

            Device.InvokeOnMainThreadAsync(() =>
            {
                int count = this.Instance.ProductsCount();
                this.EventAggregator.GetEvent<CartCountEvent>().Publish(count);
            });
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            Device.InvokeOnMainThreadAsync(() =>
            {
                int count = this.Instance.ProductsCount();
                this.EventAggregator.GetEvent<CartCountEvent>().Publish(count);
            });
        }
        #endregion

        #region Util

        #endregion
    }
}
