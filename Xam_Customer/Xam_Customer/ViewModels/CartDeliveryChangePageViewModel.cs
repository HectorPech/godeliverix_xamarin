﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Util;

namespace Xam_Customer.ViewModels
{
    public class CartDeliveryChangePageViewModel : BindableBase, INavigationAware
    {
        #region Properties
        private CartBranchItem _CartBranchItem;

        public CartBranchItem CartBranchItem
        {
            get => _CartBranchItem;
            set => SetProperty(ref _CartBranchItem, value);
        }
        private Guid _UidBranch;

        public Guid UidBranch
        {
            get => _UidBranch;
            set => SetProperty(ref _UidBranch, value);
        }
        private List<Delivery> _DeliveryBranches;

        public List<Delivery> DeliveryBranches
        {
            get => _DeliveryBranches;
            set => SetProperty(ref _DeliveryBranches, value);
        }
        private decimal _DeliveryTip;

        public decimal DeliveryTip
        {
            get => _DeliveryTip;
            set
            {
                SetProperty(ref _DeliveryTip, value);
            }
        }
        private decimal _DeliveryRate;

        public decimal DeliveryRate
        {
            get => _DeliveryRate;
            set => SetProperty(ref _DeliveryRate, value);
        }
        private decimal _Total;

        public decimal Total
        {
            get => _Total;
            set => SetProperty(ref _Total, value);
        }

        #endregion
        #region Internal
        public IHttpRequestService _httpRequestService { get; set; }
        public IAppInstaceService _appInstanceService { get; set; }
        public INavigationService _navigationService { get; set; }
        #endregion
        #region Command
        public DelegateCommand<string> ChangeDelivery { get; set; }
        public DelegateCommand CmdAdd10 { get; set; }
        public DelegateCommand CmdAdd20 { get; set; }
        public DelegateCommand CmdAdd30 { get; set; }
        public DelegateCommand CmdAdd40 { get; set; }
        public DelegateCommand CmdBack { get; set; }
        #endregion
        public CartDeliveryChangePageViewModel(
            IHttpRequestService httpRequestService,
            IAppInstaceService appInstaceService,
            INavigationService navigationService)
        {
            _httpRequestService = httpRequestService;
            _navigationService = navigationService;
            _appInstanceService = appInstaceService;
            ChangeDelivery = new DelegateCommand<string>(ChangeDeliveryBranch);

            CmdAdd10 = new DelegateCommand(AddTen);
            CmdAdd20 = new DelegateCommand(AddTwenty);
            CmdAdd30 = new DelegateCommand(AddThirty);
            CmdAdd40 = new DelegateCommand(AddFourty);
            CmdBack = new DelegateCommand(BackPage);
        }



        #region Implementation
        private void AddTen()
        {
            changeDeliveryAction(10.00m);
        }
        private void AddTwenty()
        {
            changeDeliveryAction(20.00m);
        }
        private void AddThirty()
        {
            changeDeliveryAction(30.00m);
        }
        private void AddFourty()
        {
            changeDeliveryAction(40.00m);
        }

        public void changeDeliveryAction(decimal price)
        {
            DeliveryTip = price;
            _appInstanceService.UpdateDeliveryTip(UidBranch, price);
            DeliveryTip = price;
            Total = DeliveryRate + DeliveryTip;
        }
        public void changeDeliveryAction()
        {
            //DeliveryTip = DeliveryNewTip;
            _appInstanceService.UpdateDeliveryTip(UidBranch, DeliveryTip);
            Total = DeliveryRate + DeliveryTip;
            // DeliveryTip = DeliveryNewTip;
        }
        private async void BackPage()
        {
            await _navigationService.GoBackAsync();
        }
        private void ChangeDeliveryBranch(string Uid)
        {
            var delivery = DeliveryBranches.FirstOrDefault(d => d.Uid.ToString() == Uid);
            _appInstanceService.UpdateDeliveryOrder(UidBranch, delivery.Uid, delivery.Price);
            _navigationService.GoBackAsync();
        }
        private async void ObtenerTarifarios()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("TipoDeBusqueda", "Cliente");
            parameters.Add("ZonaEntrega", _appInstanceService.GetDeliveryColonie());
            parameters.Add("uidSucursal", CartBranchItem.StrUidBranch.ToString());
            var DeliveryBranchesRequest = await this._httpRequestService.GetAsync<IEnumerable<Delivery>>("Tarifario/GetBuscarTarifarios_Movil", parameters);

            DeliveryBranches = DeliveryBranchesRequest.Result.ToList();
            foreach (var item in DeliveryBranches)
            {
                item.UrlImageCompany = "https://www.godeliverix.net/" + "Vista/" + item.UrlImageCompany.Remove(0, 3);
            }
        }


        #endregion
        #region Navigation
        public void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("Data"))
            {
                this.UidBranch = new Guid(parameters.GetValue<string>("Data"));
                this.CartBranchItem = this._appInstanceService.GetCartBranchItemById(this.UidBranch);
                DeliveryTip = _CartBranchItem.DeliveryTips;
                DeliveryRate = _CartBranchItem.DeliveryRate;
                Total = DeliveryRate + DeliveryTip;
                ObtenerTarifarios();
            }
        }
        #endregion
    }
}
