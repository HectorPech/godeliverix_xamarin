﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Xam_Customer.Core.Model.System;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.ViewModels.Core;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class AdvancedOptionsLocationViewModel : BaseNavigationViewModel
    {
        #region Propiedades
        private string _Longitud;

        public string Longitud
        {
            get { return _Longitud; }
            set { SetProperty(ref _Longitud, value); }
        }
        private string _Latitud;

        public string Latitud
        {
            get { return _Latitud; }
            set { SetProperty(ref _Latitud, value); }
        }

        #endregion
        #region Services
        public IPageDialogService _pageDialogService { get; set; }
        public INavigationService _navigationService { get; set; }
        public IEventAggregator _eventAggregator { get; set; }
        public IHttpRequestService HttpService { get; set; }

        #endregion
        #region Commands
        public DelegateCommand Nextpage { get; set; }
        public DelegateCommand Previouspage { get; set; }

        #endregion
        public AdvancedOptionsLocationViewModel(IEventAggregator eventAggregator, IPageDialogService pageDialogService,
            INavigationService navigationService, IHttpRequestService httpRequestService) : base(navigationService)
        {
            Previouspage = new DelegateCommand(Regresar);
            Nextpage = new DelegateCommand(Continuar);
            _pageDialogService = pageDialogService;
            _navigationService = navigationService;
            _eventAggregator = eventAggregator;
            HttpService = httpRequestService;
        }
        private void Continuar()
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                double lat = 0;
                double longi = 0;
                bool result = true;
                if (!double.TryParse(Longitud, out longi))
                {
                    await this._pageDialogService.DisplayAlertAsync("", "Longitud invalida", AppResources.AlertAcceptText);
                    result = false;
                    return;
                }
                if (!double.TryParse(Latitud, out lat))
                {
                    await this._pageDialogService.DisplayAlertAsync("", "Latitud", AppResources.AlertAcceptText);
                    result = false;
                    return;
                }
                if (result)
                {
                    var pin = new MapPin() { Identifier = "", Longitude = longi.ToString(), Latitude = lat.ToString() };
                    //_eventAggregator.GetEvent<Xam_Customer.Core.Events.UpdateMapPinEvent>().Publish(pin);
                    await _navigationService.GoBackAsync();
                }
            });

        }
        private void Regresar()
        {
            _navigationService.GoBackAsync();
        }
        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            var Mpin = new MapPin() { Identifier = "", Longitude = Longitud, Latitude = Latitud};
            parameters.Add("pin", Mpin);
        }
        public override void Initialize(INavigationParameters parameters)
        {
            parameters.ContainsKey("pin");
            var mappin = parameters.GetValue<MapPin>("pin");
            if (mappin != null)
            {
                Latitud = mappin.Latitude;
                Longitud = mappin.Longitude;
            }
            else
            {
                Latitud = string.Empty;
                Longitud = string.Empty;
            }

        }
    }
}
