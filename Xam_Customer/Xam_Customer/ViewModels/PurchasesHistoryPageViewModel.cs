﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.Core;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Util;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.PurchaseHistory;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xam_Customer.Views.Popup;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class PurchasesHistoryPageViewModel : BaseViewModel
    {
        #region Property
        private ObservableCollection<PurchaseHistory> purchases;
        public ObservableCollection<PurchaseHistory> Purchases
        {
            get { return purchases; }
            set { SetProperty(ref purchases, value); }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }


        private ObservableCollection<PurchaseOrderHistory> orders;
        public ObservableCollection<PurchaseOrderHistory> Orders
        {
            get { return orders; }
            set { SetProperty(ref orders, value); }
        }

        #endregion

        #region Private
        private Guid UserUid { get; set; } = Guid.Empty;
        #endregion

        #region Command
        public DelegateCommand<PurchaseHistory> ViewPurchaseDetailCommand { get; }
        public DelegateCommand<OrderHistory> ViewOrderDetailCommand { get; }
        public DelegateCommand DisplayFiltersCommand { get; }
        public DelegateCommand RefreshCommand { get; }
        #endregion

        #region Services
        private readonly IHttpRequestService _HttpService;
        private readonly IAppInstaceService _appInstaceService;
        #endregion

        public PurchasesHistoryPageViewModel(
                INavigationService navigationService,
                IDialogService dialogService,
                IPageDialogService pageDialogService,
                IHttpRequestService httpRequestService,
                IAppInstaceService appInstaceService
            ) : base(navigationService, dialogService, pageDialogService)
        {
            this._HttpService = httpRequestService;
            this._appInstaceService = appInstaceService;
            this.ViewPurchaseDetailCommand = new DelegateCommand<PurchaseHistory>(this.ViewPurchaseDetail, this.CanSubmit);
            this.ViewOrderDetailCommand = new DelegateCommand<OrderHistory>(this.ViewOrderDetail);
            this.DisplayFiltersCommand = new DelegateCommand(this.ShowFilters);
            this.RefreshCommand = new DelegateCommand(() => { Task.Run(this.Init); });

            this.Purchases = new ObservableCollection<PurchaseHistory>();
            this.Orders = new ObservableCollection<PurchaseOrderHistory>();

            UserSession session = Settings.UserSession;
            this.UserUid = session == null ? Guid.Empty : Guid.Parse(session.StrUid);
        }

        #region Implementation
        private async void ViewPurchaseDetail(PurchaseHistory purchase)
        {
            Guid uid = purchase.PurchaseUid;

            var status = await this.NavigationService.NavigateAsync($"{nameof(PurchaseDetailPage)}", new NavigationParameters() {
                { "uid", uid }
            });
        }

        private async void ViewOrderDetail(OrderHistory order)
        {
            await this.NavigationService.NavigateAsync($"{nameof(OrderHistoryDetailPage)}", new NavigationParameters()
                {
                    { "uid" , order.OrderUid.ToString() }
                });
        }

        private async Task Init()
        {
            this.IsBusy = true;
            // UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);

            // Validar acceso a internet
            if (Connectivity.NetworkAccess != NetworkAccess.Internet)
            {
                this.IsBusy = false;
                await this.NetworkError(this.NavigationService.GetNavigationUriPath());
                return;
            }
            Dictionary<string, string> pairs = new Dictionary<string, string>();
            pairs.Add("uid", this.UserUid.ToString());

            var purchasesRequest = await this._HttpService.GetAsync<CommonDataSource<PurchaseHistory>>("Orders/ReadAllUserPurchases", pairs);

            if (purchasesRequest.Code == HttpResponseCode.ServerError)
            {
                this.ServiceNotAvailable(this.NavigationService.GetNavigationUriPath());
            }

            if (purchasesRequest.Code == HttpResponseCode.Success)
            {
                CommonDataSource<PurchaseHistory> history = purchasesRequest.Result;
                foreach (PurchaseHistory purchase in history.Payload)
                {
                    switch (purchase.PaymentMethod)
                    {
                        case "Efectivo":
                            purchase.PaymentIcon = MaterialFontIcons.Cash;
                            purchase.PaymentMethod = PurchaseHistoryLang.PaymentType_Cash;
                            break;
                        case "Tarjeta":
                            purchase.PaymentIcon = MaterialFontIcons.CreditCardOutline;
                            purchase.PaymentMethod = PurchaseHistoryLang.PaymentType_Card;
                            break;
                        case "Monedero":
                            purchase.PaymentIcon = MaterialFontIcons.Wallet;
                            purchase.PaymentMethod = PurchaseHistoryLang.PaymentType_EWallet;
                            break;
                        default:
                            purchase.PaymentIcon = MaterialFontIcons.CurrencyUsdCircleOutline;
                            break;
                    }
                    switch (purchase.PaymentStatus)
                    {
                        case "Pagado":
                            purchase.StatusColor = Color.FromHex("#4caf50");
                            break;
                        case "Denegado":
                            purchase.StatusColor = Color.FromHex("#ffeb3b");
                            break;
                        case "Cancelado":
                            purchase.StatusColor = Color.FromHex("#f44336");
                            break;
                        case "Reembolsado":
                            purchase.StatusColor = Color.FromHex("#2196f3");
                            break;
                        case "Error":
                            purchase.StatusColor = Color.FromHex("#f44336");
                            break;
                        case "Pendiente":
                            purchase.StatusColor = Color.FromHex("#ff9800");
                            break;
                        default:
                            purchase.StatusColor = Color.FromHex("#3c4252");
                            break;
                    }

                    foreach (var order in purchase.Orders)
                    {
                        switch (order.Status)
                        {
                            case "Creado":
                                order.StatusColor = OrderStatusColor.Created.Color;
                                order.Status = AppResources.StatusOrder_Created;
                                break;
                            case "Elaborado":
                                order.StatusColor = OrderStatusColor.Elaborated.Color;
                                order.Status = AppResources.StatusOrder_Making;
                                break;
                            case "Entregado":
                                order.StatusColor = OrderStatusColor.Delivered.Color;
                                order.Status = AppResources.StatusOrder_Delivered;
                                break;
                            case "Finalizado":
                                order.StatusColor = OrderStatusColor.Finalized.Color;
                                order.Status = AppResources.StatusOrder_Finished;
                                break;
                            case "Cancelado":
                                order.StatusColor = OrderStatusColor.Canceled.Color;
                                order.Status = AppResources.StatusOrder_Canceled;
                                break;
                            case "Espera de confirmacion":
                                order.StatusColor = OrderStatusColor.WaitingForConfirmation.Color;
                                order.Status = AppResources.StatusOrder_Waiting;
                                break;
                            case "Confirmado":
                                order.StatusColor = OrderStatusColor.Confirmed.Color;
                                order.Status = AppResources.StatusOrder_Confirmed;
                                break;
                            case "Enviando":
                                order.StatusColor = OrderStatusColor.Sended.Color;
                                order.Status = AppResources.StatusOrder_Delivering;
                                break;
                        }


                        order.Total = (order.Total + order.Tips + order.Delivery + order.CardPaymentComission + order.DeliveryCardPaymentComission);
                    }

                    purchase.TotalNoDiscount = purchase.Total;
                    purchase.Total = purchase.Total - (purchase.WalletDiscount.HasValue ? purchase.WalletDiscount.Value : 0);
                    purchase.AddressIdentifier = $"{PurchaseHistoryLang.PaymentDetail_DeliveryLocation} {purchase.AddressIdentifier}";
                }

                this.Purchases = new ObservableCollection<PurchaseHistory>(history.Payload);
            }
            //  UserDialogs.Instance.HideLoading();
            this.IsBusy = false;
        }

        bool CanSubmit(PurchaseHistory purchase)
        {
            return !this.IsBusy;
        }

        private void ShowFilters()
        {
            this.DialogService.ShowDialog($"{nameof(PurchaseFiltersDialog)}");
        }
        #endregion

        public override void Initialize(INavigationParameters parameters)
        {
            Task.Run(async () => { await this.Init(); });
        }
    }
}
