﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http.Headers;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Helpers;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xam_Customer.Views.Popup;

namespace Xam_Customer.ViewModels
{
    public class HomePageViewModel : BaseViewModel
    {

        #region Properties
        private List<Product> _products;
        public List<Product> Products
        {
            get { return _products; }
            set { SetProperty(ref _products, value); }
        }

        private bool _loadingList;
        public bool LoadingList
        {
            get { return _loadingList; }
            set { SetProperty(ref _loadingList, value); }
        }

        private int _total;
        public int Total
        {
            get { return _total; }
            set { SetProperty(ref _total, value); }
        }

        private string _deliveryAddress;
        public string DeliveryAddress
        {
            get { return $"Entregar en {_deliveryAddress}"; }
            set { SetProperty(ref _deliveryAddress, value); }
        }
        #endregion

        #region Internal
        private IHttpRequestService _HttpService { get; }
        private IPageDialogService _pageDialogService { get; }
        #endregion

        public HomePageViewModel(
            INavigationService navigationService,
            IDialogService dialogService,
            IPageDialogService pageDialogService,
            IHttpRequestService httpService
            ) : base(navigationService, dialogService, pageDialogService)
        {
            this._HttpService = httpService;
            _pageDialogService = pageDialogService;
            this.Products = new List<Product>();

            this.GenerateFakeData();

            this.RefreshList = new DelegateCommand(this.GenerateFakeData);
            this.SearchCommand = new DelegateCommand(this.Search);
            this.SelectProduct = new DelegateCommand<string>(this.Product_Clicked);

            //AddressSession address = Settings.AddressSession;
            //if (address != null)
            //{
            //    this.DeliveryAddress = address.FullAddress;
            //}
        }

        #region Command
        public DelegateCommand RefreshList { get; }
        public DelegateCommand SearchCommand { get; }

        public DelegateCommand<string> SelectProduct { get; }
        #endregion

        #region Implementation
        private void GenerateFakeData()
        {
            this.LoadingList = true;

            Random random = new Random();
            //string imgPath = "https://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2017/04/pizzapepperoni0.jpg";

            List<Product> products = new List<Product>();
            int quantity = random.Next(6, 25);

            //for (int i = 0; i < quantity; i++)
            //{
            //    products.Add(new Product()
            //    {
            //        Uid = Guid.NewGuid(),
            //        Price = random.Next(99, 250),
            //        Description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore",
            //        Name = $"Product {i}",
            //        ImgPath = imgPath
            //    });
            //}

            this.Total = products.Count();
            this.Products = new List<Product>(products);

            this.LoadingList = false;
        }

        private async void Request()
        {
            var result = await this._HttpService.GetAsync<IEnumerable<Post>>("posts");
            if (result.Code == HttpResponseCode.Success)
            {

            }
            else
            {

            }
        }

        private async void Product_Clicked(string uid)
        {
            var product = Products.Find(p=>p.StrUid == uid);
            var param = new NavigationParameters();
            param.Add("Data",product);
            await this.NavigationService.NavigateAsync("ProductSearchDescriptionPage",param);
        }

        private void Search()
        {
            // this.DialogService.ShowDialog($"{nameof(FiltersPage)}");
        }
        #endregion
    }
}
