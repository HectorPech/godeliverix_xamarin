﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Views;
using Xam_Customer.Views.Registry;

namespace Xam_Customer.ViewModels
{
    public class WelcomePageViewModel : BindableBase
    {

        #region Services
        private INavigationService _navigationService { get; }
        private IHttpRequestService _HttpRequestService { get; }
        private IDialogService _PageDialogService { get; }
        private IAppInstaceService _appInstaceService { get; }
        #endregion

        public WelcomePageViewModel(
            INavigationService navigationService,
            IHttpRequestService httpRequestService,
            IDialogService dialogService,
            IAppInstaceService appInstaceService
            )
        {
            this._appInstaceService = appInstaceService;
            this._PageDialogService = dialogService;
            this._navigationService = navigationService;
            this._HttpRequestService = httpRequestService;

            this.LoginCommand = new DelegateCommand(this.Login_Clicked);
            this.SignInCommand = new DelegateCommand(this.SingIn_Clicked);
            this.GuestCommand = new DelegateCommand(this.Guest_Clicked);
        }

        #region Commands
        public DelegateCommand LoginCommand { get; }
        public DelegateCommand SignInCommand { get; }
        public DelegateCommand GuestCommand { get; }
        #endregion

        #region Region
        private async void Login_Clicked()
        {
            await this._navigationService.NavigateAsync($"{nameof(LoginPage)}");
        }
        private async void SingIn_Clicked()
        {
            await this._navigationService.NavigateAsync($"{nameof(RegistryStep1Page)}");
        }
        private async void Guest_Clicked()
        {
            await this._navigationService.NavigateAsync($"{nameof(LocationPage)}");
        }
        #endregion

        #region Implement
       
        #endregion
    }
}
