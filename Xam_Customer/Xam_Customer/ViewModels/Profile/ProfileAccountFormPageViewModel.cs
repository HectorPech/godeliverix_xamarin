﻿using Acr.UserDialogs;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.SignIn;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.Profile;
using Xam_Customer.ViewModels.Core;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class ProfileAccountFormPageViewModel : BaseNavigationViewModel
    {
        #region Properties
        private List<PhoneLada> _LadaList;
        public List<PhoneLada> LadaList
        {
            get { return _LadaList; }
            set { SetProperty(ref _LadaList, value); }
        }
        private PhoneLada _Lada;

        public PhoneLada Lada
        {
            get { return _Lada; }
            set { SetProperty(ref _Lada, value); }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }

        private string firstLastName;
        public string FirstLastName
        {
            get { return firstLastName; }
            set { SetProperty(ref firstLastName, value); }
        }

        private string secondLastName;
        public string SecondLastName
        {
            get { return secondLastName; }
            set { SetProperty(ref secondLastName, value); }
        }

        private DateTime birthday;
        public DateTime Birthday
        {
            get { return birthday; }
            set { SetProperty(ref birthday, value); }
        }

        private string idemail;
        public string idEmail
        {
            get { return idemail; }
            set { SetProperty(ref idemail, value); }
        }
        private string email;
        public string Email
        {
            get { return email; }
            set { SetProperty(ref email, value); }
        }


        private string lada;
        public string StrLada
        {
            get { return lada; }
            set { SetProperty(ref lada, value); }
        }
        private string phone;
        public string Phone
        {
            get { return phone; }
            set { SetProperty(ref phone, value); }
        }

        private string idphone;
        public string idPhone
        {
            get { return idphone; }
            set { SetProperty(ref idphone, value); }
        }

        private bool isBusy;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        #endregion

        #region Private
        private UserInformation User { get; set; }
        private Guid UidUsuario { get; set; }
        #endregion

        #region Command
        public DelegateCommand ConfirmCommand { get; }
        #endregion

        #region Services
        private IHttpRequestService HttpService { get; }
        private IPageDialogService PageDialogService { get; }
        #endregion

        public ProfileAccountFormPageViewModel(
            INavigationService navigationService,
            IHttpRequestService httpRequestService,
            IPageDialogService pageDialogService
            ) : base(navigationService)
        {
            this.HttpService = httpRequestService;
            this.PageDialogService = pageDialogService;

            this.ConfirmCommand = new DelegateCommand(this.Confirm);

            this.UidUsuario = Guid.Parse(Settings.UserSession.StrUid);
        }

        #region Implementation
        public void Confirm()
        {
            if (IsBusy)
            {
                return;
            }

            Device.InvokeOnMainThreadAsync(async () =>
            {
                UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);
                this.IsBusy = true;

                if (string.IsNullOrEmpty(this.Name))
                {
                    UserDialogs.Instance.HideLoading();
                    this.IsBusy = false;
                    await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AccountForm_Name} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                    return;
                }

                if (string.IsNullOrEmpty(this.FirstLastName))
                {
                    UserDialogs.Instance.HideLoading();
                    this.IsBusy = false;
                    await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AccountForm_FirstLastname} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                    return;
                }

                //if (string.IsNullOrEmpty(this.SecondLastName))
                //{
                //    UserDialogs.Instance.HideLoading();
                //    this.IsBusy = false;
                //    await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AccountForm_SecondLastname} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                //    return;
                //}

                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("UidUsuario", this.User.UserData.Uid.ToString());
                parameters.Add("perfil", "4F1E1C4B-3253-4225-9E46-DD7D1940DA19");
                parameters.Add("Nombre", this.Name);
                parameters.Add("ApellidoPaterno", this.FirstLastName);
                parameters.Add("ApellidoMaterno", this.SecondLastName);
                parameters.Add("fnacimiento", Birthday.Month + "/" + Birthday.Day + "/" + Birthday.Year);

                var userRequest = await this.HttpService.GetEmptyAsync("Usuario/GetActualizarUsuario_Movil", parameters);

                if (userRequest == HttpResponseCode.Success)
                {
                    Dictionary<string, string> param = new Dictionary<string, string>();
                    param.Add("UidTelefono", idPhone);
                    param.Add("Numero", Phone);
                    param.Add("UidTipoDeTelefono", User.PhoneInformation.UidType);
                    param.Add("UidLada", Lada.Uid);
                    await this.HttpService.GetEmptyAsync("Telefono/GetActualizaTelefono_MovilConLada", param);

                    this.IsBusy = false;
                    Settings.UserSession = new UserSession()
                    {
                        StrUid = User.UserData.Uid,
                        FisrtName = Name ?? "",
                        FisrtLastName = FirstLastName ?? "",
                        SecondLastName = SecondLastName ?? "",
                        BirthDay = Birthday.ToString("MM-dd-yyyy"),
                        UserName = User.UserData.Username
                    };

                    await this.NavigationService.GoBackAsync();

                }
                else
                {
                    this.IsBusy = false;
                    await this.PageDialogService.DisplayAlertAsync(AppResources.Error, AppResources.UnexpectedErrorOccurred, AppResources.AlertAcceptText);
                }

                UserDialogs.Instance.HideLoading();
            });
        }

        public async Task InitializeData()
        {
            await Device.InvokeOnMainThreadAsync(async () =>
            {
                this.IsBusy = true;
                UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);

                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("UidUsuario", this.UidUsuario.ToString());
                parameters.Add("UIDPERFIL", "4F1E1C4B-3253-4225-9E46-DD7D1940DA19");

                var userRequest = await this.HttpService.GetAsync<UserInformation>("Usuario/GetBuscarUsuarios_Movil", parameters);

                if (userRequest.Code == HttpResponseCode.Success)
                {
                    var ladaRequest = await this.HttpService.GetAsync<IEnumerable<PhoneLada>>("Telefono/GetReadAllInternationalLadas");
                    LadaList = ladaRequest.Result.ToList();

                    this.User = userRequest.Result;

                    this.Name = userRequest.Result.UserData.Name;
                    this.FirstLastName = userRequest.Result.UserData.LastName;
                    this.SecondLastName = userRequest.Result.UserData.SecondLastName;

                    this.Birthday = this.StringToDate(this.User.UserData.BirthDay);
                    idPhone = User.PhoneInformation.Uid;
                    Lada = LadaList.Find(l => l.Uid == User.PhoneInformation.UidLada.ToString());

                    Phone = User.PhoneInformation.number;
                }
                else
                {
                    await this.PageDialogService.DisplayAlertAsync(AppResources.Error, AppResources.UnexpectedErrorOccurred, AppResources.AlertAcceptText);
                    await this.NavigationService.GoBackAsync();
                }

                this.IsBusy = false;
                UserDialogs.Instance.HideLoading();
            });
        }

        private DateTime StringToDate(string strDate)
        {
            try
            {
                string day = strDate.Substring(0, 2);
                string month = strDate.Substring(3, 2);
                string year = strDate.Substring(6, 4);
                DateTime date = new DateTime(int.Parse(year), int.Parse(month), int.Parse(day));

                return date;
            }
            catch (Exception ex)
            {
                return new DateTime(2000, 01, 01);
            }
        }
        #endregion

        public override void Initialize(INavigationParameters parameters)
        {
            Task.Run(async () => { await this.InitializeData(); });
        }
    }
}
