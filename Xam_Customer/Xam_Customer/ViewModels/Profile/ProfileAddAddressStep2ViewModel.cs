﻿using Acr.UserDialogs;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model.Core;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.Profile;
using Xam_Customer.ViewModels.Core;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class ProfileAddAddressStep2ViewModel : BaseNavigationViewModel
    {
        #region Properties
        private string title;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        private string subtitle;
        public string Subtitle
        {
            get { return subtitle; }
            set { SetProperty(ref subtitle, value); }
        }

        private Color deleteColor;
        public Color DeleteColor
        {
            get { return deleteColor; }
            set { SetProperty(ref deleteColor, value); }
        }
        private bool deleteenable;
        public bool Deleteenable
        {
            get { return deleteenable; }
            set { SetProperty(ref deleteenable, value); }
        }
        private bool isBusy;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        private ObservableCollection<ListboxItemModel> paises;
        public ObservableCollection<ListboxItemModel> Paises
        {
            get { return paises; }
            set { SetProperty(ref paises, value); }
        }

        private ObservableCollection<ListboxItemModel> estados;
        public ObservableCollection<ListboxItemModel> Estados
        {
            get { return estados; }
            set { SetProperty(ref estados, value); }
        }

        private ObservableCollection<ListboxItemModel> municipios;
        public ObservableCollection<ListboxItemModel> Municipios
        {
            get { return municipios; }
            set { SetProperty(ref municipios, value); }
        }

        private ObservableCollection<ListboxItemModel> ciudades;
        public ObservableCollection<ListboxItemModel> Ciudades
        {
            get { return ciudades; }
            set { SetProperty(ref ciudades, value); }
        }

        private ObservableCollection<ListboxItemModel> colonias;
        public ObservableCollection<ListboxItemModel> Colonias
        {
            get { return colonias; }
            set { SetProperty(ref colonias, value); }
        }

        private bool statusAddress;
        public bool StatusAddress
        {
            get { return statusAddress; }
            set { SetProperty(ref statusAddress, value); }
        }
        private bool defaultAddress;
        public bool DefaultAddress
        {
            get { return defaultAddress; }
            set
            {
                SetProperty(ref defaultAddress, value);
            }
        }
        private string identificador;
        public string Identificador
        {
            get { return identificador; }
            set { SetProperty(ref identificador, value); }
        }

        private ListboxItemModel pais;
        public ListboxItemModel Pais
        {
            get { return pais; }
            set
            {
                if (this.IsBusy) { return; }
                SetProperty(ref pais, value);
                Device.InvokeOnMainThreadAsync(async () => { await this.ObtenerEstados(value.Uid); });
            }
        }

        private ListboxItemModel estado;
        public ListboxItemModel Estado
        {
            get { return estado; }
            set
            {
                if (this.IsBusy) { return; }
                SetProperty(ref estado, value);
                Device.InvokeOnMainThreadAsync(async () => { await this.ObtenerMunicipios(value.Uid); });
            }
        }

        private ListboxItemModel municipio;
        public ListboxItemModel Municipio
        {
            get { return municipio; }
            set
            {
                if (this.IsBusy) { return; }
                SetProperty(ref municipio, value);
                Device.InvokeOnMainThreadAsync(async () => { await this.ObtenerCiudades(value.Uid); });
            }
        }

        private ListboxItemModel ciudad;
        public ListboxItemModel Ciudad
        {
            get { return ciudad; }
            set
            {
                if (this.IsBusy) { return; }
                SetProperty(ref ciudad, value);
                Device.InvokeOnMainThreadAsync(async () => { await this.ObtenerColonias(value.Uid); });
            }
        }

        private ListboxItemModel colonia;
        public ListboxItemModel Colonia
        {
            get { return colonia; }
            set
            {
                SetProperty(ref colonia, value);
                this.ObtenerCodigoPostal(value.Uid);
            }
        }

        private string calle;
        public string Calle
        {
            get { return calle; }
            set { SetProperty(ref calle, value); }
        }

        private string entreCalle;
        public string EntreCalle
        {
            get { return entreCalle; }
            set { SetProperty(ref entreCalle, value); }
        }

        private string yCalle;
        public string YCalle
        {
            get { return yCalle; }
            set { SetProperty(ref yCalle, value); }
        }

        private string lote;
        public string Lote
        {
            get { return lote; }
            set { SetProperty(ref lote, value); }
        }

        private string codigoPostal;
        public string CodigoPostal
        {
            get { return codigoPostal; }
            set { SetProperty(ref codigoPostal, value); }
        }

        private string manzana;
        public string Manzana
        {
            get { return manzana; }
            set { SetProperty(ref manzana, value); }
        }

        private string referencias;
        public string Referencias
        {
            get { return referencias; }
            set { SetProperty(ref referencias, value); }
        }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        #endregion

        #region Private
        private Address __Address;

        public Address Address
        {
            get { return __Address; }
            set { SetProperty(ref __Address, value); }
        }

        private string _Action;

        public string Action
        {
            get { return _Action; }
            set { _Action = value; }
        }
        public Guid UidUsuario { get; set; }
        #endregion

        #region Services
        private IHttpRequestService HttpService { get; }
        private IPageDialogService PageDialogService { get; }
        private INavigationService _navigationService { get; }
        #endregion

        #region Commands
        public DelegateCommand UpdateLocationCommand { get; }
        public DelegateCommand SaveCommand { get; }
        public DelegateCommand DeleteAddress { get; }
        public DelegateCommand municipalityCommand { get; }
        public DelegateCommand CityCommand { get; }
        public DelegateCommand SuburCommand { get; }
        public DelegateCommand NextPage { get; set; }
        public DelegateCommand BackPage { get; set; }
        public DelegateCommand ToggleSwitch { get; set; }
        #endregion
        public ProfileAddAddressStep2ViewModel(INavigationService navigationService, IHttpRequestService httpRequestService, IPageDialogService pageDialogService) : base(navigationService)
        {
            HttpService = httpRequestService;
            _navigationService = navigationService;
            PageDialogService = pageDialogService;
            this.Paises = new ObservableCollection<ListboxItemModel>();
            this.Estados = new ObservableCollection<ListboxItemModel>();
            this.Municipios = new ObservableCollection<ListboxItemModel>();
            this.Ciudades = new ObservableCollection<ListboxItemModel>();
            this.Colonias = new ObservableCollection<ListboxItemModel>();
            NextPage = new DelegateCommand(Next);
            BackPage = new DelegateCommand(back);
            ToggleSwitch = new DelegateCommand(checaDireccionPredeterminada);
        }

        private async void Next()
        {
            UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);

            if (this.Pais == null)
            {
                UserDialogs.Instance.HideLoading();
                this.IsBusy = false;
                await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_Country} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                return;
            }

            if (this.Estado == null)
            {
                UserDialogs.Instance.HideLoading();
                this.IsBusy = false;
                await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_State} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                return;
            }

            if (this.Municipio == null)
            {
                UserDialogs.Instance.HideLoading();
                this.IsBusy = false;
                await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_Municipality} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                return;
            }

            if (this.Ciudad == null)
            {
                UserDialogs.Instance.HideLoading();
                this.IsBusy = false;
                await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_City} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                return;
            }

            if (this.Colonia == null)
            {
                UserDialogs.Instance.HideLoading();
                this.IsBusy = false;
                await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_Neighborhood} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                return;
            }

            if (string.IsNullOrEmpty(this.Referencias))
            {
                UserDialogs.Instance.HideLoading();
                this.IsBusy = false;
                await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_References} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                return;
            }

            Address.Country = Pais.StrUid;
            Address.CountryName = Pais.Name;
            Address.StateName = Estado.Name;
            Address.UidState = Estado.StrUid;
            Address.Municipality = Municipio.StrUid;
            Address.UidCity = Ciudad.StrUid;
            Address.CityName = Ciudad.Name;
            Address.UidSuburb = Colonia.StrUid;
            Address.SuburbName = Colonia.Name;
            Address.Reference = Referencias;
            Address.DefaultAddress = DefaultAddress;

            UserDialogs.Instance.HideLoading();

            NavigationParameters parameters = new NavigationParameters();
            parameters.Add("data", Address);
            parameters.Add("action", Action);
            await _navigationService.NavigateAsync("ProfileAddAddressStep3", parameters);
        }
        private async void back() { await _navigationService.GoBackAsync(); }
        #region List informacion methods
        private async Task ObtenerPaises()
        {
            this.IsBusy = true;

            var result = await this.HttpService.GetAsync<IEnumerable<ListboxItemModel>>("Direccion/ObtenerPaises");
            if (result.Code == HttpResponseCode.Success)
            {
                foreach (var item in result.Result)
                {
                    if (item.Uid != Guid.Empty)
                    {
                        this.Paises.Add(item);
                    }
                }
            }

            this.IsBusy = false;

            if (this.Address != null)
            {
                ListboxItemModel pais = this.Paises.FirstOrDefault(i => i.Uid == Guid.Parse(this.Address.Country));
                if (pais != null)
                {
                    this.Pais = pais;
                }
            }
            if (this.Estados.Any())
            {
                this.Estados = new ObservableCollection<ListboxItemModel>();
            }
            if (this.Municipios.Any())
            {
                this.Municipios = new ObservableCollection<ListboxItemModel>();
            }
            if (this.Ciudades.Any())
            {
                this.Ciudades = new ObservableCollection<ListboxItemModel>();
            }
            if (this.Colonias.Any())
            {
                this.Colonias = new ObservableCollection<ListboxItemModel>();
            }
        }
        private async Task ObtenerEstados(Guid uid)
        {
            if (this.IsBusy)
            {
                return;
            }

            this.IsBusy = true;

            if (this.Estados.Any())
            {
                this.Estados = new ObservableCollection<ListboxItemModel>();
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidPais", uid.ToString());

            var result = await this.HttpService.GetAsync<IEnumerable<ListboxItemModel>>("Direccion/ObtenerEstadosPais", parameters);
            if (result.Code == HttpResponseCode.Success)
            {
                foreach (var item in result.Result)
                {
                    if (item.Uid != Guid.Empty)
                    {
                        this.Estados.Add(item);
                    }
                }
            }

            this.IsBusy = false;

            if (this.Address != null)
            {
                ListboxItemModel lEstado = this.Estados.FirstOrDefault(i => i.Uid == Guid.Parse(this.Address.UidState));
                if (lEstado != null)
                {
                    this.Estado = lEstado;
                }
            }
            if (this.Municipios.Any())
            {
                this.Municipios = new ObservableCollection<ListboxItemModel>();
            }
            if (this.Ciudades.Any())
            {
                this.Ciudades = new ObservableCollection<ListboxItemModel>();
            }
            if (this.Colonias.Any())
            {
                this.Colonias = new ObservableCollection<ListboxItemModel>();
            }
        }
        private async Task ObtenerMunicipios(Guid uid)
        {
            if (this.IsBusy)
            {
                return;
            }

            this.IsBusy = true;

            if (this.Municipios.Any())
            {
                this.Municipios = new ObservableCollection<ListboxItemModel>();
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidEstado", uid.ToString());

            var result = await this.HttpService.GetAsync<IEnumerable<ListboxItemModel>>("Direccion/ObtenerMunicipiosEstado", parameters);
            if (result.Code == HttpResponseCode.Success)
            {
                foreach (var item in result.Result)
                {
                    if (item.Uid != Guid.Empty)
                    {
                        this.Municipios.Add(item);
                    }
                }
            }

            this.IsBusy = false;

            if (this.Address != null)
            {
                ListboxItemModel lMunicipio = this.Municipios.FirstOrDefault(i => i.Uid == Guid.Parse(this.Address.Municipality));
                if (lMunicipio != null)
                {
                    this.Municipio = lMunicipio;
                }
            }
            if (this.Ciudades.Any())
            {
                this.Ciudades = new ObservableCollection<ListboxItemModel>();
            }
            if (this.Colonias.Any())
            {
                this.Colonias = new ObservableCollection<ListboxItemModel>();
            }
        }
        private async Task ObtenerCiudades(Guid uid)
        {
            if (this.IsBusy)
            {
                return;
            }

            this.IsBusy = true;

            if (this.Ciudades.Any())
            {
                this.Ciudades = new ObservableCollection<ListboxItemModel>();
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidMunicipio", uid.ToString());

            var result = await this.HttpService.GetAsync<IEnumerable<ListboxItemModel>>("Direccion/ObtenerCiudadesMunicipio", parameters);
            if (result.Code == HttpResponseCode.Success)
            {
                foreach (var item in result.Result)
                {
                    if (item.Uid != Guid.Empty)
                    {
                        this.Ciudades.Add(item);
                    }
                }
            }

            this.IsBusy = false;

            if (this.Address != null)
            {
                ListboxItemModel lCiudad = this.Ciudades.FirstOrDefault(i => i.Uid == Guid.Parse(this.Address.UidCity));
                if (lCiudad != null)
                {
                    this.Ciudad = lCiudad;
                }
            }
            if (this.Colonias.Any())
            {
                this.Colonias = new ObservableCollection<ListboxItemModel>();
            }
        }

        private async Task ObtenerColonias(Guid uid)
        {
            if (this.IsBusy)
            {
                return;
            }

            this.IsBusy = true;

            if (this.Colonias.Any())
            {
                this.Colonias = new ObservableCollection<ListboxItemModel>();
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidCiudad", uid.ToString());

            var result = await this.HttpService.GetAsync<IEnumerable<ListboxItemModel>>("Direccion/ObtenerColoniasCiudad", parameters);
            if (result.Code == HttpResponseCode.Success)
            {
                foreach (var item in result.Result)
                {
                    if (item.Uid != Guid.Empty)
                    {
                        this.Colonias.Add(item);
                    }
                }
            }

            this.IsBusy = false;

            if (this.Address != null)
            {
                ListboxItemModel lColonia = this.Colonias.FirstOrDefault(i => i.Uid == Guid.Parse(this.Address.UidSuburb));
                if (lColonia != null)
                {
                    this.Colonia = lColonia;
                }
            }
        }

        private async void ObtenerCodigoPostal(Guid Uid)
        {
            await loadZipCode(Uid);
        }
        private async Task loadZipCode(Guid Uid)
        {
            if (this.IsBusy)
            {
                return;
            }

            this.IsBusy = true;

            if (!string.IsNullOrEmpty(CodigoPostal))
            {
                this.CodigoPostal = string.Empty;
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidColonia", Uid.ToString());

            var result = await this.HttpService.GetAsync<Address>("Direccion/ObtenerCodigoPostal", parameters);
            if (result.Code == HttpResponseCode.Success)
            {
                var colonia = result.Result;
                CodigoPostal = colonia.ZipCode;
                IsBusy = false;
            }
        }

        private void checaDireccionPredeterminada()
        {
            if (!string.IsNullOrEmpty(Action) && Address.DefaultAddress)
            {
                DefaultAddress = true;
            }
        }
        #endregion
        public override async void Initialize(INavigationParameters parameters)
        {
            UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);

            if (parameters.ContainsKey("action"))
            {
                this.Action = parameters.GetValue<string>("action");
            }
            else
            {
                this.Action = "New";
            }
            Deleteenable = false;
            DeleteColor = Color.Transparent;
            if (parameters.ContainsKey("data"))
            {
                this.Address = parameters.GetValue<Address>("data");

                this.Identificador = this.Address.Identifier;
                this.Calle = this.Address.Street0;
                //this.EntreCalle = this._Address.Street1;
                //this.YCalle = this._Address.Street2;
                //this.Lote = this._Address.Lot;
                //this.CodigoPostal = this._Address.ZipCode;
                //this.Manzana = this._Address.Block;
                this.Referencias = this.Address.Reference;

                this.Latitude = this.Address.Latitude;
                this.Longitude = this.Address.Longitude;
                this.DefaultAddress = this.Address.DefaultAddress;

            }

            this.Title = this.Action == "Edit" ? ProfileLang.AddressForm_Edit_Text : ProfileLang.AddressForm_Add_Text;

            if (this.Action == "FirstAddress")
            {
                this.Subtitle = ProfileLang.AddressForm_AddDefault;
                this.DefaultAddress = true;
            }
            await Task.WhenAll(ObtenerPaises());

            UserDialogs.Instance.HideLoading();

        }
    }
}
