﻿using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Model.System;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.Profile;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xam_Customer.Views.Profile;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class ProfileAddressesPageViewModel : BaseNavigationViewModel
    {
        #region Properties
        private ObservableCollection<Address> addresses;
        public ObservableCollection<Address> Addresses
        {
            get { return addresses; }
            set { SetProperty(ref addresses, value); }
        }
        private ObservableCollection<Address> addressesDefault;
        public ObservableCollection<Address> AddressesDefault
        {
            get { return addressesDefault; }
            set { SetProperty(ref addressesDefault, value); }
        }

        private bool isLoadingList;
        public bool IsLoadingList
        {
            get { return isLoadingList; }
            set { SetProperty(ref isLoadingList, value); }
        }



        private bool noAddresses;
        public bool NoAddresses
        {
            get { return noAddresses; }
            set { SetProperty(ref noAddresses, value); }
        }
        private bool editingMode;
        public bool EditingMode
        {
            get { return editingMode; }
            set { SetProperty(ref editingMode, value); }
        }
        private bool selectingMode;
        public bool SelectingMode
        {
            get { return selectingMode; }
            set { SetProperty(ref selectingMode, value); }
        }
        private string _SearchWord;

        public string SearchWord
        {
            get { return _SearchWord; }
            set { SetProperty(ref _SearchWord, value); }
        }

        #endregion

        #region Private
        public Guid UidUser { get; set; }

        private List<Address> UserAddresses { get; set; }
        #endregion

        #region Services
        public IHttpRequestService HttpService { get; set; }
        public IAppInstaceService _appInstanceService { get; set; }
        public IPageDialogService _PageDialogService { get; set; }
        #endregion

        #region Command
        public DelegateCommand<string> AddressClickedCommand { get; }
        public DelegateCommand NewAddressCommand { get; }
        public DelegateCommand DefaultAddressCommand { get; }
        public DelegateCommand RefreshCommand { get; }
        public DelegateCommand SearchAddress { get; }

        public DelegateCommand<string> DeleteAddress { get; }
        #endregion

        public ProfileAddressesPageViewModel(
            INavigationService navigationService,
            IHttpRequestService httpRequestService,
            IAppInstaceService appInstaceService,
            IPageDialogService pageDialogService
            ) : base(navigationService)
        {
            this.HttpService = httpRequestService;
            this._appInstanceService = appInstaceService;
            this._PageDialogService = pageDialogService;
            this.AddressClickedCommand = new DelegateCommand<string>(this.AddressClicked);
            this.NewAddressCommand = new DelegateCommand(this.NewAddress);
            this.DefaultAddressCommand = new DelegateCommand(this.ChangeDetaulftAddressByBotton);
            this.DeleteAddress = new DelegateCommand<string>(this.eliminar);
            this.RefreshCommand = new DelegateCommand(async () => { await InitializeData(); });
            this.SearchAddress = new DelegateCommand(Search);

            this.UidUser = Guid.Parse(Settings.UserSession.StrUid);
            this.Addresses = new ObservableCollection<Address>();
            this.AddressesDefault = new ObservableCollection<Address>();
        }

        #region Implementation
        public void Search()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("UidUsuario", this.UidUser.ToString());

                var addressesRequest = await this.HttpService.GetAsync<IEnumerable<Address>>("Direccion/GetObtenerDireccionUsuario_Movil", parameters);

                if (addressesRequest.Code == HttpResponseCode.Success)
                {
                    if (!string.IsNullOrEmpty(SearchWord))
                    {
                        List<Address> Address = new List<Address>();
                        if (string.IsNullOrEmpty(SearchWord))
                        {
                            Address = addressesRequest.Result.ToList();
                        }
                        else
                        {
                            Address = addressesRequest.Result.Where(d => d.Identifier.ToLower().Contains(SearchWord.ToLower())).ToList();

                        }
                        if (SelectingMode)
                        {
                            AddressesDefault = new ObservableCollection<Address>();
                            foreach (var address in Address)
                            {
                                this.AddressesDefault.Add(new Address()
                                {
                                    Uid = address.Uid,
                                    Identifier = address.Identifier,
                                    DefaultAddress = address.DefaultAddress,
                                    UidState = address.UidState,
                                    UidSuburb = address.UidSuburb,
                                    UidCity = address.UidCity,
                                    Country = address.Country,
                                    Municipality = address.Municipality,
                                    Reference = address.Reference,
                                    Longitude = address.Longitude,
                                    Latitude = address.Latitude,
                                    Street0 = $"{address.SuburbName}, {address.CityName}, {address.StateName}, {address.CountryName} "
                                });
                            }
                        }
                        else
                        {
                            Addresses = new ObservableCollection<Address>();
                            foreach (var address in Address)
                            {
                                this.Addresses.Add(new Address()
                                {
                                    Uid = address.Uid,
                                    Identifier = address.Identifier,
                                    DefaultAddress = address.DefaultAddress,
                                    UidState = address.UidState,
                                    UidSuburb = address.UidSuburb,
                                    UidCity = address.UidCity,
                                    Country = address.Country,
                                    Municipality = address.Municipality,
                                    Reference = address.Reference,
                                    Longitude = address.Longitude,
                                    Latitude = address.Latitude,
                                    Street0 = $"{address.SuburbName}, {address.CityName}, {address.StateName}, {address.CountryName} "
                                });
                            }
                        }
                    }
                }
            });
        }
        public void ChangeDetaulftAddress(string Uid)
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                Address direccion = Addresses.Where(p => p.Uid == Uid).FirstOrDefault();
                var respuesta = false;
                if (_appInstanceService.ProductsCount() == 0)
                {
                    respuesta = await _PageDialogService.DisplayAlertAsync(ProfileLang.AddressForm_TittleDefaultAddress, ProfileLang.AddressForm_MessageDefaultAddress + " " + direccion.Identifier + "?", AppResources.Error_General_Confirm, AppResources.Error_General_CancelUpdate);
                }
                else
                {
                    respuesta = await _PageDialogService.DisplayAlertAsync(ProfileLang.AddressForm_TittleDefaultAddress, ProfileLang.AddressForm_MessageDefaultAddress + " " + direccion.Identifier + "?" + ProfileLang.AddressForm_MessageDefaultAddress2, AppResources.Error_General_Confirm, AppResources.Error_General_CancelUpdate);
                }
                if (respuesta)
                {
                    AddressUpload upload = new AddressUpload()
                    {
                        Uid = Guid.Parse(direccion.Uid),
                        Identificador = direccion.Identifier,
                        UidPais = new Guid(direccion.Country),
                        UidEstado = new Guid(direccion.UidState),
                        UidMunicipio = new Guid(direccion.Municipality),
                        UidCiudad = new Guid(direccion.UidCity),
                        UidColonia = new Guid(direccion.UidSuburb),
                        UidUsuario = new Guid(Settings.UserSession.StrUid),
                        Calle = "",
                        EntreCalle = string.IsNullOrEmpty("") ? "" : "",
                        yCalle = string.IsNullOrEmpty("") ? "" : "",
                        Lote = "",
                        CodigoPostal = "",
                        Manzana = "",
                        Referencias = direccion.Reference,
                        Latitude = direccion.Latitude,
                        Longitude = direccion.Longitude,
                        Status = true,
                        DefaultAddress = true
                    };
                    string content = JsonConvert.SerializeObject(upload);
                    HttpResponseCode responseCode = await this.HttpService.PostAsync("Direccion/Actualizar", content);
                    if (responseCode == HttpResponseCode.Success)
                    {
                        await this._PageDialogService.DisplayAlertAsync("", ProfileLang.AddressForm_UpdatedSuccessfully, AppResources.AlertAcceptText);
                        await this.InitializeData();
                        _appInstanceService.UserSelectedDeliveryAddress(direccion.Uid, direccion.UidState, direccion.UidSuburb, direccion.Identifier, double.Parse(direccion.Latitude), double.Parse(direccion.Longitude));
                        _appInstanceService.ClearCart();
                    }
                }
            });
        }
        public void ChangeDetaulftAddressByBotton()
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                Address direccion = AddressesDefault.Where(p => p.DefaultAddress == true).FirstOrDefault();
                if (_appInstanceService.GetUserSelectedDeliveryAddress() != direccion.Uid)
                {
                    var respuesta = false;
                    if (_appInstanceService.ProductsCount() == 0)
                    {
                        respuesta = await _PageDialogService.DisplayAlertAsync(ProfileLang.AddressForm_TittleDefaultAddress, ProfileLang.AddressForm_MessageDefaultAddress + " " + direccion.Identifier + "?", AppResources.Error_General_Confirm, AppResources.Error_General_CancelUpdate);
                    }
                    else
                    {
                        respuesta = await _PageDialogService.DisplayAlertAsync(ProfileLang.AddressForm_TittleDefaultAddress, ProfileLang.AddressForm_MessageDefaultAddress + " " + direccion.Identifier + "?" + ProfileLang.AddressForm_MessageDefaultAddress2, AppResources.Error_General_Confirm, AppResources.Error_General_CancelUpdate);
                    }
                    if (respuesta)
                    {
                        AddressUpload upload = new AddressUpload()
                        {
                            Uid = Guid.Parse(direccion.Uid),
                            Identificador = direccion.Identifier,
                            UidPais = new Guid(direccion.Country),
                            UidEstado = new Guid(direccion.UidState),
                            UidMunicipio = new Guid(direccion.Municipality),
                            UidCiudad = new Guid(direccion.UidCity),
                            UidColonia = new Guid(direccion.UidSuburb),
                            UidUsuario = new Guid(Settings.UserSession.StrUid),
                            Calle = "",
                            EntreCalle = string.IsNullOrEmpty("") ? "" : "",
                            yCalle = string.IsNullOrEmpty("") ? "" : "",
                            Lote = "",
                            CodigoPostal = "",
                            Manzana = "",
                            Referencias = direccion.Reference,
                            Latitude = direccion.Latitude,
                            Longitude = direccion.Longitude,
                            Status = true,
                            DefaultAddress = true
                        };
                        string content = JsonConvert.SerializeObject(upload);
                        HttpResponseCode responseCode = await this.HttpService.PostAsync("Direccion/Actualizar", content);
                        if (responseCode == HttpResponseCode.Success)
                        {
                            //await this._PageDialogService.DisplayAlertAsync("", ProfileLang.AddressForm_UpdatedSuccessfully, AppResources.AlertAcceptText);
                            await this.InitializeData();
                            _appInstanceService.UserSelectedDeliveryAddress(direccion.Uid, direccion.UidState, direccion.UidSuburb, direccion.Identifier, double.Parse(direccion.Latitude), double.Parse(direccion.Longitude));
                            _appInstanceService.ClearCart();
                        }
                    }
                }
                await NavigationService.GoBackAsync();
            });
        }
        private async Task InitializeData()
        {
            this.IsLoadingList = true;
            this.Addresses = new ObservableCollection<Address>();
            this.AddressesDefault = new ObservableCollection<Address>();

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidUsuario", this.UidUser.ToString());

            var addressesRequest = await this.HttpService.GetAsync<IEnumerable<Address>>("Direccion/GetObtenerDireccionUsuario_Movil", parameters);

            if (addressesRequest.Code == HttpResponseCode.Success)
            {
                this.UserAddresses = addressesRequest.Result.ToList();
                this.NoAddresses = !addressesRequest.Result.Any();

                if (UserAddresses.Count > 0)
                {
                    try
                    {
                        foreach (Address address in this.UserAddresses)
                        {
                            if (address.Reference == "No hay información")
                            {
                                address.Reference = string.Empty;
                            }
                            this.Addresses.Add(new Address()
                            {
                                Uid = address.Uid,
                                Identifier = address.Identifier,
                                DefaultAddress = address.DefaultAddress,
                                UidState = address.UidState,
                                UidSuburb = address.UidSuburb,
                                UidCity = address.UidCity,
                                Country = address.Country,
                                Municipality = address.Municipality,
                                Reference = address.Reference,
                                Longitude = address.Longitude,
                                Latitude = address.Latitude,
                                Street0 = $"{address.SuburbName}, {address.CityName}, {address.StateName}, {address.CountryName} "
                            });
                            this.AddressesDefault.Add(new Address()
                            {
                                Uid = address.Uid,
                                Identifier = address.Identifier,
                                DefaultAddress = address.DefaultAddress,
                                UidState = address.UidState,
                                UidSuburb = address.UidSuburb,
                                UidCity = address.UidCity,
                                Country = address.Country,
                                Municipality = address.Municipality,
                                Reference = address.Reference,
                                Longitude = address.Longitude,
                                Latitude = address.Latitude,
                                Street0 = $"{address.SuburbName}, {address.CityName}, {address.StateName}, {address.CountryName} "
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        throw;
                    }

                    _appInstanceService.HasAddresses = true;
                    _appInstanceService.SetAddressTotal(0);
                    _appInstanceService.SetAddressTotal(UserAddresses.Count);
                    this.IsLoadingList = false;
                }
                else
                {
                    _appInstanceService.SelectDeliveryAddress("", "", "", 0.0, 0.0);
                    _appInstanceService.UserSelectedDeliveryAddress("", "", "", "", 0.0, 0.0);
                    _appInstanceService.HasAddresses = false;
                    _appInstanceService.SetAddressTotal(0);
                }
            }
            else
            {

            }
            this.IsLoadingList = false;
        }
        public void eliminar(string Uid)
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                Address direccion = Addresses.Where(p => p.Uid == Uid).FirstOrDefault();

                var answer = await _PageDialogService.DisplayAlertAsync(ProfileLang.AddressForm_DeleteLocationTitle, ProfileLang.AddressForm_DeleteLocationMessage + " " + direccion.Identifier, AppResources.Error_General_Confirm, AppResources.Error_General_CancelUpdate);
                if (answer)
                {
                    AddressUpload upload = new AddressUpload()
                    {
                        Uid = Guid.Parse(direccion.Uid),
                        Identificador = direccion.Identifier,
                        UidPais = new Guid(direccion.Country),
                        UidEstado = new Guid(direccion.UidState),
                        UidMunicipio = new Guid(direccion.Municipality),
                        UidCiudad = new Guid(direccion.UidCity),
                        UidColonia = new Guid(direccion.UidSuburb),
                        UidUsuario = new Guid(Settings.UserSession.StrUid),
                        Calle = "",
                        EntreCalle = string.IsNullOrEmpty("") ? "" : "",
                        yCalle = string.IsNullOrEmpty("") ? "" : "",
                        Lote = "",
                        CodigoPostal = "",
                        Manzana = "",
                        Referencias = direccion.Reference,
                        Latitude = direccion.Latitude,
                        Longitude = direccion.Longitude,
                        Status = false,
                        DefaultAddress = false
                    };
                    string content = JsonConvert.SerializeObject(upload);
                    HttpResponseCode responseCode = await this.HttpService.PostAsync("Direccion/Actualizar", content);
                    if (responseCode == HttpResponseCode.Success)
                    {
                        await this._PageDialogService.DisplayAlertAsync("", ProfileLang.AddressForm_UpdatedSuccessfully, AppResources.AlertAcceptText);
                        await this.InitializeData();
                    }

                }
            });
        }
        public void AddressEdit(string Uid)
        {
            Address direccion = Addresses.Where(p => p.Uid == Uid).FirstOrDefault();
            if (direccion != null)
            {
                Device.InvokeOnMainThreadAsync(async () =>
                {
                    await this.NavigationService.NavigateAsync($"{nameof(ProfileAddAddressStep1)}", new NavigationParameters()
                    {
                        { "data", direccion},
                        { "action", "Edit" },
                        { "mapLocation", new MapPin(){Identifier = direccion.Identifier, Latitude = direccion.Latitude, Longitude = direccion.Longitude } }
                    });
                });
            }
        }
        public void AddressClicked(string Uid)
        {
            Address direccion = AddressesDefault.Where(p => p.Uid == Uid).FirstOrDefault();
            if (direccion != null)
            {
                foreach (var item in AddressesDefault)
                {
                    item.DefaultAddress = false;
                }
                direccion.DefaultAddress = true;

            }
        }
        private async void NewAddress()
        {
            if (UserAddresses.Count == 0)
            {
                await this.NavigationService.NavigateAsync($"{nameof(ProfileAddAddressStep1)}", new NavigationParameters() { { "action", "FirstAddress" }, });
            }
            else
            {
                await this.NavigationService.NavigateAsync($"{nameof(ProfileAddAddressStep1)}", new NavigationParameters() { { "action", "" }, });
            }
        }
        #endregion

        public override void Initialize(INavigationParameters parameters)
        {
            Device.InvokeOnMainThreadAsync(async () => { await this.InitializeData(); });
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("Edicion"))
            {
                EditingMode = true;
                SelectingMode = false;
            }
            if (parameters.ContainsKey("Seleccion"))
            {
                EditingMode = false;
                SelectingMode = true;
            }
            if (parameters.GetNavigationMode() == NavigationMode.Back)
            {
                Device.InvokeOnMainThreadAsync(async () => { await this.InitializeData(); });
            }
        }
    }
}
