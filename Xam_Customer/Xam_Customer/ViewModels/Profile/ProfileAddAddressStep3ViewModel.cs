﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.ViewModels.Core;
using Xamarin.Forms;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Model.System;
using Prism.Events;
using Newtonsoft.Json;
using Acr.UserDialogs;
using Xam_Customer.Core.Enum;
using Xam_Customer.Resources.LangResx.Profile;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Views.Layout;
using Prism.Services;
using Xam_Customer.Core.Model;
using Xam_Customer.Helpers;
using Xam_Customer.Core.Services.Abstract;

namespace Xam_Customer.ViewModels
{
    public class ProfileAddAddressStep3ViewModel : BaseNavigationViewModel
    {
        #region Services
        public IEventAggregator _eventAggregator { get; set; }
        public INavigationService _navigationService { get; set; }
        public IPageDialogService PageDialogService { get; set; }
        public IHttpRequestService HttpService { get; set; }
        private IAppInstaceService AppInstaceService { get; }

        #endregion
        public ProfileAddAddressStep3ViewModel(IEventAggregator eventAggregator,
            INavigationService navigationService,
            IPageDialogService pageDialogService,
            IHttpRequestService httpRequestService,
            IAppInstaceService appInstaceService) : base(navigationService)
        {
            this._eventAggregator = eventAggregator;
            this._navigationService = navigationService;
            this.PageDialogService = pageDialogService;
            this.HttpService = httpRequestService;
            this.AppInstaceService = appInstaceService;
            BackPage = new DelegateCommand(Regresar);
            Save = new DelegateCommand(Guardar);
            UserSession user = Settings.UserSession;
            this.UidUsuario = Guid.Parse(user.StrUid);
        }


        private string _Action;

        public string Action
        {
            get { return _Action; }
            set { _Action = value; }
        }

        private Guid _UidUsuario;

        public Guid UidUsuario
        {
            get { return _UidUsuario; }
            set { SetProperty(ref _UidUsuario, value); }
        }

        #region Address
        private bool defaultAddress;
        public bool DefaultAddress
        {
            get { return defaultAddress; }
            set { SetProperty(ref defaultAddress, value); }
        }
        private string _identifier;
        public string Identifier
        {
            get { return _identifier; }
            set { SetProperty(ref _identifier, value); }
        }
        private string _Country;
        public string Country
        {
            get { return _Country; }
            set { SetProperty(ref _Country, value); }
        }
        private string _State;
        public string State
        {
            get { return _State; }
            set { SetProperty(ref _State, value); }
        }
        private string _City;
        public string City
        {
            get { return _City; }
            set { SetProperty(ref _City, value); }
        }
        private string _street;
        public string Street
        {
            get { return _street; }
            set { SetProperty(ref _street, value); }
        }
        private string _SuburbName;
        public string SuburbName
        {
            get { return _SuburbName; }
            set { SetProperty(ref _SuburbName, value); }
        }

        private Address direccion;
        #endregion
        #region Commands
        public DelegateCommand BackPage { get; set; }
        public DelegateCommand Save { get; set; }
        #endregion

        #region Implementation
        private async void Guardar()
        {
            AddressUpload upload = new AddressUpload()
            {
                Uid = Guid.Empty,
                Identificador = this.direccion.Identifier,
                UidPais = new Guid(this.direccion.Country),
                UidEstado = new Guid(this.direccion.UidState),
                UidMunicipio = new Guid(this.direccion.Municipality),
                UidCiudad = new Guid(this.direccion.UidCity),
                UidColonia = new Guid(this.direccion.UidSuburb),
                UidUsuario = UidUsuario,
                Calle = "",
                EntreCalle = "",
                yCalle = "",
                Lote = "",
                CodigoPostal = "",
                Manzana = "",
                Referencias = direccion.Reference,
                Latitude = direccion.Latitude,
                Longitude = direccion.Longitude,
                Status = true,
                DefaultAddress = direccion.DefaultAddress
            };
            UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);
            if (this.Action == "Edit")
            {
                upload.Uid = Guid.Parse(this.direccion.Uid);
                string content = JsonConvert.SerializeObject(upload);
                HttpResponseCode responseCode = await this.HttpService.PostAsync("Direccion/Actualizar", content);
                if (responseCode == HttpResponseCode.Success)
                {
                    UserDialogs.Instance.HideLoading();
                    await this.PageDialogService.DisplayAlertAsync("", ProfileLang.AddressForm_UpdatedSuccessfully, AppResources.AlertAcceptText);
                    await this.NavigationService.GoBackToRootAsync();
                }
                else
                {
                    UserDialogs.Instance.HideLoading();
                    await this.PageDialogService.DisplayAlertAsync(AppResources.Error, AppResources.UnexpectedErrorOccurred, AppResources.AlertAcceptText);
                }
            }
            else if (this.Action == "FirstAddress")
            {
                upload.DefaultAddress = true;
                string content = JsonConvert.SerializeObject(upload);
                HttpResponseCode responseCode = await this.HttpService.PostAsync("Direccion/Agregar", content);
                if (responseCode == HttpResponseCode.Success)
                {
                    UserDialogs.Instance.HideLoading();
                    await this.PageDialogService.DisplayAlertAsync("", ProfileLang.AddressForm_AddedSuccessfully, AppResources.AlertAcceptText);
                }
                else
                {
                    UserDialogs.Instance.HideLoading();
                    await this.PageDialogService.DisplayAlertAsync(AppResources.Error, AppResources.UnexpectedErrorOccurred, AppResources.AlertAcceptText);
                }
            }
            else
            {
                string content = JsonConvert.SerializeObject(upload);
                HttpResponseCode responseCode = await this.HttpService.PostAsync("Direccion/Agregar", content);
                if (responseCode == HttpResponseCode.Success)
                {
                    UserDialogs.Instance.HideLoading();
                    await this.PageDialogService.DisplayAlertAsync("", ProfileLang.AddressForm_AddedSuccessfully, AppResources.AlertAcceptText);
                    //await this.NavigationService.GoBackToRootAsync();
                }
                else
                {
                    UserDialogs.Instance.HideLoading();
                    await this.PageDialogService.DisplayAlertAsync(AppResources.Error, AppResources.UnexpectedErrorOccurred, AppResources.AlertAcceptText);
                }
            }
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidUsuario", this.UidUsuario.ToString());
            var addressesRequest = await this.HttpService.GetAsync<IEnumerable<Address>>("Direccion/GetObtenerDireccionUsuario_Movil", parameters);
            if (addressesRequest.Code == HttpResponseCode.Success)
            {
                AppInstaceService.HasAddresses = true;
                var address = addressesRequest.Result.SingleOrDefault(c => c.DefaultAddress == true);
                if (addressesRequest.Result.Count() > 0)
                {
                    AppInstaceService.SetAddressTotal(addressesRequest.Result.Count());
                }
                else
                {
                    AppInstaceService.SetAddressTotal(0);
                }
                if (address != null)
                {
                    AppInstaceService.SelectDeliveryAddress("", "", "", 0.0, 0.0);
                    AppInstaceService.UserSelectedDeliveryAddress(address.Uid, address.UidState, address.UidSuburb, address.Identifier, double.Parse(address.Latitude), double.Parse(address.Longitude));
                }
                else
                {
                    AppInstaceService.SelectDeliveryAddress("", "", "", 0.0, 0.0);
                    AppInstaceService.UserSelectedDeliveryAddress("", "", "", "", 0.0, 0.0);
                }
                NavigationParameters pairs = new NavigationParameters();
                pairs.Add("navigationType", NavigationType.Logged);
                await NavigationService.NavigateAsync($"/{nameof(NavigationMasterDetailPage)}/NavigationPage/ProfilePage", pairs);
            }
            else
            {
                await this.PageDialogService.DisplayAlertAsync(AppResources.Error, AppResources.UnexpectedErrorOccurred, AppResources.AlertAcceptText);
            }
        }
        private void Regresar()
        {
            _navigationService.GoBackAsync();
        }
        public override void OnNavigatedTo(INavigationParameters parameters)
        {

        }
        public override void Initialize(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("data"))
            {
                direccion = parameters.GetValue<Address>("data");
                Action = parameters.GetValue<string>("action");
                Identifier = direccion.Identifier;
                Country = direccion.CountryName;
                State = direccion.StateName;
                City = direccion.CityName;
                Street = direccion.Reference;
                SuburbName = direccion.SuburbName;
                DefaultAddress = direccion.DefaultAddress;
                _eventAggregator.GetEvent<Xam_Customer.Core.Events.UpdateMapPinEvent>().Publish(new MapPin() { Identifier = direccion.Identifier, Latitude = direccion.Latitude, Longitude = direccion.Longitude });
            }
        }
        #endregion
    }
}
