﻿using Acr.UserDialogs;
using Plugin.Clipboard;
using Prism.AppModel;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.Profile;
using Xam_Customer.ViewModels.Core;

namespace Xam_Customer.ViewModels.Profile
{
    public class ProfileRewardsPageViewModel : BaseNavigationViewModel, IPageLifecycleAware
    {
        #region Properties
        private string code;
        public string Code
        {
            get { return code; }
            set { SetProperty(ref code, value); }
        }

        private bool isLoading;
        public bool IsLoading
        {
            get { return isLoading; }
            set { SetProperty(ref isLoading, value); }
        }

        private string shareSummary;
        public string ShareSummary
        {
            get { return shareSummary; }
            set { SetProperty(ref shareSummary, value); }
        }

        private string promotionSummary;
        public string PromotionSummary
        {
            get { return promotionSummary; }
            set { SetProperty(ref promotionSummary, value); }
        }

        #endregion

        #region Private
        protected UserSession Session { get; }
        #endregion

        #region Services
        private IHttpRequestService HttpService { get; }
        private IAppInstaceService Instance { get; }
        #endregion

        #region Commands
        public DelegateCommand CopyToClipboardCommand { get; set; }
        #endregion

        public ProfileRewardsPageViewModel(
            INavigationService navigationService,
            IHttpRequestService httpRequestService,
            IAppInstaceService appInstaceService
            ) : base(navigationService)
        {
            this.HttpService = httpRequestService;
            this.Instance = appInstaceService;
            this.Session = Settings.UserSession;

            this.CopyToClipboardCommand = new DelegateCommand(() =>
            {
                if (string.IsNullOrEmpty(this.Code))
                    return;

                CrossClipboard.Current.SetText(this.Code);

                UserDialogs.Instance.Toast(ProfileLang.CopiedToClipboard);
            });
        }

        #region Implementation
        private async void LoadData()
        {
            this.IsLoading = true;

            var settingsResponse = await this.HttpService.GetAsync<SignInRewardCodesConfig>("v2/codes/GetCodeConfig");
            if (settingsResponse.Code == Xam_Customer.Core.Enum.HttpResponseCode.Success)
            {
                this.PromotionSummary = string.Format(AppResources.ShareThisCodeWithFriendsAndReceiveWallet, settingsResponse.Result.RedeemsRequired.ToString(), settingsResponse.Result.ParentRewardValue.ToString("N2"));
            }
            else
            {
                this.PromotionSummary = ProfileLang.ShareThisCode;
            }

            Dictionary<string, string> pairs = new Dictionary<string, string>();
            pairs.Add("uid", this.Session.StrUid);

            var response = await this.HttpService.GetAsync<UserSignInRewardCode>("v2/codes/GetByUser", pairs);
            if (response.Code == Xam_Customer.Core.Enum.HttpResponseCode.Success)
            {
                this.Code = response.Result.Code;

                pairs.Clear();
                pairs.Add("UidCode", response.Result.Uid.ToString());
                var sResponse = await this.HttpService.GetAsync<IEnumerable<UserSignInRewardCode>>("v2/codes/GetAllParentChildRedeems", pairs);
                if (response.Code == Xam_Customer.Core.Enum.HttpResponseCode.Success)
                {
                    int count = sResponse.Result.Count();
                    if (count == 0)
                        this.ShareSummary = AppResources.YouHaveNoFriendsRegisteredWithThisCode;
                    else
                        this.ShareSummary = string.Format(AppResources.NFriendsRegisteredWithThisCode, count.ToString());
                }
                else
                {
                    this.ShareSummary = AppResources.YouHaveNoFriendsRegisteredWithThisCode;
                }
            }
            else
            {
                this.Code = "WRONG-01";
            }

            this.IsLoading = false;
        }
        #endregion

        #region Extension
        public void OnAppearing()
        {
            this.LoadData();
        }

        public void OnDisappearing()
        {

        }
        #endregion
    }
}
