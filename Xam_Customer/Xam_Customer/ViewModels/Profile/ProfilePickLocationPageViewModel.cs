﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Events;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Model.System;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class ProfilePickLocationPageViewModel : BaseNavigationViewModel
    {
        #region Properties


        private string _city;
        public string City
        {
            get { return _city; }
            set { SetProperty(ref _city, value); }
        }

        private string _Uidstate;
        public string UidState
        {
            get { return _Uidstate; }
            set { SetProperty(ref _Uidstate, value); }
        }
        private string _state;
        public string State
        {
            get { return _state; }
            set { SetProperty(ref _state, value); }
        }

        private string _country;
        public string Country
        {
            get { return _country; }
            set { SetProperty(ref _country, value); }
        }

        private string _fullAddress;
        public string FullAddress
        {
            get { return _fullAddress; }
            set { SetProperty(ref _fullAddress, value); }
        }
        private double _long;
        public double Longitude
        {
            get { return _long; }
            set { SetProperty(ref _long, value); }
        }
        private double _lat;
        public double Latitude
        {
            get { return _lat; }
            set { SetProperty(ref _lat, value); }
        }
        private Address selectedAddress;
        public Address SelectedAddress
        {
            get { return selectedAddress; }
            set { SetProperty(ref selectedAddress, value); }
        }


        #endregion

        #region Services
        public IHttpRequestService HttpService { get; set; }
        #endregion
        private bool LocationAdded { get; set; }

        #region Command
        public DelegateCommand NextCommand { get; }
        private IEventAggregator EventAggregator { get; }
        #endregion

        public ProfilePickLocationPageViewModel(
            INavigationService navigationService,
            IHttpRequestService httpRequestService,
            IEventAggregator eventAggregator
            ) : base(navigationService)
        {
            this.EventAggregator = eventAggregator;

            this.NextCommand = new DelegateCommand(Next);
            HttpService = httpRequestService;
            this.Latitude = 0;
            this.Longitude = 0;
        }

        #region Implementation
        private void Next()
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                if (this.Latitude == 0 || this.Longitude == 0)
                {
                    return;
                }


                this.LocationAdded = true;
                await this.NavigationService.GoBackAsync();
            });
        }
        public async Task LoadAddress()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("StrNombreCiudad", City);
            parameters.Add("CodigoEstado", State);
            parameters.Add("CodigoPais", Country);
            var AddressRequest = await this.HttpService.GetAsync<IEnumerable<Address>>("Direccion/GetObtenerDireccionConDatosDeGoogle_Movil", parameters);
            var AddresList = AddressRequest.Result.ToList();
            SelectedAddress = AddresList[0];
            SelectedAddress.Street0 = FullAddress;
        }
        #endregion

        public override void Initialize(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("pin"))
            {
                MapPin pin = parameters.GetValue<MapPin>("pin");

                double lLongitude = 0;
                double lLatitude = 0;

                if (double.TryParse(pin.Latitude, out lLatitude) && double.TryParse(pin.Longitude, out lLongitude))
                {
                    this.Latitude = double.Parse(pin.Latitude);
                    this.Longitude = double.Parse(pin.Longitude);

                    EventAggregator.GetEvent<ProfilePickLocationEvent>()
                        .Publish(
                        new MapPin()
                        {
                            Identifier = "",
                            Latitude = pin.Latitude,
                            Longitude = pin.Longitude
                        });
                }
            }
        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            if (this.LocationAdded)
            {
                parameters.Add("fromPage", "PickLocation");
                parameters.Add("pickMapPin", new MapPin()
                {
                    Latitude = this.Latitude.ToString(),
                    Longitude = this.Longitude.ToString()
                });
                parameters.Add("LocationLoaded", SelectedAddress);
            }
        }
    }
}
