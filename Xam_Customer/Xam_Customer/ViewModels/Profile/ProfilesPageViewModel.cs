﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Util;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.Cart;
using Xam_Customer.Resources.LangResx.Profile;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xam_Customer.Views.Profile;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class ProfilesPageViewModel : BaseNavigationViewModel
    {
        #region Properties
        private string _CompleteName;
        public string CompleteName
        {
            get => _CompleteName;
            set => SetProperty(ref _CompleteName, value);
        }

        public ObservableCollection<ProfileItemsList> Options { get; set; }
        #endregion

        #region Private
        private UserSession Session { get; }
        #endregion

        #region Command
        public DelegateCommand<ProfileItemsList> OptionSelectedCommand { get; }
        #endregion

        #region Services
        private IHttpRequestService HttpService { get; }
        #endregion

        private enum OptionCommandName
        {
            EditAccount,
            EditUser,
            Addresses,
            Wallet,
            Rewards
        }
        public IAppInstaceService _appInstance { get; set; }
        public ProfilesPageViewModel(
            INavigationService navigationService,
            IDialogService dialogService,
            IAppInstaceService appInstaceService,
            IPageDialogService pageDialogService,
            IHttpRequestService httpRequestService
            ) : base(navigationService)
        {
            this.HttpService = httpRequestService;

            _appInstance = appInstaceService;
            this.OptionSelectedCommand = new DelegateCommand<ProfileItemsList>(this.OptionSelected);
            this.Options = new ObservableCollection<ProfileItemsList>();
            this.Session = Settings.UserSession;
        }

        #region Implementation
        private void InitializeData()
        {
            var user = Settings.UserSession;
            CompleteName = $"{user.FisrtName} {user.FisrtLastName} {user.SecondLastName}";
            this.Options.Clear();

            this.Options.Add(new ProfileItemsList()
            {
                Icon = MaterialFontIcons.AccountOutline,
                Name = ProfileLang.Option_EditAccount,
                CommandName = OptionCommandName.EditAccount.ToString(),
                CurrentValue = $"{user.FisrtName} {user.FisrtLastName}"
            });

            this.Options.Add(new ProfileItemsList()
            {
                Icon = MaterialFontIcons.LockOutline,
                Name = ProfileLang.Option_EditUser,
                CommandName = OptionCommandName.EditUser.ToString(),
                CurrentValue = user.UserName
            });

            string addressesTotal = "";
            if (_appInstance.GetAddressTotal() == 1)
            {
                addressesTotal = _appInstance.GetAddressTotal() + " " + ProfileLang.Option_Address;
            }
            else
            {
                addressesTotal = _appInstance.GetAddressTotal() + " " + ProfileLang.Option_Addresses;
            }
            this.Options.Add(new ProfileItemsList()
            {
                Icon = MaterialFontIcons.MapMarkerMultipleOutline,
                Name = addressesTotal,
                CommandName = OptionCommandName.Addresses.ToString()
            });

            this.Options.Add(new ProfileItemsList()
            {
                Icon = MaterialFontIcons.WalletOutline,
                Name = AppResources.Wallet,
                CommandName = OptionCommandName.Wallet.ToString(),
                CurrentValue = string.Empty
            });

            this.Options.Add(new ProfileItemsList()
            {
                Icon = MaterialFontIcons.TagTextOutline,
                Name = AppResources.Rewards,
                CommandName = OptionCommandName.Rewards.ToString(),
                CurrentValue = string.Empty
            });

            Task.Run(this.GetWalletBalance);
        }

        private async Task GetWalletBalance()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("uidUser", this.Session.StrUid);

            var walletRequest = await HttpService.GetAsync<Wallet>("Wallet/GetBalance", parameters);
            var walletItem = this.Options.FirstOrDefault(o => o.CommandName == OptionCommandName.Wallet.ToString());

            await Device.InvokeOnMainThreadAsync(() =>
            {
                if (walletRequest.Code == HttpResponseCode.Success)
                {
                    if (walletItem != null)
                    {
                        walletItem.CurrentValue = $"${walletRequest.Result.Amount.ToString("N2")} MXN";
                    }
                }
                else
                {
                    if (walletItem != null)
                    {
                        walletItem.CurrentValue = CartLang.Cart_CantLoadWallet;
                    }
                }
            });
        }

        private void OptionSelected(ProfileItemsList item)
        {
            string navigateTo = string.Empty;
            NavigationParameters parameters = new NavigationParameters();

            switch (item.CommandName)
            {
                case "EditAccount":
                    navigateTo = nameof(ProfileAccountFormPage);
                    break;
                case "EditUser":
                    navigateTo = nameof(ProfileUserFormPage);
                    break;
                case "Addresses":
                    parameters.Add("Edicion", "Edicion");
                    navigateTo = nameof(ProfileAddressesPage);
                    break;
                case "Wallet":
                    navigateTo = nameof(ProfileWalletPage);
                    break;
                case "Rewards":
                    navigateTo = nameof(ProfileRewardsPage);
                    break;
                default:
                    break;
            }



            if (!string.IsNullOrEmpty(navigateTo))
            {
                Device.InvokeOnMainThreadAsync(async () =>
                {
                    await this.NavigationService.NavigateAsync(navigateTo, parameters);
                });
            }

        }

        #endregion

        public override void Initialize(INavigationParameters parameters)
        {
            this.InitializeData();
        }
        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            var user = Settings.UserSession;
            CompleteName = $"{user.FisrtName} {user.FisrtLastName} {user.SecondLastName}";
            base.OnNavigatedFrom(parameters);
        }
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            Device.InvokeOnMainThreadAsync(() => { this.InitializeData(); });
        }
    }
}
