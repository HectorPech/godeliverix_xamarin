﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Events;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.Address;
using Xam_Customer.Core.Model.Core;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Model.System;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.Profile;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xam_Customer.Views.Layout;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class ProfileAddressFormPageViewModel : BaseNavigationViewModel
    {
        #region Properties
        private string title;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        private string subtitle;
        public string Subtitle
        {
            get { return subtitle; }
            set { SetProperty(ref subtitle, value); }
        }

        private Color deleteColor;
        public Color DeleteColor
        {
            get { return deleteColor; }
            set { SetProperty(ref deleteColor, value); }
        }
        private bool deleteenable;
        public bool Deleteenable
        {
            get { return deleteenable; }
            set { SetProperty(ref deleteenable, value); }
        }
        private bool isBusy;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        private ObservableCollection<ListboxItemModel> paises;
        public ObservableCollection<ListboxItemModel> Paises
        {
            get { return paises; }
            set { SetProperty(ref paises, value); }
        }

        private ObservableCollection<ListboxItemModel> estados;
        public ObservableCollection<ListboxItemModel> Estados
        {
            get { return estados; }
            set { SetProperty(ref estados, value); }
        }

        private ObservableCollection<ListboxItemModel> municipios;
        public ObservableCollection<ListboxItemModel> Municipios
        {
            get { return municipios; }
            set { SetProperty(ref municipios, value); }
        }

        private ObservableCollection<ListboxItemModel> ciudades;
        public ObservableCollection<ListboxItemModel> Ciudades
        {
            get { return ciudades; }
            set { SetProperty(ref ciudades, value); }
        }

        private ObservableCollection<ListboxItemModel> colonias;
        public ObservableCollection<ListboxItemModel> Colonias
        {
            get { return colonias; }
            set { SetProperty(ref colonias, value); }
        }

        private bool statusAddress;
        public bool StatusAddress
        {
            get { return statusAddress; }
            set { SetProperty(ref statusAddress, value); }
        }
        private bool defaultAddress;
        public bool DefaultAddress
        {
            get { return defaultAddress; }
            set { SetProperty(ref defaultAddress, value); }
        }
        private string identificador;
        public string Identificador
        {
            get { return identificador; }
            set { SetProperty(ref identificador, value); }
        }

        private ListboxItemModel pais;
        public ListboxItemModel Pais
        {
            get { return pais; }
            set
            {
                if (this.IsBusy) { return; }
                SetProperty(ref pais, value);
                Device.InvokeOnMainThreadAsync(async () => { await this.ObtenerEstados(value.Uid); });
            }
        }

        private ListboxItemModel estado;
        public ListboxItemModel Estado
        {
            get { return estado; }
            set
            {
                if (this.IsBusy) { return; }
                SetProperty(ref estado, value);
                Device.InvokeOnMainThreadAsync(async () => { await this.ObtenerMunicipios(value.Uid); });
            }
        }

        private ListboxItemModel municipio;
        public ListboxItemModel Municipio
        {
            get { return municipio; }
            set
            {
                if (this.IsBusy) { return; }
                SetProperty(ref municipio, value);
                Device.InvokeOnMainThreadAsync(async () => { await this.ObtenerCiudades(value.Uid); });
            }
        }

        private ListboxItemModel ciudad;
        public ListboxItemModel Ciudad
        {
            get { return ciudad; }
            set
            {
                if (this.IsBusy) { return; }
                SetProperty(ref ciudad, value);
                Device.InvokeOnMainThreadAsync(async () => { await this.ObtenerColonias(value.Uid); });
            }
        }

        private ListboxItemModel colonia;
        public ListboxItemModel Colonia
        {
            get { return colonia; }
            set
            {
                SetProperty(ref colonia, value);
                this.ObtenerCodigoPostal(value.Uid);
            }
        }

        private string calle;
        public string Calle
        {
            get { return calle; }
            set { SetProperty(ref calle, value); }
        }

        private string entreCalle;
        public string EntreCalle
        {
            get { return entreCalle; }
            set { SetProperty(ref entreCalle, value); }
        }

        private string yCalle;
        public string YCalle
        {
            get { return yCalle; }
            set { SetProperty(ref yCalle, value); }
        }

        private string lote;
        public string Lote
        {
            get { return lote; }
            set { SetProperty(ref lote, value); }
        }

        private string codigoPostal;
        public string CodigoPostal
        {
            get { return codigoPostal; }
            set { SetProperty(ref codigoPostal, value); }
        }

        private string manzana;
        public string Manzana
        {
            get { return manzana; }
            set { SetProperty(ref manzana, value); }
        }

        private string referencias;
        public string Referencias
        {
            get { return referencias; }
            set { SetProperty(ref referencias, value); }
        }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        #endregion

        #region Private
        private Address _Address { get; set; }
        public string Action { get; set; }
        public Guid UidUsuario { get; set; }
        #endregion

        #region Services
        private IHttpRequestService HttpService { get; }
        private IEventAggregator EventAggregator { get; }
        private IPageDialogService PageDialogService { get; }
        private IAppInstaceService AppInstaceService { get; }
        #endregion

        #region Commands
        public DelegateCommand UpdateLocationCommand { get; }
        public DelegateCommand SaveCommand { get; }
        public DelegateCommand DeleteAddress { get; }
        public DelegateCommand municipalityCommand { get; }
        public DelegateCommand CityCommand { get; }
        public DelegateCommand SuburCommand { get; }
        #endregion

        public ProfileAddressFormPageViewModel(
            INavigationService navigationService,
            IPageDialogService pageDialogService,
            IHttpRequestService httpRequestService,
            IEventAggregator eventAggregator,
            IAppInstaceService appInstaceService
            ) : base(navigationService)
        {
            this.HttpService = httpRequestService;
            this.EventAggregator = eventAggregator;
            this.PageDialogService = pageDialogService;
            this.AppInstaceService = appInstaceService;

            this.SaveCommand = new DelegateCommand(this.Save);
            DeleteAddress = new DelegateCommand(this.Delete);
            this.UpdateLocationCommand = new DelegateCommand(this.UpdateLocation);

            this.Paises = new ObservableCollection<ListboxItemModel>();
            this.Estados = new ObservableCollection<ListboxItemModel>();
            this.Municipios = new ObservableCollection<ListboxItemModel>();
            this.Ciudades = new ObservableCollection<ListboxItemModel>();
            this.Colonias = new ObservableCollection<ListboxItemModel>();

            UserSession user = Settings.UserSession;
            this.UidUsuario = Guid.Parse(user.StrUid);

        }

        #region Implementation

        private void Save()
        {
            if (this.IsBusy)
            {
                return;
            }

            Device.InvokeOnMainThreadAsync(async () =>
            {
                this.IsBusy = true;
                UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);

                if (string.IsNullOrEmpty(this.Identificador))
                {
                    UserDialogs.Instance.HideLoading();
                    this.IsBusy = false;
                    await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_Identifier} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                    return;
                }

                if (this.Pais == null)
                {
                    UserDialogs.Instance.HideLoading();
                    this.IsBusy = false;
                    await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_Country} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                    return;
                }

                if (this.Estado == null)
                {
                    UserDialogs.Instance.HideLoading();
                    this.IsBusy = false;
                    await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_State} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                    return;
                }

                if (this.Municipio == null)
                {
                    UserDialogs.Instance.HideLoading();
                    this.IsBusy = false;
                    await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_Municipality} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                    return;
                }

                if (this.Ciudad == null)
                {
                    UserDialogs.Instance.HideLoading();
                    this.IsBusy = false;
                    await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_City} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                    return;
                }

                if (this.Colonia == null)
                {
                    UserDialogs.Instance.HideLoading();
                    this.IsBusy = false;
                    await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_Neighborhood} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                    return;
                }

                if (string.IsNullOrEmpty(this.Calle))
                {
                    UserDialogs.Instance.HideLoading();
                    this.IsBusy = false;
                    await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_Street} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                    return;
                }

                //if (string.IsNullOrEmpty(this.EntreCalle))
                //{
                //        UserDialogs.Instance.HideLoading();
                //        this.IsBusy = false;
                //        await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_BetweenStreet} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                //    return;
                //}

                //if (string.IsNullOrEmpty(this.YCalle))
                //{
                //        UserDialogs.Instance.HideLoading();
                //        this.IsBusy = false;
                //        await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_AndStreet} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                //    return;
                //}

                if (string.IsNullOrEmpty(this.CodigoPostal))
                {
                    UserDialogs.Instance.HideLoading();
                    this.IsBusy = false;
                    await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_PostalCode} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                    return;
                }

                //if (string.IsNullOrEmpty(this.Lote))
                //{
                //    UserDialogs.Instance.HideLoading();
                //    this.IsBusy = false;
                //    await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_Lot} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                //    return;
                //}

                //if (string.IsNullOrEmpty(this.Manzana))
                //{
                //    UserDialogs.Instance.HideLoading();
                //    this.IsBusy = false;
                //    await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_Block} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                //    return;
                //}
                if (_Address == null)
                {
                    UserDialogs.Instance.HideLoading();
                    this.IsBusy = false;
                    await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_RequiredLongLat} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                    return;
                }
                if (string.IsNullOrEmpty(this.Referencias))
                {
                    UserDialogs.Instance.HideLoading();
                    this.IsBusy = false;
                    await this.PageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_References} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                    return;
                }

                AddressUpload upload = new AddressUpload()
                {
                    Uid = Guid.Empty,
                    Identificador = this.Identificador,
                    UidPais = this.Pais.Uid,
                    UidEstado = this.Estado.Uid,
                    UidMunicipio = this.Municipio.Uid,
                    UidCiudad = this.Ciudad.Uid,
                    UidColonia = this.Colonia.Uid,
                    UidUsuario = this.UidUsuario,
                    Calle = this.Calle,
                    EntreCalle = string.IsNullOrEmpty(this.EntreCalle) ? "" : this.EntreCalle,
                    yCalle = string.IsNullOrEmpty(this.YCalle) ? "" : this.YCalle,
                    Lote = this.Lote,
                    CodigoPostal = this.CodigoPostal,
                    Manzana = this.Manzana,
                    Referencias = this.Referencias,
                    Latitude = this.Latitude,
                    Longitude = this.Longitude,
                    Status = true,
                    DefaultAddress = DefaultAddress
                };

                if (this.Action == "Edit")
                {
                    upload.Uid = Guid.Parse(this._Address.Uid);

                    string content = JsonConvert.SerializeObject(upload);
                    HttpResponseCode responseCode = await this.HttpService.PostAsync("Direccion/Actualizar", content);

                    if (responseCode == HttpResponseCode.Success)
                    {
                        UserDialogs.Instance.HideLoading();
                        this.IsBusy = false;
                        await this.PageDialogService.DisplayAlertAsync("", ProfileLang.AddressForm_UpdatedSuccessfully, AppResources.AlertAcceptText);
                        await this.NavigationService.GoBackAsync();
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        this.IsBusy = false;
                        await this.PageDialogService.DisplayAlertAsync(AppResources.Error, AppResources.UnexpectedErrorOccurred, AppResources.AlertAcceptText);
                    }
                }
                else if (this.Action == "FirstAddress")
                {
                    upload.DefaultAddress = true;
                    string content = JsonConvert.SerializeObject(upload);
                    HttpResponseCode responseCode = await this.HttpService.PostAsync("Direccion/Agregar", content);
                    if (responseCode == HttpResponseCode.Success)
                    {
                        UserDialogs.Instance.HideLoading();
                        await this.PageDialogService.DisplayAlertAsync("", ProfileLang.AddressForm_AddedSuccessfully, AppResources.AlertAcceptText);

                        Dictionary<string, string> parameters = new Dictionary<string, string>();
                        parameters.Add("UidUsuario", this.UidUsuario.ToString());

                        var addressesRequest = await this.HttpService.GetAsync<IEnumerable<Address>>("Direccion/GetObtenerDireccionUsuario_Movil", parameters);

                        if (addressesRequest.Code == HttpResponseCode.Success)
                        {
                            AppInstaceService.HasAddresses = true;
                            var address = addressesRequest.Result.SingleOrDefault(c => c.DefaultAddress == true);
                            if (address != null)
                            {
                                AppInstaceService.SelectDeliveryAddress("", "", "", 0.0, 0.0);
                                AppInstaceService.UserSelectedDeliveryAddress(address.Uid, address.UidState, address.UidSuburb, address.Identifier, double.Parse(address.Latitude), double.Parse(address.Longitude));

                                NavigationParameters pairs = new NavigationParameters();
                                pairs.Add("navigationType", NavigationType.Logged);
                                await NavigationService.NavigateAsync($"/{nameof(NavigationMasterDetailPage)}/NavigationPage/Default", pairs);
                            }
                            else
                            {
                                await this.PageDialogService.DisplayAlertAsync(AppResources.Error, AppResources.UnexpectedErrorOccurred, AppResources.AlertAcceptText);
                            }
                        }
                        else
                        {
                            await this.PageDialogService.DisplayAlertAsync(AppResources.Error, AppResources.UnexpectedErrorOccurred, AppResources.AlertAcceptText);
                        }
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        this.IsBusy = false;
                        await this.PageDialogService.DisplayAlertAsync(AppResources.Error, AppResources.UnexpectedErrorOccurred, AppResources.AlertAcceptText);
                    }
                }
                else
                {
                    string content = JsonConvert.SerializeObject(upload);
                    HttpResponseCode responseCode = await this.HttpService.PostAsync("Direccion/Agregar", content);
                    if (responseCode == HttpResponseCode.Success)
                    {
                        UserDialogs.Instance.HideLoading();
                        this.IsBusy = false;
                        await this.PageDialogService.DisplayAlertAsync("", ProfileLang.AddressForm_AddedSuccessfully, AppResources.AlertAcceptText);
                        await this.NavigationService.GoBackAsync();
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        this.IsBusy = false;
                        await this.PageDialogService.DisplayAlertAsync(AppResources.Error, AppResources.UnexpectedErrorOccurred, AppResources.AlertAcceptText);
                    }
                }
            });
        }

        private void Delete()
        {
            Task.Run(async () =>
            {
                AddressUpload upload = new AddressUpload()
                {
                    Uid = Guid.Empty,
                    Identificador = this.Identificador,
                    UidPais = this.Pais.Uid,
                    UidEstado = this.Estado.Uid,
                    UidMunicipio = this.Municipio.Uid,
                    UidCiudad = this.Ciudad.Uid,
                    UidColonia = this.Colonia.Uid,
                    UidUsuario = this.UidUsuario,
                    Calle = this.Calle,
                    EntreCalle = string.IsNullOrEmpty(this.EntreCalle) ? "" : this.EntreCalle,
                    yCalle = string.IsNullOrEmpty(this.YCalle) ? "" : this.YCalle,
                    Lote = this.Lote,
                    CodigoPostal = this.CodigoPostal,
                    Manzana = this.Manzana,
                    Referencias = this.Referencias,
                    Latitude = this.Latitude,
                    Longitude = this.Longitude,
                    Status = false,
                    DefaultAddress = DefaultAddress
                };
                upload.Uid = Guid.Parse(this._Address.Uid);
                string content = JsonConvert.SerializeObject(upload);
                HttpResponseCode responseCode = await this.HttpService.PostAsync("Direccion/Actualizar", content);
            });
            NavigationService.GoBackAsync();
        }

        private void UpdateLocation()
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                await this.NavigationService.NavigateAsync(nameof(ProfilePickLocationPage),
                    new NavigationParameters()
                    {
                        {"pin", new MapPin() {Latitude = this.Latitude, Longitude = this.Longitude } }
                    });
            });
        }

        public override void Initialize(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("action"))
            {
                this.Action = parameters.GetValue<string>("action");

                //Deleteenable = true;
                //DeleteColor = Color.White;
            }
            else
            {
                this.Action = "New";
            }
            Deleteenable = false;
            DeleteColor = Color.Transparent;
            if (parameters.ContainsKey("data"))
            {
                this._Address = parameters.GetValue<Address>("data");

                this.Identificador = this._Address.Identifier;
                this.Calle = this._Address.Street0;
                this.EntreCalle = this._Address.Street1;
                this.YCalle = this._Address.Street2;
                this.Lote = this._Address.Lot;
                this.CodigoPostal = this._Address.ZipCode;
                this.Manzana = this._Address.Block;
                this.Referencias = this._Address.Reference;

                this.Latitude = this._Address.Latitude;
                this.Longitude = this._Address.Longitude;
                this.DefaultAddress = this._Address.DefaultAddress;

                Deleteenable = true;
                DeleteColor = Color.Orange;
            }

            if (parameters.ContainsKey("mapLocation"))
            {
                MapPin mapPin = parameters.GetValue<MapPin>("mapLocation");

                EventAggregator.GetEvent<ProfilePickAddressEvent>().Publish(
                    new MapPin()
                    {
                        Identifier = "",
                        Latitude = mapPin.Latitude,
                        Longitude = mapPin.Longitude
                    });
            }

            this.Title = this.Action == "Edit" ? ProfileLang.AddressForm_Edit_Text : ProfileLang.AddressForm_Add_Text;

            if (this.Action == "FirstAddress")
            {
                this.Subtitle = ProfileLang.AddressForm_AddDefault;
                this.DefaultAddress = true;
            }

            Device.InvokeOnMainThreadAsync(async () => { await this.ObtenerPaises(); });
        }

        private async Task ObtenerPaises()
        {
            this.IsBusy = true;

            var result = await this.HttpService.GetAsync<IEnumerable<ListboxItemModel>>("Direccion/ObtenerPaises");
            if (result.Code == HttpResponseCode.Success)
            {
                foreach (var item in result.Result)
                {
                    if (item.Uid != Guid.Empty)
                    {
                        this.Paises.Add(item);
                    }
                }
            }

            this.IsBusy = false;

            if (this.Action.Equals("Edit") || this._Address != null)
            {
                ListboxItemModel pais = this.Paises.FirstOrDefault(i => i.Uid == Guid.Parse(this._Address.Country));
                if (pais != null)
                {
                    this.Pais = pais;
                }
            }
        }

        public async void CargaPaises()
        {
            this.IsBusy = true;

            var result = await this.HttpService.GetAsync<IEnumerable<ListboxItemModel>>("Direccion/ObtenerPaises");
            if (result.Code == HttpResponseCode.Success)
            {
                foreach (var item in result.Result)
                {
                    if (item.Uid != Guid.Empty)
                    {
                        this.Paises.Add(item);
                    }
                }
            }

            this.IsBusy = false;

            if (this.Action.Equals("Edit") || this._Address != null)
            {
                ListboxItemModel pais = this.Paises.FirstOrDefault(i => i.Uid == Guid.Parse(this._Address.Country));
                if (pais != null)
                {
                    this.Pais = pais;
                }
            }
        }
        private async Task ObtenerEstados(Guid uid)
        {
            if (this.IsBusy)
            {
                return;
            }

            this.IsBusy = true;

            if (this.Estados.Any())
            {
                this.Estados = new ObservableCollection<ListboxItemModel>();
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidPais", uid.ToString());

            var result = await this.HttpService.GetAsync<IEnumerable<ListboxItemModel>>("Direccion/ObtenerEstadosPais", parameters);
            if (result.Code == HttpResponseCode.Success)
            {
                foreach (var item in result.Result)
                {
                    if (item.Uid != Guid.Empty)
                    {
                        this.Estados.Add(item);
                    }
                }
            }

            this.IsBusy = false;

            if (this.Action.Equals("Edit") || this._Address != null)
            {
                ListboxItemModel lEstado = this.Estados.FirstOrDefault(i => i.Uid == Guid.Parse(this._Address.UidState));
                if (lEstado != null)
                {
                    this.Estado = lEstado;
                }
            }
        }
        private async Task ObtenerMunicipios(Guid uid)
        {
            if (this.IsBusy)
            {
                return;
            }

            this.IsBusy = true;

            if (this.Municipios.Any())
            {
                this.Municipios = new ObservableCollection<ListboxItemModel>();
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidEstado", uid.ToString());

            var result = await this.HttpService.GetAsync<IEnumerable<ListboxItemModel>>("Direccion/ObtenerMunicipiosEstado", parameters);
            if (result.Code == HttpResponseCode.Success)
            {
                foreach (var item in result.Result)
                {
                    if (item.Uid != Guid.Empty)
                    {
                        this.Municipios.Add(item);
                    }
                }
            }

            this.IsBusy = false;

            if (this.Action.Equals("Edit") || this._Address != null)
            {
                ListboxItemModel lMunicipio = this.Municipios.FirstOrDefault(i => i.Uid == Guid.Parse(this._Address.Municipality));
                if (lMunicipio != null)
                {
                    this.Municipio = lMunicipio;
                }
            }
        }
        private async Task ObtenerCiudades(Guid uid)
        {
            if (this.IsBusy)
            {
                return;
            }

            this.IsBusy = true;

            if (this.Ciudades.Any())
            {
                this.Ciudades = new ObservableCollection<ListboxItemModel>();
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidMunicipio", uid.ToString());

            var result = await this.HttpService.GetAsync<IEnumerable<ListboxItemModel>>("Direccion/ObtenerCiudadesMunicipio", parameters);
            if (result.Code == HttpResponseCode.Success)
            {
                foreach (var item in result.Result)
                {
                    if (item.Uid != Guid.Empty)
                    {
                        this.Ciudades.Add(item);
                    }
                }
            }

            this.IsBusy = false;

            if (this.Action.Equals("Edit") || this._Address != null)
            {
                ListboxItemModel lCiudad = this.Ciudades.FirstOrDefault(i => i.Uid == Guid.Parse(this._Address.UidCity));
                if (lCiudad != null)
                {
                    this.Ciudad = lCiudad;
                }
            }
        }

        private async Task ObtenerColonias(Guid uid)
        {
            if (this.IsBusy)
            {
                return;
            }

            this.IsBusy = true;

            if (this.Colonias.Any())
            {
                this.Colonias = new ObservableCollection<ListboxItemModel>();
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidCiudad", uid.ToString());

            var result = await this.HttpService.GetAsync<IEnumerable<ListboxItemModel>>("Direccion/ObtenerColoniasCiudad", parameters);
            if (result.Code == HttpResponseCode.Success)
            {
                foreach (var item in result.Result)
                {
                    if (item.Uid != Guid.Empty)
                    {
                        this.Colonias.Add(item);
                    }
                }
            }

            this.IsBusy = false;

            if (this.Action.Equals("Edit") || this._Address != null)
            {
                ListboxItemModel lColonia = this.Colonias.FirstOrDefault(i => i.Uid == Guid.Parse(this._Address.UidSuburb));
                if (lColonia != null)
                {
                    this.Colonia = lColonia;
                }
            }
        }

        private async void ObtenerCodigoPostal(Guid Uid)
        {
            await loadZipCode(Uid);
        }
        private async Task loadZipCode(Guid Uid)
        {
            if (this.IsBusy)
            {
                return;
            }

            this.IsBusy = true;

            if (!string.IsNullOrEmpty(CodigoPostal))
            {
                this.CodigoPostal = string.Empty;
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidColonia", Uid.ToString());

            var result = await this.HttpService.GetAsync<Address>("Direccion/ObtenerCodigoPostal", parameters);
            if (result.Code == HttpResponseCode.Success)
            {
                var colonia = result.Result;
                CodigoPostal = colonia.ZipCode;
                IsBusy = false;
            }
        }

        #region Map methods

        #endregion
        #endregion

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.GetNavigationMode() == NavigationMode.Back)
            {
                if (parameters.ContainsKey("fromPage"))
                {
                    string from = parameters.GetValue<string>("fromPage");

                    if (from.Equals("PickLocation"))
                    {
                        if (parameters.ContainsKey("pickMapPin"))
                        {
                            MapPin mapPin = parameters.GetValue<MapPin>("pickMapPin");
                            this.Longitude = mapPin.Longitude;
                            this.Latitude = mapPin.Latitude;

                            EventAggregator.GetEvent<ProfilePickAddressEvent>()
                                .Publish(
                                new MapPin()
                                {
                                    Identifier = "",
                                    Latitude = mapPin.Latitude,
                                    Longitude = mapPin.Longitude
                                });
                        }
                    }
                    if (parameters.ContainsKey("LocationLoaded"))
                    {
                        this._Address = parameters.GetValue<Address>("LocationLoaded");

                        Device.InvokeOnMainThreadAsync(async () => { await this.ObtenerPaises(); Calle = _Address.Street0; });

                    }
                }
            }
        }
    }
}
