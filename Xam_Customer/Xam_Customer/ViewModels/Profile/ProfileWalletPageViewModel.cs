﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Util;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.ViewModels.Core;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels.Profile
{
    public class ProfileWalletPageViewModel : BaseNavigationViewModel
    {
        #region Properties
        private ObservableCollection<WalletTransaction> transactions;
        public ObservableCollection<WalletTransaction> Transactions
        {
            get { return transactions; }
            set { SetProperty(ref transactions, value); }
        }

        private bool noTransactions;
        public bool NoTransactions
        {
            get { return noTransactions; }
            set { SetProperty(ref noTransactions, value); }
        }

        private bool isLoading;
        public bool IsLoading
        {
            get { return isLoading; }
            set { SetProperty(ref isLoading, value); }
        }

        private decimal walletBalance;
        public decimal WalletBalance
        {
            get { return walletBalance; }
            set { SetProperty(ref walletBalance, value); }
        }

        #endregion

        #region Private
        private UserSession Session { get; }
        #endregion

        #region Command
        public DelegateCommand RefreshCommand { get; }
        #endregion

        #region Services
        private IHttpRequestService HttpService { get; }
        #endregion

        public ProfileWalletPageViewModel(
            INavigationService navigationService,
            IHttpRequestService httpRequestService
            ) : base(navigationService)
        {
            this.HttpService = httpRequestService;

            this.Transactions = new ObservableCollection<WalletTransaction>();
            this.RefreshCommand = new DelegateCommand(() => { Task.Run(this.ReadAllTransactions); });
            this.Session = Settings.UserSession;
        }

        #region Implementation
        public override void Initialize(INavigationParameters parameters)
        {
            base.Initialize(parameters);

            Task.Run(this.ReadAllTransactions);
        }

        public async Task ReadAllTransactions()
        {
            this.IsLoading = true;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("uidUser", this.Session.StrUid);

            var walletRequest = await HttpService.GetAsync<Wallet>("Wallet/GetBalance", parameters);

            if (walletRequest.Code == HttpResponseCode.Success)
            {
                this.WalletBalance = walletRequest.Result.Amount;
            }
            else
            {
                this.WalletBalance = 0;
            }

            var request = await this.HttpService.GetAsync<IEnumerable<WalletTransaction>>("Wallet/ReadAllTransactions", parameters);

            if (request.Code == HttpResponseCode.Success)
            {
                var result = new List<WalletTransaction>();
                foreach (WalletTransaction transaction in request.Result)
                {
                    if (transaction.UidType == TipoMovimiento.Agregar)
                    {
                        transaction.IsIncome = true;
                        transaction.AmountColor = Color.FromHex("#4caf50");
                    }

                    if (transaction.UidType == TipoMovimiento.Retirar)
                    {
                        transaction.IsExpense = true;
                        transaction.AmountColor = Color.FromHex("#f44336");
                    }

                    if (transaction.UidConcept == Concepto.DepositoBancario)
                        transaction.Concept = AppResources.BankDeposit;

                    if (transaction.UidConcept == Concepto.PagoGoDeliverix)
                        transaction.Concept = AppResources.PaymentGoDeliverix;

                    if (transaction.UidConcept == Concepto.Reembolso)
                        transaction.Concept = AppResources.Refund;

                    if (transaction.UidConcept == Concepto.TarjetaPrepago)
                        transaction.Concept = AppResources.PrepaidCard;

                    if (transaction.UidConcept == Concepto.Promocion)
                        transaction.Concept = AppResources.Promotion;

                    switch (transaction.Entity)
                    {
                        case EntityType.Empresa:
                            break;
                        case EntityType.Sucursal:
                            break;
                        case EntityType.Productos:
                            break;
                        case EntityType.Monedero:
                            break;
                        case EntityType.Movimientos:
                            break;
                        case EntityType.Orden:
                            break;
                        case EntityType.OndenSucursal:
                            transaction.Description = AppResources.Order;
                            transaction.EntityValue = $" #{transaction.EntityValue}";
                            break;
                        case EntityType.UserSignInRewardCode:
                            transaction.Description = AppResources.SignInCode;
                            transaction.EntityValue = $" {transaction.EntityValue}";
                            break;
                        case EntityType.AllUserSignInRewardCode:
                            break;
                    }

                    result.Add(transaction);
                }

                await Device.InvokeOnMainThreadAsync(() =>
                {
                    this.NoTransactions = result.Any();
                    result = result.OrderByDescending(v => v.Date).ToList();
                    this.Transactions = new ObservableCollection<WalletTransaction>(result);
                });
            }
            else
            {
                this.NoTransactions = true;
            }

            this.IsLoading = false;
        }
        #endregion
    }
}
