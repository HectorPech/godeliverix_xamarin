﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Unity;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model.SignIn;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.Profile;
using Xam_Customer.ViewModels.Core;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class ProfileUserFormPageViewModel : BaseNavigationViewModel
    {
        #region Properties
        private string username;
        public string Username
        {
            get { return username; }
            set { SetProperty(ref username, value); }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { SetProperty(ref email, value); }
        }

        private string currentPassword;
        public string CurrentPassword
        {
            get { return currentPassword; }
            set { SetProperty(ref currentPassword, value); }
        }

        private string newPassword;
        public string NewPassword
        {
            get { return newPassword; }
            set { SetProperty(ref newPassword, value); }
        }

        private string confirmPassword;
        public string ConfirmPassword
        {
            get { return confirmPassword; }
            set { SetProperty(ref confirmPassword, value); }
        }

        private bool showRestorePasswordMode;
        public bool ShowRestorePasswordMode
        {
            get { return showRestorePasswordMode; }
            set { SetProperty(ref showRestorePasswordMode, value); }
        }

        private bool restorePasswordMode;
        public bool RestorePasswordMode
        {
            get { return restorePasswordMode; }
            set { SetProperty(ref restorePasswordMode, value); }
        }

        private bool isBusy;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        #endregion

        #region Private
        /// <summary>
        /// La contraseña actual del usuario
        /// </summary>
        private string UserPassword { get; set; }
        private UserInformation User { get; set; }

        #endregion

        #region Services
        private IPageDialogService PageDialogService { get; }
        private IHttpRequestService HttpService { get; }
        #endregion

        #region Command
        public DelegateCommand RestorePasswordCommand { get; }
        public DelegateCommand ConfirmRestorePasswordCommand { get; }
        public DelegateCommand CancelRestorePasswordCommand { get; }
        #endregion

        public ProfileUserFormPageViewModel(
            INavigationService navigationService,
            IPageDialogService pageDialogService,
            IHttpRequestService httpRequestService
            ) : base(navigationService)
        {
            this.PageDialogService = pageDialogService;
            this.HttpService = httpRequestService;

            this.RestorePasswordCommand = new DelegateCommand(this.RestorePassword);
            this.ConfirmRestorePasswordCommand = new DelegateCommand(this.ConfirmRestorePassword);
            this.CancelRestorePasswordCommand = new DelegateCommand(this.CancelRestorePassword);

            this.RestorePasswordMode = false;
            this.ShowRestorePasswordMode = true;

            this.UserPassword = "123";

            Device.InvokeOnMainThreadAsync(async () => { await GetData(); });
        }



        #region Implementation

        private async Task GetData()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidUsuario", Settings.UserSession.StrUid);
            parameters.Add("UIDPERFIL", "4F1E1C4B-3253-4225-9E46-DD7D1940DA19");
            var userRequest = await this.HttpService.GetAsync<UserInformation>("Usuario/GetBuscarUsuarios_Movil", parameters);
            if (userRequest.Code == HttpResponseCode.Success)
            {
                this.User = userRequest.Result;
                UserPassword = User.UserData.Password;
                Email = User.Email;
                Username = User.UserData.Username;

            }
            else
            {
                await this.PageDialogService.DisplayAlertAsync(AppResources.Error, AppResources.UnexpectedErrorOccurred, AppResources.AlertAcceptText);
                await this.NavigationService.GoBackAsync();
            }
        }
        private void RestorePassword()
        {
            this.RestorePasswordMode = true;
            this.ShowRestorePasswordMode = false;
        }

        private void ConfirmRestorePassword()
        {
            this.RestorePasswordMode = false;
            this.IsBusy = true;
            Device.InvokeOnMainThreadAsync(async () =>
            {
                if (string.IsNullOrEmpty(this.CurrentPassword)
                    || string.IsNullOrEmpty(this.NewPassword)
                    || string.IsNullOrEmpty(this.ConfirmPassword))
                {
                    await this.PageDialogService.DisplayAlertAsync("", ProfileLang.UserForm_PasswordsAreRequired, AppResources.AlertAcceptText);
                    return;
                }

                if (!this.CurrentPassword.Equals(this.UserPassword))
                {
                    await this.PageDialogService.DisplayAlertAsync("", ProfileLang.UserForm_CurrentPasswordInvalid, AppResources.AlertAcceptText);
                    return;
                }

                if (!this.NewPassword.Equals(this.ConfirmPassword))
                {
                    await this.PageDialogService.DisplayAlertAsync("", ProfileLang.UserForm_PasswordNotMatch, AppResources.AlertAcceptText);
                    return;
                }

                await Task.Delay(3000);
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("UidUsuario", this.User.UserData.Uid.ToString());
                parameters.Add("perfil", "4F1E1C4B-3253-4225-9E46-DD7D1940DA19");
                parameters.Add("password", this.NewPassword);

                var userRequest = await this.HttpService.GetEmptyAsync("Usuario/GetActualizarUsuario_Movil", parameters);
                if (userRequest == HttpResponseCode.Success)
                {
                    await this.PageDialogService.DisplayAlertAsync("", ProfileLang.UserForm_PasswordRestoredSuccessfully, AppResources.AlertAcceptText);
                    this.ShowRestorePasswordMode = true;
                    this.RestorePasswordMode = false;

                    this.CurrentPassword = string.Empty;
                    this.NewPassword = string.Empty;
                    this.ConfirmPassword = string.Empty;
                    this.IsBusy = false;
                }
            });
        }

        private void CancelRestorePassword()
        {
            this.RestorePasswordMode = false;
            this.ShowRestorePasswordMode = true;

            this.CurrentPassword = string.Empty;
            this.NewPassword = string.Empty;
            this.ConfirmPassword = string.Empty;
        }
        #endregion
    }
}
