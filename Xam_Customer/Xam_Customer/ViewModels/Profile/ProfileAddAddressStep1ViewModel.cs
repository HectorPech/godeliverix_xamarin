﻿using Acr.UserDialogs;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.Core;
using Xam_Customer.Core.Model.GoogleApi;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Model.System;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Util;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.Profile;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Xam_Customer.ViewModels
{
    public class ProfileAddAddressStep1ViewModel : BaseNavigationViewModel
    {
        #region Properties
        private bool _canContinue;

        public bool CanContinue
        {
            get { return _canContinue; }
            set { SetProperty(ref _canContinue, value); }
        }

        private string _Identifier;

        public string Identifier
        {
            get { return _Identifier; }
            set { SetProperty(ref _Identifier, value); }
        }
        private Address __Address;

        public Address Address
        {
            get { return __Address; }
            set { SetProperty(ref __Address, value); }
        }
        private string _Action;

        public string Action
        {
            get { return _Action; }
            set { _Action = value; }
        }
        private string _City;

        public string City
        {
            get { return _City; }
            set { SetProperty(ref _City, value); }
        }

        private string _State;

        public string State
        {
            get { return _State; }
            set { SetProperty(ref _State, value); }
        }

        private string _Country;

        public string Country
        {
            get { return _Country; }
            set { SetProperty(ref _Country, value); }
        }

        private string _Neighborhood;

        public string Neighborhood
        {
            get { return _Neighborhood; }
            set { SetProperty(ref _Neighborhood, value); }
        }

        internal bool OnBackButtonPressed()
        {
            Regresar();
            return true;
        }

        private List<Colonies> _Colonies;

        public List<Colonies> Colonies
        {
            get { return _Colonies; }
            set { SetProperty(ref _Colonies, value); }
        }


        #region GoogleMaps
        private string _StrSearchPlace;

        public string SearchPlace
        {
            get { return _StrSearchPlace; }
            set
            {
                SetProperty(ref _StrSearchPlace, value);
            }
        }
        private List<GoogleMapsPredictions> _listPredictions;

        public List<GoogleMapsPredictions> ListPredictions
        {
            get { return _listPredictions; }
            set { SetProperty(ref _listPredictions, value); }
        }
        private bool _SearchVisible;

        public bool SearchVisible
        {
            get { return _SearchVisible; }
            set { SetProperty(ref _SearchVisible, value); }
        }
        public DelegateCommand<string> SelectedPlace { get; set; }

        #endregion


        #region Propiedades de la ubicacion
        private bool isBusy;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        private ObservableCollection<ListboxItemModel> paises;
        public ObservableCollection<ListboxItemModel> Paises
        {
            get { return paises; }
            set { SetProperty(ref paises, value); }
        }

        private ObservableCollection<ListboxItemModel> estados;
        public ObservableCollection<ListboxItemModel> Estados
        {
            get { return estados; }
            set { SetProperty(ref estados, value); }
        }

        private ObservableCollection<ListboxItemModel> municipios;
        public ObservableCollection<ListboxItemModel> Municipios
        {
            get { return municipios; }
            set { SetProperty(ref municipios, value); }
        }

        private ObservableCollection<ListboxItemModel> ciudades;
        public ObservableCollection<ListboxItemModel> Ciudades
        {
            get { return ciudades; }
            set { SetProperty(ref ciudades, value); }
        }

        private ObservableCollection<ListboxItemModel> colonias;
        public ObservableCollection<ListboxItemModel> Colonias
        {
            get { return colonias; }
            set { SetProperty(ref colonias, value); }
        }

        private bool statusAddress;
        public bool StatusAddress
        {
            get { return statusAddress; }
            set { SetProperty(ref statusAddress, value); }
        }
        private bool defaultAddress;
        public bool DefaultAddress
        {
            get { return defaultAddress; }
            set
            {
                if (Action == "FirstAddress")
                {
                }
                else if (Address.DefaultAddress)
                {
                }
                else
                {
                    SetProperty(ref defaultAddress, value);
                }
            }
        }
        private string identificador;
        public string Identificador
        {
            get { return identificador; }
            set { SetProperty(ref identificador, value); }
        }

        private ListboxItemModel pais;
        public ListboxItemModel Pais
        {
            get { return pais; }
            set
            {
                if (this.IsBusy) { return; }
                SetProperty(ref pais, value);
                Device.InvokeOnMainThreadAsync(async () => { await this.ObtenerEstados(value.Uid); });
            }
        }

        private ListboxItemModel estado;
        public ListboxItemModel Estado
        {
            get { return estado; }
            set
            {
                if (this.IsBusy) { return; }
                SetProperty(ref estado, value);
                Device.InvokeOnMainThreadAsync(async () => { await this.ObtenerMunicipios(value.Uid); });
            }
        }

        private ListboxItemModel municipio;
        public ListboxItemModel Municipio
        {
            get { return municipio; }
            set
            {
                if (this.IsBusy) { return; }
                SetProperty(ref municipio, value);
                Device.InvokeOnMainThreadAsync(async () => { await this.ObtenerCiudades(value.Uid); });
            }
        }

        private ListboxItemModel ciudad;
        public ListboxItemModel Ciudad
        {
            get { return ciudad; }
            set
            {
                if (this.IsBusy) { return; }
                SetProperty(ref ciudad, value);
                Device.InvokeOnMainThreadAsync(async () => { await this.ObtenerColonias(value.Uid); });
            }
        }

        private ListboxItemModel colonia;
        public ListboxItemModel Colonia
        {
            get { return colonia; }
            set
            {
                SetProperty(ref colonia, value);
                this.ObtenerCodigoPostal(value.Uid);
            }
        }

        private string calle;
        public string Calle
        {
            get { return calle; }
            set { SetProperty(ref calle, value); }
        }

        private string entreCalle;
        public string EntreCalle
        {
            get { return entreCalle; }
            set { SetProperty(ref entreCalle, value); }
        }

        private string yCalle;
        public string YCalle
        {
            get { return yCalle; }
            set { SetProperty(ref yCalle, value); }
        }

        private string lote;
        public string Lote
        {
            get { return lote; }
            set { SetProperty(ref lote, value); }
        }

        private string codigoPostal;
        public string CodigoPostal
        {
            get { return codigoPostal; }
            set { SetProperty(ref codigoPostal, value); }
        }

        private string manzana;
        public string Manzana
        {
            get { return manzana; }
            set { SetProperty(ref manzana, value); }
        }

        private string referencias;
        public string Referencias
        {
            get { return referencias; }
            set { SetProperty(ref referencias, value); }
        }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        private MapPin _Mpin;

        public MapPin Mpin
        {
            get { return _Mpin; }
            set { SetProperty(ref _Mpin, value); }
        }

        #endregion
        #endregion

        #region Services
        public IPageDialogService _pageDialogService { get; set; }
        public INavigationService _navigationService { get; set; }
        public IEventAggregator _eventAggregator { get; set; }
        public IHttpRequestService HttpService { get; set; }

        #endregion
        #region Command
        public DelegateCommand Nextpage { get; set; }
        public DelegateCommand Previouspage { get; set; }
        public DelegateCommand AdvancedOptions { get; set; }
        #endregion
        public ProfileAddAddressStep1ViewModel(IEventAggregator eventAggregator, IPageDialogService pageDialogService,
            INavigationService navigationService, IHttpRequestService httpRequestService) : base(navigationService)
        {
            _pageDialogService = pageDialogService;
            _navigationService = navigationService;
            _eventAggregator = eventAggregator;
            HttpService = httpRequestService;
            this.Paises = new ObservableCollection<ListboxItemModel>();
            this.Estados = new ObservableCollection<ListboxItemModel>();
            this.Municipios = new ObservableCollection<ListboxItemModel>();
            this.Ciudades = new ObservableCollection<ListboxItemModel>();
            this.Colonias = new ObservableCollection<ListboxItemModel>();
            Nextpage = new DelegateCommand(Continuar);
            Previouspage = new DelegateCommand(Regresar);
            AdvancedOptions = new DelegateCommand(MasOpciones);
            SelectedPlace = new DelegateCommand<string>(DetallesDeUbicacionGoogle);
            Colonies = new List<Colonies>();
            SearchVisible = false;
        }


        #region Navigation

        #endregion

        #region GoogleMaps
        public async Task BuscarDireccionesEnGoogle()
        {
            ListPredictions = new List<GoogleMapsPredictions>();
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("input", SearchPlace.Replace(" ", "+"));
            parameters.Add("types", "geocode");
            parameters.Add("location", "20.6279142,-87.0847216");
            parameters.Add("radius", "5000");
            parameters.Add("strictbounds", "");
            parameters.Add("language", "es");
            parameters.Add("key", ApplicationConstants.GoogleMapsApiKey);
            var AddressRequest = await this.HttpService.GetThirdPartServicesAsync<GoogleMapsApiResponse>("https://maps.googleapis.com/maps/api/place/autocomplete/json", parameters);
            var GoogleResponse = AddressRequest.Result;
            foreach (var item in GoogleResponse.Predictions.ToList())
            {
                ListPredictions.Add(new GoogleMapsPredictions() { PlaceName = item.PlaceName, IdPlace = item.IdPlace });
            }
            SearchVisible = true;
        }
        private void DetallesDeUbicacionGoogle(string IdPlace)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("place_id", IdPlace);
                parameters.Add("key", ApplicationConstants.GoogleMapsApiKey);
                var AddressRequest = await this.HttpService.GetThirdPartServicesAsync<GoogleMapsApiResponse>("https://maps.googleapis.com/maps/api/place/details/json", parameters);
                var GoogleResponse = AddressRequest.Result;
                if (string.IsNullOrEmpty(Identifier))
                {
                    Identifier = " ";
                }
                Mpin = new MapPin() { Identifier = Identifier, Longitude = GoogleResponse.PlaceDetailsResult.PlaceLocationDetails.LatLongDetails.Longitude.ToString(), Latitude = GoogleResponse.PlaceDetailsResult.PlaceLocationDetails.LatLongDetails.Latitude.ToString() };
                await MuestraUbicacion(double.Parse(Mpin.Latitude), double.Parse(Mpin.Longitude));
                SearchVisible = false;
                UserDialogs.Instance.HideLoading();
            });
        }
        #endregion
        #region Implementation

        private void MasOpciones()
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                NavigationParameters parameters = new NavigationParameters();
                parameters.Add("pin", Mpin);
                await _navigationService.NavigateAsync("AdvancedOptionsLocation", parameters);
            });
        }
        private void Continuar()
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                //if (CanContinue)
                //{
                if (string.IsNullOrEmpty(Identifier.Trim()))
                {
                    await _pageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_Identifier} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                    return;
                }
                if (Address == null)
                {
                    await this._pageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_RequiredLongLat} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                    return;
                }
                //if (this.Pais == null)
                //{
                //    UserDialogs.Instance.HideLoading();
                //    this.IsBusy = false;
                //    await this._pageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_Country} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                //    return;
                //}

                //if (this.Estado == null)
                //{
                //    UserDialogs.Instance.HideLoading();
                //    this.IsBusy = false;
                //    await this._pageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_State} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                //    return;
                //}

                //if (this.Municipio == null)
                //{
                //    UserDialogs.Instance.HideLoading();
                //    this.IsBusy = false;
                //    await this._pageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_Municipality} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                //    return;
                //}

                //if (this.Ciudad == null)
                //{
                //    UserDialogs.Instance.HideLoading();
                //    this.IsBusy = false;
                //    await this._pageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_City} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                //    return;
                //}

                //if (this.Colonia == null)
                //{
                //    UserDialogs.Instance.HideLoading();
                //    this.IsBusy = false;
                //    await this._pageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_Neighborhood} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                //    return;
                //}

                //if (string.IsNullOrEmpty(this.Referencias))
                //{
                //    UserDialogs.Instance.HideLoading();
                //    this.IsBusy = false;
                //    await this._pageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_References} {ProfileLang.AddressForm_RequiredField}", AppResources.AlertAcceptText);
                //    return;
                //}

                //Address.Country = Pais.StrUid;
                //Address.CountryName = Pais.Name;
                //Address.StateName = Estado.Name;
                //Address.UidState = Estado.StrUid;
                //Address.Municipality = Municipio.StrUid;
                //Address.UidCity = Ciudad.StrUid;
                //Address.CityName = Ciudad.Name;
                //Address.UidSuburb = Colonia.StrUid;
                //Address.SuburbName = Colonia.Name;
                //Address.Reference = Referencias;
                //Address.DefaultAddress = DefaultAddress;
                Address.Longitude = Longitude;
                Address.Latitude = Latitude;
                Address.Identifier = Identifier;

                UserDialogs.Instance.HideLoading();
                if (Action == "FirstAddress")
                {
                    Address.DefaultAddress = true;
                }
                NavigationParameters parameters = new NavigationParameters();
                parameters.Add("data", Address);
                parameters.Add("action", Action);
                await _navigationService.NavigateAsync("ProfileAddAddressStep2", parameters);
                //}
                //else
                //{
                //    await this._pageDialogService.DisplayAlertAsync("", AppResources.Location_NoSubursAvaibles, AppResources.AlertAcceptText);
                //}
            });
        }
        private void Regresar()
        {
            _navigationService.GoBackAsync();
        }
        public override async void Initialize(INavigationParameters parameters)
        {

            if (parameters.ContainsKey("action"))
            {
                Action = parameters.GetValue<string>("action");
                CanContinue = false;
                if (Action == "Edit")
                {
                    if (parameters.ContainsKey("data"))
                    {
                        Address = parameters.GetValue<Address>("data");
                        Identifier = Address.Identifier;
                        Referencias = Address.Reference;

                        if (parameters.ContainsKey("mapLocation"))
                        {
                            var pin = parameters.GetValue<MapPin>("mapLocation");
                            Longitude = pin.Longitude;
                            Latitude = pin.Latitude;
                            CanContinue = true;
                            _eventAggregator.GetEvent<Xam_Customer.Core.Events.UpdateMapPinEvent>().Publish(pin);
                        }
                        await this.ObtenerPaises();
                    }
                }
                if (Action == "")
                {
                    await UbicacionActual();

                }
                if (Action == "FirstAddress")
                {
                    await this._pageDialogService.DisplayAlertAsync("", $"{ProfileLang.AddressForm_AddDefault}", AppResources.AlertAcceptText);
                    await UbicacionActual();
                }
            }
        }
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.GetNavigationMode() == Prism.Navigation.NavigationMode.Back)
            {
                if (parameters.ContainsKey("pin"))
                {
                    Mpin = parameters.GetValue<MapPin>("pin");
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await MuestraUbicacion(double.Parse(Mpin.Latitude), double.Parse(Mpin.Longitude));
                    });
                }
            }
        }
        public async Task MuestraUbicacion(double latitude, double longitud)
        {
            UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);
            Mpin = new MapPin() { };
            try
            {
                var geo = new Geocoder();
                var placemarks = await Geocoding.GetPlacemarksAsync(latitude, longitud);
                CanContinue = false;
                Placemark placemark = placemarks.FirstOrDefault(p =>
                    !string.IsNullOrEmpty(p.AdminArea)
                    && !string.IsNullOrEmpty(p.CountryName)
                    && !string.IsNullOrEmpty(p.Thoroughfare)
                    && !string.IsNullOrEmpty(p.Locality)
                );
                Placemark NoPlacemark = placemarks.FirstOrDefault(p =>
                    !string.IsNullOrEmpty(p.AdminArea)
                    && !string.IsNullOrEmpty(p.CountryName)
                );
                if (placemark != null)
                {
                    this.Country = placemark.CountryName;
                    this.State = placemark.AdminArea;
                    this.City = placemark.Locality;
                    this.Neighborhood = placemark.SubLocality;

                    await GetColonies();
                    if (Action == "Edit")
                    {
                        if (!string.IsNullOrEmpty(Address.Identifier))
                        {
                            Identifier = Address.Identifier;
                        }
                    }
                    else if (string.IsNullOrEmpty(Identifier))
                    {
                        Identifier = " ";
                    }
                    Mpin = new MapPin() { Identifier = Identifier, Longitude = longitud.ToString(), Latitude = latitude.ToString() };

                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters.Add("StrNombreCiudad", City);
                    parameters.Add("CodigoEstado", State);
                    parameters.Add("CodigoPais", Country);
                    var AddressRequest = await this.HttpService.GetAsync<IEnumerable<Address>>("Direccion/GetObtenerDireccionConDatosDeGoogle_Movil", parameters);
                    var AddresList = AddressRequest.Result.ToList();
                    if (Action == "Edit")
                    {
                        AddresList[0].Uid = Address.Uid;
                        AddresList[0].Identifier = Address.Identifier;
                    }
                    if (AddresList.Count > 0)
                    {
                        Address = AddresList[0];
                        Address.Longitude = longitud.ToString();
                        Address.Latitude = latitude.ToString();
                        if (!string.IsNullOrEmpty(Address.Identifier))
                        {
                            Identifier = Address.Identifier;
                        }
                        else if (string.IsNullOrEmpty(Identifier))
                        {
                            Identifier = " ";
                        }
                        if (this.Colonies != null)
                        {
                            if (!string.IsNullOrEmpty(Neighborhood))
                            {
                                Colonies colony = this.Colonies.SingleOrDefault(c => c.Name.ToLower().Equals(this.Neighborhood.ToLower()));
                                if (colony != null)
                                {
                                    Address.UidSuburb = colony.Uid;
                                }
                            }
                            CanContinue = true;
                            await LlenaFormulario(Address);
                        }
                        else
                        {
                            await this._pageDialogService.DisplayAlertAsync("", AppResources.Location_NoSubursAvaibles, AppResources.AlertAcceptText);
                        }
                    }

                }
                else if (NoPlacemark != null)
                {
                    this.Country = NoPlacemark.CountryName;
                    this.State = NoPlacemark.AdminArea;
                    this.Neighborhood = NoPlacemark.SubLocality;

                    if (Action == "Edit")
                    {
                        if (!string.IsNullOrEmpty(Address.Identifier))
                        {
                            Identifier = Address.Identifier;
                        }
                    }
                    else if (string.IsNullOrEmpty(Identifier))
                    {
                        Identifier = " ";
                    }
                    Mpin = new MapPin() { Identifier = Identifier, Longitude = longitud.ToString(), Latitude = latitude.ToString() };

                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters.Add("CodigoEstado", State);
                    parameters.Add("CodigoPais", Country);
                    var AddressRequest = await this.HttpService.GetAsync<IEnumerable<Address>>("Direccion/GetObtenerDireccionConDatosDeGoogle_Movil", parameters);
                    var AddresList = AddressRequest.Result.ToList();
                    if (Action == "Edit")
                    {
                        AddresList[0].Uid = Address.Uid;
                        AddresList[0].Identifier = Address.Identifier;
                    }
                    if (AddresList.Count > 0)
                    {
                        Address = AddresList[0];
                        Address.Longitude = longitud.ToString();
                        Address.Latitude = latitude.ToString();
                        if (!string.IsNullOrEmpty(Address.Identifier))
                        {
                            Identifier = Address.Identifier;
                        }
                        else if (string.IsNullOrEmpty(Identifier))
                        {
                            Identifier = " ";
                        }
                    }
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                await this._pageDialogService.DisplayAlertAsync("", fnsEx.Message, AppResources.AlertAcceptText);
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
                await this._pageDialogService.DisplayAlertAsync("", fneEx.Message, AppResources.AlertAcceptText);
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                await this._pageDialogService.DisplayAlertAsync("", pEx.Message, AppResources.AlertAcceptText);
            }
            catch (Exception ex)
            {
                // Unable to get location
                await this._pageDialogService.DisplayAlertAsync("", ex.Message, AppResources.AlertAcceptText);
            }
            _eventAggregator.GetEvent<Xam_Customer.Core.Events.UpdateMapPinEvent>().Publish(Mpin);

            UserDialogs.Instance.HideLoading();
        }
        public async Task UbicacionActual()
        {
            UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);
            Mpin = new MapPin() { };
            try
            {
                var location = await Geolocation.GetLocationAsync();

                if (location != null)
                {
                    var geo = new Geocoder();
                    var placemarks = await Geocoding.GetPlacemarksAsync(location.Latitude, location.Longitude);
                    CanContinue = false;

                    Placemark placemark = placemarks.FirstOrDefault(p =>
                        !string.IsNullOrEmpty(p.AdminArea)
                        && !string.IsNullOrEmpty(p.CountryName)
                        && !string.IsNullOrEmpty(p.Locality)
                        && !string.IsNullOrEmpty(p.Thoroughfare)
                    );
                    Placemark NoPlacemark = placemarks.FirstOrDefault(p =>
                    !string.IsNullOrEmpty(p.AdminArea)
                    && !string.IsNullOrEmpty(p.CountryName)
                );
                    if (placemark != null)
                    {
                        this.Country = placemark.CountryName;
                        this.State = placemark.AdminArea;
                        this.City = placemark.Locality;
                        this.Neighborhood = placemark.SubLocality;

                        await GetColonies();
                        if (Action == "Edit")
                        {
                            if (!string.IsNullOrEmpty(Address.Identifier))
                            {
                                Identifier = Address.Identifier;
                            }
                        }
                        else if (string.IsNullOrEmpty(Identifier))
                        {
                            Identifier = " ";
                        }
                        Mpin = new MapPin() { Identifier = Identifier, Longitude = location.Longitude.ToString(), Latitude = location.Latitude.ToString() };

                        Dictionary<string, string> parameters = new Dictionary<string, string>();
                        parameters.Add("StrNombreCiudad", City);
                        parameters.Add("CodigoEstado", State);
                        parameters.Add("CodigoPais", Country);
                        var AddressRequest = await this.HttpService.GetAsync<IEnumerable<Address>>("Direccion/GetObtenerDireccionConDatosDeGoogle_Movil", parameters);
                        var AddresList = AddressRequest.Result.ToList();
                        if (Action == "Edit")
                        {
                            AddresList[0].Uid = Address.Uid;
                            AddresList[0].Identifier = Address.Identifier;
                        }
                        if (AddresList.Count > 0)
                        {
                            Address = AddresList[0];
                            Address.Longitude = location.Longitude.ToString();
                            Address.Latitude = location.Latitude.ToString();

                            if (this.Colonies != null)
                            {
                                Colonies colony = this.Colonies.SingleOrDefault(c => c.Name.ToLower().Equals(this.Neighborhood.ToLower()));
                                if (colony != null)
                                {
                                    Address.UidSuburb = colony.Uid;
                                }
                                await LlenaFormulario(Address);
                                CanContinue = true;
                            }
                            else
                            {
                                await this._pageDialogService.DisplayAlertAsync("", AppResources.Location_NoSubursAvaibles, AppResources.AlertAcceptText);
                            }
                        }
                    }
                    else if (NoPlacemark != null)
                    {
                        this.Country = NoPlacemark.CountryName;
                        this.State = NoPlacemark.AdminArea;
                        this.Neighborhood = NoPlacemark.SubLocality;

                        if (Action == "Edit")
                        {
                            if (!string.IsNullOrEmpty(Address.Identifier))
                            {
                                Identifier = Address.Identifier;
                            }
                        }
                        else if (string.IsNullOrEmpty(Identifier))
                        {
                            Identifier = " ";
                        }
                        Mpin = new MapPin() { Identifier = Identifier, Longitude = location.Longitude.ToString(), Latitude = location.Latitude.ToString() };

                        Dictionary<string, string> parameters = new Dictionary<string, string>();
                        parameters.Add("CodigoEstado", State);
                        parameters.Add("CodigoPais", Country);
                        var AddressRequest = await this.HttpService.GetAsync<IEnumerable<Address>>("Direccion/GetObtenerDireccionConDatosDeGoogle_Movil", parameters);
                        var AddresList = AddressRequest.Result.ToList();
                        if (Action == "Edit")
                        {
                            AddresList[0].Uid = Address.Uid;
                            AddresList[0].Identifier = Address.Identifier;
                        }
                        if (AddresList.Count > 0)
                        {
                            Address = AddresList[0];
                            Address.Longitude = location.Longitude.ToString();
                            Address.Latitude = location.Latitude.ToString();
                            if (!string.IsNullOrEmpty(Address.Identifier))
                            {
                                Identifier = Address.Identifier;
                            }
                            else if (string.IsNullOrEmpty(Identifier))
                            {
                                Identifier = " ";
                            }
                        }
                    }
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                await this._pageDialogService.DisplayAlertAsync("", fnsEx.Message, AppResources.AlertAcceptText);
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
                await this._pageDialogService.DisplayAlertAsync("", fneEx.Message, AppResources.AlertAcceptText);
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                await this._pageDialogService.DisplayAlertAsync("", pEx.Message, AppResources.AlertAcceptText);
            }
            catch (Exception ex)
            {
                // Unable to get location
                await this._pageDialogService.DisplayAlertAsync("", ex.Message, AppResources.AlertAcceptText);
            }
            _eventAggregator.GetEvent<Xam_Customer.Core.Events.UpdateMapPinEvent>().Publish(Mpin);

            UserDialogs.Instance.HideLoading();
        }


        private async Task LlenaFormulario(Address ubicacion)
        {
            this.Referencias = ubicacion.Reference;
            this.Latitude = ubicacion.Latitude;
            this.Longitude = ubicacion.Longitude;
            await this.ObtenerPaises();
        }
        private async Task GetColonies()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("StrNombreCiudad", City);
            parameters.Add("CodigoEstado", State);
            parameters.Add("CodigoPais", Country);

            var ColoniesRequest = await this.HttpService.GetAsync<IEnumerable<AddressSession>>("Direccion/GetObtenerDireccionConDatosDeGoogle_Movil", parameters);
            if (ColoniesRequest.Code == HttpResponseCode.Success)
            {
                var ciudades = ColoniesRequest.Result.ToList();

                // No hay ciudades
                if (ciudades.Count == 0)
                {
                    await this._pageDialogService.DisplayAlertAsync("", AppResources.Location_NocitiesAvaibles, AppResources.AlertAcceptText);
                    return;
                }

                AddressSession ciudad = ciudades[0];
                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add("UidCiudad", ciudad.Uid);
                var ColonieRequest = await this.HttpService.GetAsync<IEnumerable<Colonies>>("Direccion/GetObtenerColonias_Movil", param);

                if (ColonieRequest.Code == HttpResponseCode.Success)
                {
                    this.Colonies = ColonieRequest.Result.ToList();
                    // No hay ciudades
                    if (Colonies.Count == 0)
                    {
                        await this._pageDialogService.DisplayAlertAsync("", AppResources.Location_NoSubursAvaibles, AppResources.AlertAcceptText);
                        return;
                    }
                }
                else
                {
                    await this._pageDialogService.DisplayAlertAsync("", AppResources.Error_General_Message, AppResources.AlertAcceptText);
                }

            }
        }


        #endregion

        #region List informacion methods
        private async Task ObtenerPaises()
        {
            this.IsBusy = true;

            var result = await this.HttpService.GetAsync<IEnumerable<ListboxItemModel>>("Direccion/ObtenerPaises");
            if (result.Code == HttpResponseCode.Success)
            {
                foreach (var item in result.Result)
                {
                    if (item.Uid != Guid.Empty)
                    {
                        this.Paises.Add(item);
                    }
                }
            }

            this.IsBusy = false;

            if (this.Address != null)
            {
                ListboxItemModel pais = this.Paises.FirstOrDefault(i => i.Uid == Guid.Parse(this.Address.Country));
                if (pais != null)
                {
                    this.Pais = pais;
                }
            }
            if (this.Estados.Any())
            {
                this.Estados = new ObservableCollection<ListboxItemModel>();
            }
            if (this.Municipios.Any())
            {
                this.Municipios = new ObservableCollection<ListboxItemModel>();
            }
            if (this.Ciudades.Any())
            {
                this.Ciudades = new ObservableCollection<ListboxItemModel>();
            }
            if (this.Colonias.Any())
            {
                this.Colonias = new ObservableCollection<ListboxItemModel>();
            }
        }
        private async Task ObtenerEstados(Guid uid)
        {
            if (this.IsBusy)
            {
                return;
            }

            this.IsBusy = true;

            if (this.Estados.Any())
            {
                this.Estados = new ObservableCollection<ListboxItemModel>();
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidPais", uid.ToString());

            var result = await this.HttpService.GetAsync<IEnumerable<ListboxItemModel>>("Direccion/ObtenerEstadosPais", parameters);
            if (result.Code == HttpResponseCode.Success)
            {
                foreach (var item in result.Result)
                {
                    if (item.Uid != Guid.Empty)
                    {
                        this.Estados.Add(item);
                    }
                }
            }

            this.IsBusy = false;

            if (this.Address != null)
            {
                ListboxItemModel lEstado = this.Estados.FirstOrDefault(i => i.Uid == Guid.Parse(this.Address.UidState));
                if (lEstado != null)
                {
                    this.Estado = lEstado;
                }
            }
            if (this.Municipios.Any())
            {
                this.Municipios = new ObservableCollection<ListboxItemModel>();
            }
            if (this.Ciudades.Any())
            {
                this.Ciudades = new ObservableCollection<ListboxItemModel>();
            }
            if (this.Colonias.Any())
            {
                this.Colonias = new ObservableCollection<ListboxItemModel>();
            }
        }
        private async Task ObtenerMunicipios(Guid uid)
        {
            if (this.IsBusy)
            {
                return;
            }

            this.IsBusy = true;

            if (this.Municipios.Any())
            {
                this.Municipios = new ObservableCollection<ListboxItemModel>();
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidEstado", uid.ToString());

            var result = await this.HttpService.GetAsync<IEnumerable<ListboxItemModel>>("Direccion/ObtenerMunicipiosEstado", parameters);
            if (result.Code == HttpResponseCode.Success)
            {
                foreach (var item in result.Result)
                {
                    if (item.Uid != Guid.Empty)
                    {
                        this.Municipios.Add(item);
                    }
                }
            }

            this.IsBusy = false;

            if (this.Address != null)
            {
                ListboxItemModel lMunicipio = this.Municipios.FirstOrDefault(i => i.Uid == Guid.Parse(this.Address.Municipality));
                if (lMunicipio != null)
                {
                    this.Municipio = lMunicipio;
                }
            }
            if (this.Ciudades.Any())
            {
                this.Ciudades = new ObservableCollection<ListboxItemModel>();
            }
            if (this.Colonias.Any())
            {
                this.Colonias = new ObservableCollection<ListboxItemModel>();
            }
        }
        private async Task ObtenerCiudades(Guid uid)
        {
            if (this.IsBusy)
            {
                return;
            }

            this.IsBusy = true;

            if (this.Ciudades.Any())
            {
                this.Ciudades = new ObservableCollection<ListboxItemModel>();
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidMunicipio", uid.ToString());

            var result = await this.HttpService.GetAsync<IEnumerable<ListboxItemModel>>("Direccion/ObtenerCiudadesMunicipio", parameters);
            if (result.Code == HttpResponseCode.Success)
            {
                foreach (var item in result.Result)
                {
                    if (item.Uid != Guid.Empty)
                    {
                        this.Ciudades.Add(item);
                    }
                }
            }

            this.IsBusy = false;

            if (this.Address != null)
            {
                ListboxItemModel lCiudad = this.Ciudades.FirstOrDefault(i => i.Uid == Guid.Parse(this.Address.UidCity));
                if (lCiudad != null)
                {
                    this.Ciudad = lCiudad;
                }
            }
            if (this.Colonias.Any())
            {
                this.Colonias = new ObservableCollection<ListboxItemModel>();
            }
        }

        private async Task ObtenerColonias(Guid uid)
        {
            if (this.IsBusy)
            {
                return;
            }

            this.IsBusy = true;

            if (this.Colonias.Any())
            {
                this.Colonias = new ObservableCollection<ListboxItemModel>();
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidCiudad", uid.ToString());

            var result = await this.HttpService.GetAsync<IEnumerable<ListboxItemModel>>("Direccion/ObtenerColoniasCiudad", parameters);
            if (result.Code == HttpResponseCode.Success)
            {
                foreach (var item in result.Result)
                {
                    if (item.Uid != Guid.Empty)
                    {
                        this.Colonias.Add(item);
                    }
                }
            }

            this.IsBusy = false;

            if (this.Address != null)
            {
                ListboxItemModel lColonia = this.Colonias.FirstOrDefault(i => i.Uid == Guid.Parse(this.Address.UidSuburb));
                if (lColonia != null)
                {
                    this.Colonia = lColonia;
                }
            }

            UserDialogs.Instance.HideLoading();
        }

        private async void ObtenerCodigoPostal(Guid Uid)
        {
            await loadZipCode(Uid);
        }
        private async Task loadZipCode(Guid Uid)
        {
            if (this.IsBusy)
            {
                return;
            }

            this.IsBusy = true;

            if (!string.IsNullOrEmpty(CodigoPostal))
            {
                this.CodigoPostal = string.Empty;
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("UidColonia", Uid.ToString());

            var result = await this.HttpService.GetAsync<Address>("Direccion/ObtenerCodigoPostal", parameters);
            if (result.Code == HttpResponseCode.Success)
            {
                var colonia = result.Result;
                CodigoPostal = colonia.ZipCode;
                IsBusy = false;
            }
        }

        #endregion
    }
}
