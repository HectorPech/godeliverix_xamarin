﻿using Com.OneSignal;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Services.Implementation;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xam_Customer.Views.Layout;
using Xam_Customer.Views.Profile;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class LoginPageViewModel : BaseViewModel
    {
        #region Properties
        private bool rememberAcount;
        public bool RememberAcount
        {
            get { return rememberAcount; }
            set { SetProperty(ref rememberAcount, value); }
        }
        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }
        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private bool isLoading;
        public bool IsLoading
        {
            get { return isLoading; }
            set { SetProperty(ref isLoading, value); }
        }

        #endregion

        #region Command
        public DelegateCommand cmdCreateAccount { get; }
        public DelegateCommand cmdForgotAccount { get; }
        public DelegateCommand GoBackCommand { get; }

        public DelegateCommand LoginCommand { get; }

        #endregion

        #region Services
        public IAppInstaceService _appInstanceService { get; set; }
        public IHttpRequestService _httpRequestService { get; set; }
        #endregion
        public LoginPageViewModel(
            INavigationService navigationService,
            IPageDialogService pageDialogService,
            IDialogService dialogService,
            IAppInstaceService appInstanceService,
            IHttpRequestService httpRequestService
            ) : base(navigationService, dialogService, pageDialogService)
        {
            _appInstanceService = appInstanceService;
            _httpRequestService = httpRequestService;
            cmdCreateAccount = new DelegateCommand(NewAccount);
            cmdForgotAccount = new DelegateCommand(ForgotAccount);
            this.GoBackCommand = new DelegateCommand(async () => { await navigationService.GoBackAsync(); });
            this.LoginCommand = new DelegateCommand(this.Login);
            if (!string.IsNullOrEmpty(Settings.RememberLastUser))
            {
                Username = Settings.RememberLastUser;
            }
        }

        protected async void NewAccount()
        {
            await NavigationService.NavigateAsync("RegistryStep1Page");
        }
        private async void ForgotAccount()
        {
            await this.NavigationService.NavigateAsync($"{nameof(NavigationMasterDetailPage)}/{nameof(ForgotAccountPage)}");
        }

        private async void Login()
        {
            if (this.IsLoading)
            {
                return;
            }
            this.IsLoading = true;
            if (string.IsNullOrEmpty(this.Username))
            {
                this.IsLoading = false;
                await this.PageDialogService.DisplayAlertAsync("", AppResources.RequiredEmail, AppResources.AlertAcceptText);
                return;
            }
            if (string.IsNullOrEmpty(this.Password))
            {
                this.IsLoading = false;
                await this.PageDialogService.DisplayAlertAsync("", AppResources.SignInStepTwo_RequiredPassword, AppResources.AlertAcceptText);
                return;
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("correoElectronico", Username);
            parameters.Add("Contrasena", Password);
            var UserRequest = await this._httpRequestService.GetAsync<User>("Profile/GetInicioDeSessionCliente_Movil", parameters);
            var oUser = UserRequest.Result;
            if (oUser != null && oUser.Uid != Guid.Empty.ToString())
            {
                Settings.UserSession = new UserSession()
                {
                    StrUid = oUser.Uid,
                    FisrtName = oUser.Name ?? "",
                    FisrtLastName = oUser.LastName ?? "",
                    BirthDay = oUser.BirthDay,
                    UserName = oUser.Username
                };

                if (RememberAcount)
                {
                    Settings.RememberUser = "Yes";
                }
                else
                {
                    Settings.RememberUser = string.Empty;
                }
                Settings.RememberLastUser = Username;

                Dictionary<string, string> parametro = new Dictionary<string, string>();
                parametro.Add("UidUsuario", oUser.Uid);
                var AddressRequest = await this._httpRequestService.GetAsync<IEnumerable<Address>>("Direccion/GetObtenerDireccionUsuario_Movil", parametro);
                this.IsLoading = false;
                var direcciones = AddressRequest.Result.ToList();
                if (direcciones.Count > 0)
                {
                    _appInstanceService.HasAddresses = true;
                    var address = direcciones.Find(c => c.DefaultAddress == true);
                    _appInstanceService.SelectDeliveryAddress("", "", "", 0.0, 0.0);
                    if (address != null)
                    {
                        _appInstanceService.UserSelectedDeliveryAddress(address.Uid, address.UidState, address.UidSuburb, address.Identifier, double.Parse(address.Latitude), double.Parse(address.Longitude));
                    }
                    _appInstanceService.SetAddressTotal(direcciones.Count);
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        NavigationParameters pairs = new NavigationParameters();
                        pairs.Add("navigationType", NavigationType.Logged);
                        await NavigationService.NavigateAsync($"/{nameof(NavigationMasterDetailPage)}/NavigationPage/{nameof(StoresPage)}", pairs);
                    });
                }
                else
                {
                    _appInstanceService.HasAddresses = false;
                    _appInstanceService.SetAddressTotal(0);
                    NavigationParameters pairs = new NavigationParameters();
                    pairs.Add("action", "FirstAddress");
                    await NavigationService.NavigateAsync($"/{nameof(ProfileAddAddressStep1)}", pairs);
                }
            }
            else
            {
                this.IsLoading = false;
                await this.PageDialogService.DisplayAlertAsync("", AppResources.InvalidUsernamePassword, AppResources.AlertAcceptText);
                return;
            }
        }
    }
}
