﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using Xam_Customer.Core.Events;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Views;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class ChangeProductBranchPageViewModel : BindableBase, INavigatedAware, IInitialize
    {

        private Branch _SelectedBranch;
        public Branch SelectedBranch
        {
            get => _SelectedBranch;
            set => SetProperty(ref _SelectedBranch, value);
        }

        private string _ImagePath;
        public string ImagePath
        {
            get => _ImagePath;
            set => SetProperty(ref _ImagePath, value);
        }

        private bool _loadingList;
        public bool LoadingList
        {
            get { return _loadingList; }
            set { SetProperty(ref _loadingList, value); }
        }

        private string _TittlePage;
        public string TittlePage
        {
            get { return _TittlePage; }
            set { SetProperty(ref _TittlePage, value); }
        }

        private string _UidCompany;
        public string UidCompany
        {
            get { return _UidCompany; }
            set { SetProperty(ref _UidCompany, value); }
        }
        private string _AvaibleBranches;
        public string AvaibleBranches
        {
            get { return _AvaibleBranches; }
            set { SetProperty(ref _AvaibleBranches, value); }
        }

        private int _total;
        public int Total
        {
            get { return _total; }
            set { SetProperty(ref _total, value); }
        }

        private ObservableCollection<Product> _products;
        public ObservableCollection<Product> Products
        {
            get { return _products; }
            set { SetProperty(ref _products, value); }
        }

        private List<Branch> _BranchList;
        public List<Branch> BranchList
        {
            get => _BranchList;
            set => SetProperty(ref _BranchList, value);
        }

        #region Internal
        //private IHttpRequestService _HttpService { get; }
        //private IDialogService _dialogService { set; get; }
        private INavigationService _navigationServices { set; get; }
        private IEventAggregator _ea { set; get; }
        private IAppInstaceService _appInstanceService { set; get; }
        #endregion

        #region Command
        public DelegateCommand CmdRefresh { get; set; }
        public DelegateCommand<string> SelectBranch { get; set; }
        public DelegateCommand ViewCompanyPage { get; set; }
        public DelegateCommand GoToCart { get; }
        #endregion

        public ChangeProductBranchPageViewModel(INavigationService navigationService,
            IAppInstaceService appInstaceService,
            IEventAggregator ea)
        {
            _appInstanceService = appInstaceService;
            _navigationServices = navigationService;
            _ea = ea;
            ViewCompanyPage = new DelegateCommand(CompanyPage);
            this.GoToCart = new DelegateCommand(async () => await this._navigationServices.NavigateAsync($"{nameof(CartPage)}"));
            ////CmdRefresh = new DelegateCommand(GetBranches);
        }

        private async void CompanyPage()
        {
            await this._navigationServices
                    .NavigateAsync(nameof(StoreDetailPage), new NavigationParameters()
                    {
                    { "uid", Guid.Parse(UidCompany) }
                    });
        }
        private void GetBranches()
        {
            LoadingList = true;
            Random random = new Random();
            //string imgPath = "https://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2017/04/pizzapepperoni0.jpg";

            List<Product> products = new List<Product>();
            int quantity = random.Next(2, 5);

            //for (int i = 0; i < quantity; i++)
            //{
            //    products.Add(new Product()
            //    {
            //        Uid = Guid.NewGuid(),
            //        Price = random.Next(99, 250),
            //        Description = random.Next(1,23).ToString(),
            //        Name = $"Sucursal {i}",
            //        ImgPath = imgPath
            //    });
            //}

            this.Total = products.Count();
            LoadingList = false;
            this.Products = new ObservableCollection<Product>(products);
        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            //throw new NotImplementedException();
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            Device.InvokeOnMainThreadAsync(() =>
            {
                int count = this._appInstanceService.ProductsCount();
                this._ea.GetEvent<CartCountEvent>().Publish(count);
            });

            if (parameters.ContainsKey("Data"))
            {
                BranchList = parameters.GetValue<List<Branch>>("Data");
                Total = BranchList.Count;
                AvaibleBranches = AppResources.ChangeProductBrach_BrachesAvaible + " :" + Total;
            }
            if (parameters.ContainsKey("ImgInc"))
            {
                ImagePath = parameters.GetValue<string>("ImgInc");
            }
            if (parameters.ContainsKey("CompanyName"))
            {
                TittlePage = parameters.GetValue<string>("CompanyName");
                UidCompany = parameters.GetValue<string>("UidCompany");
            }
            if (parameters.ContainsKey("SelectedBrach"))
            {
                SelectedBranch = parameters.GetValue<Branch>("SelectedBrach");
                BranchList[BranchList.FindIndex(b => b.Identificador == SelectedBranch.Identificador)].SelectedColorItem = Xamarin.Forms.Color.Red;
                SelectBranch = new DelegateCommand<string>(ChangeBranch);
            }
        }

        private void ChangeBranch(string UidSucursal)
        {
            BranchList[BranchList.FindIndex(b => b.Identificador == SelectedBranch.Identificador)].SelectedColorItem = Xamarin.Forms.Color.Transparent;
            SelectedBranch = BranchList.Find(b => b.Uid == UidSucursal);
            BranchList[BranchList.FindIndex(b => b.Identificador == SelectedBranch.Identificador)].SelectedColorItem = Xamarin.Forms.Color.Red;
            _ea.GetEvent<BranchSelectedEvent>().Publish(SelectedBranch);
        }

        public void Initialize(INavigationParameters parameters)
        {
            Device.InvokeOnMainThreadAsync(() =>
            {
                int count = this._appInstanceService.ProductsCount();
                this._ea.GetEvent<CartCountEvent>().Publish(count);
            });
        }
    }
}
