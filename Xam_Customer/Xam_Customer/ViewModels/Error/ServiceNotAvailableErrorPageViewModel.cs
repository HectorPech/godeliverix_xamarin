﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Services.Abstract;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels.Error
{
    public class ServiceNotAvailableErrorPageViewModel : BindableBase, INavigationAware
    {
        #region Properties
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }
        #endregion

        #region Private
        /// <summary>
        /// Si se cumplen los requisitos se redirecciona a esta vista
        /// </summary>
        public string PreviousPage { get; set; }
        #endregion

        #region Services
        private INavigationService _navigationService { get; }
        private IHttpRequestService _HttpService { get; }
        #endregion

        #region Command
        public DelegateCommand RetryCommand { get; }
        #endregion

        public ServiceNotAvailableErrorPageViewModel(
            INavigationService navigationService,
            IHttpRequestService httpRequestService
            )
        {
            this._navigationService = navigationService;
            this._HttpService = httpRequestService;

            this.RetryCommand = new DelegateCommand(async () => { await this.Retry(); }, this.CanSubmit);
        }

        #region Implementation
        private async Task Retry()
        {
            this.IsBusy = true;
            await Task.Delay(1000);
            var result = await this._HttpService.GetAsync<IEnumerable<string>>("Default/Get");
            if (result.Code == HttpResponseCode.Success)
            {
                this.IsBusy = false;
                await Device.InvokeOnMainThreadAsync(async () =>
                {
                    await this._navigationService.NavigateAsync(this.PreviousPage);
                });
            }
            else
            {
                this.IsBusy = false;
            }
        }

        bool CanSubmit()
        {
            return !this.IsBusy;
        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {

        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            string previousPageKey = "previousPage";
            if (parameters.ContainsKey(previousPageKey))
            {
                this.PreviousPage = parameters.GetValue<string>(previousPageKey);
            }
        }
        #endregion
    }
}
