﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using Xam_Customer.Core.Services.Abstract;
using Xamarin.Essentials;

namespace Xam_Customer.ViewModels.Error
{
    public class NetworkErrorPageViewModel : BindableBase, INavigationAware
    {
        #region Properties
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }
        #endregion

        #region Private
        /// <summary>
        /// Si se cumplen los requisitos se redirecciona a esta vista
        /// </summary>
        public string PreviousPage { get; set; }
        #endregion

        #region Services
        private INavigationService _navigationService { get; }
        private IPageDialogService _PageDialogService { get; }
        private IHttpRequestService _HttpService { get; }
        #endregion

        #region Command
        public DelegateCommand RetryCommand { get; }
        #endregion

        public NetworkErrorPageViewModel(
            INavigationService navigationService,
            IPageDialogService pageDialogService,
            IHttpRequestService httpRequestService
            )
        {
            this._navigationService = navigationService;
            this._PageDialogService = pageDialogService;
            this._HttpService = httpRequestService;

            this.RetryCommand = new DelegateCommand(this.Retry, CanSubmit);
        }

        #region Implementation
        private async void Retry()
        {
            this.IsBusy = true;
            var current = Connectivity.NetworkAccess;
            await System.Threading.Tasks.Task.Delay(2000);

            if (current == NetworkAccess.Internet)
            {
                this.IsBusy = false;
                await this._navigationService.NavigateAsync(this.PreviousPage);
            }
            else
            {
                this.IsBusy = false;
            }
        }

        bool CanSubmit()
        {
            return !this.IsBusy;
        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {

        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            string previousPageKey = "previousPage";
            if (parameters.ContainsKey(previousPageKey))
            {
                this.PreviousPage = parameters.GetValue<string>(previousPageKey);
            }
        }
        #endregion
    }
}
