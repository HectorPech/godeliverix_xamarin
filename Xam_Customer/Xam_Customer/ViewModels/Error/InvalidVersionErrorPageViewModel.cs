﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using Xam_Customer.Core.Services.Abstract;

namespace Xam_Customer.ViewModels.Error
{
    public class InvalidVersionErrorPageViewModel : BindableBase
    {
        #region Services
        private INavigationService _navigationService { get; }
        #endregion

        #region Command
        public DelegateCommand UpdateCommand { get; }
        #endregion

        public InvalidVersionErrorPageViewModel(
            INavigationService navigationService
            )
        {
            this._navigationService = navigationService;

            this.UpdateCommand = new DelegateCommand(this.Update);
        }

        #region Implementation
        private async void Update()
        {

        }
        #endregion
    }
}
