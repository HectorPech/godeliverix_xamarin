﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.System;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Util;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.PurchaseHistory;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class PurchaseDetailPageViewModel : BaseNavigationViewModel
    {
        #region Properties
        private string ordersInfo;
        public string OrdersInfo
        {
            get { return ordersInfo; }
            set { SetProperty(ref ordersInfo, value); }
        }

        private string title;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        private string _PaymentType;
        public string PaymentType
        {
            get { return _PaymentType; }
            set { SetProperty(ref _PaymentType, value); }
        }

        private string _PaymentStatus;
        public string PaymentStatus
        {
            get { return _PaymentStatus; }
            set { SetProperty(ref _PaymentStatus, value); }
        }

        private bool isBusy;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }


        private string _identifier;
        public string Identifier
        {
            get { return _identifier; }
            set { SetProperty(ref _identifier, value); }
        }

        private string _Country;
        public string Country
        {
            get { return _Country; }
            set { SetProperty(ref _Country, value); }
        }

        private string _State;
        public string State
        {
            get { return _State; }
            set { SetProperty(ref _State, value); }
        }

        private string _City;
        public string City
        {
            get { return _City; }
            set { SetProperty(ref _City, value); }
        }

        private string _street;
        public string Street
        {
            get { return _street; }
            set { SetProperty(ref _street, value); }
        }

        private ObservableCollection<PurchaseHistoryDetailOrder> _Orders;
        public ObservableCollection<PurchaseHistoryDetailOrder> Orders
        {
            get { return _Orders; }
            set { SetProperty(ref _Orders, value); }
        }

        private decimal subtotal;
        public decimal Subtotal
        {
            get { return subtotal; }
            set { SetProperty(ref subtotal, value); }
        }

        private decimal shipment;
        public decimal Shipment
        {
            get { return shipment; }
            set { SetProperty(ref shipment, value); }
        }

        private decimal tax;
        public decimal Tax
        {
            get { return tax; }
            set { SetProperty(ref tax, value); }
        }

        private decimal tips;
        public decimal Tips
        {
            get { return tips; }
            set { SetProperty(ref tips, value); }
        }

        private decimal total;
        public decimal Total
        {
            get { return total; }
            set { SetProperty(ref total, value); }
        }

        private bool cancelAvailable;
        public bool CancelAvailable
        {
            get { return cancelAvailable; }
            set { SetProperty(ref cancelAvailable, value); }
        }

        private bool hasWalletDiscount;
        public bool HasWalletDiscount
        {
            get { return hasWalletDiscount; }
            set { SetProperty(ref hasWalletDiscount, value); }
        }

        private decimal walletDiscount;
        public decimal WalletDiscount
        {
            get { return walletDiscount; }
            set { SetProperty(ref walletDiscount, value); }
        }

        private decimal totalNoDiscount;
        public decimal TotalNoDiscount
        {
            get { return totalNoDiscount; }
            set { SetProperty(ref totalNoDiscount, value); }
        }

        #endregion

        #region Private
        public Guid Uid { get; set; }
        #endregion

        #region Command
        public DelegateCommand<PurchaseHistoryDetailOrder> ViewOrderDetailCommand { get; }
        public DelegateCommand ReloadInfo { get; }
        #endregion

        #region Services
        private IHttpRequestService HttpService { get; }
        private IEventAggregator _eventAggregator { get; }
        private IPageDialogService _pageDialogService { get; }
        private IAppInstaceService Instance { get; }
        #endregion

        public PurchaseDetailPageViewModel(
            INavigationService navigationService,
            IHttpRequestService httpRequestService,
            IEventAggregator eventAggregator,
            IPageDialogService pageDialogService,
            IAppInstaceService appInstaceService
            ) : base(navigationService)
        {
            this.HttpService = httpRequestService;
            this._eventAggregator = eventAggregator;
            this._pageDialogService = pageDialogService;
            this.Instance = appInstaceService;

            this.Orders = new ObservableCollection<PurchaseHistoryDetailOrder>();
            this.ViewOrderDetailCommand = new DelegateCommand<PurchaseHistoryDetailOrder>(this.ViewOrderDetail);
            this.ReloadInfo = new DelegateCommand(this.reload);
        }

        #region Implementation
        private void reload()
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                await this.Load();
            });
        }

        public void ViewOrderDetail(PurchaseHistoryDetailOrder order)
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                await this.NavigationService.NavigateAsync($"{nameof(OrderHistoryDetailPage)}", new NavigationParameters()
                {
                    { "uid" , order.OrderUid }
                });
            });
        }

        private async Task Load()
        {
            this.IsBusy = true;

            await Device.InvokeOnMainThreadAsync(() =>
            {
                UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);
            });

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("uid", Uid.ToString());

            var orderRequest = await this.HttpService.GetAsync<PurchaseHistoryDetail>("Orders/GetPurchaseDetail", parameters);

            if (orderRequest.Code == Xam_Customer.Core.Enum.HttpResponseCode.Success)
            {
                PurchaseHistoryDetail purchase = orderRequest.Result;
                Title = $"{PurchaseHistoryLang.PaymentDetail_OrderTittle} # {purchase.Folio}";
                PaymentStatus = purchase.PaymentStatus;

                Identifier = purchase.AddressIdentifier;
                Country = purchase.AddressCountry;
                State = purchase.AddressState;
                City = purchase.AddressCity;
                Street = purchase.AddressStreet0;

                switch (purchase.PaymentMethod)
                {
                    case "Efectivo":
                        PaymentType = PurchaseHistoryLang.PaymentType_Cash;
                        break;
                    case "Tarjeta":
                        PaymentType = PurchaseHistoryLang.PaymentType_Card;
                        break;
                    case "Monedero":
                        PaymentType = PurchaseHistoryLang.PaymentType_EWallet;
                        break;
                }

                foreach (var item in purchase.Orders)
                {
                    switch (item.Status)
                    {
                        case "Creado":
                            item.StatusColor = OrderStatusColor.Created.Color;
                            item.Status = AppResources.StatusOrder_Created;
                            break;
                        case "Elaborado":
                            item.StatusColor = OrderStatusColor.Elaborated.Color;
                            item.Status = AppResources.StatusOrder_Making;
                            break;
                        case "Entregado":
                            item.StatusColor = OrderStatusColor.Delivered.Color;
                            item.Status = AppResources.StatusOrder_Delivered;
                            break;
                        case "Finalizado":
                            item.StatusColor = OrderStatusColor.Finalized.Color;
                            item.Status = AppResources.StatusOrder_Finished;
                            break;
                        case "Cancelado":
                            item.StatusColor = OrderStatusColor.Canceled.Color;
                            item.Status = AppResources.StatusOrder_Canceled;
                            break;
                        case "Espera de confirmacion":
                            item.StatusColor = OrderStatusColor.WaitingForConfirmation.Color;
                            item.Status = AppResources.StatusOrder_Waiting;
                            break;
                        case "Confirmado":
                            item.StatusColor = OrderStatusColor.Confirmed.Color;
                            item.Status = AppResources.StatusOrder_Confirmed;
                            break;
                        case "Enviando":
                            item.StatusColor = OrderStatusColor.Sended.Color;
                            item.Status = AppResources.StatusOrder_Delivering;
                            break;
                        default:
                            break;
                    }

                    item.Total += item.Tips + item.Delivery + item.CardPaymentComission + item.DeliveryCardPaymentComission;
                    item.TotalNoDiscount = item.Total;
                    item.Total = item.HasDiscount ? (item.Total - item.WalletDiscount.Value) : item.Total;
                }

                this.Orders = new ObservableCollection<PurchaseHistoryDetailOrder>(purchase.Orders.ToList());

                decimal orderTotal = this.Orders.Sum(s => s.TotalNoDiscount);

                Tips = this.Orders.Sum(s => s.Tips);
                Shipment = this.Orders.Sum(s => s.Delivery);
                Subtotal = orderTotal - this.Orders.Sum(s => s.Delivery);

                this.WalletDiscount = 0;
                this.HasWalletDiscount = this.Orders.Where(i => i.WalletDiscount.HasValue).Any();
                if (this.HasWalletDiscount)
                {
                    this.WalletDiscount = this.Orders.Sum(i => i.WalletDiscount.HasValue ? i.WalletDiscount.Value : 0);
                }

                this.TotalNoDiscount = orderTotal;
                Total = (orderTotal) - this.WalletDiscount;

                _eventAggregator.GetEvent<Xam_Customer.Core.Events.UpdateMapPinEvent>().Publish(new MapPin()
                {
                    Identifier = Identifier,
                    Latitude = purchase.AddressLatitude,
                    Longitude = purchase.AddressLongitude
                });

                await Device.InvokeOnMainThreadAsync(() =>
                {
                    UserDialogs.Instance.HideLoading();
                    this.IsBusy = false;
                });
            }
            else
            {
                // TODO: CATCH ERROR
            }
        }
        #endregion

        public override void Initialize(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("uid"))
            {
                this.Uid = parameters.GetValue<Guid>("uid");

                Task.Run(async () => { await this.Load(); });
            }
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.GetNavigationMode() == NavigationMode.Back)
            {
                Device.InvokeOnMainThreadAsync(async () =>
                {
                    if (parameters.ContainsKey("status"))
                    {
                        await this.Load();
                    }
                });
            }
        }
    }
}
