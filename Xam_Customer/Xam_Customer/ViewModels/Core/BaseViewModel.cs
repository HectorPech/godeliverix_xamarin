﻿using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System.Threading.Tasks;
using Xam_Customer.Views.Error;

namespace Xam_Customer.ViewModels.Core
{
    public class BaseViewModel : BindableBase, INavigationAware, IInitialize
    {
        #region Internal    
        public INavigationService NavigationService { get; set; }
        public IDialogService DialogService { get; set; }
        public IPageDialogService PageDialogService { get; set; }
        #endregion

        public BaseViewModel(
            INavigationService navigationService,
            IDialogService dialogService,
            IPageDialogService pageDialogService)
        {
            this.NavigationService = navigationService;
            this.DialogService = dialogService;
            this.PageDialogService = pageDialogService;
        }

        #region Implementation
        /// <summary>
        /// Redireccion a la pagina de error de red
        /// </summary>
        /// <param name="sourcePage"></param>
        /// <returns></returns>
        public Task NetworkError(string sourcePage)
        {
            return this.NavigationService.NavigateAsync($"/{nameof(NetworkErrorPage)}", new NavigationParameters()
            {
                { "previousPage", sourcePage }
            });
        }

        /// <summary>
        /// Redireccion a la pagina de error del servicio no disponible
        /// </summary>
        /// <param name="sourcePage"></param>
        public async void ServiceNotAvailable(string sourcePage)
        {
            await this.NavigationService.NavigateAsync($"/{nameof(ServiceNotAvailableErrorPage)}", new NavigationParameters()
            {
                { "previousPage", sourcePage }
            });
        }
        #endregion

        public virtual void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public virtual void OnNavigatedTo(INavigationParameters parameters)
        {
        }

        public virtual void Initialize(INavigationParameters parameters)
        {
        }
    }
}
