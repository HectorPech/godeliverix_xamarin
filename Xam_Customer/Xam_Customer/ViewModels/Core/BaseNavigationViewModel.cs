﻿using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.ViewModels.Core
{
    public class BaseNavigationViewModel : BindableBase, INavigationAware, IInitialize
    {
        public INavigationService NavigationService { get; }

        public BaseNavigationViewModel(
            INavigationService navigationService
            )
        {
            this.NavigationService = navigationService;
        }

        public virtual void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public virtual void OnNavigatedTo(INavigationParameters parameters)
        {
        }

        public virtual void Initialize(INavigationParameters parameters)
        {
        }
    }
}
