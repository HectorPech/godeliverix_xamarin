﻿using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.ViewModels.Core
{
    public class NavigationPageViewModel : BindableBase, INavigationAware
    {
        public NavigationPageViewModel()
        {

        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
        }
    }
}
