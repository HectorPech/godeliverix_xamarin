﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.Historical;
using Xam_Customer.Core.Model.System;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Util;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.PurchaseHistory;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.TizenSpecific;

namespace Xam_Customer.ViewModels
{
    public class OrderHistoryDetailPageViewModel : BaseNavigationViewModel
    {
        #region Properties
        #region Controls properties
        private double _StatusBlockHeight;
        public double StatusBlockHeight
        {
            get { return _StatusBlockHeight; }
            set { SetProperty(ref _StatusBlockHeight, value); }
        }
        private string _DeliveryTextCode;
        public string DeliveryTextCode
        {
            get { return _DeliveryTextCode; }
            set { SetProperty(ref _DeliveryTextCode, value); }
        }
        #endregion
        #region Branch information
        private string _CompanyName;
        public string CompanyName
        {
            get { return _CompanyName; }
            set { SetProperty(ref _CompanyName, value); }
        }
        private string _BranchName;
        public string BranchName
        {
            get { return _BranchName; }
            set { SetProperty(ref _BranchName, value); }
        }
        private string _ImageUrl;
        public string ImageUrl
        {
            get { return _ImageUrl; }
            set { SetProperty(ref _ImageUrl, value); }
        }
        private string _StatusName;
        public string StatusName
        {
            get { return _StatusName; }
            set { SetProperty(ref _StatusName, value); }
        }

        private Color statusColor;
        public Color StatusColor
        {
            get { return statusColor; }
            set { SetProperty(ref statusColor, value); }
        }
        #endregion

        private List<OrderHistoryDetailProduct> _Products;
        public List<OrderHistoryDetailProduct> Products
        {
            get { return _Products; }
            set { SetProperty(ref _Products, value); }
        }

        private List<OrderHistoryDetailTimeLine> _statusLog;
        public List<OrderHistoryDetailTimeLine> StatusLog
        {
            get { return _statusLog; }
            set { SetProperty(ref _statusLog, value); }
        }

        private long folio;
        public long Folio
        {
            get { return folio; }
            set { SetProperty(ref folio, value); }
        }

        private decimal subtotal;
        public decimal Subtotal
        {
            get { return subtotal; }
            set { SetProperty(ref subtotal, value); }
        }

        private decimal shipment;
        public decimal Shipment
        {
            get { return shipment; }
            set { SetProperty(ref shipment, value); }
        }

        private decimal tips;
        public decimal Tips
        {
            get { return tips; }
            set { SetProperty(ref tips, value); }
        }

        private decimal total;
        public decimal Total
        {
            get { return total; }
            set { SetProperty(ref total, value); }
        }

        private string _DeliveryCode;
        public string DeliveryCode
        {
            get { return _DeliveryCode; }
            set { SetProperty(ref _DeliveryCode, value); }
        }

        private bool Updated { get; set; }

        private bool cancelAvailable;
        public bool CancelAvailable
        {
            get { return cancelAvailable; }
            set { SetProperty(ref cancelAvailable, value); }
        }

        private decimal walletDiscount;
        public decimal WalletDiscount
        {
            get { return walletDiscount; }
            set { SetProperty(ref walletDiscount, value); }
        }

        private bool useEWalletToPayment;
        public bool UseEWalletToPayment
        {
            get { return useEWalletToPayment; }
            set { SetProperty(ref useEWalletToPayment, value); }
        }

        private decimal totalNoDiscount;
        public decimal TotalNoDiscount
        {
            get { return totalNoDiscount; }
            set { SetProperty(ref totalNoDiscount, value); }
        }

        private bool showCommission;
        public bool ShowCommission
        {
            get { return showCommission; }
            set { SetProperty(ref showCommission, value); }
        }

        private decimal commissions;
        public decimal Commission
        {
            get { return commissions; }
            set { SetProperty(ref commissions, value); }
        }

        private decimal orderSubtotal;
        public decimal OrderSubtotal
        {
            get { return orderSubtotal; }
            set { SetProperty(ref orderSubtotal, value); }
        }

        #endregion

        #region Private
        public string Uid { get; set; }
        #endregion

        #region Command        
        public DelegateCommand ViewDeliveryCode { get; }
        public DelegateCommand CancelOrden { get; }
        #endregion

        #region Services
        private IHttpRequestService _httpRequestService { get; set; }
        private IPageDialogService _pageDialogService { get; set; }
        private IEventAggregator __eventAggregator { get; set; }
        private IAppInstaceService Instance { get; }
        #endregion

        public OrderHistoryDetailPageViewModel(
            INavigationService navigationService,
            IHttpRequestService httpRequestService,
            IPageDialogService pageDialogService,
            IEventAggregator ea,
            IAppInstaceService appInstaceService
            ) : base(navigationService)
        {
            this.Instance = appInstaceService;
            __eventAggregator = ea;

            ViewDeliveryCode = new DelegateCommand(ViewCode);
            _httpRequestService = httpRequestService;
            _pageDialogService = pageDialogService;

            Products = new List<OrderHistoryDetailProduct>();
            StatusLog = new List<OrderHistoryDetailTimeLine>();

            CancelOrden = new DelegateCommand(CancelarOrden);

            StatusBlockHeight = 100;
            Updated = false;

            this.StatusColor = Color.FromHex("#f1f1f1");
        }

        #region Implementation
        private void ViewCode()
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                await _pageDialogService.DisplayAlertAsync(PurchaseHistoryLang.DeliveryCode, DeliveryCode, AppResources.AlertAcceptText);
            });
        }
        private async void CancelarOrden()
        {
            await Device.InvokeOnMainThreadAsync(async () =>
            {
                if (StatusName != AppResources.StatusOrder_Canceled)
                {
                    var result = await _pageDialogService.DisplayAlertAsync(PurchaseHistoryLang.Cancel_QuestionMessage, "#" + Folio.ToString(), PurchaseHistoryLang.Yes, "No");
                    if (result)
                    {
                        //var parametro = new Dictionary<string, string>();
                        //parametro.Add("UidEstatus", "A2D33D7C-2E2E-4DC6-97E3-73F382F30D93");
                        //parametro.Add("StrParametro", "S");
                        //parametro.Add("UidOrden", Uid);
                        //await _httpRequestService.GetEmptyAsync("Orden/GetAgregaEstatusALaOrden", parametro);

                        Guid UidDirecion = Guid.Parse(this.Instance.GetUserSelectedDeliveryAddress());
                        Guid UidUsuario = Guid.Parse(Settings.UserSession.StrUid);

                        CancelOrderRequest request = new CancelOrderRequest()
                        {
                            UidDireccion = UidDirecion,
                            UidUsuario = UidUsuario,
                            UidOrdenSucursal = Guid.Parse(Uid)
                        };

                        await this._httpRequestService.PostAsync("Orders/Cancel", JsonConvert.SerializeObject(request));

                        await _pageDialogService.DisplayAlertAsync(PurchaseHistoryLang.Cancel_MessageTitleSuccess, PurchaseHistoryLang.Cancel_OrderOneOrder + " " + Folio, AppResources.AlertAcceptText);
                        await GetOrderDetail();
                        Updated = true;
                        await this.NavigationService.GoBackAsync();
                    }
                }
                else
                {
                    await _pageDialogService.DisplayAlertAsync(AppResources.Error_General_Title, PurchaseHistoryLang.Cancel_CancelOneOrder, AppResources.AlertAcceptText);

                }

            });

        }
        public async Task GetOrderDetail()
        {
            await Device.InvokeOnMainThreadAsync(() => { UserDialogs.Instance.ShowLoading(AppResources.Text_Loading); });

            Dictionary<string, string> pairs = new Dictionary<string, string>();
            pairs.Add("uid", Uid);

            var orderRequest = await this._httpRequestService.GetAsync<OrderHistoryDetail>("Orders/GetPurchaseOrder", pairs);
            if (orderRequest.Code == Xam_Customer.Core.Enum.HttpResponseCode.Success)
            {
                OrderHistoryDetail order = orderRequest.Result;
                this.StatusLog = order.Timeline.ToList();
                this.Products = order.Products.ToList();

                this.Folio = long.Parse(order.BranchFolio);
                this.Uid = order.OrderUid.ToString();
                this.CompanyName = order.Company;
                this.BranchName = order.Branch;

                // Ajustes del log de status
                if (this.StatusLog.Count > 0)
                {
                    StatusLog = StatusLog.OrderByDescending(o => o.Date).ToList();
                    StatusLog[0].First = true;
                    StatusLog[0].NotFirst = false;

                    StatusLog[StatusLog.Count - 1].NotLastOne = false;

                    foreach (var log in StatusLog)
                    {
                        switch (log.Name)
                        {
                            case "Creado":
                                log.Name = AppResources.StatusOrder_Created;
                                break;
                            case "Elaborado":
                                log.Name = AppResources.StatusOrder_Making;
                                break;
                            case "Entregado":
                                log.Name = AppResources.StatusOrder_Delivered;
                                break;
                            case "Finalizado":
                                log.Name = AppResources.StatusOrder_Finished;
                                break;
                            case "Cancelado":
                                log.Name = AppResources.StatusOrder_Canceled;
                                break;
                            case "Espera de confirmacion":
                                log.Name = AppResources.StatusOrder_Waiting;
                                break;
                            case "Confirmado":
                                log.Name = AppResources.StatusOrder_Confirmed;
                                break;
                            case "Enviando":
                                log.Name = AppResources.StatusOrder_Delivering;
                                break;
                        }
                    }
                }

                // Estatus de la orden
                switch (order.Status)
                {
                    case "Creado":
                        this.StatusColor = OrderStatusColor.Created.Color;
                        this.StatusName = AppResources.StatusOrder_Created;
                        break;
                    case "Elaborado":
                        this.StatusColor = OrderStatusColor.Elaborated.Color;
                        this.StatusName = AppResources.StatusOrder_Making;
                        break;
                    case "Entregado":
                        this.StatusColor = OrderStatusColor.Delivered.Color;
                        this.StatusName = AppResources.StatusOrder_Delivered;
                        break;
                    case "Finalizado":
                        this.StatusColor = OrderStatusColor.Finalized.Color;
                        this.StatusName = AppResources.StatusOrder_Finished;
                        break;
                    case "Cancelado":
                        this.StatusColor = OrderStatusColor.Canceled.Color;
                        this.StatusName = AppResources.StatusOrder_Canceled;
                        break;
                    case "Espera de confirmacion":
                        this.StatusColor = OrderStatusColor.WaitingForConfirmation.Color;
                        this.StatusName = AppResources.StatusOrder_Waiting;
                        this.CancelAvailable = true;
                        break;
                    case "Confirmado":
                        this.StatusColor = OrderStatusColor.Confirmed.Color;
                        this.StatusName = AppResources.StatusOrder_Confirmed;
                        break;
                    case "Enviando":
                        this.StatusColor = OrderStatusColor.Sended.Color;
                        this.StatusName = AppResources.StatusOrder_Delivering;
                        break;
                }

                this.UseEWalletToPayment = order.WalletDiscount.HasValue;
                this.WalletDiscount = order.WalletDiscount.HasValue ? order.WalletDiscount.Value : 0;

                this.OrderSubtotal = order.Products.Sum(p => p.Total) - (order.IncludeCPTS ? order.CardPaymentComission : 0);
                this.Shipment = order.Delivery - (order.IncludeCPTD ? order.DeliveryCardPaymentComission : 0);
                this.ImageUrl = "http://www.godeliverix.net/Vista/" + order.CompanyImg;
                this.Tips = order.Tips - (order.IncludeCPTD ? order.DeliveryTipsCardPaymentComission : 0);

                this.Subtotal = this.OrderSubtotal + this.Shipment + this.Tips;
                this.Commission = order.CardPaymentComission = order.DeliveryCardPaymentComission + order.DeliveryTipsCardPaymentComission;
                this.TotalNoDiscount = Subtotal + Shipment + Tips;
                this.Total = (Subtotal + this.Commission) - walletDiscount;

                this.DeliveryCode = order.DeliveryCode;
                this.DeliveryTextCode = AppResources.DeliveryCode_Text + DeliveryCode;
                __eventAggregator.GetEvent<Xam_Customer.Core.Events.GetCodeQREvent>().Publish(new CODE()
                {
                    Code = DeliveryCode,
                });

                await Device.InvokeOnMainThreadAsync(() => { UserDialogs.Instance.HideLoading(); });
            }
            else
            {
                // TODO: Catch Error and display message
            }
        }
        #endregion

        public override void Initialize(INavigationParameters parameters)
        {
            Device.InvokeOnMainThreadAsync(async () =>
            {
                if (parameters.ContainsKey("uid"))
                {
                    this.Uid = parameters.GetValue<string>("uid");
                    await GetOrderDetail();
                }
            });
        }
        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            if (Updated)
            {
                parameters.Add("status", "Updated");
            }
        }
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.GetNavigationMode() == NavigationMode.Back)
            {
                // Device.InvokeOnMainThreadAsync(async () => { await get(); });
            }
        }
    }
}
