﻿using Acr.UserDialogs;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Services.Implementation;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.Cart;
using Xam_Customer.Views.Payment;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class PaymentTypeViewModel : BindableBase, INavigatedAware
    {
        #region Properties
        private ObservableCollection<PaymentTypeItem> _PaymentList;
        public ObservableCollection<PaymentTypeItem> PaymentList
        {
            get { return _PaymentList; }
            set { SetProperty(ref _PaymentList, value); }
        }

        private ObservableCollection<CartBranchItemListView> _cartBranchItems;
        public ObservableCollection<CartBranchItemListView> CartBranchItems
        {
            get { return _cartBranchItems; }
            set { SetProperty(ref _cartBranchItems, value); }
        }

        private decimal _subtotal;
        public decimal Subtotal
        {
            get { return this._subtotal; }
            set { SetProperty(ref this._subtotal, value); }
        }

        private decimal _deliveryRate;
        public decimal DeliveryRate
        {
            get { return this._deliveryRate; }
            set { SetProperty(ref this._deliveryRate, value); }
        }

        private decimal _deliveryTip;
        public decimal DeliveryTip
        {
            get { return this._deliveryTip; }
            set { SetProperty(ref this._deliveryTip, value); }
        }

        private decimal _total;
        public decimal Total
        {
            get { return this._total; }
            set { SetProperty(ref this._total, value); }
        }

        public decimal SaldoMonedero { get; set; }

        private decimal cardPaymentCommission;
        public decimal CardPaymentCommission
        {
            get { return cardPaymentCommission; }
            set { SetProperty(ref cardPaymentCommission, value); }
        }

        private bool showCardPaymentCommission;
        public bool ShowCardPaymentCommission
        {
            get { return showCardPaymentCommission; }
            set { SetProperty(ref showCardPaymentCommission, value); }
        }

        private bool useEWalletToPayment;
        public bool UseEWalletToPayment
        {
            get { return useEWalletToPayment; }
            set
            {
                SetProperty(ref useEWalletToPayment, value);

                if (!InitialLoading)
                {
                    if (value)
                    {
                        if (this.SaldoMonedero >= (this.Total - this.CardPaymentCommission))
                        {
                            this.WalletDiscount = (this.Total - this.CardPaymentCommission);
                        }
                        else if (this.SaldoMonedero < (this.Total - this.CardPaymentCommission))
                        {
                            this.WalletDiscount = this.SaldoMonedero;
                        }

                        this._appInstaceService.ApplyWalletDiscount(this.WalletDiscount);
                    }
                    else
                    {
                        this.WalletDiscount = 0;
                        this._appInstaceService.RemoveWalletDiscount();
                    }

                    this.NewWalletBalance = (this.SaldoMonedero - this.WalletDiscount);
                    this.CalculateTotal();
                }
            }
        }

        private decimal walletDiscount;
        public decimal WalletDiscount
        {
            get { return walletDiscount; }
            set { SetProperty(ref walletDiscount, value); }
        }

        private string walletSummary;
        public string WalletSummary
        {
            get { return walletSummary; }
            set { SetProperty(ref walletSummary, value); }
        }

        private decimal newWalletBalance;
        public decimal NewWalletBalance
        {
            get { return newWalletBalance; }
            set { SetProperty(ref newWalletBalance, value); }
        }

        #endregion

        #region Private
        public AvailablePaymentType? SelectedPaymentType { get; set; }

        private Commission _Commission { get; set; }

        private List<CartBranchItem> _CartItems { get; set; }

        private bool InitialLoading { get; set; }
        #endregion

        #region Internal
        public IAppInstaceService _appInstaceService { get; set; }
        public INavigationService _navigationService { get; set; }
        public IPageDialogService _PageDialogService { get; set; }
        public IHttpRequestService HttpService { get; }
        #endregion

        #region Command
        public DelegateCommand<int?> PaymentSelected { get; }
        public DelegateCommand NextCommand { get; }
        #endregion

        public PaymentTypeViewModel(
            IAppInstaceService appInstaceService,
            INavigationService navigationService,
            IPageDialogService pageDialogService,
            IHttpRequestService httpRequestService
            )
        {
            _PageDialogService = pageDialogService;
            _appInstaceService = appInstaceService;
            _navigationService = navigationService;
            this.HttpService = httpRequestService;

            PaymentSelected = new DelegateCommand<int?>(PaymentDetails);
            this.NextCommand = new DelegateCommand(async () => { await this.Next(); });

            PaymentList = new ObservableCollection<PaymentTypeItem>();
            PaymentList.Add(new PaymentTypeItem()
            {
                Type = AvailablePaymentType.Cash,
                Icon = "\U000f0114",
                Name = CartLang.PayMent_Cash,
                Unselected = true
            });
            //PaymentList.Add(new PaymentTypeItem()
            //{
            //    Type = AvailablePaymentType.Wallet,
            //    Icon = "\U000f0f1d",
            //    Name = CartLang.Payment_wallet,
            //    Unselected = true
            //});
            PaymentList.Add(new PaymentTypeItem()
            {
                Type = AvailablePaymentType.CreditDebitCard,
                Icon = "\U000f0676",
                Name = CartLang.Payment_Card,
                Unselected = true,
                Description = CartLang.SelectPayment_CardTypeSubtitle
            });
        }

        #region Implementation
        private void PaymentDetails(int? type)
        {

            Device.InvokeOnMainThreadAsync(() =>
            {
                if (!type.HasValue)
                {
                    return;
                }

                foreach (var item in this.PaymentList)
                {
                    if (item.Type == (AvailablePaymentType)type)
                    {
                        item.Selected = true;
                        this._appInstaceService.PaymentType = item.Type;

                        if (item.Type == AvailablePaymentType.CreditDebitCard)
                        {
                            foreach (var cart in this._CartItems)
                            {
                                this._appInstaceService.ApplyCommissionCartBranchItem(cart.Uid, this._Commission.Percent);
                            }
                        }
                        else
                        {
                            foreach (var cart in this._CartItems)
                            {
                                this._appInstaceService.ClearCommissionCartBranchItem(cart.Uid);
                            }
                        }
                    }
                    else
                    {
                        item.Selected = false;
                    }
                }

                this.SelectedPaymentType = (AvailablePaymentType)type;
                this.CalculateTotal();
            });
        }

        private async void CargarMonedero()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);
            });

            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("uidUser", Settings.UserSession.StrUid);

            var saldoMoneneroRequest = await HttpService.GetAsync<Wallet>("Wallet/GetBalance", param);

            if (saldoMoneneroRequest.Code == HttpResponseCode.Success)
            {
                SaldoMonedero = saldoMoneneroRequest.Result.Amount;

                this.WalletSummary = $"{CartLang.Cart_WalletAvailable} ${this.SaldoMonedero.ToString("N2")}";
            }
            else
            {
                this.WalletSummary = CartLang.Cart_CantLoadWallet;
            }

            foreach (var item in this.PaymentList)
            {
                if (item.Type == AvailablePaymentType.Wallet)
                {
                    item.Description = this.WalletSummary;
                    break;
                }
            }

            var commissionRequest = await HttpService.GetAsync<Commission>("Comisiones/ObtenerComisionEmpresa");
            if (commissionRequest.Code == HttpResponseCode.Success)
            {
                this._Commission = commissionRequest.Result;
            }
            else
            {
                this._Commission = new Commission() { Percent = 4.06 };
            }

            if (this._appInstaceService.PaymentType != AvailablePaymentType.None)
            {
                this.SelectedPaymentType = this._appInstaceService.PaymentType;

                foreach (var item in this.PaymentList)
                {
                    if (item.Type == this.SelectedPaymentType)
                    {
                        item.Selected = true;
                    }
                    else
                    {
                        item.Selected = false;
                    }
                }
            }

            this.CalculateTotal();

            Device.BeginInvokeOnMainThread(() =>
            {
                UserDialogs.Instance.HideLoading();
            });
        }

        public async Task Next()
        {
            if (!SelectedPaymentType.HasValue)
            {
                await this._PageDialogService.DisplayAlertAsync("", CartLang.SelectPayment_NotSelectedMessage, AppResources.AlertAcceptText);
                return;
            }

            var parametro = new NavigationParameters();
            parametro.Add("Data", SelectedPaymentType.Value);

            // Validar forma de pago y monto minimo
            if (SelectedPaymentType == AvailablePaymentType.CreditDebitCard && Total < 100)
            {
                await this._PageDialogService.DisplayAlertAsync("", CartLang.SelectPayment_InvalidAmount, AppResources.AlertAcceptText);
                return;
            }
            if (SelectedPaymentType == AvailablePaymentType.Wallet && Total > SaldoMonedero)
            {
                await this._PageDialogService.DisplayAlertAsync("", CartLang.SelectPayment_InvalidAmountWallet, AppResources.AlertAcceptText);
                return;
            }
            await this._navigationService.NavigateAsync($"{nameof(PaymentDetail)}", parametro);
        }

        private void CalculateTotal()
        {
            this._CartItems = this._appInstaceService.ReadAllCartBranchItems();

            if (this.InitialLoading)
            {
                if (this._CartItems.Where(i => i.WalletDiscount.HasValue).Any())
                {
                    this.UseEWalletToPayment = true;
                    this.WalletDiscount = this._CartItems.Sum(i => i.WalletDiscount.HasValue ? i.WalletDiscount.Value : 0);
                }

                if (this.SelectedPaymentType == AvailablePaymentType.CreditDebitCard)
                {
                    foreach (var cart in this._CartItems)
                    {
                        this._appInstaceService.ApplyCommissionCartBranchItem(cart.Uid, this._Commission.Percent);
                    }
                }
                else
                {
                    foreach (var cart in this._CartItems)
                    {
                        this._appInstaceService.ClearCommissionCartBranchItem(cart.Uid);
                    }
                }
            }

            if (this.SelectedPaymentType != null)
            {
                this.CardPaymentCommission = this._CartItems.Sum(c => c.TotalCommissions);
                this.ShowCardPaymentCommission = this.SelectedPaymentType == AvailablePaymentType.CreditDebitCard;
            }
            else
            {
                this.CardPaymentCommission = 0;
                this.ShowCardPaymentCommission = false;
            }

            this.Subtotal = this._CartItems.Sum(i => i.OrderSubtotal);
            this.DeliveryRate = this._CartItems.Sum(i => (i.DeliveryRateSubtotal));
            this.DeliveryTip = this._CartItems.Sum(i => (i.DeliveryTipsSubtotal));

            this.Total = (this.Subtotal + this.DeliveryRate + DeliveryTip + this.CardPaymentCommission) - this.WalletDiscount;

            this.InitialLoading = false;
        }
        #endregion

        #region Navigation
        public void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            this.InitialLoading = true;
            this.CargarMonedero();
        }
        #endregion

    }
}
