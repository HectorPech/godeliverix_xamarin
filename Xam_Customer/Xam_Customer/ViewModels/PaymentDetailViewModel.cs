﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Events;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Model.System;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.Resources.LangResx.Cart;
using Xam_Customer.Views;
using Xam_Customer.Views.Payment;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class PaymentDetailViewModel : BindableBase, INavigationAware
    {
        #region Properties
        #region Controls text
        #region Delivery properties
        private string _identifier;
        public string Identifier
        {
            get { return this._identifier; }
            set { SetProperty(ref this._identifier, value); }
        }
        private string _country;
        public string Country
        {
            get { return this._country; }
            set { SetProperty(ref this._country, value); }
        }
        private string _state;
        public string State
        {
            get { return this._state; }
            set { SetProperty(ref this._state, value); }
        }
        private string _Municipality;
        public string Municipality
        {
            get { return this._Municipality; }
            set { SetProperty(ref this._Municipality, value); }
        }
        private string _City;
        public string City
        {
            get { return this._City; }
            set { SetProperty(ref this._City, value); }
        }
        private string _suburb;
        public string Suburb
        {
            get { return this._suburb; }
            set { SetProperty(ref this._suburb, value); }
        }
        private string _street;
        public string Street
        {
            get { return this._street; }
            set { SetProperty(ref this._street, value); }
        }
        private string _block;
        public string Block
        {
            get { return this._block; }
            set { SetProperty(ref this._block, value); }
        }
        private string _Lot;
        public string Lot
        {
            get { return this._Lot; }
            set { SetProperty(ref this._Lot, value); }
        }
        private string _ZipCode;
        public string ZipCode
        {
            get { return this._ZipCode; }
            set { SetProperty(ref this._ZipCode, value); }
        }
        private string _Reference;
        public string Reference
        {
            get { return this._Reference; }
            set { SetProperty(ref this._Reference, value); }
        }
        private string _Longitude;
        public string Longitude
        {
            get { return this._Longitude; }
            set { SetProperty(ref this._Longitude, value); }
        }
        private string _Latitude;
        public string Latitude
        {
            get { return this._Latitude; }
            set { SetProperty(ref this._Latitude, value); }
        }
        #endregion


        private string _PayName;
        public string PayName
        {
            get { return this._PayName; }
            set { SetProperty(ref this._PayName, value); }
        }
        private string _PayType;
        public string PayType
        {
            get { return this._PayType; }
            set { SetProperty(ref this._PayType, value); }
        }
        private string _PayButton;
        public string PayButton
        {
            get { return this._PayButton; }
            set { SetProperty(ref this._PayButton, value); }
        }
        private string _DeliveryTo;
        public string DeliveryTo
        {
            get { return this._DeliveryTo; }
            set { SetProperty(ref this._DeliveryTo, value); }
        }
        #endregion

        private string _DeliveryAddress;
        public string DeliveryAddress
        {
            get { return this._DeliveryAddress; }
            set { SetProperty(ref this._DeliveryAddress, value); }
        }

        private decimal _subtotal;
        public decimal Subtotal
        {
            get { return this._subtotal; }
            set { SetProperty(ref this._subtotal, value); }
        }

        private decimal _deliveryRate;
        public decimal DeliveryRate
        {
            get { return this._deliveryRate; }
            set { SetProperty(ref this._deliveryRate, value); }
        }

        private decimal _deliveryTip;
        public decimal DeliveryTip
        {
            get { return this._deliveryTip; }
            set { SetProperty(ref this._deliveryTip, value); }
        }

        private decimal cardPaymentCommission;
        public decimal CardPaymentCommission
        {
            get { return cardPaymentCommission; }
            set { SetProperty(ref cardPaymentCommission, value); }
        }

        private bool showCardPaymentCommission;
        public bool ShowCardPaymentCommission
        {
            get { return showCardPaymentCommission; }
            set { SetProperty(ref showCardPaymentCommission, value); }
        }


        private decimal _total;
        public decimal Total
        {
            get { return this._total; }
            set { SetProperty(ref this._total, value); }
        }

        private bool _InformacionDeMonedero;
        public bool InformacionDeMonedero
        {
            get { return this._InformacionDeMonedero; }
            set { SetProperty(ref this._InformacionDeMonedero, value); }
        }

        private decimal _SaldoMonedero;
        public decimal SaldoMonedero
        {
            get { return this._SaldoMonedero; }
            set { SetProperty(ref this._SaldoMonedero, value); }
        }

        private decimal nuevoSaldoMonedero;
        public decimal NuevoSaldoMonedero
        {
            get { return nuevoSaldoMonedero; }
            set { SetProperty(ref nuevoSaldoMonedero, value); }
        }

        private decimal walletDiscount;
        public decimal WalletDiscount
        {
            get { return walletDiscount; }
            set { SetProperty(ref walletDiscount, value); }
        }

        private bool useEWalletToPayment;
        public bool UseEWalletToPayment
        {
            get { return useEWalletToPayment; }
            set { SetProperty(ref useEWalletToPayment, value); }
        }
        private bool _HizoPago;

        public bool PagoRealizado
        {
            get { return _HizoPago; }
            set { _HizoPago = value; }
        }

        public ObservableCollection<CartBranchItem> Items { get; set; }

        #endregion

        #region Private
        public string PaymentyTypeName { get; set; }
        private List<CartBranchItem> _CartItems { get; set; }

        /// <summary>
        /// El total real sin descuentos
        /// </summary>
        public decimal TotalWithTaxes { get; set; }
        #endregion

        #region Command
        public DelegateCommand Pay { get; set; }
        #endregion

        #region Internal
        public IAppInstaceService _appInstaceService { get; set; }
        public IHttpRequestService _httpRequestService { get; set; }
        public INavigationService _navigationService { get; set; }
        public IPageDialogService _pageDialogService { get; set; }
        private IEventAggregator _eventAggregator { get; }
        #endregion

        public PaymentDetailViewModel(
            IAppInstaceService appInstaceService,
            IHttpRequestService httpRequestService,
            INavigationService navigationService,
            IPageDialogService pageDialogService,
            IEventAggregator ea
            )
        {
            _pageDialogService = pageDialogService;
            _navigationService = navigationService;
            _appInstaceService = appInstaceService;
            _httpRequestService = httpRequestService;
            this._eventAggregator = ea;

            Pay = new DelegateCommand(GoToPay);

            this.Items = new ObservableCollection<CartBranchItem>();
        }

        #region Navigation
        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            if (PagoRealizado)
            {
                parameters.Add("Pago", "Si");

            }
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("Data"))
            {
                PagoRealizado = false;
                this.InitializeData(parameters.GetValue<AvailablePaymentType>("Data"));
            }
        }
        #endregion

        #region Implementation
        public async Task CargarMonedero()
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("UidUsuario", Settings.UserSession.StrUid);
            var MonederoRequest = await _httpRequestService.GetAsync<decimal>("Monedero/Get_Movil", param);
            SaldoMonedero = MonederoRequest.Result;
        }
        private void GoToPay()
        {
            Device.InvokeOnMainThreadAsync(async () => { await pagar(); });
        }

        private async Task pagar()
        {
            if (PayType == CartLang.PaymentButtonNext)
            {
                await Device.InvokeOnMainThreadAsync(async () => { await this._navigationService.NavigateAsync($"{nameof(CardPaymentPage)}"); });
            }
            if (PayType == CartLang.PaymentButtonConfirm)
            {
                await SendOrder("6518C044-CE40-41F4-9344-92F0C200A8C2");

            }
            if (PayType == CartLang.PaymentButtonPay)
            {
                await SendOrder("13DC10FE-FE47-48D6-A427-DD2F6DE0C564");
            }
        }
        private async Task SendOrder(string UidFormaDeCobro)
        {
            await Device.InvokeOnMainThreadAsync(() =>
            {
                UserDialogs.Instance.ShowLoading(AppResources.Text_Processing);
            });

            UserSession session = Settings.UserSession;

            string UidOrder = Guid.NewGuid().ToString();
            for (int i = 0; i < _CartItems.Count; i++)
            {
                Random Codigo = new Random();
                long DeliveryCode = Codigo.Next(1000, 9999);
                decimal TotalBranch = 0.00m;
                string UidOrderBranch = Guid.NewGuid().ToString();
                TotalBranch = this._CartItems[i].CartItems.Sum(o => o.Total);
                foreach (var item in _CartItems[i].CartItems)
                {
                    string UidNote = Guid.Empty.ToString();
                    if (item.HasNotes)
                    {
                        UidNote = Guid.NewGuid().ToString();
                    }
                    Dictionary<string, string> parametros = new Dictionary<string, string>();
                    parametros.Add("UIDORDEN", UidOrderBranch);
                    parametros.Add("UIDSECCIONPRODUCTO", item.Uid.ToString());
                    parametros.Add("INTCANTIDAD", item.Quantity.ToString());
                    parametros.Add("STRCOSTO", item.Total.ToString());
                    parametros.Add("UidSucursal", _CartItems[i].UidBranch.ToString());
                    parametros.Add("UidRegistroEncarrito", item.CartId.ToString());
                    parametros.Add("UidNota", UidNote);
                    parametros.Add("StrMensaje", item.Notes);
                    parametros.Add("UidTarifario", _CartItems[i].UidDelivery);
                    //Envia los productos a la base de datos
                    await _httpRequestService.GetEmptyAsync("Orden/GetGuardarProductos_Movil", parametros);
                }

                // Envia la orden a la base de datos
                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add("UIDORDEN", UidOrder);
                param.Add("Total", this.TotalWithTaxes.ToString());
                param.Add("Uidusuario", Settings.UserSession.StrUid);
                param.Add("UidDireccion", _appInstaceService.GetUserSelectedDeliveryAddress());
                param.Add("Uidsucursal", _CartItems[i].UidBranch.ToString());
                param.Add("totalSucursal", TotalBranch.ToString());
                param.Add("UidRelacionOrdenSucursal", UidOrderBranch);
                param.Add("LngCodigoDeEntrega", DeliveryCode.ToString());
                param.Add("UidTarifario", _CartItems[i].UidDelivery);
                await _httpRequestService.GetEmptyAsync("Orden/GetGuardarOrden_Movil", param);

                // Guardar orden de la sucursal
                BranchePaymentRequest request = new BranchePaymentRequest()
                {
                    UidOrden = Guid.Parse(UidOrder),
                    UidDireccion = Guid.Parse(_appInstaceService.GetUserSelectedDeliveryAddress()),
                    UidRelacionOrdenSucursal = Guid.Parse(UidOrderBranch),
                    UidSucursal = this._CartItems[i].UidBranch,
                    UidTarifario = Guid.Parse(this._CartItems[i].UidDelivery),
                    UidUsuario = Guid.Parse(session.StrUid),
                    Monto = this.TotalWithTaxes,
                    MontoSucursal = TotalBranch,
                    CodigoEntrega = DeliveryCode,
                    DescuentoMonedero = this._CartItems[i].WalletDiscount,
                    ComisionTarjeta = this._CartItems[i].OrderSubtotalComission,
                    ComisionTarjetaRepartidor = (this._CartItems[i].DeliveryRateSubtotalComission + this._CartItems[i].DeliveryTipsSubtotalComission),
                    IncludeCPTS = this._CartItems[i].SupplierIncludeCardPaymentCommission,
                    IncludeCPTD = this._CartItems[i].DeliveryIncludeCardPaymentCommission,
                };

                await this._httpRequestService.PostAsync("Payments/RegistryBranchePayment", JsonConvert.SerializeObject(request));

                //Envia la relacion al tarifario
                Dictionary<string, string> parametro = new Dictionary<string, string>();
                parametro.Add("UidOrdenSucursal", UidOrderBranch);
                parametro.Add("DPropina", _CartItems[i].DeliveryTips.ToString());
                parametro.Add("UidTarifario", _CartItems[i].UidDelivery.ToString());
                await _httpRequestService.GetEmptyAsync("Tarifario/GetGuardarTarifario_Movil", parametro);
                //Agrega el estatus a la orden
                parametro = new Dictionary<string, string>();
                parametro.Add("UidEstatus", "DE294EFC-C549-4DDD-A0D1-B0E1E2039ECC");
                parametro.Add("StrParametro", "U");
                parametro.Add("UidUsuario", Settings.UserSession.StrUid);
                parametro.Add("UidOrden", UidOrderBranch);
                parametro.Add("UidSucursal", _CartItems[i].UidBranch.ToString());
                await _httpRequestService.GetEmptyAsync("Orden/GetAgregaEstatusALaOrden_Movil", parametro);
            }
            //Aqui se llama el tipo de pago
            Dictionary<string, string> pagoParam = new Dictionary<string, string>();
            pagoParam.Add("UidOrden", UidOrder);
            pagoParam.Add("UidPago", Guid.NewGuid().ToString());
            pagoParam.Add("UidFormaDeCobro", UidFormaDeCobro);
            pagoParam.Add("MMonto", Total.ToString());
            pagoParam.Add("UidEstatusDeCobro", "E728622B-97D7-431F-B01C-7E0B5F8F3D31");
            await _httpRequestService.GetEmptyAsync("Pagos/GetInsertarPago_Movil", pagoParam);
            //Si es monedero registra el movimiento del pago
            if (UidFormaDeCobro == "13DC10FE-FE47-48D6-A427-DD2F6DE0C564")
            {
                Dictionary<string, string> MonederoParam = new Dictionary<string, string>();
                MonederoParam.Add("UidUsuario", Settings.UserSession.StrUid);
                MonederoParam.Add("TipoDeMovimiento", "6C7F4C2E-0D27-4200-9485-7BE331066D33");
                MonederoParam.Add("Concepto", "DCA75F23-5DDC-4EA5-B088-6D5B187F76F4");
                MonederoParam.Add("Monto", Total.ToString());
                MonederoParam.Add("DireccionMovimiento", _appInstaceService.GetUserSelectedDeliveryAddress());
                await _httpRequestService.GetEmptyAsync("Monedero/GetMovimientoMonedero", MonederoParam);
            }
            _appInstaceService.ClearCart();
            PagoRealizado = true;
            await Device.InvokeOnMainThreadAsync(async () =>
            {
                UserDialogs.Instance.HideLoading();
                await _pageDialogService.DisplayAlertAsync("", $"{CartLang.Cart_Paid} {PayName}", CartLang.Paymetn_Ok);
                await _navigationService.GoBackToRootAsync();
            });

        }
        public async Task LoadDeliveryLocation()
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("UidDireccion", _appInstaceService.GetUserSelectedDeliveryAddress());
            var AddressRequest = await _httpRequestService.GetAsync<Address>("Direccion/GetDireccionCompleta_Movil", param);
            Address address = AddressRequest.Result;
            Identifier = address.Identifier;
            Country = address.Country;
            State = address.UidState;
            Municipality = address.Municipality;
            City = address.UidCity;
            Suburb = address.UidSuburb;
            Street = address.Street0;
            Block = address.Block;
            Lot = address.Lot;
            ZipCode = address.ZipCode;
            if (address.Reference != "No hay información")
            {
                Reference = address.Reference;
            }
            else
            {
                Reference = string.Empty;
            }
            DeliveryTo = Identifier;
            Longitude = address.Longitude;
            Latitude = address.Latitude;

            _eventAggregator.GetEvent<UpdateMapPinEvent>().Publish(new MapPin() { Latitude = Latitude, Longitude = Longitude });
        }

        public void InitializeData(AvailablePaymentType paymentType)
        {

            UserDialogs.Instance.ShowLoading(AppResources.Text_Loading);

            this._CartItems = this._appInstaceService.ReadAllCartBranchItems();
            this.Subtotal = this._CartItems.Sum(i => i.OrderSubtotal);
            this.DeliveryRate = this._CartItems.Sum(i => (i.DeliveryRateSubtotal));
            this.DeliveryTip = this._CartItems.Sum(i => (i.DeliveryTipsSubtotal));
            this.CardPaymentCommission = this._CartItems.Sum(c => c.TotalCommissions);

            this.WalletDiscount = 0;
            this.UseEWalletToPayment = false;

            if (this._CartItems.Where(i => i.WalletDiscount.HasValue).Any())
            {
                this.UseEWalletToPayment = true;
                this.WalletDiscount = this._CartItems.Sum(i => i.WalletDiscount.HasValue ? i.WalletDiscount.Value : 0);
            }


            this.Total = (this.Subtotal + this.DeliveryRate + DeliveryTip + this.CardPaymentCommission) - this.WalletDiscount;
            this.TotalWithTaxes = this.Subtotal + this.DeliveryRate + DeliveryTip + this.CardPaymentCommission;

            foreach (CartBranchItem item in this._CartItems)
            {
                this.Items.Add(item);
            }

            InformacionDeMonedero = false;
            this.ShowCardPaymentCommission = false;
            if (paymentType == AvailablePaymentType.CreditDebitCard)
            {
                PayName = CartLang.Payment_Card;
                PayType = CartLang.PaymentButtonNext;
                PayButton = CartLang.PaymentButtonNext;
                this.ShowCardPaymentCommission = true;
            }
            else if (paymentType == AvailablePaymentType.Wallet)
            {
                PayName = CartLang.Payment_wallet;
                PayType = CartLang.PaymentButtonPay;
                PayButton = CartLang.PaymentButtonPay;
                PayButton += " $" + Total;
                InformacionDeMonedero = true;
                Task.Run(async () =>
                {
                    await CargarMonedero();
                });

                this.NuevoSaldoMonedero = this.SaldoMonedero - this.Total;
            }
            else if (paymentType == AvailablePaymentType.Cash)
            {
                PayName = CartLang.PayMent_Cash;
                PayType = CartLang.PaymentButtonConfirm;
                PayButton = CartLang.PaymentButtonConfirm;
                PayButton += " $" + Total;
            }
            Task.Run(async () =>
            {
                await LoadDeliveryLocation();
            });
            UserDialogs.Instance.HideLoading();

        }
        #endregion
    }
}
