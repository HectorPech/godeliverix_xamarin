using Acr.UserDialogs;
using Prism.AppModel;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services.Dialogs;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Events;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.Core;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Util;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xam_Customer.Views.Popup;
using Xam_Customer.Views.RgPopup;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class DefaultViewModel : BaseNavigationViewModel, IPageLifecycleAware, IInitialize
    {
        #region Properties
        private ObservableCollection<ProductStore> _products;
        public ObservableCollection<ProductStore> Products
        {
            get { return _products; }
            set { SetProperty(ref _products, value); }
        }

        private bool _loadingList;
        public bool LoadingList
        {
            get { return _loadingList; }
            set { SetProperty(ref _loadingList, value); }
        }

        private string _total;
        public string Total
        {
            get { return _total; }
            set { SetProperty(ref _total, value); }
        }
        private Product _product;

        public Product oProduct
        {
            get { return _product; }
            set { SetProperty(ref _product, value); }
        }
        private bool _PanelProducto;

        public bool PanelProducto
        {
            get { return _PanelProducto; }
            set { SetProperty(ref _PanelProducto, value); }
        }
        private bool _PanelEmpresa;

        public bool PanelEmpresa
        {
            get { return _PanelEmpresa; }
            set { SetProperty(ref _PanelEmpresa, value); }
        }
        private string _busquedaPorplabra;

        public string BusquedaPorPalabra
        {
            get { return _busquedaPorplabra; }
            set { SetProperty(ref _busquedaPorplabra, value); }
        }

        #region Location Properties

        private string _DeliveryLocation;

        public string DeliveryLocation
        {
            get { return _DeliveryLocation; }
            set { SetProperty(ref _DeliveryLocation, value); }
        }
        #endregion

        #region Translates 


        private string _FiltersName;

        public string FiltersName
        {
            get { return _FiltersName; }
            set { SetProperty(ref _FiltersName, value); }
        }
        #endregion

        private bool noProducts;
        public bool NoProducts
        {
            get { return noProducts; }
            set { SetProperty(ref noProducts, value); }
        }
        #endregion

        #region Internal
        private IHttpRequestService _HttpService { get; }
        private IDialogService _dialogService { set; get; }
        private INavigationService _navigationServices { set; get; }
        private IAppInstaceService _appInstanceService { set; get; }
        private IEventAggregator EventAggregator { get; }
        #endregion

        #region Private
        private int PageIndex { get; set; }
        private int ListCount { get; set; }
        private bool IsLoadingPopup { get; set; }
        #endregion

        public DefaultViewModel(
            INavigationService navigationServices,
            IHttpRequestService httpService,
            IDialogService dialogService,
            IAppInstaceService appInstaceService,
            IEventAggregator eventAggregator
            ) : base(navigationServices)
        {
            this._appInstanceService = appInstaceService;
            this._HttpService = httpService;
            this._dialogService = dialogService;
            this._navigationServices = navigationServices;
            this.EventAggregator = eventAggregator;

            this.Products = new ObservableCollection<ProductStore>();
            this.Total = $"{this.Products.Count()} {AppResources.DefaultSearch}";

            this.CartCommand = new DelegateCommand(this.GoToCart);
            this.RefreshList = new DelegateCommand(async () =>
            {
                this.PageIndex = 0;
                this.ListCount = 0;
                this.Products.Clear();
                await this.GetProducts();
            });
            this.SelectProduct = new DelegateCommand<string>(this.Product_Clicked);
            this.SearchCommand = new DelegateCommand(Search);
            this.ChangeLocation = new DelegateCommand(ChangeLocationPage);
            this.CmdSearchByText = new DelegateCommand(async () =>
            {
                try
                {

                    DependencyService.Get<IKeyboardHelper>().HideKeyboard();

                    this.PageIndex = 0;
                    this.ListCount = 0;
                    this.Products.Clear();
                    await this.GetProducts();
                }
                catch (Exception ex)
                {
                    // TODO
                }
            });
            this.AddItemsCommand = new DelegateCommand(async () => await this.GetProducts());

            //this.DisplayFilterDialogCommand = new DelegateCommand(this.DisplayFilterDialog); 
            this.DisplayFilterDialogCommand = new DelegateCommand(async () =>
            {
                if (IsLoadingPopup) return;

                this.IsLoadingPopup = true;
                await PopupNavigation.Instance.PushAsync(new ProductFilterDialog(this._appInstanceService, eventAggregator, DialogSource.ProductsPage));
                this.IsLoadingPopup = false;
            });

            this.PanelProducto = true;
            this.PanelEmpresa = false;

            this.FiltersName = AppResources.DefaultFiltersName;
            LoadDeliveryAddres();

            this.EventAggregator.GetEvent<ProductFilterChangedEvent>().Subscribe(async (ProductFilter obj) =>
            {
                FilterParameterType parameterType = FilterParameterType.None;
                Guid uidParameter = Guid.Empty;

                if (obj.UidSubcategory != Guid.Empty)
                {
                    parameterType = FilterParameterType.Subcategoria;
                    uidParameter = obj.UidSubcategory;
                }
                else if (obj.UidCategory != Guid.Empty)
                {
                    parameterType = FilterParameterType.Categoria;
                    uidParameter = obj.UidCategory;
                }
                else if (obj.UidType != Guid.Empty)
                {
                    parameterType = FilterParameterType.Giro;
                    uidParameter = obj.UidType;
                }
                else if (obj.UidType == Guid.Empty)
                {
                    parameterType = FilterParameterType.None;
                    uidParameter = Guid.Empty;
                }

                this._appInstanceService.ProductFilter = obj;
                this._appInstanceService.ProductFilter.Parameter = parameterType;
                this._appInstanceService.ProductFilter.UidSelected = uidParameter;

                this.PageIndex = 0;
                this.ListCount = 0;
                this.Products.Clear();
                await this.GetProducts();
            });
        }


        #region Command
        public DelegateCommand RefreshList { get; }
        public DelegateCommand SearchCommand { get; }
        public DelegateCommand<string> SelectProduct { get; }
        public DelegateCommand ChangeLocation { get; }
        public DelegateCommand CmdSearchByText { get; }
        public DelegateCommand CartCommand { get; }

        public DelegateCommand DisplayFilterDialogCommand { get; }

        public DelegateCommand AddItemsCommand { get; set; }
        #endregion

        #region Implementation
        private void LoadDeliveryAddres()
        {
            if (Settings.UserSession == null)
            {
                this.DeliveryLocation = AppResources.DefaultDelivery + " " + _appInstanceService.GetDeliverySelected();
            }
            else
            {
                if (!string.IsNullOrEmpty(_appInstanceService.GetUserDeliverySelected()))
                {
                    this.DeliveryLocation = AppResources.DefaultDelivery + " " + _appInstanceService.GetUserDeliverySelected();
                }
                else
                {
                    Device.InvokeOnMainThreadAsync(async () => { await this._navigationServices.NavigateAsync($"{nameof(ProfileAddressesPage)}"); });
                }
            }
        }
        private async void GoToCart()
        {
            await this._navigationServices.NavigateAsync($"{nameof(CartPage)}");
        }
        private void BusquedaPorTexto()
        {
            this.Products = new ObservableCollection<ProductStore>();
            Task.Run(async () =>
            {
                this.PageIndex = 0;
                await GetProducts();
            });
        }
        private void ChangeLocationPage()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (Settings.UserSession == null)
                {
                    await this._navigationServices.NavigateAsync($"/{nameof(LocationPage)}");
                }
                else
                {
                    NavigationParameters parameters = new NavigationParameters();
                    parameters.Add("Seleccion", "Seleccion");
                    await this._navigationServices.NavigateAsync($"{nameof(ProfileAddressesPage)}", parameters);
                }
            });
        }
        public async Task CargarProductos()
        {
            this.LoadingList = true;

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("TipoFiltro", _appInstanceService.GetSearchType());
            parameters.Add("Dia", _appInstanceService.GetDayOfTheWeek());
            parameters.Add("UidEstado", _appInstanceService.GetDeliveryState());
            parameters.Add("UidColonia", _appInstanceService.GetDeliveryColonie());
            parameters.Add("UidFiltro", _appInstanceService.GetUidFilter());

            var ProductRequest = await this._HttpService.GetAsync<IEnumerable<ProductStore>>("Store/ReadAll", parameters);
            if (ProductRequest.Code == HttpResponseCode.Success)
            {
                this.NoProducts = !ProductRequest.Result.Any();
                foreach (ProductStore item in ProductRequest.Result)
                {
                    Products.Add(item);
                }
                this.Total = $"{this.Products.Count()} {AppResources.DefaultSearch}";
                this.LoadingList = false;
            }
            else
            {
                this.Total = $"0 {AppResources.DefaultSearch}";
                this.LoadingList = false;
            }
        }

        private void RefrescaLista()
        {
            if (!PanelProducto)
            {
                GenerateFakeData("Empresa");
            }
            else
            {
                GenerateFakeData("Producto");
            }
        }
        private void GenerateFakeData(string Busqueda)
        {
            this.LoadingList = true;

            Random random = new Random();
            //string imgPath = "https://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2017/04/pizzapepperoni0.jpg";

            List<Product> products = new List<Product>();
            int quantity = random.Next(6, 25);
            //switch (Busqueda)
            //{
            //    case "Empresa":
            //        for (int i = 0; i < quantity; i++)
            //        {
            //            products.Add(new Product()
            //            {
            //                Uid = Guid.NewGuid(),
            //                Price = random.Next(99, 250),
            //                Description = random.Next(1, 23).ToString(),
            //                Name = $"Empresa {i}",
            //                ImgPath = imgPath
            //            });
            //        }
            //        break;
            //    case "Producto":
            //        for (int i = 0; i < quantity; i++)
            //        {
            //            products.Add(new Product()
            //            {
            //                Uid = Guid.NewGuid(),
            //                Price = random.Next(99, 250),
            //                Description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore",
            //                Name = $"Product {i}",
            //                ImgPath = imgPath
            //            });
            //        }
            //        break;
            //    default:
            //        break;
            //}


            //this.Total = products.Count();
            //this.Products = new ObservableCollection<Product>(products);

            this.LoadingList = false;
        }
        private async void Product_Clicked(string uid)
        {
            var product = Products.Where(p => p.StrUid == uid).FirstOrDefault();

            if (product.Available)
            {
                var param = new NavigationParameters();
                param.Add("Data", new Product()
                {
                    CompanyName = product.CompanyName,
                    StrUid = product.StrUid,
                    CostoEnvio = 0,
                    Name = product.Name,
                    Description = product.Description,
                    Price = product.Price,
                    ImgPathProduct = product.ImgUrl,
                    ImgPathCompany = product.CompanyImgUrl
                });
                await this._navigationServices.NavigateAsync($"{nameof(ProductSearchDescriptionPage)}", param);
            }
            else
            {
                UserDialogs.Instance.Toast(AppResources.ProductNotAvailable);
            }
        }

        private void Search()
        {
            _navigationServices.NavigateAsync("GeneralFiltersPage");
        }
        public void DisplayFilterDialog()
        {
            this._dialogService.ShowDialog($"{nameof(FiltersDialog)}", null, this.FilterDialogClosed);
        }
        public void FilterDialogClosed(IDialogResult dialogResult)
        {
            if (!dialogResult.Parameters.Keys.Any())
            {
                return;
            }

            FilterParameterType parameterType = FilterParameterType.None;
            Guid uidParameter = Guid.Empty;

            Guid uidGiro = dialogResult.Parameters.ContainsKey("uidGiro") ? dialogResult.Parameters.GetValue<Guid>("uidGiro") : Guid.Empty;
            this._appInstanceService.SelectGiro(uidGiro.ToString());

            Guid uidCategoria = dialogResult.Parameters.ContainsKey("uidCategoria") ? dialogResult.Parameters.GetValue<Guid>("uidCategoria") : Guid.Empty;
            this._appInstanceService.SelectCategoria(uidCategoria.ToString());

            Guid uidSubcategoria = dialogResult.Parameters.ContainsKey("uidSubcategoria") ? dialogResult.Parameters.GetValue<Guid>("uidSubcategoria") : Guid.Empty;
            this._appInstanceService.SelectSubcategoria(uidSubcategoria.ToString());

            if (uidSubcategoria != Guid.Empty)
            {
                parameterType = FilterParameterType.Subcategoria;
                uidParameter = uidSubcategoria;
            }
            else if (uidCategoria != Guid.Empty)
            {
                parameterType = FilterParameterType.Categoria;
                uidParameter = uidCategoria;
            }
            else if (uidGiro != Guid.Empty)
            {
                parameterType = FilterParameterType.Giro;
                uidParameter = uidGiro;
            }

            this._appInstanceService.SetUidFilter(uidParameter.ToString());
            this._appInstanceService.ChangeSearchType(parameterType.ToString());

            this.PageIndex = 0;
            Task.Run(async () => { await this.GetProducts(); });
        }

        public async Task GetProducts()
        {
            if (this.LoadingList)
                return;

            if (this.Products.Count() > 0 && this.Products.Count() == this.ListCount)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    this.LoadingList = false;
                });
                return;
            }

            //Products.Clear();

            Device.BeginInvokeOnMainThread(() => this.LoadingList = true);

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("TipoFiltro", _appInstanceService.ProductFilter.Parameter.ToString());
            parameters.Add("Dia", _appInstanceService.GetDayOfTheWeek());
            parameters.Add("UidEstado", _appInstanceService.GetDeliveryState());
            parameters.Add("UidColonia", _appInstanceService.GetDeliveryColonie());
            parameters.Add("UidFiltro", _appInstanceService.ProductFilter.UidSelected.ToString());
            parameters.Add("PageNumber", this.PageIndex.ToString());
            parameters.Add("PageSize", ApplicationConstants.RowsToShow);

            parameters.Add("SortField", this._appInstanceService.ProductFilter.OrderBy.Value);
            parameters.Add("SortDirection", this._appInstanceService.ProductFilter.OrderBy.Direction);

            if (!string.IsNullOrEmpty(this.BusquedaPorPalabra))
            {
                parameters.Add("filtro", this.BusquedaPorPalabra);
            }

            parameters.Add("Available", this._appInstanceService.ProductFilter.OnlyAvailable ? "true" : "false");

            var ProductRequest = await this._HttpService.GetAsync<CommonDataSource<ProductStore>>("Store/SearchProducts", parameters);
            if (ProductRequest.Code == HttpResponseCode.Success)
            {
                this.NoProducts = !ProductRequest.Result.Payload.Any() && this.Products.Count == 0;
                this.ListCount = ProductRequest.Result.Count;
                foreach (ProductStore item in ProductRequest.Result.Payload)
                {
                    item.ImgUrl = "https://www.godeliverix.net/Vista/" + item.ImgUrl;
                    item.CompanyImgUrl = "https://www.godeliverix.net/Vista/" + item.CompanyImgUrl;
                    Products.Add(item);
                }
                this.Total = $"{this.Products.Count()} {AppResources.Of} {this.ListCount} {AppResources.DefaultSearch}";

                if (!NoProducts)
                    this.PageIndex++;

                this.LoadingList = false;
            }
            else
            {
                this.LoadingList = false;
            }
        }
        #endregion

        #region Page Lifecycle
        public void OnAppearing()
        {
            //int count = this._appInstanceService.ProductsCount();
            //this.EventAggregator.GetEvent<CartCountEvent>().Publish(count);

            this.PageIndex = 0;
            Device.InvokeOnMainThreadAsync(async () => { await this.GetProducts(); });
        }

        public void OnDisappearing()
        {
        }

        public override void Initialize(INavigationParameters parameters)
        {
            Device.InvokeOnMainThreadAsync(() =>
            {
                int count = this._appInstanceService.ProductsCount();
                this.EventAggregator.GetEvent<CartCountEvent>().Publish(count);
            });

            LoadDeliveryAddres();
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.GetNavigationMode() == NavigationMode.Back)
            {
                LoadDeliveryAddres();
                if (parameters.ContainsKey("Pago"))
                {
                    Device.InvokeOnMainThreadAsync(async () =>
                    {
                        await NavigationService.NavigateAsync($"{nameof(PurchasesHistoryPage)}");
                    });
                }
            }

            Device.InvokeOnMainThreadAsync(() =>
            {
                int count = this._appInstanceService.ProductsCount();
                this.EventAggregator.GetEvent<CartCountEvent>().Publish(count);
            });
        }

        #endregion
    }
}
