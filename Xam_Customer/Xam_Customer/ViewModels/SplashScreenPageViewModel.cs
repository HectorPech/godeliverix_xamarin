﻿using Prism.AppModel;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Model.System;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Util;
using Xam_Customer.Helpers;
using Xam_Customer.Resources.LangResx;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xam_Customer.Views.Error;
using Xam_Customer.Views.Layout;
using Xam_Customer.Views.Profile;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class SplashScreenPageViewModel : BaseNavigationViewModel
    {
        #region Properties
        public string _version;
        public string Version
        {
            get { return this._version; }
            set { SetProperty(ref this._version, value); }
        }
        #endregion

        #region Internal
        private IAppInstaceService AppInstaceService { get; set; }
        protected IHttpRequestService HttpRequestService { get; set; }
        protected IPageDialogService _pageDialogService { get; set; }
        #endregion

        public SplashScreenPageViewModel(
            INavigationService navigationService,
            IAppInstaceService appInstaceService,
            IHttpRequestService httpRequestService,
            IPageDialogService pageDialogService
            ) : base(navigationService)
        {
            this.HttpRequestService = httpRequestService;
            this._pageDialogService = pageDialogService;
            this.AppInstaceService = appInstaceService;
            this.Version = ApplicationConstants.Version;
        }

        #region Implementation
        public async Task Initial()
        {

            var current = Connectivity.NetworkAccess;
            if (current != NetworkAccess.Internet)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    // Connection to internet is not available
                    await this.NavigationService.NavigateAsync($"/{nameof(NetworkErrorPage)}", new NavigationParameters()
                {
                        { "previousPage", "SplashScreenPage" }
                });
                });
                return;
            }
            int intento = 0;
            Intentalo:
            intento += intento;
            var GiroRequest = await this.HttpRequestService.GetAsync<IEnumerable<Giro>>("CatalogosDeFiltros/GetGiroMovil");
            if (GiroRequest.Code == HttpResponseCode.Success)
            {
                var giros = GiroRequest.Result.ToList();
                AppInstaceService.AddGiros(new Giro()
                {
                    Uid = Guid.Empty.ToString(),
                    Name = AppResources.All
                });
                foreach (var item in giros)
                {
                    AppInstaceService.AddGiros(item);
                }
                AppInstaceService.SelectGiro(giros[0].Uid);
                AppInstaceService.ChangeSearchType(FilterParameterType.None.ToString());
                AppInstaceService.SetUidFilter(Guid.Empty.ToString());

                var ConfiguracionInicial = await this.HttpRequestService.GetAsync<InitialConfig>("ConfiguracionInicial/GetClientConfig");
                ApplicationConstants.RowsToShow = ConfiguracionInicial.Result.Value.ToString();
            }
            else
            if (GiroRequest.Code == HttpResponseCode.ServerError)
            {
                if (intento == 0)
                {
                    intento += 1;
                    goto Intentalo;
                }
                else
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await this.NavigationService.NavigateAsync($"/{nameof(ServiceNotAvailableErrorPage)}", new NavigationParameters()
                {
                        { "previousPage", "SplashScreenPage" }
                });
                    });
                    return;
                }
            }

            UserSession user = Settings.UserSession;
            Colonies address = Settings.AddressSession;
            InboundNotificationEvent notificationEvent = Settings.InboundNotificationSession;
            CultureInfo ConfiguracionDiaEspanol = new CultureInfo("Es-Es");
            string Dia = ConfiguracionDiaEspanol.DateTimeFormat.GetDayName(DateTime.Now.DayOfWeek);
            AppInstaceService.SetDayOfTheWeek(Dia);

            if (notificationEvent != null)
            {
                Settings.InboundNotificationSession = null;

                Device.BeginInvokeOnMainThread(async () =>
                {
                    await this.NavigationService.NavigateAsync("/ProfilePage");
                });
                return;
            }

            if (user == null && address == null)
            {

                Device.BeginInvokeOnMainThread(async () =>
                {
                    await this.NavigationService.NavigateAsync($"/{nameof(WelcomePage)}");
                });
            }
            else if (address != null && user == null)
            {

                Device.BeginInvokeOnMainThread(async () =>
                {
                    AppInstaceService.SelectDeliveryAddress(address.UidEstado, address.Uid, address.Name, address.Longitude, address.Latitude);
                    NavigationParameters pairs = new NavigationParameters();
                    pairs.Add("navigationType", NavigationType.Guest);
                    await NavigationService.NavigateAsync($"/{nameof(NavigationMasterDetailPage)}/NavigationPage/{nameof(StoresPage)}", pairs);
                });
            }
            else if (user != null)
            {
                if (!string.IsNullOrEmpty(Settings.RememberUser))
                {
                    Dictionary<string, string> parametro = new Dictionary<string, string>();
                    parametro.Add("UidUsuario", user.StrUid);
                    var AddressRequest = await this.HttpRequestService.GetAsync<IEnumerable<Address>>("Direccion/GetObtenerDireccionUsuario_Movil", parametro);
                    var direcciones = AddressRequest.Result.ToList();

                    if (direcciones.Count > 0)
                    {
                        AppInstaceService.HasAddresses = true;
                        var direccion = direcciones.Find(c => c.DefaultAddress == true);
                        if (direccion != null)
                        {
                            AppInstaceService.SelectDeliveryAddress("", "", "", 0.0, 0.0);
                            AppInstaceService.UserSelectedDeliveryAddress(direccion.Uid, direccion.UidState, direccion.UidSuburb, direccion.Identifier, double.Parse(direccion.Latitude), double.Parse(direccion.Longitude));
                            AppInstaceService.SetAddressTotal(direcciones.Count);

                            Device.BeginInvokeOnMainThread(async () =>
                            {
                                NavigationParameters pairs = new NavigationParameters();
                                pairs.Add("navigationType", NavigationType.Logged);
                                await NavigationService.NavigateAsync($"/{nameof(NavigationMasterDetailPage)}/NavigationPage/StoresPage", pairs);
                            });
                        }
                        else
                        {
                            AppInstaceService.SetAddressTotal(0);

                            Device.BeginInvokeOnMainThread(async () =>
                            {
                                NavigationParameters pairs = new NavigationParameters();
                                pairs.Add("navigationType", NavigationType.Logged);
                                await NavigationService.NavigateAsync($"/{nameof(NavigationMasterDetailPage)}/NavigationPage/StoresPage/ProfileAddressesPage", pairs);
                            });
                        }

                    }
                    else
                    {
                        AppInstaceService.HasAddresses = false;

                        NavigationParameters pairs = new NavigationParameters();
                        pairs.Add("action", "FirstAddress");
                        await NavigationService.NavigateAsync($"/NavigationPage/LoginPage/{nameof(ProfileAddAddressStep1)}", pairs);
                    }
                }
                else
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        Settings.UserSession = null;
                        Settings.AddressSession = null;
                        await this.NavigationService.NavigateAsync($"/{nameof(WelcomePage)}");
                    });
                }

            }
        }
        #endregion

        #region Page Lifecycle
        public override void Initialize(INavigationParameters parameters)
        {
            string versionApp = "";
            if (Device.RuntimePlatform == Device.Android)
            {
                versionApp = "87b2dcfd-205a-4260-9092-1ce48b28aa4a";
            }
            if (Device.RuntimePlatform == Device.iOS)
            {
                versionApp = "310cba91-57a5-4699-91fe-3677c2718907";
            }
            Device.InvokeOnMainThreadAsync(async () =>
            {
                Iniciar:

                //Dictionary<string, string> param = new Dictionary<string, string>();
                //param.Add("Uid", versionApp);
                //var versionActual = await this.HttpRequestService.GetAsync<string>("Version/GetUltimaVersionMovil", param);
                //var valor = Version.CompareTo(versionActual.Result);
                //if (valor >= 0)
                //{
                    await this.Initial();
                //}
                //else
                //{
                //    var action = await _pageDialogService.DisplayAlertAsync(AppResources.UpdatedAvailable_Tittle, AppResources.UpdatedAvailable_Message + " " + versionActual.Result + "", AppResources.Error_General_Update, AppResources.Error_General_CancelUpdate);
                //    if (action)
                //    {
                //        var urlStore = "";
                //        if (Device.RuntimePlatform == Device.Android)
                //        {
                //            urlStore = "https://play.google.com/store/apps/details?id=com.CompuAndSoft.GDCliente";
                //        }
                //        if (Device.RuntimePlatform == Device.iOS)
                //        {
                //            urlStore = "http://appstore.com/compuandsoft/godeliverix";
                //        }
                //        await Launcher.OpenAsync(new Uri(urlStore));
                //    }
                //    else
                //    {
                //        goto Iniciar;
                //    }
                //}
            });

        }
        #endregion
    }
}
