﻿using Prism.AppModel;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Resources.LangResx.Cart;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.Views;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels
{
    public class CartOrderDetailPageViewModel : BaseViewModel, INavigationAware, IPageLifecycleAware
    {
        #region Properties
        private ObservableCollection<CartItem> _cartItems;
        public ObservableCollection<CartItem> CartItems
        {
            get { return _cartItems; }
            set { SetProperty(ref _cartItems, value); }
        }

        /// <summary>
        /// Nombre de la empresa
        /// </summary>
        private string _companyName;
        public string CompanyName
        {
            get { return _companyName; }
            set { SetProperty(ref _companyName, value); }
        }

        /// <summary>
        /// Url del logo de la empresa suministradora
        /// </summary>
        private string _companyImgUrl;
        public string CompanyImgUrl
        {
            get { return _companyImgUrl; }
            set { SetProperty(ref _companyImgUrl, value); }
        }
        /// <summary>
        /// Url del logo de la empresa distribuidora
        /// </summary>
        private string _deliveryCompanyImgUrl;
        public string deliveryCompanyImgUrl
        {
            get { return _deliveryCompanyImgUrl; }
            set { SetProperty(ref _deliveryCompanyImgUrl, value); }
        }
        /// <summary>
        /// nombre de la empresa distribuidora
        /// </summary>
        private string _DeliveryCompany;
        public string DeliveryCompany
        {
            get { return _DeliveryCompany; }
            set { SetProperty(ref _DeliveryCompany, value); }
        }
        /// <summary>
        /// Nombre de la sucursal
        /// </summary>
        private string _deliveryBanch;
        public string DeliveryBanch
        {
            get { return _deliveryBanch; }
            set { SetProperty(ref _deliveryBanch, value); }
        }

        /// <summary>
        /// Nombre de la sucursal
        /// </summary>
        private string _branchName;
        public string BranchName
        {
            get { return _branchName; }
            set { SetProperty(ref _branchName, value); }
        }

        /// <summary>
        /// Costo total de los productos
        /// </summary>
        private decimal _subtotal;
        public decimal Subtotal
        {
            get { return _subtotal; }
            set { SetProperty(ref _subtotal, value); }
        }

        /// <summary>
        /// Total del envio
        /// </summary>
        private decimal _deliveryTotal;
        public decimal DeliveryTotal
        {
            get { return _deliveryTotal; }
            set { SetProperty(ref _deliveryTotal, value); }
        }
        /// <summary>
        /// Costo de envio
        /// </summary>
        private decimal _deliveryRate;
        public decimal DeliveryRate
        {
            get { return _deliveryRate; }
            set { SetProperty(ref _deliveryRate, value); }
        }

        /// <summary>
        /// Propina
        /// </summary>
        private decimal _deliveryTip;
        public decimal DeliveryTip
        {
            get { return _deliveryTip; }
            set { SetProperty(ref _deliveryTip, value); }
        }

        /// <summary>
        /// Total
        /// </summary>
        private decimal _total;
        public decimal Total
        {
            get { return _total; }
            set { SetProperty(ref _total, value); }
        }

        private decimal walletDiscount;
        public decimal WalletDiscount
        {
            get { return walletDiscount; }
            set { SetProperty(ref walletDiscount, value); }
        }

        private bool useEWalletToPayment;
        public bool UseEWalletToPayment
        {
            get { return useEWalletToPayment; }
            set { SetProperty(ref useEWalletToPayment, value); }
        }
        #endregion

        #region Private
        /// <summary>
        /// Identificador de la sucursal
        /// </summary>
        public Guid BranchUid { get; set; }
        /// <summary>
        /// Identificador del elemento del carrito
        /// </summary>
        public Guid CartBranchUid { get; set; }
        private CartBranchItem _CartBranchItem { get; set; }
        private List<CartItem> _CartItems { get; set; }
        #endregion

        #region Services
        private IHttpRequestService _HttpService { get; }
        private IAppInstaceService AppInstace { get; }
        #endregion

        #region Command
        public DelegateCommand AcceptCommand { get; }
        public DelegateCommand ChangeDeliveryCommand { get; }
        public DelegateCommand<string> EditProductCommand { get; }
        public DelegateCommand RemoveCommand { get; set; }
        #endregion

        public CartOrderDetailPageViewModel(
            INavigationService navigationService,
            IDialogService dialogService,
            IPageDialogService pageDialogService,
            IHttpRequestService httpRequestService,
            IAppInstaceService appInstaceService
            ) : base(navigationService, dialogService, pageDialogService)
        {
            this._HttpService = httpRequestService;
            this.AppInstace = appInstaceService;

            this.AcceptCommand = new DelegateCommand(async () => { await this.NavigationService.GoBackAsync(); });
            this.EditProductCommand = new DelegateCommand<string>(this.EditProduct);
            this.ChangeDeliveryCommand = new DelegateCommand(this.ChangeDeliveryBranch);
            this.RemoveCommand = new DelegateCommand(this.Remove);
            this._CartItems = new List<CartItem>();
            this.CartItems = new ObservableCollection<CartItem>();
        }

        #region Implementation
        private async void ChangeDeliveryBranch()
        {
            this._CartBranchItem = this.AppInstace.GetCartBranchItemById(this.CartBranchUid);

            await this.NavigationService.NavigateAsync($"{nameof(CartDeliveryChangePage)}", new NavigationParameters()
                {
                    { "Data", _CartBranchItem.StrUid }
                });
        }
        private async void EditProduct(string uid)
        {
            int index = this._CartItems.FindIndex(i => i.StrUid == uid);
            if (index >= 0)
            {
                var Producto = _CartItems[index];
                Guid guid = Producto.Uid;
                Product product = new Product()
                {
                    StrUid = Producto.StrUidProduct,
                    UidSeccionPoducto = Producto.Uid.ToString(),
                    CompanyName = this._CartBranchItem.CompanyName,
                    Description = Producto.ProductDescription,
                    ImgPathCompany = this._CartBranchItem.CompanyImgUrl,
                    ImgPathProduct = Producto.ImgPathProduct,
                    Name = Producto.ProductName,
                    Price = Producto.Total,
                    Quantity = Producto.Quantity,
                    Notes = Producto.Notes
                };

                await this.NavigationService.NavigateAsync($"{nameof(CartProductDescription)}", new NavigationParameters()
                {
                    { "Data", product },
                    { "CartItemId", guid },
                    { "CartBranchItemId", this.CartBranchUid }
                });
            }
        }
        private async void Remove()
        {
            bool answer = await this.PageDialogService.DisplayAlertAsync("", CartLang.Cart_ClearOrderMessagge + " " + DeliveryBanch + "?", CartLang.Cart_Yes, CartLang.Cart_No);
            if (answer)
            {
                this.AppInstace.RemoveBranchItemsFromCart(CartBranchUid.ToString());
                await this.NavigationService.GoBackAsync();
            }
        }
        private void ReadAllCartItems()
        {
            this._CartBranchItem = this.AppInstace.GetCartBranchItemById(this.CartBranchUid);
            if (this._CartBranchItem != null)
            {
                this._CartItems = this._CartBranchItem.CartItems;
                this.BranchName = this._CartBranchItem.BranchName;
                this.CompanyName = this._CartBranchItem.CompanyName;
                this.CompanyImgUrl = this._CartBranchItem.CompanyImgUrl;
                this.deliveryCompanyImgUrl = _CartBranchItem.ImageDeliveryCompany;
                this.DeliveryBanch = _CartBranchItem.DeliveryBranch;
                this.DeliveryCompany = _CartBranchItem.DeliveryCompany;
                this.DeliveryTip = _CartBranchItem.DeliveryTips;
                this.BranchUid = new Guid(_CartBranchItem.StrUidBranch);
                this.CartItems = new ObservableCollection<CartItem>(this._CartItems);
                this.CalculateTotals();
            }
            else
            {
                this.NavigationService.GoBackAsync();
            }
        }

        private void CalculateTotals()
        {
            this.DeliveryRate = this._CartBranchItem.DeliveryRate;
            this.DeliveryTotal = this._CartBranchItem.DeliveryRate + _CartBranchItem.DeliveryTips;

            this.Subtotal = this._CartItems.Sum(i => i.Total);

            this.WalletDiscount = 0;
            this.UseEWalletToPayment = false;

            if (this._CartBranchItem.WalletDiscount.HasValue)
            {
                this.UseEWalletToPayment = true;
                this.WalletDiscount = this._CartBranchItem.WalletDiscount.Value;
            }

            this.Total = (this.Subtotal + DeliveryTotal) - this.WalletDiscount;
            //this.Total = this.Subtotal + this.DeliveryRate;
        }
        #endregion

        #region NavigationAware
        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("uid"))
            {
                this.CartBranchUid = parameters.GetValue<Guid>("uid");
                this.ReadAllCartItems();
            }
        }
        #endregion

        #region Page Lifecycle 
        public void OnAppearing()
        {
            if (this.CartBranchUid != Guid.Empty)
            {
                this.ReadAllCartItems();
            }
        }

        public void OnDisappearing()
        {
        }
        #endregion
    }
}
