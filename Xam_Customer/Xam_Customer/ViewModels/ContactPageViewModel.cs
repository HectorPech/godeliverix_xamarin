﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.ViewModels.Core;
using Xamarin.Essentials;

namespace Xam_Customer.ViewModels
{
    public class ContactPageViewModel : BaseNavigationViewModel
    {
        #region Properties
        private string phone;
        public string Phone
        {
            get { return phone; }
            set { SetProperty(ref phone, value); }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { SetProperty(ref email, value); }
        }

        private string title;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set { SetProperty(ref description, value); }
        }

        private bool isLoading;
        public bool IsLoading
        {
            get { return isLoading; }
            set { SetProperty(ref isLoading, value); }
        }

        #endregion

        #region Service
        protected IHttpRequestService HttpService { get; }
        #endregion

        #region Commands
        public DelegateCommand PhoneClickCommand { get; }
        public DelegateCommand EmailClickCommand { get; }
        #endregion

        public ContactPageViewModel(
            INavigationService navigationService,
            IHttpRequestService httpRequestService
            ) : base(navigationService)
        {
            this.HttpService = httpRequestService;

            this.PhoneClickCommand = new DelegateCommand(this.PhoneClick);
            this.EmailClickCommand = new DelegateCommand(this.EmailClick);
        }

        #region Implementation
        private void PhoneClick()
        {
            if (isLoading) return;

            try
            {
                PhoneDialer.Open(this.Phone);
            }
            catch (ArgumentNullException anEx)
            {
                // Number was null or white space
            }
            catch (FeatureNotSupportedException ex)
            {
                // Phone Dialer is not supported on this device.
            }
            catch (Exception ex)
            {
                // Other error has occurred.
            }
        }
        private async void EmailClick()
        {
            if (isLoading) return;

            try
            {
                var message = new EmailMessage
                {
                    Subject = "GoDeliverix Contact",
                    Body = "",
                    To = new List<string>() { this.Email },
                    //Cc = ccRecipients,
                    //Bcc = bccRecipients
                };
                await Xamarin.Essentials.Email.ComposeAsync(message);
            }
            catch (FeatureNotSupportedException fbsEx)
            {
                // Email is not supported on this device
            }
            catch (Exception ex)
            {
                // Some other exception occurred
            }
        }

        private async Task GetResources()
        {
            this.IsLoading = true;
            try
            {
                string lang = System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
                var response = await this.HttpService.GetAsync<IEnumerable<EntityTranslation>>($"v2/Translations/GetTranslations?type={(int)EntityType.AppContactView}");
                if (response.Code == HttpResponseCode.Success)
                {
                    EntityTranslation title = response.Result.SingleOrDefault(r => r.Key.Trim() == "Titulo");
                    EntityTranslation description = response.Result.SingleOrDefault(r => r.Key.Trim() == "Descripcion");

                    this.Title = title == null ? "" : (lang == "es" ? title.Es : title.En);
                    this.Description = description == null ? "" : (lang == "es" ? description.Es : description.En);
                }

                var responseConfig = await this.HttpService.GetAsync<IEnumerable<ConfiguracionClienteMovil>>("v2/Config/GetConfigByNames?names=contact_phone;contact_email");
                if (responseConfig.Code == HttpResponseCode.Success)
                {
                    ConfiguracionClienteMovil phone = responseConfig.Result.SingleOrDefault(r => r.VchNombre == "contact_phone");
                    if (phone != null)
                        this.Phone = phone.VchValor;

                    ConfiguracionClienteMovil email = responseConfig.Result.SingleOrDefault(r => r.VchNombre == "contact_email");
                    if (email != null)
                        this.Email = email.VchValor;
                }
                this.IsLoading = false;
            }
            catch (Exception ex)
            {
                // TODO: display message 
            }
        }
        #endregion

        public override void Initialize(INavigationParameters parameters)
        {
            base.Initialize(parameters);
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            this.GetResources();
        }
    }
}
