﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xam_Customer.Core.Model.JsonPlaceHolder;
using Xam_Customer.Core.Model.System;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Util;
using Xam_Customer.Resources.LangResx;
using Xamarin.Forms;

namespace Xam_Customer.ViewModels.Popup
{
    public class FiltersDialogViewModel : BindableBase, IDialogAware
    {
        #region Properties
        private string _lblTitleGiro;
        public string LblTitleGiro
        {
            get { return _lblTitleGiro; }
            set { SetProperty(ref _lblTitleGiro, value); }
        }

        private string _lblTitleCategoria;
        public string LblTitleCategoria
        {
            get { return _lblTitleCategoria; }
            set { SetProperty(ref _lblTitleCategoria, value); }
        }

        private string _lblTitleSubcategoria;
        public string LblTitleSubcategoria
        {
            get { return _lblTitleSubcategoria; }
            set { SetProperty(ref _lblTitleSubcategoria, value); }
        }

        private ObservableCollection<CheckboxItemSelection> _giros;
        public ObservableCollection<CheckboxItemSelection> Giros
        {
            get { return _giros; }
            set { SetProperty(ref _giros, value); }
        }

        private ObservableCollection<CheckboxItemSelection> _categories;
        public ObservableCollection<CheckboxItemSelection> Categories
        {
            get { return _categories; }
            set { SetProperty(ref _categories, value); }
        }

        private ObservableCollection<CheckboxItemSelection> _subcategories;
        public ObservableCollection<CheckboxItemSelection> Subcategories
        {
            get { return _subcategories; }
            set { SetProperty(ref _subcategories, value); }
        }

        private CheckboxItemSelection giroSelected;

        public CheckboxItemSelection GiroSelected
        {
            get { return giroSelected; }
            set { SetProperty(ref giroSelected, value); }
        }

        private CheckboxItemSelection categoriaSelected;

        public CheckboxItemSelection CategoriaSelected
        {
            get { return categoriaSelected; }
            set { SetProperty(ref categoriaSelected, value); }
        }

        private CheckboxItemSelection subcategoriaSelected;

        public CheckboxItemSelection SubcategoriaSelected
        {
            get { return subcategoriaSelected; }
            set { SetProperty(ref subcategoriaSelected, value); }
        }


        private string _currentGiro;
        public string CurrentGiro
        {
            get { return _currentGiro; }
            set { SetProperty(ref _currentGiro, value); }
        }

        private string _currentCategory;
        public string CurrentCategory
        {
            get { return _currentCategory; }
            set { SetProperty(ref _currentCategory, value); }
        }

        private string _currentSubcategory;
        public string CurrentSubcategory
        {
            get { return _currentSubcategory; }
            set { SetProperty(ref _currentSubcategory, value); }
        }

        private Guid SelectedGiro;
        private Guid SelectedCategory;
        private Guid SelectedSubcategory;

        private ObservableCollection<CheckboxItemSelection> orderByOptions;
        public ObservableCollection<CheckboxItemSelection> OrderByOptions
        {
            get { return orderByOptions; }
            set { SetProperty(ref orderByOptions, value); }
        }

        #endregion

        #region Private
        public bool IsLoadingCategories { get; set; }
        public bool IsLoadingSubcategories { get; set; }

        private ApplicationFilter SelectedOrderBy { get; set; }
        #endregion

        #region Services
        private IAppInstaceService AppInstaceService { get; }
        private IHttpRequestService HttpService { get; }
        #endregion

        #region Command
        public DelegateCommand<string> SelectGiroCommand { get; }
        public DelegateCommand<string> SelectCategoryCommand { get; }
        public DelegateCommand<string> SelectSubcategoryCommand { get; }
        public DelegateCommand<CheckboxItemSelection> SelectOrderCommand { get; }

        public DelegateCommand ApplyFiltersCommand { get; }
        #endregion

        public FiltersDialogViewModel(
            IAppInstaceService appInstaceService,
            IHttpRequestService httpRequestService)
        {
            this.AppInstaceService = appInstaceService;
            this.HttpService = httpRequestService;

            this.LblTitleGiro = AppResources.FilterGiro;
            this.LblTitleCategoria = AppResources.FilterCategoria;
            this.LblTitleSubcategoria = AppResources.FilterSubcategoria;

            this.Giros = new ObservableCollection<CheckboxItemSelection>();
            this.Categories = new ObservableCollection<CheckboxItemSelection>();
            this.Subcategories = new ObservableCollection<CheckboxItemSelection>();

            string strGiro = this.AppInstaceService.GetGiroSelected();
            if (!Guid.TryParse(strGiro, out this.SelectedGiro)) { this.SelectedGiro = Guid.Empty; }

            string strCategoria = this.AppInstaceService.GetCategoriaSelected();
            if (!Guid.TryParse(strCategoria, out this.SelectedCategory)) { this.SelectedCategory = Guid.Empty; }

            string strSubcategoria = this.AppInstaceService.GetSubcategoriaSelected();
            if (!Guid.TryParse(strSubcategoria, out this.SelectedSubcategory)) { this.SelectedSubcategory = Guid.Empty; }

            this.SelectGiroCommand = new DelegateCommand<string>(this.SelectGiro, this.CanSelectGiro);
            this.LoadGiros();
            SelectGiro(strGiro);
            Device.InvokeOnMainThreadAsync(async () => { await this.LoadCategories(); });
            this.SelectCategoryCommand = new DelegateCommand<string>(this.SelectCategory, this.CanSelectCategory);
            // this.LoadCategories();
            this.SelectSubcategoryCommand = new DelegateCommand<string>(this.SelectSubcategory, this.CanSelectSubcategory);

            this.SelectOrderCommand = new DelegateCommand<CheckboxItemSelection>(this.SelectOrderBy);

            this.ApplyFiltersCommand = new DelegateCommand(this.ApplyFilters);

            this.OrderByOptions = new ObservableCollection<CheckboxItemSelection>
            {
                new CheckboxItemSelection() { Uid = Guid.Empty, Id = ProductFilterOrder.None.Index, Name = AppResources.DontOrder, Selected = true },
                new CheckboxItemSelection() { Uid = Guid.Empty, Id = ProductFilterOrder.NameAsc.Index, Name = AppResources.AscendingName, Selected = false },
                new CheckboxItemSelection() { Uid = Guid.Empty, Id = ProductFilterOrder.NameDesc.Index, Name = AppResources.DescendingName, Selected = false },
                new CheckboxItemSelection() { Uid = Guid.Empty, Id = ProductFilterOrder.PriceAsc.Index, Name = AppResources.LowestPrice, Selected = false },
                new CheckboxItemSelection() { Uid = Guid.Empty, Id = ProductFilterOrder.PriceDesc.Index, Name = AppResources.HighestPrice, Selected = false }
            };

            this.SelectedOrderBy = ProductFilterOrder.None;
        }

        #region Implementation
        private void SelectGiro(string uid)
        {
            this.SelectedGiro = Guid.Parse(uid);

            foreach (var giro in this.Giros)
            {
                giro.Selected = giro.Uid == this.SelectedGiro;

                if (giro.Selected)
                {
                    this.CurrentGiro = giro.Name;
                }
            }

            this.SelectedCategory = Guid.Empty;
            this.CurrentCategory = "";
            this.SelectedSubcategory = Guid.Empty;

            this.Subcategories = new ObservableCollection<CheckboxItemSelection>();
            this.CurrentSubcategory = "";
            this.Subcategories.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Name = "All", Selected = true });

            Task.Run(async () => await this.LoadCategories());
        }
        private bool CanSelectGiro(string value)
        {
            return !this.IsLoadingCategories;
        }

        private void SelectCategory(string uid)
        {
            this.SelectedCategory = Guid.Parse(uid);

            foreach (var category in this.Categories)
            {
                category.Selected = category.Uid == this.SelectedCategory;

                if (category.Selected)
                {
                    this.CurrentCategory = category.Name;
                }
            }

            this.SelectedSubcategory = Guid.Empty;
            this.CurrentSubcategory = "";
            Task.Run(async () => await this.LoadSubcategories());
        }
        private bool CanSelectCategory(string value)
        {
            return !this.IsLoadingSubcategories;
        }

        private void SelectSubcategory(string uid)
        {
            this.SelectedSubcategory = Guid.Parse(uid);

            foreach (var subcategory in this.Subcategories)
            {
                subcategory.Selected = subcategory.Uid == this.SelectedSubcategory;

                if (subcategory.Selected)
                {
                    this.CurrentSubcategory = subcategory.Name;
                }
            }
        }
        private bool CanSelectSubcategory(string value)
        {
            return !this.IsLoadingSubcategories;
        }

        public void LoadGiros()
        {
            var giros = this.AppInstaceService.GetGiroList();

            this.Giros.Add(new CheckboxItemSelection()
            {
                Uid = Guid.Empty,
                Selected = Guid.Empty == this.SelectedGiro,
                Name = "All"
            });

            if (Guid.Empty == this.SelectedGiro)
            {
                this.CurrentGiro = "All";
            }

            foreach (var giro in giros)
            {
                Guid uid = Guid.Parse(giro.Uid);
                this.Giros.Add(new CheckboxItemSelection()
                {

                    Uid = uid,
                    Selected = uid == this.SelectedGiro,
                    Name = giro.Name
                });

                if (uid == this.SelectedGiro)
                {
                    GiroSelected = new CheckboxItemSelection()
                    {
                        Uid = uid,
                        Selected = uid == this.SelectedGiro,
                        Name = giro.Name
                    };
                    this.CurrentGiro = giro.Name;
                }
            }
        }
        public async Task LoadCategories()
        {
            this.IsLoadingCategories = true;
            this.Categories = new ObservableCollection<CheckboxItemSelection>();

            if (this.SelectedGiro != Guid.Empty)
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("UidGiro", SelectedGiro.ToString());
                parameters.Add("TipoDeRespuesta", "seleccionar");
                var CategoriaRequest = await this.HttpService.GetAsync<IEnumerable<Categoria>>("CatalogosDeFiltros/GetCategoriaMovil", parameters);
                if (CategoriaRequest.Code == Xam_Customer.Core.Enum.HttpResponseCode.Success)
                {
                    foreach (var category in CategoriaRequest.Result)
                    {
                        Guid uid = Guid.Parse(category.Uid);
                        this.Categories.Add(new CheckboxItemSelection()
                        {
                            Uid = uid,
                            Selected = uid == this.SelectedCategory,
                            Name = category.Name
                        });

                        if (uid == this.SelectedCategory)
                        {
                            categoriaSelected = new CheckboxItemSelection()
                            {
                                Uid = uid,
                                Selected = uid == this.SelectedCategory,
                                Name = category.Name
                            };
                            this.CurrentCategory = category.Name;
                        }
                    }
                }
                else
                {
                    this.Categories.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Name = "All", Selected = true });
                }

                this.IsLoadingCategories = false;
            }
            else
            {
                this.Categories.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Name = "All", Selected = true });
                this.IsLoadingCategories = false;
            }
        }
        public async Task LoadSubcategories()
        {
            this.IsLoadingSubcategories = true;
            this.Subcategories = new ObservableCollection<CheckboxItemSelection>();

            if (this.SelectedCategory != Guid.Empty)
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("uidCategoria", SelectedCategory.ToString());
                parameters.Add("TipoDeRespuesta", "seleccionar");

                var SubcategoriaRequest = await this.HttpService.GetAsync<IEnumerable<Subcategoria>>("CatalogosDeFiltros/GetSubCategoriaMovil", parameters);
                if (SubcategoriaRequest.Code == Xam_Customer.Core.Enum.HttpResponseCode.Success)
                {
                    foreach (var subcategoria in SubcategoriaRequest.Result)
                    {
                        Guid uid = Guid.Parse(subcategoria.Uid);
                        this.Subcategories.Add(new CheckboxItemSelection()
                        {
                            Uid = uid,
                            Selected = uid == this.SelectedSubcategory,
                            Name = subcategoria.Name
                        });

                        if (uid == this.SelectedSubcategory)
                        {
                            SubcategoriaSelected = new CheckboxItemSelection()
                            {
                                Uid = uid,
                                Selected = uid == this.SelectedSubcategory,
                                Name = subcategoria.Name
                            };
                            this.CurrentSubcategory = subcategoria.Name;
                        }
                    }
                }
                else
                {
                    this.Subcategories.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Name = "All", Selected = true });
                }
                this.IsLoadingSubcategories = false;
            }
            else
            {
                this.Subcategories.Add(new CheckboxItemSelection() { Uid = Guid.Empty, Name = "All", Selected = true });
                this.IsLoadingSubcategories = false;
            }
        }

        private void SelectOrderBy(CheckboxItemSelection item)
        {
            foreach (CheckboxItemSelection option in this.OrderByOptions)
            {
                if (option.Id == item.Id)
                {
                    switch (option.Id)
                    {
                        case 0:
                            this.SelectedOrderBy = ProductFilterOrder.None;
                            break;
                        case 1:
                            this.SelectedOrderBy = ProductFilterOrder.NameAsc;
                            break;
                        case 2:
                            this.SelectedOrderBy = ProductFilterOrder.NameDesc;
                            break;
                        case 3:
                            this.SelectedOrderBy = ProductFilterOrder.PriceAsc;
                            break;
                        case 4:
                            this.SelectedOrderBy = ProductFilterOrder.PriceDesc;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void ApplyFilters()
        {
            DialogParameters parameters = new DialogParameters();
            parameters.Add("uidGiro", this.SelectedGiro);
            parameters.Add("uidCategoria", this.SelectedCategory);
            parameters.Add("uidSubcategoria", this.SelectedSubcategory);

            this.RequestClose(parameters);
        }
        #endregion

        #region Dialog        
        public event Action<IDialogParameters> RequestClose;

        public bool CanCloseDialog() => true;

        public void OnDialogClosed()
        {
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
        }
        #endregion
    }
}
