﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Xam_Customer.ViewModels.Popup
{
    public class ConfirmationDialogViewModel : BindableBase, IDialogAware
    {
        #region Properties
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private string _message;
        public string Message
        {
            get { return _message; }
            set { SetProperty(ref _message, value); }
        }
        #endregion

        #region Command
        public DelegateCommand CloseCommand { get; }
        public DelegateCommand AcceptCommand { get; }
        #endregion

        public ConfirmationDialogViewModel()
        {
            this.CloseCommand = new DelegateCommand(this.Cancell);
            this.AcceptCommand = new DelegateCommand(this.Accept);
        }

        #region Implementation
        private void Cancell()
        {
            this.RequestClose(new DialogParameters() { { "response", false } });
        }
        private void Accept()
        {
            this.RequestClose(new DialogParameters() { { "response", true } });
        }
        #endregion

        #region Dialog Aware
        public event Action<IDialogParameters> RequestClose;

        public bool CanCloseDialog() => true;

        public void OnDialogClosed()
        {
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            if (parameters.ContainsKey("title"))
            {
                this.Title = parameters.GetValue<string>("title");
            }

            if (parameters.ContainsKey("message"))
            {
                this.Message = parameters.GetValue<string>("message");
            }
        }
        #endregion
    }
}
