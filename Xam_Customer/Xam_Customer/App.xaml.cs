using Com.OneSignal;
using Com.OneSignal.Abstractions;
using Prism;
using Prism.Ioc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xam_Customer.Core.Enum;
using Xam_Customer.Core.Model.System;
using Xam_Customer.Core.Services.Abstract;
using Xam_Customer.Core.Services.Implementation;
using Xam_Customer.Helpers;
using Xam_Customer.ViewModels;
using Xam_Customer.ViewModels.Core;
using Xam_Customer.ViewModels.Error;
using Xam_Customer.ViewModels.Layout;
using Xam_Customer.ViewModels.Popup;
using Xam_Customer.ViewModels.Profile;
using Xam_Customer.ViewModels.Registry;
using Xam_Customer.Views;
using Xam_Customer.Views.About;
using Xam_Customer.Views.Error;
using Xam_Customer.Views.Layout;
using Xam_Customer.Views.Payment;
using Xam_Customer.Views.Popup;
using Xam_Customer.Views.Profile;
using Xam_Customer.Views.Registry;
using Xamarin.Essentials.Implementation;
using Xamarin.Essentials.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Xam_Customer
{
    public partial class App
    {
        /// <summary>
        /// Indicates if the application is active
        /// </summary>
        protected bool IsActive { get; set; } = false;

        protected string OneSignalKey = "b505c19f-b636-40a8-a6f6-35a3120eada8";

        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();
            Device.SetFlags(new string[] { "Expander_Experimental", "Shapes_Experimental" });

            //Remove this method to stop OneSignal Debugging  
            OneSignal.Current.SetLogLevel(LOG_LEVEL.VERBOSE, LOG_LEVEL.NONE);
            OneSignal.Current.StartInit(OneSignalKey)
            //.HandleNotificationReceived(HandleNotificationReceived)
            .Settings(new Dictionary<string, bool>() {
                { IOSSettings.kOSSettingsKeyAutoPrompt, false },
                { IOSSettings.kOSSettingsKeyInAppLaunchURL, false }
            })
            .HandleNotificationOpened(async (action) =>
            {
                if (action.notification.payload.additionalData.ContainsKey("type")
                    && action.notification.payload.additionalData.ContainsKey("uid"))
                {

                    if (this.IsActive)
                    {
                        await this.NavigationService.NavigateAsync("/ProfilePage");
                    }
                    else
                    {
                        InboundNotificationEvent notificationEvent = new InboundNotificationEvent()
                        {
                            Message = action.notification.payload.body,
                            Type = (NotificationEventType)action.notification.payload.additionalData["type"],
                            Uid = (string)action.notification.payload.additionalData["uid"]
                        };
                        Settings.InboundNotificationSession = notificationEvent;
                    }
                }
            })
            .InFocusDisplaying(OSInFocusDisplayOption.Notification)
            .EndInit();

            // The promptForPushNotificationsWithUserResponse function will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 7)
            OneSignal.Current.RegisterForPushNotifications();

            this.IsActive = true;
            await NavigationService.NavigateAsync($"{nameof(SplashScreenPage)}");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IAppInfo, AppInfoImplementation>();
            containerRegistry.RegisterSingleton<IAppInstaceService, AppInstaceService>();
            containerRegistry.Register<IHttpRequestService, HttpRequestService>();

            containerRegistry.RegisterForNavigation<NavigationPage, NavigationPageViewModel>("NavigationPage");
            containerRegistry.RegisterForNavigation<NavigationMasterDetailPage, NavigationMasterDetailPageViewModel>();
            containerRegistry.RegisterForNavigation<HomePage, HomePageViewModel>();
            containerRegistry.RegisterForNavigation<LocationPage, LocationPageViewModel>();
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<RegistryStep1Page, RegistryStep1PageViewModel>();
            containerRegistry.RegisterForNavigation<RegistryStep2Page, RegistryStep2PageViewModel>();
            containerRegistry.RegisterForNavigation<RegistryStep3Page, RegistryStep3PageViewModel>();
            containerRegistry.RegisterForNavigation<RegistryTermsAndConditions, RegistryTermsAndConditionsViewModel>();
            containerRegistry.RegisterForNavigation<RegistryStep4Page, RegistryStep4PageViewModel>();
            containerRegistry.RegisterForNavigation<ForgotAccountPage, ForgotAccountPageViewModel>();
            containerRegistry.RegisterForNavigation<Default, DefaultViewModel>();
            containerRegistry.RegisterForNavigation<SplashScreenPage, SplashScreenPageViewModel>();
            containerRegistry.RegisterForNavigation<ProductSearchDescriptionPage, ProductSearchDescriptionPageViewModel>();
            containerRegistry.RegisterForNavigation<ChangeProductBranchPage, ChangeProductBranchPageViewModel>();

            // Ventanas de dialogo
            containerRegistry.RegisterDialog<ConfirmationDialog, ConfirmationDialogViewModel>();
            containerRegistry.RegisterDialog<FiltersDialog, FiltersDialogViewModel>();
            containerRegistry.RegisterDialog<PurchaseFiltersDialog, PurchaseFiltersDialogViewModel>();

            // Error Pages
            containerRegistry.RegisterForNavigation<InvalidVersionErrorPage, InvalidVersionErrorPageViewModel>();
            containerRegistry.RegisterForNavigation<ServiceNotAvailableErrorPage, ServiceNotAvailableErrorPageViewModel>();
            containerRegistry.RegisterForNavigation<NetworkErrorPage, NetworkErrorPageViewModel>();

            containerRegistry.RegisterForNavigation<WelcomePage, WelcomePageViewModel>();
            containerRegistry.RegisterForNavigation<CartPage, CartPageViewModel>();
            containerRegistry.RegisterForNavigation<CartOrderDetailPage, CartOrderDetailPageViewModel>();
            containerRegistry.RegisterForNavigation<CartProductDescription, CartProductDescriptionViewModel>();
            containerRegistry.RegisterForNavigation<OrderHistoryPage, OrderHistoryPageViewModel>();
            containerRegistry.RegisterForNavigation<CartDeliveryChangePage, CartDeliveryChangePageViewModel>();
            containerRegistry.RegisterForNavigation<ProfilePage, ProfilesPageViewModel>();
            containerRegistry.RegisterForNavigation<PaymentType, PaymentTypeViewModel>();
            containerRegistry.RegisterForNavigation<PaymentDetail, PaymentDetailViewModel>();
            containerRegistry.RegisterForNavigation<CardPaymentPage, CardPaymentPageViewModel>();
            containerRegistry.RegisterForNavigation<PurchasesHistoryPage, PurchasesHistoryPageViewModel>();
            containerRegistry.RegisterForNavigation<PurchaseDetailPage, PurchaseDetailPageViewModel>();
            containerRegistry.RegisterForNavigation<OrderHistoryDetailPage, OrderHistoryDetailPageViewModel>();
            containerRegistry.RegisterForNavigation<ProfileAddressesPage, ProfileAddressesPageViewModel>();
            containerRegistry.RegisterForNavigation<ProfileAccountFormPage, ProfileAccountFormPageViewModel>();
            containerRegistry.RegisterForNavigation<ProfileAddressFormPage, ProfileAddressFormPageViewModel>();
            containerRegistry.RegisterForNavigation<ProfilePickLocationPage, ProfilePickLocationPageViewModel>();
            containerRegistry.RegisterForNavigation<ProfileUserFormPage, ProfileUserFormPageViewModel>();
            containerRegistry.RegisterForNavigation<StoresPage, StoresPageViewModel>();
            containerRegistry.RegisterForNavigation<StoreDetailPage, StoreDetailPageViewModel>();
            containerRegistry.RegisterForNavigation<ProfileAddAddressStep1, ProfileAddAddressStep1ViewModel>();
            containerRegistry.RegisterForNavigation<ProfileAddAddressStep2, ProfileAddAddressStep2ViewModel>();
            containerRegistry.RegisterForNavigation<ProfileAddAddressStep3, ProfileAddAddressStep3ViewModel>();
            containerRegistry.RegisterForNavigation<ProfileWalletPage, ProfileWalletPageViewModel>();
            containerRegistry.RegisterForNavigation<AboutPage, AboutPageViewModel>();
            containerRegistry.RegisterForNavigation<TermsAndConditionsPage, TermsAndConditionsPageViewModel>();
            containerRegistry.RegisterForNavigation<ProfileRewardsPage, ProfileRewardsPageViewModel>();
            containerRegistry.RegisterForNavigation<AdvancedOptionsLocation, AdvancedOptionsLocationViewModel>();
            containerRegistry.RegisterForNavigation<ContactPage, ContactPageViewModel>();
        }

        protected override void OnResume()
        {
            this.IsActive = true;
            base.OnResume();
        }

        protected override void OnSleep()
        {
            this.IsActive = false;
            base.OnSleep();
        }
    }
}
